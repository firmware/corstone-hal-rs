// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#![no_std]

pub use corstone300_hal as hal;

// TODO: add extra impls for ethernet peripheral
