// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#![no_std]
#![no_main]

use panic_halt as _;

use embedded_hal::digital::v2::{InputPin, OutputPin, PinState, ToggleableOutputPin};

use corstone300_bsp::hal::{
    fpgaio::{FPGAIOExt, Parts},
    pac,
};

#[cortex_m_rt::entry]
fn main() -> ! {
    let p = pac::Peripherals::take().unwrap();
    unsafe {
        p.FPGAIO.led.write_with_zero(|w| w);
    }

    let Parts {
        mut ul0,
        mut pb1led,
        mut pb2led,
        button0,
        button1,
        clocks,
        ..
    } = p.FPGAIO.split();

    let _ = ul0.set_high();
    let mut counter = 0;

    loop {
        let _ = pb1led.set_state(if button0.is_high().unwrap() {
            PinState::High
        } else {
            PinState::Low
        });
        let _ = pb2led.set_state(if button1.is_high().unwrap() {
            PinState::High
        } else {
            PinState::Low
        });

        let new_cnt = clocks.clk100hz() / 50;
        if counter != new_cnt {
            counter = new_cnt;
            let _ = ul0.toggle();
        }
    }
}
