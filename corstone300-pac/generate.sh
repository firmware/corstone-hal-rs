#!/bin/bash
# Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

COPYRIGHT="Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>"
LICENSE=MIT

set -x

# Clean up
rm -rf src
build.rs
device.x

# Re-generate
xsltproc svd/an547.xsl svd/SSE300.svd | svd2rust  --const_generic -o .
form -i lib.rs -o src
rm lib.rs
cargo fmt
reuse addheader --copyright "$COPYRIGHT" --license "$LICENSE" --recursive src build.rs
reuse addheader --copyright "$COPYRIGHT" --license "$LICENSE" --style c --multi-line device.x

cargo clippy --fix --allow-dirty --target x86_64-unknown-linux-gnu
