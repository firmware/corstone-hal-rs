// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Secure Debug Configuration Status"]
    pub secdbgstat: SECDBGSTAT,
    #[doc = "0x04 - Secure Debug Configuration Set"]
    pub secdbgset: SECDBGSET,
    #[doc = "0x08 - Secure Debug Configuration Clear"]
    pub secdbgclr: SECDBGCLR,
    #[doc = "0x0c - System Security Control"]
    pub scsecctrl: SCSECCTRL,
    #[doc = "0x10 - Clock Configuration Register 0."]
    pub clk_cfg0: CLK_CFG0,
    #[doc = "0x14 - Clock Configuration Register 1."]
    pub clk_cfg1: CLK_CFG1,
    #[doc = "0x18 - Clock Force"]
    pub clock_force: CLOCK_FORCE,
    _reserved7: [u8; 0xe4],
    #[doc = "0x100 - Reset Syndrome"]
    pub reset_syndrome: RESET_SYNDROME,
    #[doc = "0x104 - Reset Mask"]
    pub reset_mask: RESET_MASK,
    #[doc = "0x108 - Software Reset"]
    pub swreset: SWRESET,
    #[doc = "0x10c - General Purpose Retention"]
    pub gretreg: GRETREG,
    #[doc = "0x110 - Initial Secure Reset Vector Register For CPU 0"]
    pub initsvrtor0: INITSVRTOR0,
    _reserved12: [u8; 0x0c],
    #[doc = "0x120 - CPU Boot wait control after reset"]
    pub cpuwait: CPUWAIT,
    #[doc = "0x124 - NMI Enable Register"]
    pub nmi_enable: NMI_ENABLE,
    _reserved14: [u8; 0xd4],
    #[doc = "0x1fc - Power Configuration and Control."]
    pub pwrctrl: PWRCTRL,
    #[doc = "0x200 - External Wakeup Control"]
    pub pdcm_pd_sys_sense: PDCM_PD_SYS_SENSE,
    #[doc = "0x204 - PDCM PD_CPU0 Sensitivity."]
    pub pdcm_pd_cpu0_sense: PDCM_PD_CPU0_SENSE,
    _reserved17: [u8; 0x0c],
    #[doc = "0x214 - PDCM PD_VMR0 Sensitivity."]
    pub pdcm_pd_vmr0_sense: PDCM_PD_VMR0_SENSE,
    #[doc = "0x218 - PDCM PD_VMR1 Sensitivity."]
    pub pdcm_pd_vmr1_sense: PDCM_PD_VMR1_SENSE,
    _reserved19: [u8; 0x0db4],
    #[doc = "0xfd0 - Peripheral ID 4"]
    pub pidr4: PIDR4,
    _reserved20: [u8; 0x0c],
    #[doc = "0xfe0 - Peripheral ID 0"]
    pub pidr0: PIDR0,
    #[doc = "0xfe4 - Peripheral ID 1"]
    pub pidr1: PIDR1,
    #[doc = "0xfe8 - Peripheral ID 2"]
    pub pidr2: PIDR2,
    #[doc = "0xfec - Peripheral ID 3"]
    pub pidr3: PIDR3,
    #[doc = "0xff0 - Component ID 0"]
    pub cidr0: CIDR0,
    #[doc = "0xff4 - Component ID 1"]
    pub cidr1: CIDR1,
    #[doc = "0xff8 - Component ID 2"]
    pub cidr2: CIDR2,
    #[doc = "0xffc - Component ID 3"]
    pub cidr3: CIDR3,
}
#[doc = "SECDBGSTAT (r) register accessor: an alias for `Reg<SECDBGSTAT_SPEC>`"]
pub type SECDBGSTAT = crate::Reg<secdbgstat::SECDBGSTAT_SPEC>;
#[doc = "Secure Debug Configuration Status"]
pub mod secdbgstat;
#[doc = "SECDBGSET (w) register accessor: an alias for `Reg<SECDBGSET_SPEC>`"]
pub type SECDBGSET = crate::Reg<secdbgset::SECDBGSET_SPEC>;
#[doc = "Secure Debug Configuration Set"]
pub mod secdbgset;
#[doc = "SECDBGCLR (w) register accessor: an alias for `Reg<SECDBGCLR_SPEC>`"]
pub type SECDBGCLR = crate::Reg<secdbgclr::SECDBGCLR_SPEC>;
#[doc = "Secure Debug Configuration Clear"]
pub mod secdbgclr;
#[doc = "SCSECCTRL (rw) register accessor: an alias for `Reg<SCSECCTRL_SPEC>`"]
pub type SCSECCTRL = crate::Reg<scsecctrl::SCSECCTRL_SPEC>;
#[doc = "System Security Control"]
pub mod scsecctrl;
#[doc = "CLK_CFG0 (rw) register accessor: an alias for `Reg<CLK_CFG0_SPEC>`"]
pub type CLK_CFG0 = crate::Reg<clk_cfg0::CLK_CFG0_SPEC>;
#[doc = "Clock Configuration Register 0."]
pub mod clk_cfg0;
#[doc = "CLK_CFG1 (rw) register accessor: an alias for `Reg<CLK_CFG1_SPEC>`"]
pub type CLK_CFG1 = crate::Reg<clk_cfg1::CLK_CFG1_SPEC>;
#[doc = "Clock Configuration Register 1."]
pub mod clk_cfg1;
#[doc = "CLOCK_FORCE (rw) register accessor: an alias for `Reg<CLOCK_FORCE_SPEC>`"]
pub type CLOCK_FORCE = crate::Reg<clock_force::CLOCK_FORCE_SPEC>;
#[doc = "Clock Force"]
pub mod clock_force;
#[doc = "RESET_SYNDROME (rw) register accessor: an alias for `Reg<RESET_SYNDROME_SPEC>`"]
pub type RESET_SYNDROME = crate::Reg<reset_syndrome::RESET_SYNDROME_SPEC>;
#[doc = "Reset Syndrome"]
pub mod reset_syndrome;
#[doc = "RESET_MASK (rw) register accessor: an alias for `Reg<RESET_MASK_SPEC>`"]
pub type RESET_MASK = crate::Reg<reset_mask::RESET_MASK_SPEC>;
#[doc = "Reset Mask"]
pub mod reset_mask;
#[doc = "SWRESET (w) register accessor: an alias for `Reg<SWRESET_SPEC>`"]
pub type SWRESET = crate::Reg<swreset::SWRESET_SPEC>;
#[doc = "Software Reset"]
pub mod swreset;
#[doc = "GRETREG (rw) register accessor: an alias for `Reg<GRETREG_SPEC>`"]
pub type GRETREG = crate::Reg<gretreg::GRETREG_SPEC>;
#[doc = "General Purpose Retention"]
pub mod gretreg;
#[doc = "INITSVRTOR0 (rw) register accessor: an alias for `Reg<INITSVRTOR0_SPEC>`"]
pub type INITSVRTOR0 = crate::Reg<initsvrtor0::INITSVRTOR0_SPEC>;
#[doc = "Initial Secure Reset Vector Register For CPU 0"]
pub mod initsvrtor0;
#[doc = "CPUWAIT (rw) register accessor: an alias for `Reg<CPUWAIT_SPEC>`"]
pub type CPUWAIT = crate::Reg<cpuwait::CPUWAIT_SPEC>;
#[doc = "CPU Boot wait control after reset"]
pub mod cpuwait;
#[doc = "NMI_ENABLE (rw) register accessor: an alias for `Reg<NMI_ENABLE_SPEC>`"]
pub type NMI_ENABLE = crate::Reg<nmi_enable::NMI_ENABLE_SPEC>;
#[doc = "NMI Enable Register"]
pub mod nmi_enable;
#[doc = "PWRCTRL (rw) register accessor: an alias for `Reg<PWRCTRL_SPEC>`"]
pub type PWRCTRL = crate::Reg<pwrctrl::PWRCTRL_SPEC>;
#[doc = "Power Configuration and Control."]
pub mod pwrctrl;
#[doc = "PDCM_PD_SYS_SENSE (rw) register accessor: an alias for `Reg<PDCM_PD_SYS_SENSE_SPEC>`"]
pub type PDCM_PD_SYS_SENSE = crate::Reg<pdcm_pd_sys_sense::PDCM_PD_SYS_SENSE_SPEC>;
#[doc = "External Wakeup Control"]
pub mod pdcm_pd_sys_sense;
#[doc = "PDCM_PD_CPU0_SENSE (rw) register accessor: an alias for `Reg<PDCM_PD_CPU0_SENSE_SPEC>`"]
pub type PDCM_PD_CPU0_SENSE = crate::Reg<pdcm_pd_cpu0_sense::PDCM_PD_CPU0_SENSE_SPEC>;
#[doc = "PDCM PD_CPU0 Sensitivity."]
pub mod pdcm_pd_cpu0_sense;
#[doc = "PDCM_PD_VMR0_SENSE (rw) register accessor: an alias for `Reg<PDCM_PD_VMR0_SENSE_SPEC>`"]
pub type PDCM_PD_VMR0_SENSE = crate::Reg<pdcm_pd_vmr0_sense::PDCM_PD_VMR0_SENSE_SPEC>;
#[doc = "PDCM PD_VMR0 Sensitivity."]
pub mod pdcm_pd_vmr0_sense;
#[doc = "PDCM_PD_VMR1_SENSE (rw) register accessor: an alias for `Reg<PDCM_PD_VMR1_SENSE_SPEC>`"]
pub type PDCM_PD_VMR1_SENSE = crate::Reg<pdcm_pd_vmr1_sense::PDCM_PD_VMR1_SENSE_SPEC>;
#[doc = "PDCM PD_VMR1 Sensitivity."]
pub mod pdcm_pd_vmr1_sense;
#[doc = "PIDR4 (r) register accessor: an alias for `Reg<PIDR4_SPEC>`"]
pub type PIDR4 = crate::Reg<pidr4::PIDR4_SPEC>;
#[doc = "Peripheral ID 4"]
pub mod pidr4;
#[doc = "PIDR0 (r) register accessor: an alias for `Reg<PIDR0_SPEC>`"]
pub type PIDR0 = crate::Reg<pidr0::PIDR0_SPEC>;
#[doc = "Peripheral ID 0"]
pub mod pidr0;
#[doc = "PIDR1 (r) register accessor: an alias for `Reg<PIDR1_SPEC>`"]
pub type PIDR1 = crate::Reg<pidr1::PIDR1_SPEC>;
#[doc = "Peripheral ID 1"]
pub mod pidr1;
#[doc = "PIDR2 (r) register accessor: an alias for `Reg<PIDR2_SPEC>`"]
pub type PIDR2 = crate::Reg<pidr2::PIDR2_SPEC>;
#[doc = "Peripheral ID 2"]
pub mod pidr2;
#[doc = "PIDR3 (r) register accessor: an alias for `Reg<PIDR3_SPEC>`"]
pub type PIDR3 = crate::Reg<pidr3::PIDR3_SPEC>;
#[doc = "Peripheral ID 3"]
pub mod pidr3;
#[doc = "CIDR0 (r) register accessor: an alias for `Reg<CIDR0_SPEC>`"]
pub type CIDR0 = crate::Reg<cidr0::CIDR0_SPEC>;
#[doc = "Component ID 0"]
pub mod cidr0;
#[doc = "CIDR1 (r) register accessor: an alias for `Reg<CIDR1_SPEC>`"]
pub type CIDR1 = crate::Reg<cidr1::CIDR1_SPEC>;
#[doc = "Component ID 1"]
pub mod cidr1;
#[doc = "CIDR2 (r) register accessor: an alias for `Reg<CIDR2_SPEC>`"]
pub type CIDR2 = crate::Reg<cidr2::CIDR2_SPEC>;
#[doc = "Component ID 2"]
pub mod cidr2;
#[doc = "CIDR3 (r) register accessor: an alias for `Reg<CIDR3_SPEC>`"]
pub type CIDR3 = crate::Reg<cidr3::CIDR3_SPEC>;
#[doc = "Component ID 3"]
pub mod cidr3;
