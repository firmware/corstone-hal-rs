// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Data Register"]
    pub data: DATA,
    #[doc = "0x04 - Data Output Register"]
    pub dataout: DATAOUT,
    _reserved2: [u8; 0x08],
    #[doc = "0x10 - Ouptut enable set Register"]
    pub outenset: OUTENSET,
    #[doc = "0x14 - Ouptut enable clear Register"]
    pub outenclr: OUTENCLR,
    #[doc = "0x18 - Alternate function set Register"]
    pub altfuncset: ALTFUNCSET,
    #[doc = "0x1c - Alternate function clear Register"]
    pub altfuncclr: ALTFUNCCLR,
    #[doc = "0x20 - Interrupt enable set Register"]
    pub intenset: INTENSET,
    #[doc = "0x24 - Interrupt enable clear Register"]
    pub intenclr: INTENCLR,
    #[doc = "0x28 - Interrupt type set Register"]
    pub inttypeset: INTTYPESET,
    #[doc = "0x2c - Interrupt type clear Register"]
    pub inttypeclr: INTTYPECLR,
    #[doc = "0x30 - Polarity-level, edge interrupt configuration set Register"]
    pub intpolset: INTPOLSET,
    #[doc = "0x34 - Polarity-level, edge interrupt configuration clear Register"]
    pub intpolclr: INTPOLCLR,
    _reserved_12_intclear: [u8; 0x04],
}
impl RegisterBlock {
    #[doc = "0x38 - Interrupt CLEAR Register"]
    #[inline(always)]
    pub fn intclear(&self) -> &INTCLEAR {
        unsafe { &*(((self as *const Self) as *const u8).add(56usize) as *const INTCLEAR) }
    }
    #[doc = "0x38 - Interrupt Status Register"]
    #[inline(always)]
    pub fn intstatus(&self) -> &INTSTATUS {
        unsafe { &*(((self as *const Self) as *const u8).add(56usize) as *const INTSTATUS) }
    }
}
#[doc = "DATA (rw) register accessor: an alias for `Reg<DATA_SPEC>`"]
pub type DATA = crate::Reg<data::DATA_SPEC>;
#[doc = "Data Register"]
pub mod data;
#[doc = "DATAOUT (rw) register accessor: an alias for `Reg<DATAOUT_SPEC>`"]
pub type DATAOUT = crate::Reg<dataout::DATAOUT_SPEC>;
#[doc = "Data Output Register"]
pub mod dataout;
#[doc = "OUTENSET (rw) register accessor: an alias for `Reg<OUTENSET_SPEC>`"]
pub type OUTENSET = crate::Reg<outenset::OUTENSET_SPEC>;
#[doc = "Ouptut enable set Register"]
pub mod outenset;
#[doc = "OUTENCLR (rw) register accessor: an alias for `Reg<OUTENCLR_SPEC>`"]
pub type OUTENCLR = crate::Reg<outenclr::OUTENCLR_SPEC>;
#[doc = "Ouptut enable clear Register"]
pub mod outenclr;
#[doc = "ALTFUNCSET (rw) register accessor: an alias for `Reg<ALTFUNCSET_SPEC>`"]
pub type ALTFUNCSET = crate::Reg<altfuncset::ALTFUNCSET_SPEC>;
#[doc = "Alternate function set Register"]
pub mod altfuncset;
#[doc = "ALTFUNCCLR (rw) register accessor: an alias for `Reg<ALTFUNCCLR_SPEC>`"]
pub type ALTFUNCCLR = crate::Reg<altfuncclr::ALTFUNCCLR_SPEC>;
#[doc = "Alternate function clear Register"]
pub mod altfuncclr;
#[doc = "INTENSET (rw) register accessor: an alias for `Reg<INTENSET_SPEC>`"]
pub type INTENSET = crate::Reg<intenset::INTENSET_SPEC>;
#[doc = "Interrupt enable set Register"]
pub mod intenset;
#[doc = "INTENCLR (rw) register accessor: an alias for `Reg<INTENCLR_SPEC>`"]
pub type INTENCLR = crate::Reg<intenclr::INTENCLR_SPEC>;
#[doc = "Interrupt enable clear Register"]
pub mod intenclr;
#[doc = "INTTYPESET (rw) register accessor: an alias for `Reg<INTTYPESET_SPEC>`"]
pub type INTTYPESET = crate::Reg<inttypeset::INTTYPESET_SPEC>;
#[doc = "Interrupt type set Register"]
pub mod inttypeset;
#[doc = "INTTYPECLR (rw) register accessor: an alias for `Reg<INTTYPECLR_SPEC>`"]
pub type INTTYPECLR = crate::Reg<inttypeclr::INTTYPECLR_SPEC>;
#[doc = "Interrupt type clear Register"]
pub mod inttypeclr;
#[doc = "INTPOLSET (rw) register accessor: an alias for `Reg<INTPOLSET_SPEC>`"]
pub type INTPOLSET = crate::Reg<intpolset::INTPOLSET_SPEC>;
#[doc = "Polarity-level, edge interrupt configuration set Register"]
pub mod intpolset;
#[doc = "INTPOLCLR (rw) register accessor: an alias for `Reg<INTPOLCLR_SPEC>`"]
pub type INTPOLCLR = crate::Reg<intpolclr::INTPOLCLR_SPEC>;
#[doc = "Polarity-level, edge interrupt configuration clear Register"]
pub mod intpolclr;
#[doc = "INTSTATUS (r) register accessor: an alias for `Reg<INTSTATUS_SPEC>`"]
pub type INTSTATUS = crate::Reg<intstatus::INTSTATUS_SPEC>;
#[doc = "Interrupt Status Register"]
pub mod intstatus;
#[doc = "INTCLEAR (w) register accessor: an alias for `Reg<INTCLEAR_SPEC>`"]
pub type INTCLEAR = crate::Reg<intclear::INTCLEAR_SPEC>;
#[doc = "Interrupt CLEAR Register"]
pub mod intclear;
