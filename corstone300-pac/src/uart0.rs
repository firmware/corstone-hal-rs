// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Receive and Transmit Data Value"]
    pub data: DATA,
    _reserved1: [u8; 0x03],
    #[doc = "0x04 - UART Status Register"]
    pub state: STATE,
    #[doc = "0x08 - UART Control Register"]
    pub ctrl: CTRL,
    _reserved_3_intclear: [u8; 0x04],
    #[doc = "0x10 - Baudrate Divider"]
    pub bauddiv: BAUDDIV,
}
impl RegisterBlock {
    #[doc = "0x0c - UART Interrupt CLEAR Register"]
    #[inline(always)]
    pub fn intclear(&self) -> &INTCLEAR {
        unsafe { &*(((self as *const Self) as *const u8).add(12usize) as *const INTCLEAR) }
    }
    #[doc = "0x0c - UART Interrupt Status Register"]
    #[inline(always)]
    pub fn intstatus(&self) -> &INTSTATUS {
        unsafe { &*(((self as *const Self) as *const u8).add(12usize) as *const INTSTATUS) }
    }
}
#[doc = "DATA (rw) register accessor: an alias for `Reg<DATA_SPEC>`"]
pub type DATA = crate::Reg<data::DATA_SPEC>;
#[doc = "Receive and Transmit Data Value"]
pub mod data;
#[doc = "STATE (rw) register accessor: an alias for `Reg<STATE_SPEC>`"]
pub type STATE = crate::Reg<state::STATE_SPEC>;
#[doc = "UART Status Register"]
pub mod state;
#[doc = "CTRL (rw) register accessor: an alias for `Reg<CTRL_SPEC>`"]
pub type CTRL = crate::Reg<ctrl::CTRL_SPEC>;
#[doc = "UART Control Register"]
pub mod ctrl;
#[doc = "INTSTATUS (r) register accessor: an alias for `Reg<INTSTATUS_SPEC>`"]
pub type INTSTATUS = crate::Reg<intstatus::INTSTATUS_SPEC>;
#[doc = "UART Interrupt Status Register"]
pub mod intstatus;
#[doc = "INTCLEAR (w) register accessor: an alias for `Reg<INTCLEAR_SPEC>`"]
pub type INTCLEAR = crate::Reg<intclear::INTCLEAR_SPEC>;
#[doc = "UART Interrupt CLEAR Register"]
pub mod intclear;
#[doc = "BAUDDIV (rw) register accessor: an alias for `Reg<BAUDDIV_SPEC>`"]
pub type BAUDDIV = crate::Reg<bauddiv::BAUDDIV_SPEC>;
#[doc = "Baudrate Divider"]
pub mod bauddiv;
