// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Counter Control Register"]
    pub cntcr: CNTCR,
    #[doc = "0x04 - Counter frequency status information"]
    pub cntsr: CNTSR,
    #[doc = "0x08..0x10 - Current count value"]
    pub cntcv: CNTCV,
    #[doc = "0x10 - Stores the Counter Scaling value"]
    pub cntscr: CNTSCR,
    _reserved4: [u8; 0x08],
    #[doc = "0x1c - Indicates additional information about Counter Scaling implementation"]
    pub cntid: CNTID,
    _reserved5: [u8; 0xb0],
    #[doc = "0xd0 - Counter Scaling register"]
    pub cntscr0: CNTSCR0,
    #[doc = "0xd4 - Counter Scaling register"]
    pub cntscr1: CNTSCR1,
    _reserved7: [u8; 0x0ef8],
    #[doc = "0xfd0 - Peripheral Identification Register 4"]
    pub cntpidr4: CNTPIDR4,
    _reserved8: [u8; 0x0c],
    #[doc = "0xfe0 - Peripheral Identification Register 0"]
    pub cntpidr0: CNTPIDR0,
    #[doc = "0xfe4 - Peripheral Identification Register 1"]
    pub cntpidr1: CNTPIDR1,
    #[doc = "0xfe8 - Peripheral Identification Register 2"]
    pub cntpidr2: CNTPIDR2,
    #[doc = "0xfec - Peripheral Identification Register 3"]
    pub cntpidr3: CNTPIDR3,
    #[doc = "0xff0 - Component Identification Register 0."]
    pub cntcidr0: CNTCIDR0,
    #[doc = "0xff4 - Component Identification Register 1"]
    pub cntcidr1: CNTCIDR1,
    #[doc = "0xff8 - Component Identification Register 2"]
    pub cntcidr2: CNTCIDR2,
    #[doc = "0xffc - Component Identification Register 3"]
    pub cntcidr3: CNTCIDR3,
}
#[doc = "CNTCR (rw) register accessor: an alias for `Reg<CNTCR_SPEC>`"]
pub type CNTCR = crate::Reg<cntcr::CNTCR_SPEC>;
#[doc = "Counter Control Register"]
pub mod cntcr;
#[doc = "CNTSR (r) register accessor: an alias for `Reg<CNTSR_SPEC>`"]
pub type CNTSR = crate::Reg<cntsr::CNTSR_SPEC>;
#[doc = "Counter frequency status information"]
pub mod cntsr;
#[doc = "CNTCV (rw) register accessor: an alias for `Reg<CNTCV_SPEC>`"]
pub type CNTCV = crate::Reg<cntcv::CNTCV_SPEC>;
#[doc = "Current count value"]
pub mod cntcv;
#[doc = "CNTSCR (rw) register accessor: an alias for `Reg<CNTSCR_SPEC>`"]
pub type CNTSCR = crate::Reg<cntscr::CNTSCR_SPEC>;
#[doc = "Stores the Counter Scaling value"]
pub mod cntscr;
#[doc = "CNTID (r) register accessor: an alias for `Reg<CNTID_SPEC>`"]
pub type CNTID = crate::Reg<cntid::CNTID_SPEC>;
#[doc = "Indicates additional information about Counter Scaling implementation"]
pub mod cntid;
#[doc = "CNTSCR0 (rw) register accessor: an alias for `Reg<CNTSCR0_SPEC>`"]
pub type CNTSCR0 = crate::Reg<cntscr0::CNTSCR0_SPEC>;
#[doc = "Counter Scaling register"]
pub mod cntscr0;
#[doc = "CNTSCR1 (rw) register accessor: an alias for `Reg<CNTSCR1_SPEC>`"]
pub type CNTSCR1 = crate::Reg<cntscr1::CNTSCR1_SPEC>;
#[doc = "Counter Scaling register"]
pub mod cntscr1;
#[doc = "CNTPIDR4 (r) register accessor: an alias for `Reg<CNTPIDR4_SPEC>`"]
pub type CNTPIDR4 = crate::Reg<cntpidr4::CNTPIDR4_SPEC>;
#[doc = "Peripheral Identification Register 4"]
pub mod cntpidr4;
#[doc = "CNTPIDR0 (r) register accessor: an alias for `Reg<CNTPIDR0_SPEC>`"]
pub type CNTPIDR0 = crate::Reg<cntpidr0::CNTPIDR0_SPEC>;
#[doc = "Peripheral Identification Register 0"]
pub mod cntpidr0;
#[doc = "CNTPIDR1 (r) register accessor: an alias for `Reg<CNTPIDR1_SPEC>`"]
pub type CNTPIDR1 = crate::Reg<cntpidr1::CNTPIDR1_SPEC>;
#[doc = "Peripheral Identification Register 1"]
pub mod cntpidr1;
#[doc = "CNTPIDR2 (r) register accessor: an alias for `Reg<CNTPIDR2_SPEC>`"]
pub type CNTPIDR2 = crate::Reg<cntpidr2::CNTPIDR2_SPEC>;
#[doc = "Peripheral Identification Register 2"]
pub mod cntpidr2;
#[doc = "CNTPIDR3 (r) register accessor: an alias for `Reg<CNTPIDR3_SPEC>`"]
pub type CNTPIDR3 = crate::Reg<cntpidr3::CNTPIDR3_SPEC>;
#[doc = "Peripheral Identification Register 3"]
pub mod cntpidr3;
#[doc = "CNTCIDR0 (r) register accessor: an alias for `Reg<CNTCIDR0_SPEC>`"]
pub type CNTCIDR0 = crate::Reg<cntcidr0::CNTCIDR0_SPEC>;
#[doc = "Component Identification Register 0."]
pub mod cntcidr0;
#[doc = "CNTCIDR1 (r) register accessor: an alias for `Reg<CNTCIDR1_SPEC>`"]
pub type CNTCIDR1 = crate::Reg<cntcidr1::CNTCIDR1_SPEC>;
#[doc = "Component Identification Register 1"]
pub mod cntcidr1;
#[doc = "CNTCIDR2 (r) register accessor: an alias for `Reg<CNTCIDR2_SPEC>`"]
pub type CNTCIDR2 = crate::Reg<cntcidr2::CNTCIDR2_SPEC>;
#[doc = "Component Identification Register 2"]
pub mod cntcidr2;
#[doc = "CNTCIDR3 (r) register accessor: an alias for `Reg<CNTCIDR3_SPEC>`"]
pub type CNTCIDR3 = crate::Reg<cntcidr3::CNTCIDR3_SPEC>;
#[doc = "Component Identification Register 3"]
pub mod cntcidr3;
