// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00..0x08 - Current count value"]
    pub cntcv: CNTCV,
    _reserved1: [u8; 0x0fc8],
    #[doc = "0xfd0 - Peripheral Identification Register 4"]
    pub cntpidr4: CNTPIDR4,
    _reserved2: [u8; 0x0c],
    #[doc = "0xfe0 - Peripheral Identification Register 0"]
    pub cntpidr0: CNTPIDR0,
    #[doc = "0xfe4 - Peripheral Identification Register 1"]
    pub cntpidr1: CNTPIDR1,
    #[doc = "0xfe8 - Peripheral Identification Register 2"]
    pub cntpidr2: CNTPIDR2,
    #[doc = "0xfec - Peripheral Identification Register 3"]
    pub cntpidr3: CNTPIDR3,
    #[doc = "0xff0 - Component Identification Register 0."]
    pub cntcidr0: CNTCIDR0,
    #[doc = "0xff4 - Component Identification Register 1"]
    pub cntcidr1: CNTCIDR1,
    #[doc = "0xff8 - Component Identification Register 2"]
    pub cntcidr2: CNTCIDR2,
    #[doc = "0xffc - Component Identification Register 3"]
    pub cntcidr3: CNTCIDR3,
}
#[doc = "CNTCV (rw) register accessor: an alias for `Reg<CNTCV_SPEC>`"]
pub type CNTCV = crate::Reg<cntcv::CNTCV_SPEC>;
#[doc = "Current count value"]
pub mod cntcv;
#[doc = "CNTPIDR4 (r) register accessor: an alias for `Reg<CNTPIDR4_SPEC>`"]
pub type CNTPIDR4 = crate::Reg<cntpidr4::CNTPIDR4_SPEC>;
#[doc = "Peripheral Identification Register 4"]
pub mod cntpidr4;
#[doc = "CNTPIDR0 (r) register accessor: an alias for `Reg<CNTPIDR0_SPEC>`"]
pub type CNTPIDR0 = crate::Reg<cntpidr0::CNTPIDR0_SPEC>;
#[doc = "Peripheral Identification Register 0"]
pub mod cntpidr0;
#[doc = "CNTPIDR1 (r) register accessor: an alias for `Reg<CNTPIDR1_SPEC>`"]
pub type CNTPIDR1 = crate::Reg<cntpidr1::CNTPIDR1_SPEC>;
#[doc = "Peripheral Identification Register 1"]
pub mod cntpidr1;
#[doc = "CNTPIDR2 (r) register accessor: an alias for `Reg<CNTPIDR2_SPEC>`"]
pub type CNTPIDR2 = crate::Reg<cntpidr2::CNTPIDR2_SPEC>;
#[doc = "Peripheral Identification Register 2"]
pub mod cntpidr2;
#[doc = "CNTPIDR3 (r) register accessor: an alias for `Reg<CNTPIDR3_SPEC>`"]
pub type CNTPIDR3 = crate::Reg<cntpidr3::CNTPIDR3_SPEC>;
#[doc = "Peripheral Identification Register 3"]
pub mod cntpidr3;
#[doc = "CNTCIDR0 (r) register accessor: an alias for `Reg<CNTCIDR0_SPEC>`"]
pub type CNTCIDR0 = crate::Reg<cntcidr0::CNTCIDR0_SPEC>;
#[doc = "Component Identification Register 0."]
pub mod cntcidr0;
#[doc = "CNTCIDR1 (r) register accessor: an alias for `Reg<CNTCIDR1_SPEC>`"]
pub type CNTCIDR1 = crate::Reg<cntcidr1::CNTCIDR1_SPEC>;
#[doc = "Component Identification Register 1"]
pub mod cntcidr1;
#[doc = "CNTCIDR2 (r) register accessor: an alias for `Reg<CNTCIDR2_SPEC>`"]
pub type CNTCIDR2 = crate::Reg<cntcidr2::CNTCIDR2_SPEC>;
#[doc = "Component Identification Register 2"]
pub mod cntcidr2;
#[doc = "CNTCIDR3 (r) register accessor: an alias for `Reg<CNTCIDR3_SPEC>`"]
pub type CNTCIDR3 = crate::Reg<cntcidr3::CNTCIDR3_SPEC>;
#[doc = "Component Identification Register 3"]
pub mod cntcidr3;
