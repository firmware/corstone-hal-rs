// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Control register"]
    pub control: CONTROL,
    #[doc = "0x04 - Status register"]
    pub status: STATUS,
    #[doc = "0x08 - Error status register"]
    pub error: ERROR,
    #[doc = "0x0c - Clock Divide Ratio register"]
    pub divide: DIVIDE,
    #[doc = "0x10 - Transmit Buffer FIFO Data register"]
    pub txbuf: TXBUF,
    #[doc = "0x14 - Receive Buffer FIFO Data register"]
    pub rxbuf: RXBUF,
    _reserved6: [u8; 0x02e8],
    #[doc = "0x300 - Integration Test Control register"]
    pub itcr: ITCR,
    #[doc = "0x304 - Integration Test Input register 1"]
    pub itip1: ITIP1,
    #[doc = "0x308 - Integration Test Output register 1"]
    pub itop1: ITOP1,
}
#[doc = "CONTROL (rw) register accessor: an alias for `Reg<CONTROL_SPEC>`"]
pub type CONTROL = crate::Reg<control::CONTROL_SPEC>;
#[doc = "Control register"]
pub mod control;
#[doc = "STATUS (r) register accessor: an alias for `Reg<STATUS_SPEC>`"]
pub type STATUS = crate::Reg<status::STATUS_SPEC>;
#[doc = "Status register"]
pub mod status;
#[doc = "ERROR (rw) register accessor: an alias for `Reg<ERROR_SPEC>`"]
pub type ERROR = crate::Reg<error::ERROR_SPEC>;
#[doc = "Error status register"]
pub mod error;
#[doc = "DIVIDE (rw) register accessor: an alias for `Reg<DIVIDE_SPEC>`"]
pub type DIVIDE = crate::Reg<divide::DIVIDE_SPEC>;
#[doc = "Clock Divide Ratio register"]
pub mod divide;
#[doc = "TXBUF (w) register accessor: an alias for `Reg<TXBUF_SPEC>`"]
pub type TXBUF = crate::Reg<txbuf::TXBUF_SPEC>;
#[doc = "Transmit Buffer FIFO Data register"]
pub mod txbuf;
#[doc = "RXBUF (r) register accessor: an alias for `Reg<RXBUF_SPEC>`"]
pub type RXBUF = crate::Reg<rxbuf::RXBUF_SPEC>;
#[doc = "Receive Buffer FIFO Data register"]
pub mod rxbuf;
#[doc = "ITCR (rw) register accessor: an alias for `Reg<ITCR_SPEC>`"]
pub type ITCR = crate::Reg<itcr::ITCR_SPEC>;
#[doc = "Integration Test Control register"]
pub mod itcr;
#[doc = "ITIP1 (rw) register accessor: an alias for `Reg<ITIP1_SPEC>`"]
pub type ITIP1 = crate::Reg<itip1::ITIP1_SPEC>;
#[doc = "Integration Test Input register 1"]
pub mod itip1;
#[doc = "ITOP1 (rw) register accessor: an alias for `Reg<ITOP1_SPEC>`"]
pub type ITOP1 = crate::Reg<itop1::ITOP1_SPEC>;
#[doc = "Integration Test Output register 1"]
pub mod itop1;
