// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `IntTCStatus` reader"]
pub struct R(crate::R<INT_TCSTATUS_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INT_TCSTATUS_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INT_TCSTATUS_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INT_TCSTATUS_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Interrupt Terminal Count Status\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [int_tcstatus](index.html) module"]
pub struct INT_TCSTATUS_SPEC;
impl crate::RegisterSpec for INT_TCSTATUS_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [int_tcstatus::R](R) reader structure"]
impl crate::Readable for INT_TCSTATUS_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets IntTCStatus to value 0"]
impl crate::Resettable for INT_TCSTATUS_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
