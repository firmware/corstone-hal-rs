// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `AHBSPPPC0` reader"]
pub struct R(crate::R<AHBSPPPC0_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<AHBSPPPC0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<AHBSPPPC0_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<AHBSPPPC0_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Secure Unprivileged Access AHB slave Peripheral Protection Control 0\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [ahbspppc0](index.html) module"]
pub struct AHBSPPPC0_SPEC;
impl crate::RegisterSpec for AHBSPPPC0_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [ahbspppc0::R](R) reader structure"]
impl crate::Readable for AHBSPPPC0_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets AHBSPPPC0 to value 0"]
impl crate::Resettable for AHBSPPPC0_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
