// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SECMSCINTSTAT` reader"]
pub struct R(crate::R<SECMSCINTSTAT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SECMSCINTSTAT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SECMSCINTSTAT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SECMSCINTSTAT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Secure MSC Interrupt Status\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [secmscintstat](index.html) module"]
pub struct SECMSCINTSTAT_SPEC;
impl crate::RegisterSpec for SECMSCINTSTAT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [secmscintstat::R](R) reader structure"]
impl crate::Readable for SECMSCINTSTAT_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets SECMSCINTSTAT to value 0"]
impl crate::Resettable for SECMSCINTSTAT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
