// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `NSMSCEXP` reader"]
pub struct R(crate::R<NSMSCEXP_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<NSMSCEXP_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<NSMSCEXP_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<NSMSCEXP_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Expansion MSC Non-Secure Configuration\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [nsmscexp](index.html) module"]
pub struct NSMSCEXP_SPEC;
impl crate::RegisterSpec for NSMSCEXP_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [nsmscexp::R](R) reader structure"]
impl crate::Readable for NSMSCEXP_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets NSMSCEXP to value 0"]
impl crate::Resettable for NSMSCEXP_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
