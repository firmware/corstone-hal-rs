// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `BRGINTEN` reader"]
pub struct R(crate::R<BRGINTEN_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<BRGINTEN_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<BRGINTEN_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<BRGINTEN_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `BRGINTEN` writer"]
pub struct W(crate::W<BRGINTEN_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<BRGINTEN_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<BRGINTEN_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<BRGINTEN_SPEC>) -> Self {
        W(writer)
    }
}
impl W {
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Bridge Buffer Error Interrupt Enable\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [brginten](index.html) module"]
pub struct BRGINTEN_SPEC;
impl crate::RegisterSpec for BRGINTEN_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [brginten::R](R) reader structure"]
impl crate::Readable for BRGINTEN_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [brginten::W](W) writer structure"]
impl crate::Writable for BRGINTEN_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets BRGINTEN to value 0"]
impl crate::Resettable for BRGINTEN_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
