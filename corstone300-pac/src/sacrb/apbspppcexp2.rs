// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `APBSPPPCEXP2` reader"]
pub struct R(crate::R<APBSPPPCEXP2_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<APBSPPPCEXP2_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<APBSPPPCEXP2_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<APBSPPPCEXP2_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `APBSPPPCEXP2` writer"]
pub struct W(crate::W<APBSPPPCEXP2_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<APBSPPPCEXP2_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<APBSPPPCEXP2_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<APBSPPPCEXP2_SPEC>) -> Self {
        W(writer)
    }
}
impl W {
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Expansion 2 Secure Unprivileged Access APB slave Peripheral Protection Control\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [apbspppcexp2](index.html) module"]
pub struct APBSPPPCEXP2_SPEC;
impl crate::RegisterSpec for APBSPPPCEXP2_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [apbspppcexp2::R](R) reader structure"]
impl crate::Readable for APBSPPPCEXP2_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [apbspppcexp2::W](W) writer structure"]
impl crate::Writable for APBSPPPCEXP2_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets APBSPPPCEXP2 to value 0"]
impl crate::Resettable for APBSPPPCEXP2_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
