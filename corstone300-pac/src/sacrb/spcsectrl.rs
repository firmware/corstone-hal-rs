// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SPCSECTRL` reader"]
pub struct R(crate::R<SPCSECTRL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SPCSECTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SPCSECTRL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SPCSECTRL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `SPCSECTRL` writer"]
pub struct W(crate::W<SPCSECTRL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<SPCSECTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<SPCSECTRL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<SPCSECTRL_SPEC>) -> Self {
        W(writer)
    }
}
impl W {
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Secure Privilege Controller Secure Configuration Control register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [spcsectrl](index.html) module"]
pub struct SPCSECTRL_SPEC;
impl crate::RegisterSpec for SPCSECTRL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [spcsectrl::R](R) reader structure"]
impl crate::Readable for SPCSECTRL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [spcsectrl::W](W) writer structure"]
impl crate::Writable for SPCSECTRL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets SPCSECTRL to value 0"]
impl crate::Resettable for SPCSECTRL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
