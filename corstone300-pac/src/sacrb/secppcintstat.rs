// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SECPPCINTSTAT` reader"]
pub struct R(crate::R<SECPPCINTSTAT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SECPPCINTSTAT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SECPPCINTSTAT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SECPPCINTSTAT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Secure PPC Interrupt Status\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [secppcintstat](index.html) module"]
pub struct SECPPCINTSTAT_SPEC;
impl crate::RegisterSpec for SECPPCINTSTAT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [secppcintstat::R](R) reader structure"]
impl crate::Readable for SECPPCINTSTAT_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets SECPPCINTSTAT to value 0"]
impl crate::Resettable for SECPPCINTSTAT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
