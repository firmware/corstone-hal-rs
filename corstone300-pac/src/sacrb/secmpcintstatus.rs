// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SECMPCINTSTATUS` reader"]
pub struct R(crate::R<SECMPCINTSTATUS_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SECMPCINTSTATUS_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SECMPCINTSTATUS_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SECMPCINTSTATUS_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Secure MPC Interrupt Status\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [secmpcintstatus](index.html) module"]
pub struct SECMPCINTSTATUS_SPEC;
impl crate::RegisterSpec for SECMPCINTSTATUS_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [secmpcintstatus::R](R) reader structure"]
impl crate::Readable for SECMPCINTSTATUS_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets SECMPCINTSTATUS to value 0"]
impl crate::Resettable for SECMPCINTSTATUS_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
