// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `AHBNSPPCEXP1` reader"]
pub struct R(crate::R<AHBNSPPCEXP1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<AHBNSPPCEXP1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<AHBNSPPCEXP1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<AHBNSPPCEXP1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `AHBNSPPCEXP1` writer"]
pub struct W(crate::W<AHBNSPPCEXP1_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<AHBNSPPCEXP1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<AHBNSPPCEXP1_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<AHBNSPPCEXP1_SPEC>) -> Self {
        W(writer)
    }
}
impl W {
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Expansion 1 Non_Secure Access AHB slave Peripheral Protection Control\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [ahbnsppcexp1](index.html) module"]
pub struct AHBNSPPCEXP1_SPEC;
impl crate::RegisterSpec for AHBNSPPCEXP1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [ahbnsppcexp1::R](R) reader structure"]
impl crate::Readable for AHBNSPPCEXP1_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [ahbnsppcexp1::W](W) writer structure"]
impl crate::Writable for AHBNSPPCEXP1_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets AHBNSPPCEXP1 to value 0"]
impl crate::Resettable for AHBNSPPCEXP1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
