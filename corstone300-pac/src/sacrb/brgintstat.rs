// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `BRGINTSTAT` reader"]
pub struct R(crate::R<BRGINTSTAT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<BRGINTSTAT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<BRGINTSTAT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<BRGINTSTAT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Bridge Buffer Error Interrupt Status\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [brgintstat](index.html) module"]
pub struct BRGINTSTAT_SPEC;
impl crate::RegisterSpec for BRGINTSTAT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [brgintstat::R](R) reader structure"]
impl crate::Readable for BRGINTSTAT_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets BRGINTSTAT to value 0"]
impl crate::Resettable for BRGINTSTAT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
