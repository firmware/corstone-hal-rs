// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Interrupt Status"]
    pub int_status: INT_STATUS,
    #[doc = "0x04 - Interrupt Terminal Count Status"]
    pub int_tcstatus: INT_TCSTATUS,
    #[doc = "0x08 - Interrupt Terminal Count Clear"]
    pub int_tcclear: INT_TCCLEAR,
}
#[doc = "IntStatus (r) register accessor: an alias for `Reg<INT_STATUS_SPEC>`"]
pub type INT_STATUS = crate::Reg<int_status::INT_STATUS_SPEC>;
#[doc = "Interrupt Status"]
pub mod int_status;
#[doc = "IntTCStatus (r) register accessor: an alias for `Reg<INT_TCSTATUS_SPEC>`"]
pub type INT_TCSTATUS = crate::Reg<int_tcstatus::INT_TCSTATUS_SPEC>;
#[doc = "Interrupt Terminal Count Status"]
pub mod int_tcstatus;
#[doc = "IntTCClear (w) register accessor: an alias for `Reg<INT_TCCLEAR_SPEC>`"]
pub type INT_TCCLEAR = crate::Reg<int_tcclear::INT_TCCLEAR_SPEC>;
#[doc = "Interrupt Terminal Count Clear"]
pub mod int_tcclear;
