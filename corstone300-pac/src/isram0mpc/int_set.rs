// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INT_SET` writer"]
pub struct W(crate::W<INT_SET_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<INT_SET_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<INT_SET_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<INT_SET_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `mpc_irq` writer - mpc_irq set. Debug purpose only"]
pub type MPC_IRQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_SET_SPEC, bool, O>;
impl W {
    #[doc = "Bit 0 - mpc_irq set. Debug purpose only"]
    #[inline(always)]
    pub fn mpc_irq(&mut self) -> MPC_IRQ_W<0> {
        MPC_IRQ_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Interrupt set. Debug purpose only\n\nThis register you can [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [int_set](index.html) module"]
pub struct INT_SET_SPEC;
impl crate::RegisterSpec for INT_SET_SPEC {
    type Ux = u32;
}
#[doc = "`write(|w| ..)` method takes [int_set::W](W) writer structure"]
impl crate::Writable for INT_SET_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets INT_SET to value 0"]
impl crate::Resettable for INT_SET_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
