// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INT_INFO2` reader"]
pub struct R(crate::R<INT_INFO2_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INT_INFO2_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INT_INFO2_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INT_INFO2_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `hmaster` reader - hmaster"]
pub type HMASTER_R = crate::FieldReader<u16, u16>;
#[doc = "Field `hnonsec` reader - hnonsec"]
pub type HNONSEC_R = crate::BitReader<bool>;
#[doc = "Field `cfg_ns` reader - cfg_ns"]
pub type CFG_NS_R = crate::BitReader<bool>;
impl R {
    #[doc = "Bits 0:15 - hmaster"]
    #[inline(always)]
    pub fn hmaster(&self) -> HMASTER_R {
        HMASTER_R::new((self.bits & 0xffff) as u16)
    }
    #[doc = "Bit 16 - hnonsec"]
    #[inline(always)]
    pub fn hnonsec(&self) -> HNONSEC_R {
        HNONSEC_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - cfg_ns"]
    #[inline(always)]
    pub fn cfg_ns(&self) -> CFG_NS_R {
        CFG_NS_R::new(((self.bits >> 17) & 1) != 0)
    }
}
#[doc = "Interrupt information 2\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [int_info2](index.html) module"]
pub struct INT_INFO2_SPEC;
impl crate::RegisterSpec for INT_INFO2_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [int_info2::R](R) reader structure"]
impl crate::Readable for INT_INFO2_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets INT_INFO2 to value 0"]
impl crate::Resettable for INT_INFO2_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
