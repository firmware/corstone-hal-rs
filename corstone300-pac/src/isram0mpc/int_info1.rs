// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INT_INFO1` reader"]
pub struct R(crate::R<INT_INFO1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INT_INFO1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INT_INFO1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INT_INFO1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Interrupt information 1\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [int_info1](index.html) module"]
pub struct INT_INFO1_SPEC;
impl crate::RegisterSpec for INT_INFO1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [int_info1::R](R) reader structure"]
impl crate::Readable for INT_INFO1_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets INT_INFO1 to value 0"]
impl crate::Resettable for INT_INFO1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
