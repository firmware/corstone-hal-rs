// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `BLK_CFG` reader"]
pub struct R(crate::R<BLK_CFG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<BLK_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<BLK_CFG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<BLK_CFG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Block Configuration\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [blk_cfg](index.html) module"]
pub struct BLK_CFG_SPEC;
impl crate::RegisterSpec for BLK_CFG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [blk_cfg::R](R) reader structure"]
impl crate::Readable for BLK_CFG_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets BLK_CFG to value 0"]
impl crate::Resettable for BLK_CFG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
