// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `BLK_MAX` reader"]
pub struct R(crate::R<BLK_MAX_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<BLK_MAX_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<BLK_MAX_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<BLK_MAX_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `BLK_SIZE` reader - Block size"]
pub type BLK_SIZE_R = crate::FieldReader<u8, u8>;
#[doc = "Field `IN_PROGRESS` reader - Initialization in progress"]
pub type IN_PROGRESS_R = crate::BitReader<bool>;
impl R {
    #[doc = "Bits 0:3 - Block size"]
    #[inline(always)]
    pub fn blk_size(&self) -> BLK_SIZE_R {
        BLK_SIZE_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bit 31 - Initialization in progress"]
    #[inline(always)]
    pub fn in_progress(&self) -> IN_PROGRESS_R {
        IN_PROGRESS_R::new(((self.bits >> 31) & 1) != 0)
    }
}
#[doc = "Maximum value of block based index register\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [blk_max](index.html) module"]
pub struct BLK_MAX_SPEC;
impl crate::RegisterSpec for BLK_MAX_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [blk_max::R](R) reader structure"]
impl crate::Readable for BLK_MAX_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets BLK_MAX to value 0"]
impl crate::Resettable for BLK_MAX_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
