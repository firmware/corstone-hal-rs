// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CTRL` reader"]
pub struct R(crate::R<CTRL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CTRL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CTRL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CTRL` writer"]
pub struct W(crate::W<CTRL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CTRL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CTRL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `CFG_SEC_RESP` reader - Security error response configuration"]
pub type CFG_SEC_RESP_R = crate::BitReader<CFG_SEC_RESP_A>;
#[doc = "Security error response configuration\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CFG_SEC_RESP_A {
    #[doc = "0: Read-As-Zero - Writes ignored"]
    RAZ_WI = 0,
    #[doc = "1: Bus Error"]
    BUSERROR = 1,
}
impl From<CFG_SEC_RESP_A> for bool {
    #[inline(always)]
    fn from(variant: CFG_SEC_RESP_A) -> Self {
        variant as u8 != 0
    }
}
impl CFG_SEC_RESP_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CFG_SEC_RESP_A {
        match self.bits {
            false => CFG_SEC_RESP_A::RAZ_WI,
            true => CFG_SEC_RESP_A::BUSERROR,
        }
    }
    #[doc = "Checks if the value of the field is `RAZ_WI`"]
    #[inline(always)]
    pub fn is_raz_wi(&self) -> bool {
        *self == CFG_SEC_RESP_A::RAZ_WI
    }
    #[doc = "Checks if the value of the field is `BUSERROR`"]
    #[inline(always)]
    pub fn is_buserror(&self) -> bool {
        *self == CFG_SEC_RESP_A::BUSERROR
    }
}
#[doc = "Field `CFG_SEC_RESP` writer - Security error response configuration"]
pub type CFG_SEC_RESP_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, CFG_SEC_RESP_A, O>;
impl<'a, const O: u8> CFG_SEC_RESP_W<'a, O> {
    #[doc = "Read-As-Zero - Writes ignored"]
    #[inline(always)]
    pub fn raz_wi(self) -> &'a mut W {
        self.variant(CFG_SEC_RESP_A::RAZ_WI)
    }
    #[doc = "Bus Error"]
    #[inline(always)]
    pub fn buserror(self) -> &'a mut W {
        self.variant(CFG_SEC_RESP_A::BUSERROR)
    }
}
#[doc = "Field `DATA_IF_GATING_REQ` reader - Data interface gating request"]
pub type DATA_IF_GATING_REQ_R = crate::BitReader<bool>;
#[doc = "Field `DATA_IF_GATING_REQ` writer - Data interface gating request"]
pub type DATA_IF_GATING_REQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, bool, O>;
#[doc = "Field `DATA_IF_GATING_ACK` reader - Data interface gating acknowledge (RO)"]
pub type DATA_IF_GATING_ACK_R = crate::BitReader<bool>;
#[doc = "Field `AUTO_INCREMENT` reader - Auto-increment"]
pub type AUTO_INCREMENT_R = crate::BitReader<bool>;
#[doc = "Field `AUTO_INCREMENT` writer - Auto-increment"]
pub type AUTO_INCREMENT_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, bool, O>;
#[doc = "Field `SEC_LOCKDOWN` reader - Security lockdown"]
pub type SEC_LOCKDOWN_R = crate::BitReader<bool>;
#[doc = "Field `SEC_LOCKDOWN` writer - Security lockdown"]
pub type SEC_LOCKDOWN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, bool, O>;
impl R {
    #[doc = "Bit 4 - Security error response configuration"]
    #[inline(always)]
    pub fn cfg_sec_resp(&self) -> CFG_SEC_RESP_R {
        CFG_SEC_RESP_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 6 - Data interface gating request"]
    #[inline(always)]
    pub fn data_if_gating_req(&self) -> DATA_IF_GATING_REQ_R {
        DATA_IF_GATING_REQ_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - Data interface gating acknowledge (RO)"]
    #[inline(always)]
    pub fn data_if_gating_ack(&self) -> DATA_IF_GATING_ACK_R {
        DATA_IF_GATING_ACK_R::new(((self.bits >> 7) & 1) != 0)
    }
    #[doc = "Bit 8 - Auto-increment"]
    #[inline(always)]
    pub fn auto_increment(&self) -> AUTO_INCREMENT_R {
        AUTO_INCREMENT_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 31 - Security lockdown"]
    #[inline(always)]
    pub fn sec_lockdown(&self) -> SEC_LOCKDOWN_R {
        SEC_LOCKDOWN_R::new(((self.bits >> 31) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 4 - Security error response configuration"]
    #[inline(always)]
    pub fn cfg_sec_resp(&mut self) -> CFG_SEC_RESP_W<4> {
        CFG_SEC_RESP_W::new(self)
    }
    #[doc = "Bit 6 - Data interface gating request"]
    #[inline(always)]
    pub fn data_if_gating_req(&mut self) -> DATA_IF_GATING_REQ_W<6> {
        DATA_IF_GATING_REQ_W::new(self)
    }
    #[doc = "Bit 8 - Auto-increment"]
    #[inline(always)]
    pub fn auto_increment(&mut self) -> AUTO_INCREMENT_W<8> {
        AUTO_INCREMENT_W::new(self)
    }
    #[doc = "Bit 31 - Security lockdown"]
    #[inline(always)]
    pub fn sec_lockdown(&mut self) -> SEC_LOCKDOWN_W<31> {
        SEC_LOCKDOWN_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "MPC Control register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [ctrl](index.html) module"]
pub struct CTRL_SPEC;
impl crate::RegisterSpec for CTRL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [ctrl::R](R) reader structure"]
impl crate::Readable for CTRL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [ctrl::W](W) writer structure"]
impl crate::Writable for CTRL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CTRL to value 0"]
impl crate::Resettable for CTRL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
