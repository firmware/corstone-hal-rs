// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `PIDR3` reader"]
pub struct R(crate::R<PIDR3_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<PIDR3_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<PIDR3_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<PIDR3_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `customer_mod_number` reader - Customer modification number"]
pub type CUSTOMER_MOD_NUMBER_R = crate::FieldReader<u8, u8>;
#[doc = "Field `evo_rev` reader - ECO revision number"]
pub type EVO_REV_R = crate::FieldReader<u8, u8>;
impl R {
    #[doc = "Bits 0:3 - Customer modification number"]
    #[inline(always)]
    pub fn customer_mod_number(&self) -> CUSTOMER_MOD_NUMBER_R {
        CUSTOMER_MOD_NUMBER_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bits 4:7 - ECO revision number"]
    #[inline(always)]
    pub fn evo_rev(&self) -> EVO_REV_R {
        EVO_REV_R::new(((self.bits >> 4) & 0x0f) as u8)
    }
}
#[doc = "Peripheral ID 3\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [pidr3](index.html) module"]
pub struct PIDR3_SPEC;
impl crate::RegisterSpec for PIDR3_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [pidr3::R](R) reader structure"]
impl crate::Readable for PIDR3_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets PIDR3 to value 0"]
impl crate::Resettable for PIDR3_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
