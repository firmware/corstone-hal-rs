// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INT_STAT` reader"]
pub struct R(crate::R<INT_STAT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INT_STAT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INT_STAT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INT_STAT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `mpc_irq` reader - mpc_irq triggered"]
pub type MPC_IRQ_R = crate::BitReader<bool>;
impl R {
    #[doc = "Bit 0 - mpc_irq triggered"]
    #[inline(always)]
    pub fn mpc_irq(&self) -> MPC_IRQ_R {
        MPC_IRQ_R::new((self.bits & 1) != 0)
    }
}
#[doc = "Interrupt state\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [int_stat](index.html) module"]
pub struct INT_STAT_SPEC;
impl crate::RegisterSpec for INT_STAT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [int_stat::R](R) reader structure"]
impl crate::Readable for INT_STAT_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets INT_STAT to value 0"]
impl crate::Resettable for INT_STAT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
