// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `PIDR5` reader"]
pub struct R(crate::R<PIDR5_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<PIDR5_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<PIDR5_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<PIDR5_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `part_number` reader - Part number"]
pub type PART_NUMBER_R = crate::FieldReader<u8, u8>;
#[doc = "Field `jep106_id_3_0` reader - jep106_id_3_0"]
pub type JEP106_ID_3_0_R = crate::FieldReader<u8, u8>;
impl R {
    #[doc = "Bits 0:3 - Part number"]
    #[inline(always)]
    pub fn part_number(&self) -> PART_NUMBER_R {
        PART_NUMBER_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bits 4:7 - jep106_id_3_0"]
    #[inline(always)]
    pub fn jep106_id_3_0(&self) -> JEP106_ID_3_0_R {
        JEP106_ID_3_0_R::new(((self.bits >> 4) & 0x0f) as u8)
    }
}
#[doc = "Peripheral ID 5\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [pidr5](index.html) module"]
pub struct PIDR5_SPEC;
impl crate::RegisterSpec for PIDR5_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [pidr5::R](R) reader structure"]
impl crate::Readable for PIDR5_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets PIDR5 to value 0"]
impl crate::Resettable for PIDR5_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
