// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    _reserved_0_control: [u8; 0x04],
    #[doc = "0x04 - Control Clear"]
    pub controlc: CONTROLC,
}
impl RegisterBlock {
    #[doc = "0x00 - Control Status"]
    #[inline(always)]
    pub fn control(&self) -> &CONTROL {
        unsafe { &*(((self as *const Self) as *const u8).add(0usize) as *const CONTROL) }
    }
    #[doc = "0x00 - Control Set"]
    #[inline(always)]
    pub fn controls(&self) -> &CONTROLS {
        unsafe { &*(((self as *const Self) as *const u8).add(0usize) as *const CONTROLS) }
    }
}
#[doc = "CONTROLS (w) register accessor: an alias for `Reg<CONTROLS_SPEC>`"]
pub type CONTROLS = crate::Reg<controls::CONTROLS_SPEC>;
#[doc = "Control Set"]
pub mod controls;
#[doc = "CONTROL (r) register accessor: an alias for `Reg<CONTROL_SPEC>`"]
pub type CONTROL = crate::Reg<control::CONTROL_SPEC>;
#[doc = "Control Status"]
pub mod control;
#[doc = "CONTROLC (rw) register accessor: an alias for `Reg<CONTROLC_SPEC>`"]
pub type CONTROLC = crate::Reg<controlc::CONTROLC_SPEC>;
#[doc = "Control Clear"]
pub mod controlc;
