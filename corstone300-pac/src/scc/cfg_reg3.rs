// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CFG_REG3` reader"]
pub struct R(crate::R<CFG_REG3_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CFG_REG3_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CFG_REG3_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CFG_REG3_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `MCC_SWITCH0` reader - MCC SWITCHES: 0 = OFF 1 = ON"]
pub type MCC_SWITCH0_R = crate::BitReader<MCC_SWITCH0_A>;
#[doc = "MCC SWITCHES: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_SWITCH0_A {
    #[doc = "0: Switch is off"]
    OFF = 0,
    #[doc = "1: Switch is on"]
    ON = 1,
}
impl From<MCC_SWITCH0_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_SWITCH0_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_SWITCH0_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_SWITCH0_A {
        match self.bits {
            false => MCC_SWITCH0_A::OFF,
            true => MCC_SWITCH0_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_SWITCH0_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_SWITCH0_A::ON
    }
}
#[doc = "Field `MCC_SWITCH1` reader - MCC SWITCHES: 0 = OFF 1 = ON"]
pub type MCC_SWITCH1_R = crate::BitReader<MCC_SWITCH1_A>;
#[doc = "MCC SWITCHES: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_SWITCH1_A {
    #[doc = "0: Switch is off"]
    OFF = 0,
    #[doc = "1: Switch is on"]
    ON = 1,
}
impl From<MCC_SWITCH1_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_SWITCH1_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_SWITCH1_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_SWITCH1_A {
        match self.bits {
            false => MCC_SWITCH1_A::OFF,
            true => MCC_SWITCH1_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_SWITCH1_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_SWITCH1_A::ON
    }
}
#[doc = "Field `MCC_SWITCH2` reader - MCC SWITCHES: 0 = OFF 1 = ON"]
pub type MCC_SWITCH2_R = crate::BitReader<MCC_SWITCH2_A>;
#[doc = "MCC SWITCHES: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_SWITCH2_A {
    #[doc = "0: Switch is off"]
    OFF = 0,
    #[doc = "1: Switch is on"]
    ON = 1,
}
impl From<MCC_SWITCH2_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_SWITCH2_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_SWITCH2_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_SWITCH2_A {
        match self.bits {
            false => MCC_SWITCH2_A::OFF,
            true => MCC_SWITCH2_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_SWITCH2_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_SWITCH2_A::ON
    }
}
#[doc = "Field `MCC_SWITCH3` reader - MCC SWITCHES: 0 = OFF 1 = ON"]
pub type MCC_SWITCH3_R = crate::BitReader<MCC_SWITCH3_A>;
#[doc = "MCC SWITCHES: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_SWITCH3_A {
    #[doc = "0: Switch is off"]
    OFF = 0,
    #[doc = "1: Switch is on"]
    ON = 1,
}
impl From<MCC_SWITCH3_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_SWITCH3_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_SWITCH3_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_SWITCH3_A {
        match self.bits {
            false => MCC_SWITCH3_A::OFF,
            true => MCC_SWITCH3_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_SWITCH3_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_SWITCH3_A::ON
    }
}
#[doc = "Field `MCC_SWITCH4` reader - MCC SWITCHES: 0 = OFF 1 = ON"]
pub type MCC_SWITCH4_R = crate::BitReader<MCC_SWITCH4_A>;
#[doc = "MCC SWITCHES: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_SWITCH4_A {
    #[doc = "0: Switch is off"]
    OFF = 0,
    #[doc = "1: Switch is on"]
    ON = 1,
}
impl From<MCC_SWITCH4_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_SWITCH4_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_SWITCH4_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_SWITCH4_A {
        match self.bits {
            false => MCC_SWITCH4_A::OFF,
            true => MCC_SWITCH4_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_SWITCH4_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_SWITCH4_A::ON
    }
}
#[doc = "Field `MCC_SWITCH5` reader - MCC SWITCHES: 0 = OFF 1 = ON"]
pub type MCC_SWITCH5_R = crate::BitReader<MCC_SWITCH5_A>;
#[doc = "MCC SWITCHES: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_SWITCH5_A {
    #[doc = "0: Switch is off"]
    OFF = 0,
    #[doc = "1: Switch is on"]
    ON = 1,
}
impl From<MCC_SWITCH5_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_SWITCH5_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_SWITCH5_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_SWITCH5_A {
        match self.bits {
            false => MCC_SWITCH5_A::OFF,
            true => MCC_SWITCH5_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_SWITCH5_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_SWITCH5_A::ON
    }
}
#[doc = "Field `MCC_SWITCH6` reader - MCC SWITCHES: 0 = OFF 1 = ON"]
pub type MCC_SWITCH6_R = crate::BitReader<MCC_SWITCH6_A>;
#[doc = "MCC SWITCHES: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_SWITCH6_A {
    #[doc = "0: Switch is off"]
    OFF = 0,
    #[doc = "1: Switch is on"]
    ON = 1,
}
impl From<MCC_SWITCH6_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_SWITCH6_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_SWITCH6_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_SWITCH6_A {
        match self.bits {
            false => MCC_SWITCH6_A::OFF,
            true => MCC_SWITCH6_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_SWITCH6_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_SWITCH6_A::ON
    }
}
#[doc = "Field `MCC_SWITCH7` reader - MCC SWITCHES: 0 = OFF 1 = ON"]
pub type MCC_SWITCH7_R = crate::BitReader<MCC_SWITCH7_A>;
#[doc = "MCC SWITCHES: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_SWITCH7_A {
    #[doc = "0: Switch is off"]
    OFF = 0,
    #[doc = "1: Switch is on"]
    ON = 1,
}
impl From<MCC_SWITCH7_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_SWITCH7_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_SWITCH7_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_SWITCH7_A {
        match self.bits {
            false => MCC_SWITCH7_A::OFF,
            true => MCC_SWITCH7_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_SWITCH7_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_SWITCH7_A::ON
    }
}
impl R {
    #[doc = "Bit 0 - MCC SWITCHES: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_switch0(&self) -> MCC_SWITCH0_R {
        MCC_SWITCH0_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - MCC SWITCHES: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_switch1(&self) -> MCC_SWITCH1_R {
        MCC_SWITCH1_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - MCC SWITCHES: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_switch2(&self) -> MCC_SWITCH2_R {
        MCC_SWITCH2_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - MCC SWITCHES: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_switch3(&self) -> MCC_SWITCH3_R {
        MCC_SWITCH3_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - MCC SWITCHES: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_switch4(&self) -> MCC_SWITCH4_R {
        MCC_SWITCH4_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5 - MCC SWITCHES: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_switch5(&self) -> MCC_SWITCH5_R {
        MCC_SWITCH5_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6 - MCC SWITCHES: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_switch6(&self) -> MCC_SWITCH6_R {
        MCC_SWITCH6_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - MCC SWITCHES: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_switch7(&self) -> MCC_SWITCH7_R {
        MCC_SWITCH7_R::new(((self.bits >> 7) & 1) != 0)
    }
}
#[doc = "\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cfg_reg3](index.html) module"]
pub struct CFG_REG3_SPEC;
impl crate::RegisterSpec for CFG_REG3_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cfg_reg3::R](R) reader structure"]
impl crate::Readable for CFG_REG3_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CFG_REG3 to value 0"]
impl crate::Resettable for CFG_REG3_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
