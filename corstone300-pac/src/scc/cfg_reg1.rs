// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CFG_REG1` reader"]
pub struct R(crate::R<CFG_REG1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CFG_REG1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CFG_REG1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CFG_REG1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CFG_REG1` writer"]
pub struct W(crate::W<CFG_REG1_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CFG_REG1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CFG_REG1_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CFG_REG1_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `MCC_LED0` reader - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED0_R = crate::BitReader<MCC_LED0_A>;
#[doc = "MCC LEDs: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_LED0_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<MCC_LED0_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_LED0_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_LED0_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_LED0_A {
        match self.bits {
            false => MCC_LED0_A::OFF,
            true => MCC_LED0_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_LED0_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_LED0_A::ON
    }
}
#[doc = "Field `MCC_LED0` writer - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED0_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG1_SPEC, MCC_LED0_A, O>;
impl<'a, const O: u8> MCC_LED0_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(MCC_LED0_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(MCC_LED0_A::ON)
    }
}
#[doc = "Field `MCC_LED1` reader - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED1_R = crate::BitReader<MCC_LED1_A>;
#[doc = "MCC LEDs: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_LED1_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<MCC_LED1_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_LED1_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_LED1_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_LED1_A {
        match self.bits {
            false => MCC_LED1_A::OFF,
            true => MCC_LED1_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_LED1_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_LED1_A::ON
    }
}
#[doc = "Field `MCC_LED1` writer - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED1_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG1_SPEC, MCC_LED1_A, O>;
impl<'a, const O: u8> MCC_LED1_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(MCC_LED1_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(MCC_LED1_A::ON)
    }
}
#[doc = "Field `MCC_LED2` reader - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED2_R = crate::BitReader<MCC_LED2_A>;
#[doc = "MCC LEDs: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_LED2_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<MCC_LED2_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_LED2_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_LED2_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_LED2_A {
        match self.bits {
            false => MCC_LED2_A::OFF,
            true => MCC_LED2_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_LED2_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_LED2_A::ON
    }
}
#[doc = "Field `MCC_LED2` writer - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED2_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG1_SPEC, MCC_LED2_A, O>;
impl<'a, const O: u8> MCC_LED2_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(MCC_LED2_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(MCC_LED2_A::ON)
    }
}
#[doc = "Field `MCC_LED3` reader - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED3_R = crate::BitReader<MCC_LED3_A>;
#[doc = "MCC LEDs: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_LED3_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<MCC_LED3_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_LED3_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_LED3_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_LED3_A {
        match self.bits {
            false => MCC_LED3_A::OFF,
            true => MCC_LED3_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_LED3_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_LED3_A::ON
    }
}
#[doc = "Field `MCC_LED3` writer - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED3_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG1_SPEC, MCC_LED3_A, O>;
impl<'a, const O: u8> MCC_LED3_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(MCC_LED3_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(MCC_LED3_A::ON)
    }
}
#[doc = "Field `MCC_LED4` reader - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED4_R = crate::BitReader<MCC_LED4_A>;
#[doc = "MCC LEDs: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_LED4_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<MCC_LED4_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_LED4_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_LED4_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_LED4_A {
        match self.bits {
            false => MCC_LED4_A::OFF,
            true => MCC_LED4_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_LED4_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_LED4_A::ON
    }
}
#[doc = "Field `MCC_LED4` writer - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED4_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG1_SPEC, MCC_LED4_A, O>;
impl<'a, const O: u8> MCC_LED4_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(MCC_LED4_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(MCC_LED4_A::ON)
    }
}
#[doc = "Field `MCC_LED5` reader - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED5_R = crate::BitReader<MCC_LED5_A>;
#[doc = "MCC LEDs: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_LED5_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<MCC_LED5_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_LED5_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_LED5_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_LED5_A {
        match self.bits {
            false => MCC_LED5_A::OFF,
            true => MCC_LED5_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_LED5_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_LED5_A::ON
    }
}
#[doc = "Field `MCC_LED5` writer - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED5_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG1_SPEC, MCC_LED5_A, O>;
impl<'a, const O: u8> MCC_LED5_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(MCC_LED5_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(MCC_LED5_A::ON)
    }
}
#[doc = "Field `MCC_LED6` reader - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED6_R = crate::BitReader<MCC_LED6_A>;
#[doc = "MCC LEDs: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_LED6_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<MCC_LED6_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_LED6_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_LED6_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_LED6_A {
        match self.bits {
            false => MCC_LED6_A::OFF,
            true => MCC_LED6_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_LED6_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_LED6_A::ON
    }
}
#[doc = "Field `MCC_LED6` writer - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED6_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG1_SPEC, MCC_LED6_A, O>;
impl<'a, const O: u8> MCC_LED6_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(MCC_LED6_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(MCC_LED6_A::ON)
    }
}
#[doc = "Field `MCC_LED7` reader - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED7_R = crate::BitReader<MCC_LED7_A>;
#[doc = "MCC LEDs: 0 = OFF 1 = ON\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MCC_LED7_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<MCC_LED7_A> for bool {
    #[inline(always)]
    fn from(variant: MCC_LED7_A) -> Self {
        variant as u8 != 0
    }
}
impl MCC_LED7_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MCC_LED7_A {
        match self.bits {
            false => MCC_LED7_A::OFF,
            true => MCC_LED7_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == MCC_LED7_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == MCC_LED7_A::ON
    }
}
#[doc = "Field `MCC_LED7` writer - MCC LEDs: 0 = OFF 1 = ON"]
pub type MCC_LED7_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG1_SPEC, MCC_LED7_A, O>;
impl<'a, const O: u8> MCC_LED7_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(MCC_LED7_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(MCC_LED7_A::ON)
    }
}
impl R {
    #[doc = "Bit 0 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led0(&self) -> MCC_LED0_R {
        MCC_LED0_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led1(&self) -> MCC_LED1_R {
        MCC_LED1_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led2(&self) -> MCC_LED2_R {
        MCC_LED2_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led3(&self) -> MCC_LED3_R {
        MCC_LED3_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led4(&self) -> MCC_LED4_R {
        MCC_LED4_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led5(&self) -> MCC_LED5_R {
        MCC_LED5_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led6(&self) -> MCC_LED6_R {
        MCC_LED6_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led7(&self) -> MCC_LED7_R {
        MCC_LED7_R::new(((self.bits >> 7) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led0(&mut self) -> MCC_LED0_W<0> {
        MCC_LED0_W::new(self)
    }
    #[doc = "Bit 1 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led1(&mut self) -> MCC_LED1_W<1> {
        MCC_LED1_W::new(self)
    }
    #[doc = "Bit 2 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led2(&mut self) -> MCC_LED2_W<2> {
        MCC_LED2_W::new(self)
    }
    #[doc = "Bit 3 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led3(&mut self) -> MCC_LED3_W<3> {
        MCC_LED3_W::new(self)
    }
    #[doc = "Bit 4 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led4(&mut self) -> MCC_LED4_W<4> {
        MCC_LED4_W::new(self)
    }
    #[doc = "Bit 5 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led5(&mut self) -> MCC_LED5_W<5> {
        MCC_LED5_W::new(self)
    }
    #[doc = "Bit 6 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led6(&mut self) -> MCC_LED6_W<6> {
        MCC_LED6_W::new(self)
    }
    #[doc = "Bit 7 - MCC LEDs: 0 = OFF 1 = ON"]
    #[inline(always)]
    pub fn mcc_led7(&mut self) -> MCC_LED7_W<7> {
        MCC_LED7_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cfg_reg1](index.html) module"]
pub struct CFG_REG1_SPEC;
impl crate::RegisterSpec for CFG_REG1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cfg_reg1::R](R) reader structure"]
impl crate::Readable for CFG_REG1_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cfg_reg1::W](W) writer structure"]
impl crate::Writable for CFG_REG1_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CFG_REG1 to value 0"]
impl crate::Resettable for CFG_REG1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
