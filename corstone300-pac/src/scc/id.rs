// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ID` reader"]
pub struct R(crate::R<ID_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ID_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ID_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ID_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `APP_REV` reader - Application note IP revision number"]
pub type APP_REV_R = crate::FieldReader<u8, u8>;
#[doc = "Field `PRI_NUM` reader - Primary Part Number: 521 = AN521"]
pub type PRI_NUM_R = crate::FieldReader<u16, u16>;
#[doc = "Field `IP_ARCH` reader - IP Architecture: 0x4 = AHB"]
pub type IP_ARCH_R = crate::FieldReader<u8, u8>;
#[doc = "Field `APP_NOTE_VAR` reader - Application note IP variant number"]
pub type APP_NOTE_VAR_R = crate::FieldReader<u8, u8>;
#[doc = "Field `IMPLEMENTER_ID` reader - Implementer ID: 0x41 = ARM"]
pub type IMPLEMENTER_ID_R = crate::FieldReader<u8, u8>;
impl R {
    #[doc = "Bits 0:3 - Application note IP revision number"]
    #[inline(always)]
    pub fn app_rev(&self) -> APP_REV_R {
        APP_REV_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bits 4:15 - Primary Part Number: 521 = AN521"]
    #[inline(always)]
    pub fn pri_num(&self) -> PRI_NUM_R {
        PRI_NUM_R::new(((self.bits >> 4) & 0x0fff) as u16)
    }
    #[doc = "Bits 16:19 - IP Architecture: 0x4 = AHB"]
    #[inline(always)]
    pub fn ip_arch(&self) -> IP_ARCH_R {
        IP_ARCH_R::new(((self.bits >> 16) & 0x0f) as u8)
    }
    #[doc = "Bits 20:23 - Application note IP variant number"]
    #[inline(always)]
    pub fn app_note_var(&self) -> APP_NOTE_VAR_R {
        APP_NOTE_VAR_R::new(((self.bits >> 20) & 0x0f) as u8)
    }
    #[doc = "Bits 24:31 - Implementer ID: 0x41 = ARM"]
    #[inline(always)]
    pub fn implementer_id(&self) -> IMPLEMENTER_ID_R {
        IMPLEMENTER_ID_R::new(((self.bits >> 24) & 0xff) as u8)
    }
}
#[doc = "\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [id](index.html) module"]
pub struct ID_SPEC;
impl crate::RegisterSpec for ID_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [id::R](R) reader structure"]
impl crate::Readable for ID_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets ID to value 0"]
impl crate::Resettable for ID_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
