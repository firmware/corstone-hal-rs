// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SYS_CFGCTRL` reader"]
pub struct R(crate::R<SYS_CFGCTRL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SYS_CFGCTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SYS_CFGCTRL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SYS_CFGCTRL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `SYS_CFGCTRL` writer"]
pub struct W(crate::W<SYS_CFGCTRL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<SYS_CFGCTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<SYS_CFGCTRL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<SYS_CFGCTRL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `DEVICE` reader - Device (value of 0/1/2 for supported clocks"]
pub type DEVICE_R = crate::FieldReader<u16, u16>;
#[doc = "Field `DEVICE` writer - Device (value of 0/1/2 for supported clocks"]
pub type DEVICE_W<'a, const O: u8> = crate::FieldWriter<'a, u32, SYS_CFGCTRL_SPEC, u16, u16, 12, O>;
#[doc = "Field `RFUNCVAL` reader - Function Value"]
pub type RFUNCVAL_R = crate::FieldReader<u8, u8>;
#[doc = "Field `RFUNCVAL` writer - Function Value"]
pub type RFUNCVAL_W<'a, const O: u8> = crate::FieldWriter<'a, u32, SYS_CFGCTRL_SPEC, u8, u8, 6, O>;
#[doc = "Field `RW_ACCESS` reader - Read/Write Access"]
pub type RW_ACCESS_R = crate::BitReader<bool>;
#[doc = "Field `RW_ACCESS` writer - Read/Write Access"]
pub type RW_ACCESS_W<'a, const O: u8> = crate::BitWriter<'a, u32, SYS_CFGCTRL_SPEC, bool, O>;
#[doc = "Field `START` reader - Start: generates interrupt on write to this bit"]
pub type START_R = crate::BitReader<bool>;
#[doc = "Field `START` writer - Start: generates interrupt on write to this bit"]
pub type START_W<'a, const O: u8> = crate::BitWriter<'a, u32, SYS_CFGCTRL_SPEC, bool, O>;
impl R {
    #[doc = "Bits 0:11 - Device (value of 0/1/2 for supported clocks"]
    #[inline(always)]
    pub fn device(&self) -> DEVICE_R {
        DEVICE_R::new((self.bits & 0x0fff) as u16)
    }
    #[doc = "Bits 20:25 - Function Value"]
    #[inline(always)]
    pub fn rfuncval(&self) -> RFUNCVAL_R {
        RFUNCVAL_R::new(((self.bits >> 20) & 0x3f) as u8)
    }
    #[doc = "Bit 30 - Read/Write Access"]
    #[inline(always)]
    pub fn rw_access(&self) -> RW_ACCESS_R {
        RW_ACCESS_R::new(((self.bits >> 30) & 1) != 0)
    }
    #[doc = "Bit 31 - Start: generates interrupt on write to this bit"]
    #[inline(always)]
    pub fn start(&self) -> START_R {
        START_R::new(((self.bits >> 31) & 1) != 0)
    }
}
impl W {
    #[doc = "Bits 0:11 - Device (value of 0/1/2 for supported clocks"]
    #[inline(always)]
    pub fn device(&mut self) -> DEVICE_W<0> {
        DEVICE_W::new(self)
    }
    #[doc = "Bits 20:25 - Function Value"]
    #[inline(always)]
    pub fn rfuncval(&mut self) -> RFUNCVAL_W<20> {
        RFUNCVAL_W::new(self)
    }
    #[doc = "Bit 30 - Read/Write Access"]
    #[inline(always)]
    pub fn rw_access(&mut self) -> RW_ACCESS_W<30> {
        RW_ACCESS_W::new(self)
    }
    #[doc = "Bit 31 - Start: generates interrupt on write to this bit"]
    #[inline(always)]
    pub fn start(&mut self) -> START_W<31> {
        START_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [sys_cfgctrl](index.html) module"]
pub struct SYS_CFGCTRL_SPEC;
impl crate::RegisterSpec for SYS_CFGCTRL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [sys_cfgctrl::R](R) reader structure"]
impl crate::Readable for SYS_CFGCTRL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [sys_cfgctrl::W](W) writer structure"]
impl crate::Writable for SYS_CFGCTRL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets SYS_CFGCTRL to value 0"]
impl crate::Resettable for SYS_CFGCTRL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
