// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SYS_CFGSTAT` reader"]
pub struct R(crate::R<SYS_CFGSTAT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SYS_CFGSTAT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SYS_CFGSTAT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SYS_CFGSTAT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `SYS_CFGSTAT` writer"]
pub struct W(crate::W<SYS_CFGSTAT_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<SYS_CFGSTAT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<SYS_CFGSTAT_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<SYS_CFGSTAT_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `COMPLETE` reader - Complete Flag"]
pub type COMPLETE_R = crate::BitReader<bool>;
#[doc = "Field `COMPLETE` writer - Complete Flag"]
pub type COMPLETE_W<'a, const O: u8> = crate::BitWriter<'a, u32, SYS_CFGSTAT_SPEC, bool, O>;
#[doc = "Field `ERROR` reader - Error Flag"]
pub type ERROR_R = crate::BitReader<bool>;
#[doc = "Field `ERROR` writer - Error Flag"]
pub type ERROR_W<'a, const O: u8> = crate::BitWriter<'a, u32, SYS_CFGSTAT_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Complete Flag"]
    #[inline(always)]
    pub fn complete(&self) -> COMPLETE_R {
        COMPLETE_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Error Flag"]
    #[inline(always)]
    pub fn error(&self) -> ERROR_R {
        ERROR_R::new(((self.bits >> 1) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Complete Flag"]
    #[inline(always)]
    pub fn complete(&mut self) -> COMPLETE_W<0> {
        COMPLETE_W::new(self)
    }
    #[doc = "Bit 1 - Error Flag"]
    #[inline(always)]
    pub fn error(&mut self) -> ERROR_W<1> {
        ERROR_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [sys_cfgstat](index.html) module"]
pub struct SYS_CFGSTAT_SPEC;
impl crate::RegisterSpec for SYS_CFGSTAT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [sys_cfgstat::R](R) reader structure"]
impl crate::Readable for SYS_CFGSTAT_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [sys_cfgstat::W](W) writer structure"]
impl crate::Writable for SYS_CFGSTAT_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets SYS_CFGSTAT to value 0"]
impl crate::Resettable for SYS_CFGSTAT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
