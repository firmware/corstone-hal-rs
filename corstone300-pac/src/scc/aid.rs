// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `AID` reader"]
pub struct R(crate::R<AID_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<AID_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<AID_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<AID_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `NUM_CFG_REG` reader - Number of SCC configuration register"]
pub type NUM_CFG_REG_R = crate::FieldReader<u8, u8>;
#[doc = "Field `MPS3_REV` reader - V2M-MPS3 target Board Revision (A=0,B=1,C=2)"]
pub type MPS3_REV_R = crate::FieldReader<u8, u8>;
#[doc = "Field `FPGA_BUILD` reader - FPGA Build Number"]
pub type FPGA_BUILD_R = crate::FieldReader<u8, u8>;
impl R {
    #[doc = "Bits 0:7 - Number of SCC configuration register"]
    #[inline(always)]
    pub fn num_cfg_reg(&self) -> NUM_CFG_REG_R {
        NUM_CFG_REG_R::new((self.bits & 0xff) as u8)
    }
    #[doc = "Bits 20:23 - V2M-MPS3 target Board Revision (A=0,B=1,C=2)"]
    #[inline(always)]
    pub fn mps3_rev(&self) -> MPS3_REV_R {
        MPS3_REV_R::new(((self.bits >> 20) & 0x0f) as u8)
    }
    #[doc = "Bits 24:31 - FPGA Build Number"]
    #[inline(always)]
    pub fn fpga_build(&self) -> FPGA_BUILD_R {
        FPGA_BUILD_R::new(((self.bits >> 24) & 0xff) as u8)
    }
}
#[doc = "\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [aid](index.html) module"]
pub struct AID_SPEC;
impl crate::RegisterSpec for AID_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [aid::R](R) reader structure"]
impl crate::Readable for AID_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets AID to value 0"]
impl crate::Resettable for AID_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
