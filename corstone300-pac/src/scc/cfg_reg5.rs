// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CFG_REG5` reader"]
pub struct R(crate::R<CFG_REG5_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CFG_REG5_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CFG_REG5_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CFG_REG5_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CFG_REG5` writer"]
pub struct W(crate::W<CFG_REG5_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CFG_REG5_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CFG_REG5_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CFG_REG5_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `DEBUG` reader - Debug: 0 = Serial Wire Debug 1 = JTAG"]
pub type DEBUG_R = crate::BitReader<bool>;
#[doc = "Field `DEBUG` writer - Debug: 0 = Serial Wire Debug 1 = JTAG"]
pub type DEBUG_W<'a, const O: u8> = crate::BitWriter<'a, u32, CFG_REG5_SPEC, bool, O>;
impl R {
    #[doc = "Bit 5 - Debug: 0 = Serial Wire Debug 1 = JTAG"]
    #[inline(always)]
    pub fn debug(&self) -> DEBUG_R {
        DEBUG_R::new(((self.bits >> 5) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 5 - Debug: 0 = Serial Wire Debug 1 = JTAG"]
    #[inline(always)]
    pub fn debug(&mut self) -> DEBUG_W<5> {
        DEBUG_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cfg_reg5](index.html) module"]
pub struct CFG_REG5_SPEC;
impl crate::RegisterSpec for CFG_REG5_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cfg_reg5::R](R) reader structure"]
impl crate::Readable for CFG_REG5_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cfg_reg5::W](W) writer structure"]
impl crate::Writable for CFG_REG5_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CFG_REG5 to value 0"]
impl crate::Resettable for CFG_REG5_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
