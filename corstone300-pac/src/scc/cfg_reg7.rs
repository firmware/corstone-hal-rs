// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CFG_REG7` reader"]
pub struct R(crate::R<CFG_REG7_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CFG_REG7_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CFG_REG7_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CFG_REG7_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cfg_reg7](index.html) module"]
pub struct CFG_REG7_SPEC;
impl crate::RegisterSpec for CFG_REG7_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cfg_reg7::R](R) reader structure"]
impl crate::Readable for CFG_REG7_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CFG_REG7 to value 0"]
impl crate::Resettable for CFG_REG7_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
