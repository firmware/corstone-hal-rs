// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `DLL` reader"]
pub struct R(crate::R<DLL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<DLL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<DLL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<DLL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `DLL` writer"]
pub struct W(crate::W<DLL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<DLL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<DLL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<DLL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `LOCKED` reader - Complete Flag"]
pub type LOCKED_R = crate::BitReader<bool>;
#[doc = "Field `LOCKED` writer - Complete Flag"]
pub type LOCKED_W<'a, const O: u8> = crate::BitWriter<'a, u32, DLL_SPEC, bool, O>;
#[doc = "Field `LOCK_UNLOCK` reader - Complete Flag"]
pub type LOCK_UNLOCK_R = crate::FieldReader<u8, u8>;
#[doc = "Field `LOCK_UNLOCK` writer - Complete Flag"]
pub type LOCK_UNLOCK_W<'a, const O: u8> = crate::FieldWriter<'a, u32, DLL_SPEC, u8, u8, 8, O>;
#[doc = "Field `LOCKED_MASKED` reader - Error Flag"]
pub type LOCKED_MASKED_R = crate::FieldReader<u8, u8>;
#[doc = "Field `LOCKED_MASKED` writer - Error Flag"]
pub type LOCKED_MASKED_W<'a, const O: u8> = crate::FieldWriter<'a, u32, DLL_SPEC, u8, u8, 8, O>;
impl R {
    #[doc = "Bit 0 - Complete Flag"]
    #[inline(always)]
    pub fn locked(&self) -> LOCKED_R {
        LOCKED_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bits 16:23 - Complete Flag"]
    #[inline(always)]
    pub fn lock_unlock(&self) -> LOCK_UNLOCK_R {
        LOCK_UNLOCK_R::new(((self.bits >> 16) & 0xff) as u8)
    }
    #[doc = "Bits 24:31 - Error Flag"]
    #[inline(always)]
    pub fn locked_masked(&self) -> LOCKED_MASKED_R {
        LOCKED_MASKED_R::new(((self.bits >> 24) & 0xff) as u8)
    }
}
impl W {
    #[doc = "Bit 0 - Complete Flag"]
    #[inline(always)]
    pub fn locked(&mut self) -> LOCKED_W<0> {
        LOCKED_W::new(self)
    }
    #[doc = "Bits 16:23 - Complete Flag"]
    #[inline(always)]
    pub fn lock_unlock(&mut self) -> LOCK_UNLOCK_W<16> {
        LOCK_UNLOCK_W::new(self)
    }
    #[doc = "Bits 24:31 - Error Flag"]
    #[inline(always)]
    pub fn locked_masked(&mut self) -> LOCKED_MASKED_W<24> {
        LOCKED_MASKED_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "DLL Lock Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [dll](index.html) module"]
pub struct DLL_SPEC;
impl crate::RegisterSpec for DLL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [dll::R](R) reader structure"]
impl crate::Readable for DLL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [dll::W](W) writer structure"]
impl crate::Writable for DLL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets DLL to value 0"]
impl crate::Resettable for DLL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
