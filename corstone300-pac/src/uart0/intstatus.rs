// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INTSTATUS` reader"]
pub struct R(crate::R<INTSTATUS_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INTSTATUS_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INTSTATUS_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INTSTATUS_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `TXINT` reader - TX Interrupt"]
pub type TXINT_R = crate::BitReader<bool>;
#[doc = "Field `RXINT` reader - RX Interrupt"]
pub type RXINT_R = crate::BitReader<bool>;
#[doc = "Field `TXOV` reader - TX Overrun Interrupt"]
pub type TXOV_R = crate::BitReader<bool>;
#[doc = "Field `RXOV` reader - RX Overrun Interrupt"]
pub type RXOV_R = crate::BitReader<bool>;
impl R {
    #[doc = "Bit 0 - TX Interrupt"]
    #[inline(always)]
    pub fn txint(&self) -> TXINT_R {
        TXINT_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - RX Interrupt"]
    #[inline(always)]
    pub fn rxint(&self) -> RXINT_R {
        RXINT_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - TX Overrun Interrupt"]
    #[inline(always)]
    pub fn txov(&self) -> TXOV_R {
        TXOV_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - RX Overrun Interrupt"]
    #[inline(always)]
    pub fn rxov(&self) -> RXOV_R {
        RXOV_R::new(((self.bits >> 3) & 1) != 0)
    }
}
#[doc = "UART Interrupt Status Register\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [intstatus](index.html) module"]
pub struct INTSTATUS_SPEC;
impl crate::RegisterSpec for INTSTATUS_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [intstatus::R](R) reader structure"]
impl crate::Readable for INTSTATUS_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets INTSTATUS to value 0"]
impl crate::Resettable for INTSTATUS_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
