// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `STATE` reader"]
pub struct R(crate::R<STATE_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<STATE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<STATE_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<STATE_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `STATE` writer"]
pub struct W(crate::W<STATE_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<STATE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<STATE_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<STATE_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `TXBF` reader - TX Buffer Full"]
pub type TXBF_R = crate::BitReader<bool>;
#[doc = "Field `RXBF` reader - RX Buffer Full"]
pub type RXBF_R = crate::BitReader<bool>;
#[doc = "Field `TXOV` reader - TX Buffer Overun (write 1 to clear)"]
pub type TXOV_R = crate::BitReader<bool>;
#[doc = "Field `TXOV` writer - TX Buffer Overun (write 1 to clear)"]
pub type TXOV_W<'a, const O: u8> = crate::BitWriter1C<'a, u32, STATE_SPEC, bool, O>;
#[doc = "Field `RXOV` reader - RX Buffer Overun (write 1 to clear)"]
pub type RXOV_R = crate::BitReader<bool>;
#[doc = "Field `RXOV` writer - RX Buffer Overun (write 1 to clear)"]
pub type RXOV_W<'a, const O: u8> = crate::BitWriter1C<'a, u32, STATE_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - TX Buffer Full"]
    #[inline(always)]
    pub fn txbf(&self) -> TXBF_R {
        TXBF_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - RX Buffer Full"]
    #[inline(always)]
    pub fn rxbf(&self) -> RXBF_R {
        RXBF_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - TX Buffer Overun (write 1 to clear)"]
    #[inline(always)]
    pub fn txov(&self) -> TXOV_R {
        TXOV_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - RX Buffer Overun (write 1 to clear)"]
    #[inline(always)]
    pub fn rxov(&self) -> RXOV_R {
        RXOV_R::new(((self.bits >> 3) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 2 - TX Buffer Overun (write 1 to clear)"]
    #[inline(always)]
    pub fn txov(&mut self) -> TXOV_W<2> {
        TXOV_W::new(self)
    }
    #[doc = "Bit 3 - RX Buffer Overun (write 1 to clear)"]
    #[inline(always)]
    pub fn rxov(&mut self) -> RXOV_W<3> {
        RXOV_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "UART Status Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [state](index.html) module"]
pub struct STATE_SPEC;
impl crate::RegisterSpec for STATE_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [state::R](R) reader structure"]
impl crate::Readable for STATE_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [state::W](W) writer structure"]
impl crate::Writable for STATE_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets STATE to value 0"]
impl crate::Resettable for STATE_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
