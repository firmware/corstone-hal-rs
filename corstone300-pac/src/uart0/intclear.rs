// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INTCLEAR` writer"]
pub struct W(crate::W<INTCLEAR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<INTCLEAR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<INTCLEAR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<INTCLEAR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `TXINT` writer - TX Interrupt"]
pub type TXINT_W<'a, const O: u8> = crate::BitWriter1C<'a, u32, INTCLEAR_SPEC, bool, O>;
#[doc = "Field `RXINT` writer - RX Interrupt"]
pub type RXINT_W<'a, const O: u8> = crate::BitWriter1C<'a, u32, INTCLEAR_SPEC, bool, O>;
#[doc = "Field `TXOV` writer - TX Overrun Interrupt"]
pub type TXOV_W<'a, const O: u8> = crate::BitWriter1C<'a, u32, INTCLEAR_SPEC, bool, O>;
#[doc = "Field `RXOV` writer - RX Overrun Interrupt"]
pub type RXOV_W<'a, const O: u8> = crate::BitWriter1C<'a, u32, INTCLEAR_SPEC, bool, O>;
impl W {
    #[doc = "Bit 0 - TX Interrupt"]
    #[inline(always)]
    pub fn txint(&mut self) -> TXINT_W<0> {
        TXINT_W::new(self)
    }
    #[doc = "Bit 1 - RX Interrupt"]
    #[inline(always)]
    pub fn rxint(&mut self) -> RXINT_W<1> {
        RXINT_W::new(self)
    }
    #[doc = "Bit 2 - TX Overrun Interrupt"]
    #[inline(always)]
    pub fn txov(&mut self) -> TXOV_W<2> {
        TXOV_W::new(self)
    }
    #[doc = "Bit 3 - RX Overrun Interrupt"]
    #[inline(always)]
    pub fn rxov(&mut self) -> RXOV_W<3> {
        RXOV_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "UART Interrupt CLEAR Register\n\nThis register you can [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [intclear](index.html) module"]
pub struct INTCLEAR_SPEC;
impl crate::RegisterSpec for INTCLEAR_SPEC {
    type Ux = u32;
}
#[doc = "`write(|w| ..)` method takes [intclear::W](W) writer structure"]
impl crate::Writable for INTCLEAR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets INTCLEAR to value 0"]
impl crate::Resettable for INTCLEAR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
