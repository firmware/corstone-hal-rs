// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CTRL` reader"]
pub struct R(crate::R<CTRL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CTRL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CTRL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CTRL` writer"]
pub struct W(crate::W<CTRL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CTRL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CTRL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `TXEN` reader - TX Enable"]
pub type TXEN_R = crate::BitReader<TXEN_A>;
#[doc = "TX Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TXEN_A {
    #[doc = "0: Disabled"]
    DISABLE = 0,
    #[doc = "1: Enabled"]
    ENABLE = 1,
}
impl From<TXEN_A> for bool {
    #[inline(always)]
    fn from(variant: TXEN_A) -> Self {
        variant as u8 != 0
    }
}
impl TXEN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> TXEN_A {
        match self.bits {
            false => TXEN_A::DISABLE,
            true => TXEN_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == TXEN_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == TXEN_A::ENABLE
    }
}
#[doc = "Field `TXEN` writer - TX Enable"]
pub type TXEN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, TXEN_A, O>;
impl<'a, const O: u8> TXEN_W<'a, O> {
    #[doc = "Disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(TXEN_A::DISABLE)
    }
    #[doc = "Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(TXEN_A::ENABLE)
    }
}
#[doc = "Field `RXEN` reader - RX Enable"]
pub type RXEN_R = crate::BitReader<RXEN_A>;
#[doc = "RX Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RXEN_A {
    #[doc = "0: Disabled"]
    DISABLE = 0,
    #[doc = "1: Enabled"]
    ENABLE = 1,
}
impl From<RXEN_A> for bool {
    #[inline(always)]
    fn from(variant: RXEN_A) -> Self {
        variant as u8 != 0
    }
}
impl RXEN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RXEN_A {
        match self.bits {
            false => RXEN_A::DISABLE,
            true => RXEN_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == RXEN_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == RXEN_A::ENABLE
    }
}
#[doc = "Field `RXEN` writer - RX Enable"]
pub type RXEN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, RXEN_A, O>;
impl<'a, const O: u8> RXEN_W<'a, O> {
    #[doc = "Disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(RXEN_A::DISABLE)
    }
    #[doc = "Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(RXEN_A::ENABLE)
    }
}
#[doc = "Field `TXINT` reader - TX Interrupt Enable"]
pub type TXINT_R = crate::BitReader<TXINT_A>;
#[doc = "TX Interrupt Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TXINT_A {
    #[doc = "0: Disabled"]
    DISABLE = 0,
    #[doc = "1: Enabled"]
    ENABLE = 1,
}
impl From<TXINT_A> for bool {
    #[inline(always)]
    fn from(variant: TXINT_A) -> Self {
        variant as u8 != 0
    }
}
impl TXINT_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> TXINT_A {
        match self.bits {
            false => TXINT_A::DISABLE,
            true => TXINT_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == TXINT_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == TXINT_A::ENABLE
    }
}
#[doc = "Field `TXINT` writer - TX Interrupt Enable"]
pub type TXINT_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, TXINT_A, O>;
impl<'a, const O: u8> TXINT_W<'a, O> {
    #[doc = "Disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(TXINT_A::DISABLE)
    }
    #[doc = "Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(TXINT_A::ENABLE)
    }
}
#[doc = "Field `RXINT` reader - RX Interrupt Enable"]
pub type RXINT_R = crate::BitReader<RXINT_A>;
#[doc = "RX Interrupt Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RXINT_A {
    #[doc = "0: Disabled"]
    DISABLE = 0,
    #[doc = "1: Enabled"]
    ENABLE = 1,
}
impl From<RXINT_A> for bool {
    #[inline(always)]
    fn from(variant: RXINT_A) -> Self {
        variant as u8 != 0
    }
}
impl RXINT_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RXINT_A {
        match self.bits {
            false => RXINT_A::DISABLE,
            true => RXINT_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == RXINT_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == RXINT_A::ENABLE
    }
}
#[doc = "Field `RXINT` writer - RX Interrupt Enable"]
pub type RXINT_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, RXINT_A, O>;
impl<'a, const O: u8> RXINT_W<'a, O> {
    #[doc = "Disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(RXINT_A::DISABLE)
    }
    #[doc = "Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(RXINT_A::ENABLE)
    }
}
#[doc = "Field `TXOVINT` reader - TX Overrun Interrupt Enable"]
pub type TXOVINT_R = crate::BitReader<TXOVINT_A>;
#[doc = "TX Overrun Interrupt Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TXOVINT_A {
    #[doc = "0: Disabled"]
    DISABLE = 0,
    #[doc = "1: Enabled"]
    ENABLE = 1,
}
impl From<TXOVINT_A> for bool {
    #[inline(always)]
    fn from(variant: TXOVINT_A) -> Self {
        variant as u8 != 0
    }
}
impl TXOVINT_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> TXOVINT_A {
        match self.bits {
            false => TXOVINT_A::DISABLE,
            true => TXOVINT_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == TXOVINT_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == TXOVINT_A::ENABLE
    }
}
#[doc = "Field `TXOVINT` writer - TX Overrun Interrupt Enable"]
pub type TXOVINT_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, TXOVINT_A, O>;
impl<'a, const O: u8> TXOVINT_W<'a, O> {
    #[doc = "Disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(TXOVINT_A::DISABLE)
    }
    #[doc = "Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(TXOVINT_A::ENABLE)
    }
}
#[doc = "Field `RVOVINT` reader - RX Overrun Interrupt Enable"]
pub type RVOVINT_R = crate::BitReader<RVOVINT_A>;
#[doc = "RX Overrun Interrupt Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RVOVINT_A {
    #[doc = "0: Disabled"]
    DISABLE = 0,
    #[doc = "1: Enabled"]
    ENABLE = 1,
}
impl From<RVOVINT_A> for bool {
    #[inline(always)]
    fn from(variant: RVOVINT_A) -> Self {
        variant as u8 != 0
    }
}
impl RVOVINT_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RVOVINT_A {
        match self.bits {
            false => RVOVINT_A::DISABLE,
            true => RVOVINT_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == RVOVINT_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == RVOVINT_A::ENABLE
    }
}
#[doc = "Field `RVOVINT` writer - RX Overrun Interrupt Enable"]
pub type RVOVINT_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, RVOVINT_A, O>;
impl<'a, const O: u8> RVOVINT_W<'a, O> {
    #[doc = "Disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(RVOVINT_A::DISABLE)
    }
    #[doc = "Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(RVOVINT_A::ENABLE)
    }
}
#[doc = "Field `HSTX` reader - High Speed Test Mode for TX only"]
pub type HSTX_R = crate::BitReader<HSTX_A>;
#[doc = "High Speed Test Mode for TX only\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum HSTX_A {
    #[doc = "0: Disabled"]
    DISABLE = 0,
    #[doc = "1: Enabled"]
    ENABLE = 1,
}
impl From<HSTX_A> for bool {
    #[inline(always)]
    fn from(variant: HSTX_A) -> Self {
        variant as u8 != 0
    }
}
impl HSTX_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> HSTX_A {
        match self.bits {
            false => HSTX_A::DISABLE,
            true => HSTX_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == HSTX_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == HSTX_A::ENABLE
    }
}
#[doc = "Field `HSTX` writer - High Speed Test Mode for TX only"]
pub type HSTX_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, HSTX_A, O>;
impl<'a, const O: u8> HSTX_W<'a, O> {
    #[doc = "Disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(HSTX_A::DISABLE)
    }
    #[doc = "Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(HSTX_A::ENABLE)
    }
}
impl R {
    #[doc = "Bit 0 - TX Enable"]
    #[inline(always)]
    pub fn txen(&self) -> TXEN_R {
        TXEN_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - RX Enable"]
    #[inline(always)]
    pub fn rxen(&self) -> RXEN_R {
        RXEN_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - TX Interrupt Enable"]
    #[inline(always)]
    pub fn txint(&self) -> TXINT_R {
        TXINT_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - RX Interrupt Enable"]
    #[inline(always)]
    pub fn rxint(&self) -> RXINT_R {
        RXINT_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - TX Overrun Interrupt Enable"]
    #[inline(always)]
    pub fn txovint(&self) -> TXOVINT_R {
        TXOVINT_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5 - RX Overrun Interrupt Enable"]
    #[inline(always)]
    pub fn rvovint(&self) -> RVOVINT_R {
        RVOVINT_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6 - High Speed Test Mode for TX only"]
    #[inline(always)]
    pub fn hstx(&self) -> HSTX_R {
        HSTX_R::new(((self.bits >> 6) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - TX Enable"]
    #[inline(always)]
    pub fn txen(&mut self) -> TXEN_W<0> {
        TXEN_W::new(self)
    }
    #[doc = "Bit 1 - RX Enable"]
    #[inline(always)]
    pub fn rxen(&mut self) -> RXEN_W<1> {
        RXEN_W::new(self)
    }
    #[doc = "Bit 2 - TX Interrupt Enable"]
    #[inline(always)]
    pub fn txint(&mut self) -> TXINT_W<2> {
        TXINT_W::new(self)
    }
    #[doc = "Bit 3 - RX Interrupt Enable"]
    #[inline(always)]
    pub fn rxint(&mut self) -> RXINT_W<3> {
        RXINT_W::new(self)
    }
    #[doc = "Bit 4 - TX Overrun Interrupt Enable"]
    #[inline(always)]
    pub fn txovint(&mut self) -> TXOVINT_W<4> {
        TXOVINT_W::new(self)
    }
    #[doc = "Bit 5 - RX Overrun Interrupt Enable"]
    #[inline(always)]
    pub fn rvovint(&mut self) -> RVOVINT_W<5> {
        RVOVINT_W::new(self)
    }
    #[doc = "Bit 6 - High Speed Test Mode for TX only"]
    #[inline(always)]
    pub fn hstx(&mut self) -> HSTX_W<6> {
        HSTX_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "UART Control Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [ctrl](index.html) module"]
pub struct CTRL_SPEC;
impl crate::RegisterSpec for CTRL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [ctrl::R](R) reader structure"]
impl crate::Readable for CTRL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [ctrl::W](W) writer structure"]
impl crate::Writable for CTRL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CTRL to value 0"]
impl crate::Resettable for CTRL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
