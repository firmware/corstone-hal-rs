// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTCIDR3` reader"]
pub struct R(crate::R<CNTCIDR3_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTCIDR3_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTCIDR3_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTCIDR3_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Component Identification Register 3\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntcidr3](index.html) module"]
pub struct CNTCIDR3_SPEC;
impl crate::RegisterSpec for CNTCIDR3_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntcidr3::R](R) reader structure"]
impl crate::Readable for CNTCIDR3_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTCIDR3 to value 0xb1"]
impl crate::Resettable for CNTCIDR3_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0xb1
    }
}
