// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTID` reader"]
pub struct R(crate::R<CNTID_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTID_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTID_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTID_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `CNTSC` reader - Indicates whether Counter Scaling is implemented"]
pub type CNTSC_R = crate::FieldReader<u8, CNTSC_A>;
#[doc = "Indicates whether Counter Scaling is implemented\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum CNTSC_A {
    #[doc = "0: Counter Scaling is not implemented"]
    CNTSCNOTIMPLEMENTED = 0,
    #[doc = "1: Counter Scaling is implemented"]
    CNTSCIMPLEMENTED = 1,
}
impl From<CNTSC_A> for u8 {
    #[inline(always)]
    fn from(variant: CNTSC_A) -> Self {
        variant as _
    }
}
impl CNTSC_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<CNTSC_A> {
        match self.bits {
            0 => Some(CNTSC_A::CNTSCNOTIMPLEMENTED),
            1 => Some(CNTSC_A::CNTSCIMPLEMENTED),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `CNTSCNOTIMPLEMENTED`"]
    #[inline(always)]
    pub fn is_cntscnotimplemented(&self) -> bool {
        *self == CNTSC_A::CNTSCNOTIMPLEMENTED
    }
    #[doc = "Checks if the value of the field is `CNTSCIMPLEMENTED`"]
    #[inline(always)]
    pub fn is_cntscimplemented(&self) -> bool {
        *self == CNTSC_A::CNTSCIMPLEMENTED
    }
}
#[doc = "Field `CNTCS` reader - Indicates whether Clock Switching is implemented"]
pub type CNTCS_R = crate::BitReader<CNTCS_A>;
#[doc = "Indicates whether Clock Switching is implemented\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CNTCS_A {
    #[doc = "0: HW-based Counter Clock Switching is not implemented"]
    CNTCSNOTIMPLEMENTED = 0,
    #[doc = "1: HW-based Counter Clock Switching is implemented"]
    CNTCSIMPLEMENTED = 1,
}
impl From<CNTCS_A> for bool {
    #[inline(always)]
    fn from(variant: CNTCS_A) -> Self {
        variant as u8 != 0
    }
}
impl CNTCS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CNTCS_A {
        match self.bits {
            false => CNTCS_A::CNTCSNOTIMPLEMENTED,
            true => CNTCS_A::CNTCSIMPLEMENTED,
        }
    }
    #[doc = "Checks if the value of the field is `CNTCSNOTIMPLEMENTED`"]
    #[inline(always)]
    pub fn is_cntcsnotimplemented(&self) -> bool {
        *self == CNTCS_A::CNTCSNOTIMPLEMENTED
    }
    #[doc = "Checks if the value of the field is `CNTCSIMPLEMENTED`"]
    #[inline(always)]
    pub fn is_cntcsimplemented(&self) -> bool {
        *self == CNTCS_A::CNTCSIMPLEMENTED
    }
}
#[doc = "Field `CNTSELCLK` reader - Indicates the clock source that the Counter is using"]
pub type CNTSELCLK_R = crate::FieldReader<u8, CNTSELCLK_A>;
#[doc = "Indicates the clock source that the Counter is using\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum CNTSELCLK_A {
    #[doc = "0: Invalid status, Counter not incrementing"]
    INVALID0 = 0,
    #[doc = "1: CLK0 (REFCLK)"]
    CLK0 = 1,
    #[doc = "2: CLK1 (FASTCLK)"]
    CLK1 = 2,
    #[doc = "3: Invalid status, Counter not incrementing"]
    INVALID1 = 3,
}
impl From<CNTSELCLK_A> for u8 {
    #[inline(always)]
    fn from(variant: CNTSELCLK_A) -> Self {
        variant as _
    }
}
impl CNTSELCLK_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CNTSELCLK_A {
        match self.bits {
            0 => CNTSELCLK_A::INVALID0,
            1 => CNTSELCLK_A::CLK0,
            2 => CNTSELCLK_A::CLK1,
            3 => CNTSELCLK_A::INVALID1,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `INVALID0`"]
    #[inline(always)]
    pub fn is_invalid0(&self) -> bool {
        *self == CNTSELCLK_A::INVALID0
    }
    #[doc = "Checks if the value of the field is `CLK0`"]
    #[inline(always)]
    pub fn is_clk0(&self) -> bool {
        *self == CNTSELCLK_A::CLK0
    }
    #[doc = "Checks if the value of the field is `CLK1`"]
    #[inline(always)]
    pub fn is_clk1(&self) -> bool {
        *self == CNTSELCLK_A::CLK1
    }
    #[doc = "Checks if the value of the field is `INVALID1`"]
    #[inline(always)]
    pub fn is_invalid1(&self) -> bool {
        *self == CNTSELCLK_A::INVALID1
    }
}
#[doc = "Field `CNTSCR_OVR` reader - Override counter enable condition for writing to CNTSCR* registers"]
pub type CNTSCR_OVR_R = crate::BitReader<CNTSCR_OVR_A>;
#[doc = "Override counter enable condition for writing to CNTSCR* registers\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CNTSCR_OVR_A {
    #[doc = "0: CNTSCR* can be written only when CNTCR.EN=0"]
    COND1 = 0,
    #[doc = "1: CNTSCR* can be written when CNTCR.EN=0 or 1"]
    COND2 = 1,
}
impl From<CNTSCR_OVR_A> for bool {
    #[inline(always)]
    fn from(variant: CNTSCR_OVR_A) -> Self {
        variant as u8 != 0
    }
}
impl CNTSCR_OVR_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CNTSCR_OVR_A {
        match self.bits {
            false => CNTSCR_OVR_A::COND1,
            true => CNTSCR_OVR_A::COND2,
        }
    }
    #[doc = "Checks if the value of the field is `COND1`"]
    #[inline(always)]
    pub fn is_cond1(&self) -> bool {
        *self == CNTSCR_OVR_A::COND1
    }
    #[doc = "Checks if the value of the field is `COND2`"]
    #[inline(always)]
    pub fn is_cond2(&self) -> bool {
        *self == CNTSCR_OVR_A::COND2
    }
}
impl R {
    #[doc = "Bits 0:3 - Indicates whether Counter Scaling is implemented"]
    #[inline(always)]
    pub fn cntsc(&self) -> CNTSC_R {
        CNTSC_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bit 16 - Indicates whether Clock Switching is implemented"]
    #[inline(always)]
    pub fn cntcs(&self) -> CNTCS_R {
        CNTCS_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bits 17:18 - Indicates the clock source that the Counter is using"]
    #[inline(always)]
    pub fn cntselclk(&self) -> CNTSELCLK_R {
        CNTSELCLK_R::new(((self.bits >> 17) & 3) as u8)
    }
    #[doc = "Bit 19 - Override counter enable condition for writing to CNTSCR* registers"]
    #[inline(always)]
    pub fn cntscr_ovr(&self) -> CNTSCR_OVR_R {
        CNTSCR_OVR_R::new(((self.bits >> 19) & 1) != 0)
    }
}
#[doc = "Indicates additional information about Counter Scaling implementation\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntid](index.html) module"]
pub struct CNTID_SPEC;
impl crate::RegisterSpec for CNTID_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntid::R](R) reader structure"]
impl crate::Readable for CNTID_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTID to value 0"]
impl crate::Resettable for CNTID_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
