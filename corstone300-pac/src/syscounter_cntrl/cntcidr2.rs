// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTCIDR2` reader"]
pub struct R(crate::R<CNTCIDR2_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTCIDR2_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTCIDR2_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTCIDR2_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Component Identification Register 2\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntcidr2](index.html) module"]
pub struct CNTCIDR2_SPEC;
impl crate::RegisterSpec for CNTCIDR2_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntcidr2::R](R) reader structure"]
impl crate::Readable for CNTCIDR2_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTCIDR2 to value 0x05"]
impl crate::Resettable for CNTCIDR2_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x05
    }
}
