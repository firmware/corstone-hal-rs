// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTSR` reader"]
pub struct R(crate::R<CNTSR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTSR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTSR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTSR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `DBGH` reader - Indicates whether the counter is halted because the Halt-on-Debug signal is asserted"]
pub type DBGH_R = crate::FieldReader<u8, DBGH_A>;
#[doc = "Indicates whether the counter is halted because the Halt-on-Debug signal is asserted\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum DBGH_A {
    #[doc = "0: Counter is not halted"]
    NOHALT = 0,
    #[doc = "1: Counter is halted"]
    HALT = 1,
}
impl From<DBGH_A> for u8 {
    #[inline(always)]
    fn from(variant: DBGH_A) -> Self {
        variant as _
    }
}
impl DBGH_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<DBGH_A> {
        match self.bits {
            0 => Some(DBGH_A::NOHALT),
            1 => Some(DBGH_A::HALT),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `NOHALT`"]
    #[inline(always)]
    pub fn is_nohalt(&self) -> bool {
        *self == DBGH_A::NOHALT
    }
    #[doc = "Checks if the value of the field is `HALT`"]
    #[inline(always)]
    pub fn is_halt(&self) -> bool {
        *self == DBGH_A::HALT
    }
}
impl R {
    #[doc = "Bits 1:4 - Indicates whether the counter is halted because the Halt-on-Debug signal is asserted"]
    #[inline(always)]
    pub fn dbgh(&self) -> DBGH_R {
        DBGH_R::new(((self.bits >> 1) & 0x0f) as u8)
    }
}
#[doc = "Counter frequency status information\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntsr](index.html) module"]
pub struct CNTSR_SPEC;
impl crate::RegisterSpec for CNTSR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntsr::R](R) reader structure"]
impl crate::Readable for CNTSR_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTSR to value 0"]
impl crate::Resettable for CNTSR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
