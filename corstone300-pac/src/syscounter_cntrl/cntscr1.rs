// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTSCR1` reader"]
pub struct R(crate::R<CNTSCR1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTSCR1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTSCR1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTSCR1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CNTSCR1` writer"]
pub struct W(crate::W<CNTSCR1_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CNTSCR1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CNTSCR1_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CNTSCR1_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `ScaleVal` reader - The amount added to the Counter Count Value for every period of the counter as determined by 1/Frequency from the current operating frequency of the system counter, the counter tick"]
pub type SCALE_VAL_R = crate::FieldReader<u32, u32>;
#[doc = "Field `ScaleVal` writer - The amount added to the Counter Count Value for every period of the counter as determined by 1/Frequency from the current operating frequency of the system counter, the counter tick"]
pub type SCALE_VAL_W<'a, const O: u8> = crate::FieldWriter<'a, u32, CNTSCR1_SPEC, u32, u32, 32, O>;
impl R {
    #[doc = "Bits 0:31 - The amount added to the Counter Count Value for every period of the counter as determined by 1/Frequency from the current operating frequency of the system counter, the counter tick"]
    #[inline(always)]
    pub fn scale_val(&self) -> SCALE_VAL_R {
        SCALE_VAL_R::new(self.bits)
    }
}
impl W {
    #[doc = "Bits 0:31 - The amount added to the Counter Count Value for every period of the counter as determined by 1/Frequency from the current operating frequency of the system counter, the counter tick"]
    #[inline(always)]
    pub fn scale_val(&mut self) -> SCALE_VAL_W<0> {
        SCALE_VAL_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Counter Scaling register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntscr1](index.html) module"]
pub struct CNTSCR1_SPEC;
impl crate::RegisterSpec for CNTSCR1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntscr1::R](R) reader structure"]
impl crate::Readable for CNTSCR1_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cntscr1::W](W) writer structure"]
impl crate::Writable for CNTSCR1_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CNTSCR1 to value 0x0100_0000"]
impl crate::Resettable for CNTSCR1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x0100_0000
    }
}
