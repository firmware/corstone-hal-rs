// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTCR` reader"]
pub struct R(crate::R<CNTCR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTCR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTCR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTCR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CNTCR` writer"]
pub struct W(crate::W<CNTCR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CNTCR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CNTCR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CNTCR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `EN` reader - Enable Counter"]
pub type EN_R = crate::BitReader<EN_A>;
#[doc = "Enable Counter\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum EN_A {
    #[doc = "0: Count is not incrementing"]
    DISABLED = 0,
    #[doc = "1: Count is incrementing"]
    ENABLED = 1,
}
impl From<EN_A> for bool {
    #[inline(always)]
    fn from(variant: EN_A) -> Self {
        variant as u8 != 0
    }
}
impl EN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> EN_A {
        match self.bits {
            false => EN_A::DISABLED,
            true => EN_A::ENABLED,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == EN_A::DISABLED
    }
    #[doc = "Checks if the value of the field is `ENABLED`"]
    #[inline(always)]
    pub fn is_enabled(&self) -> bool {
        *self == EN_A::ENABLED
    }
}
#[doc = "Field `EN` writer - Enable Counter"]
pub type EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTCR_SPEC, EN_A, O>;
impl<'a, const O: u8> EN_W<'a, O> {
    #[doc = "Count is not incrementing"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(EN_A::DISABLED)
    }
    #[doc = "Count is incrementing"]
    #[inline(always)]
    pub fn enabled(self) -> &'a mut W {
        self.variant(EN_A::ENABLED)
    }
}
#[doc = "Field `HDBG` reader - Halt on debug"]
pub type HDBG_R = crate::BitReader<HDBG_A>;
#[doc = "Halt on debug\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum HDBG_A {
    #[doc = "0: HALTREQ signal into the Counter has no effect"]
    NOHALT = 0,
    #[doc = "1: HALTREQ signal into the Counter halts the Count"]
    HALT = 1,
}
impl From<HDBG_A> for bool {
    #[inline(always)]
    fn from(variant: HDBG_A) -> Self {
        variant as u8 != 0
    }
}
impl HDBG_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> HDBG_A {
        match self.bits {
            false => HDBG_A::NOHALT,
            true => HDBG_A::HALT,
        }
    }
    #[doc = "Checks if the value of the field is `NOHALT`"]
    #[inline(always)]
    pub fn is_nohalt(&self) -> bool {
        *self == HDBG_A::NOHALT
    }
    #[doc = "Checks if the value of the field is `HALT`"]
    #[inline(always)]
    pub fn is_halt(&self) -> bool {
        *self == HDBG_A::HALT
    }
}
#[doc = "Field `HDBG` writer - Halt on debug"]
pub type HDBG_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTCR_SPEC, HDBG_A, O>;
impl<'a, const O: u8> HDBG_W<'a, O> {
    #[doc = "HALTREQ signal into the Counter has no effect"]
    #[inline(always)]
    pub fn nohalt(self) -> &'a mut W {
        self.variant(HDBG_A::NOHALT)
    }
    #[doc = "HALTREQ signal into the Counter halts the Count"]
    #[inline(always)]
    pub fn halt(self) -> &'a mut W {
        self.variant(HDBG_A::HALT)
    }
}
#[doc = "Field `SCEN` reader - Scale enable"]
pub type SCEN_R = crate::BitReader<SCEN_A>;
#[doc = "Scale enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SCEN_A {
    #[doc = "0: Scaling is not enabled"]
    SCALE_DISABLED = 0,
    #[doc = "1: Scaling is enabled"]
    SCALE_ENABLED = 1,
}
impl From<SCEN_A> for bool {
    #[inline(always)]
    fn from(variant: SCEN_A) -> Self {
        variant as u8 != 0
    }
}
impl SCEN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SCEN_A {
        match self.bits {
            false => SCEN_A::SCALE_DISABLED,
            true => SCEN_A::SCALE_ENABLED,
        }
    }
    #[doc = "Checks if the value of the field is `SCALE_DISABLED`"]
    #[inline(always)]
    pub fn is_scale_disabled(&self) -> bool {
        *self == SCEN_A::SCALE_DISABLED
    }
    #[doc = "Checks if the value of the field is `SCALE_ENABLED`"]
    #[inline(always)]
    pub fn is_scale_enabled(&self) -> bool {
        *self == SCEN_A::SCALE_ENABLED
    }
}
#[doc = "Field `SCEN` writer - Scale enable"]
pub type SCEN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTCR_SPEC, SCEN_A, O>;
impl<'a, const O: u8> SCEN_W<'a, O> {
    #[doc = "Scaling is not enabled"]
    #[inline(always)]
    pub fn scale_disabled(self) -> &'a mut W {
        self.variant(SCEN_A::SCALE_DISABLED)
    }
    #[doc = "Scaling is enabled"]
    #[inline(always)]
    pub fn scale_enabled(self) -> &'a mut W {
        self.variant(SCEN_A::SCALE_ENABLED)
    }
}
#[doc = "Field `INTRMASK` reader - Interrupt mask"]
pub type INTRMASK_R = crate::BitReader<INTRMASK_A>;
#[doc = "Interrupt mask\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum INTRMASK_A {
    #[doc = "0: Interrupt output disabled"]
    INTERRUPT_DISABLED = 0,
    #[doc = "1: Interrupt output enabled"]
    INTERRUPT_ENABLED = 1,
}
impl From<INTRMASK_A> for bool {
    #[inline(always)]
    fn from(variant: INTRMASK_A) -> Self {
        variant as u8 != 0
    }
}
impl INTRMASK_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> INTRMASK_A {
        match self.bits {
            false => INTRMASK_A::INTERRUPT_DISABLED,
            true => INTRMASK_A::INTERRUPT_ENABLED,
        }
    }
    #[doc = "Checks if the value of the field is `INTERRUPT_DISABLED`"]
    #[inline(always)]
    pub fn is_interrupt_disabled(&self) -> bool {
        *self == INTRMASK_A::INTERRUPT_DISABLED
    }
    #[doc = "Checks if the value of the field is `INTERRUPT_ENABLED`"]
    #[inline(always)]
    pub fn is_interrupt_enabled(&self) -> bool {
        *self == INTRMASK_A::INTERRUPT_ENABLED
    }
}
#[doc = "Field `INTRMASK` writer - Interrupt mask"]
pub type INTRMASK_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTCR_SPEC, INTRMASK_A, O>;
impl<'a, const O: u8> INTRMASK_W<'a, O> {
    #[doc = "Interrupt output disabled"]
    #[inline(always)]
    pub fn interrupt_disabled(self) -> &'a mut W {
        self.variant(INTRMASK_A::INTERRUPT_DISABLED)
    }
    #[doc = "Interrupt output enabled"]
    #[inline(always)]
    pub fn interrupt_enabled(self) -> &'a mut W {
        self.variant(INTRMASK_A::INTERRUPT_ENABLED)
    }
}
#[doc = "Field `PSLVERRDIS` reader - PSLVERR output disable"]
pub type PSLVERRDIS_R = crate::BitReader<PSLVERRDIS_A>;
#[doc = "PSLVERR output disable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum PSLVERRDIS_A {
    #[doc = "0: PSLVERR is permanently driven to 0"]
    PSLVERRDISABLED = 0,
    #[doc = "1: PSLVERR output that the System Counter generates dynamically"]
    PSLVERRENABLED = 1,
}
impl From<PSLVERRDIS_A> for bool {
    #[inline(always)]
    fn from(variant: PSLVERRDIS_A) -> Self {
        variant as u8 != 0
    }
}
impl PSLVERRDIS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> PSLVERRDIS_A {
        match self.bits {
            false => PSLVERRDIS_A::PSLVERRDISABLED,
            true => PSLVERRDIS_A::PSLVERRENABLED,
        }
    }
    #[doc = "Checks if the value of the field is `PSLVERRDISABLED`"]
    #[inline(always)]
    pub fn is_pslverrdisabled(&self) -> bool {
        *self == PSLVERRDIS_A::PSLVERRDISABLED
    }
    #[doc = "Checks if the value of the field is `PSLVERRENABLED`"]
    #[inline(always)]
    pub fn is_pslverrenabled(&self) -> bool {
        *self == PSLVERRDIS_A::PSLVERRENABLED
    }
}
#[doc = "Field `PSLVERRDIS` writer - PSLVERR output disable"]
pub type PSLVERRDIS_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTCR_SPEC, PSLVERRDIS_A, O>;
impl<'a, const O: u8> PSLVERRDIS_W<'a, O> {
    #[doc = "PSLVERR is permanently driven to 0"]
    #[inline(always)]
    pub fn pslverrdisabled(self) -> &'a mut W {
        self.variant(PSLVERRDIS_A::PSLVERRDISABLED)
    }
    #[doc = "PSLVERR output that the System Counter generates dynamically"]
    #[inline(always)]
    pub fn pslverrenabled(self) -> &'a mut W {
        self.variant(PSLVERRDIS_A::PSLVERRENABLED)
    }
}
#[doc = "Field `INTRCLR` reader - Interrupt clear bit, only writes of 0 are permitted, and writes of 1 are ignored."]
pub type INTRCLR_R = crate::BitReader<bool>;
#[doc = "Field `INTRCLR` writer - Interrupt clear bit, only writes of 0 are permitted, and writes of 1 are ignored."]
pub type INTRCLR_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTCR_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Enable Counter"]
    #[inline(always)]
    pub fn en(&self) -> EN_R {
        EN_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Halt on debug"]
    #[inline(always)]
    pub fn hdbg(&self) -> HDBG_R {
        HDBG_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Scale enable"]
    #[inline(always)]
    pub fn scen(&self) -> SCEN_R {
        SCEN_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Interrupt mask"]
    #[inline(always)]
    pub fn intrmask(&self) -> INTRMASK_R {
        INTRMASK_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - PSLVERR output disable"]
    #[inline(always)]
    pub fn pslverrdis(&self) -> PSLVERRDIS_R {
        PSLVERRDIS_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5 - Interrupt clear bit, only writes of 0 are permitted, and writes of 1 are ignored."]
    #[inline(always)]
    pub fn intrclr(&self) -> INTRCLR_R {
        INTRCLR_R::new(((self.bits >> 5) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Enable Counter"]
    #[inline(always)]
    pub fn en(&mut self) -> EN_W<0> {
        EN_W::new(self)
    }
    #[doc = "Bit 1 - Halt on debug"]
    #[inline(always)]
    pub fn hdbg(&mut self) -> HDBG_W<1> {
        HDBG_W::new(self)
    }
    #[doc = "Bit 2 - Scale enable"]
    #[inline(always)]
    pub fn scen(&mut self) -> SCEN_W<2> {
        SCEN_W::new(self)
    }
    #[doc = "Bit 3 - Interrupt mask"]
    #[inline(always)]
    pub fn intrmask(&mut self) -> INTRMASK_W<3> {
        INTRMASK_W::new(self)
    }
    #[doc = "Bit 4 - PSLVERR output disable"]
    #[inline(always)]
    pub fn pslverrdis(&mut self) -> PSLVERRDIS_W<4> {
        PSLVERRDIS_W::new(self)
    }
    #[doc = "Bit 5 - Interrupt clear bit, only writes of 0 are permitted, and writes of 1 are ignored."]
    #[inline(always)]
    pub fn intrclr(&mut self) -> INTRCLR_W<5> {
        INTRCLR_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Counter Control Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntcr](index.html) module"]
pub struct CNTCR_SPEC;
impl crate::RegisterSpec for CNTCR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntcr::R](R) reader structure"]
impl crate::Readable for CNTCR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cntcr::W](W) writer structure"]
impl crate::Writable for CNTCR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CNTCR to value 0"]
impl crate::Resettable for CNTCR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
