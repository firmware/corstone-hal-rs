// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTPIDR3` reader"]
pub struct R(crate::R<CNTPIDR3_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTPIDR3_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTPIDR3_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTPIDR3_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Peripheral Identification Register 3\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntpidr3](index.html) module"]
pub struct CNTPIDR3_SPEC;
impl crate::RegisterSpec for CNTPIDR3_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntpidr3::R](R) reader structure"]
impl crate::Readable for CNTPIDR3_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTPIDR3 to value 0"]
impl crate::Resettable for CNTPIDR3_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
