// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `IMSC` reader"]
pub struct R(crate::R<IMSC_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<IMSC_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<IMSC_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<IMSC_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `IMSC` writer"]
pub struct W(crate::W<IMSC_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<IMSC_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<IMSC_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<IMSC_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `RORIM` reader - Receive overrun interrupt mask"]
pub type RORIM_R = crate::BitReader<RORIM_A>;
#[doc = "Receive overrun interrupt mask\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RORIM_A {
    #[doc = "0: Receive FIFO written to while full condition interrupt is masked"]
    MASKED = 0,
    #[doc = "1: Receive FIFO written to while full condition interrupt is not masked"]
    NOT_MASKED = 1,
}
impl From<RORIM_A> for bool {
    #[inline(always)]
    fn from(variant: RORIM_A) -> Self {
        variant as u8 != 0
    }
}
impl RORIM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RORIM_A {
        match self.bits {
            false => RORIM_A::MASKED,
            true => RORIM_A::NOT_MASKED,
        }
    }
    #[doc = "Checks if the value of the field is `MASKED`"]
    #[inline(always)]
    pub fn is_masked(&self) -> bool {
        *self == RORIM_A::MASKED
    }
    #[doc = "Checks if the value of the field is `NOT_MASKED`"]
    #[inline(always)]
    pub fn is_not_masked(&self) -> bool {
        *self == RORIM_A::NOT_MASKED
    }
}
#[doc = "Field `RORIM` writer - Receive overrun interrupt mask"]
pub type RORIM_W<'a, const O: u8> = crate::BitWriter<'a, u32, IMSC_SPEC, RORIM_A, O>;
impl<'a, const O: u8> RORIM_W<'a, O> {
    #[doc = "Receive FIFO written to while full condition interrupt is masked"]
    #[inline(always)]
    pub fn masked(self) -> &'a mut W {
        self.variant(RORIM_A::MASKED)
    }
    #[doc = "Receive FIFO written to while full condition interrupt is not masked"]
    #[inline(always)]
    pub fn not_masked(self) -> &'a mut W {
        self.variant(RORIM_A::NOT_MASKED)
    }
}
#[doc = "Field `RTIM` reader - Receive timeout interrupt mask"]
pub type RTIM_R = crate::BitReader<RTIM_A>;
#[doc = "Receive timeout interrupt mask\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RTIM_A {
    #[doc = "0: Receive FIFO not empty or no read prior to timeout period interrupt is masked"]
    MASKED = 0,
    #[doc = "1: Receive FIFO not empty or no read prior to timeout period interrupt is not masked"]
    NOT_MASKED = 1,
}
impl From<RTIM_A> for bool {
    #[inline(always)]
    fn from(variant: RTIM_A) -> Self {
        variant as u8 != 0
    }
}
impl RTIM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RTIM_A {
        match self.bits {
            false => RTIM_A::MASKED,
            true => RTIM_A::NOT_MASKED,
        }
    }
    #[doc = "Checks if the value of the field is `MASKED`"]
    #[inline(always)]
    pub fn is_masked(&self) -> bool {
        *self == RTIM_A::MASKED
    }
    #[doc = "Checks if the value of the field is `NOT_MASKED`"]
    #[inline(always)]
    pub fn is_not_masked(&self) -> bool {
        *self == RTIM_A::NOT_MASKED
    }
}
#[doc = "Field `RTIM` writer - Receive timeout interrupt mask"]
pub type RTIM_W<'a, const O: u8> = crate::BitWriter<'a, u32, IMSC_SPEC, RTIM_A, O>;
impl<'a, const O: u8> RTIM_W<'a, O> {
    #[doc = "Receive FIFO not empty or no read prior to timeout period interrupt is masked"]
    #[inline(always)]
    pub fn masked(self) -> &'a mut W {
        self.variant(RTIM_A::MASKED)
    }
    #[doc = "Receive FIFO not empty or no read prior to timeout period interrupt is not masked"]
    #[inline(always)]
    pub fn not_masked(self) -> &'a mut W {
        self.variant(RTIM_A::NOT_MASKED)
    }
}
#[doc = "Field `RXIM` reader - Receive FIFO interrupt mask"]
pub type RXIM_R = crate::BitReader<RXIM_A>;
#[doc = "Receive FIFO interrupt mask\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RXIM_A {
    #[doc = "0: Receive FIFO half full or less condition interrupt is masked"]
    MASKED = 0,
    #[doc = "1: Receive FIFO half full or less condition interrupt is not masked"]
    NOT_MASKED = 1,
}
impl From<RXIM_A> for bool {
    #[inline(always)]
    fn from(variant: RXIM_A) -> Self {
        variant as u8 != 0
    }
}
impl RXIM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RXIM_A {
        match self.bits {
            false => RXIM_A::MASKED,
            true => RXIM_A::NOT_MASKED,
        }
    }
    #[doc = "Checks if the value of the field is `MASKED`"]
    #[inline(always)]
    pub fn is_masked(&self) -> bool {
        *self == RXIM_A::MASKED
    }
    #[doc = "Checks if the value of the field is `NOT_MASKED`"]
    #[inline(always)]
    pub fn is_not_masked(&self) -> bool {
        *self == RXIM_A::NOT_MASKED
    }
}
#[doc = "Field `RXIM` writer - Receive FIFO interrupt mask"]
pub type RXIM_W<'a, const O: u8> = crate::BitWriter<'a, u32, IMSC_SPEC, RXIM_A, O>;
impl<'a, const O: u8> RXIM_W<'a, O> {
    #[doc = "Receive FIFO half full or less condition interrupt is masked"]
    #[inline(always)]
    pub fn masked(self) -> &'a mut W {
        self.variant(RXIM_A::MASKED)
    }
    #[doc = "Receive FIFO half full or less condition interrupt is not masked"]
    #[inline(always)]
    pub fn not_masked(self) -> &'a mut W {
        self.variant(RXIM_A::NOT_MASKED)
    }
}
#[doc = "Field `TXIM` reader - Transmit FIFO interrupt mask"]
pub type TXIM_R = crate::BitReader<TXIM_A>;
#[doc = "Transmit FIFO interrupt mask\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TXIM_A {
    #[doc = "0: Transmit FIFO half empty or less condition interrupt is masked"]
    MASKED = 0,
    #[doc = "1: Transmit FIFO half empty or less condition interrupt is not masked"]
    NOT_MASKED = 1,
}
impl From<TXIM_A> for bool {
    #[inline(always)]
    fn from(variant: TXIM_A) -> Self {
        variant as u8 != 0
    }
}
impl TXIM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> TXIM_A {
        match self.bits {
            false => TXIM_A::MASKED,
            true => TXIM_A::NOT_MASKED,
        }
    }
    #[doc = "Checks if the value of the field is `MASKED`"]
    #[inline(always)]
    pub fn is_masked(&self) -> bool {
        *self == TXIM_A::MASKED
    }
    #[doc = "Checks if the value of the field is `NOT_MASKED`"]
    #[inline(always)]
    pub fn is_not_masked(&self) -> bool {
        *self == TXIM_A::NOT_MASKED
    }
}
#[doc = "Field `TXIM` writer - Transmit FIFO interrupt mask"]
pub type TXIM_W<'a, const O: u8> = crate::BitWriter<'a, u32, IMSC_SPEC, TXIM_A, O>;
impl<'a, const O: u8> TXIM_W<'a, O> {
    #[doc = "Transmit FIFO half empty or less condition interrupt is masked"]
    #[inline(always)]
    pub fn masked(self) -> &'a mut W {
        self.variant(TXIM_A::MASKED)
    }
    #[doc = "Transmit FIFO half empty or less condition interrupt is not masked"]
    #[inline(always)]
    pub fn not_masked(self) -> &'a mut W {
        self.variant(TXIM_A::NOT_MASKED)
    }
}
impl R {
    #[doc = "Bit 0 - Receive overrun interrupt mask"]
    #[inline(always)]
    pub fn rorim(&self) -> RORIM_R {
        RORIM_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Receive timeout interrupt mask"]
    #[inline(always)]
    pub fn rtim(&self) -> RTIM_R {
        RTIM_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Receive FIFO interrupt mask"]
    #[inline(always)]
    pub fn rxim(&self) -> RXIM_R {
        RXIM_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Transmit FIFO interrupt mask"]
    #[inline(always)]
    pub fn txim(&self) -> TXIM_R {
        TXIM_R::new(((self.bits >> 3) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Receive overrun interrupt mask"]
    #[inline(always)]
    pub fn rorim(&mut self) -> RORIM_W<0> {
        RORIM_W::new(self)
    }
    #[doc = "Bit 1 - Receive timeout interrupt mask"]
    #[inline(always)]
    pub fn rtim(&mut self) -> RTIM_W<1> {
        RTIM_W::new(self)
    }
    #[doc = "Bit 2 - Receive FIFO interrupt mask"]
    #[inline(always)]
    pub fn rxim(&mut self) -> RXIM_W<2> {
        RXIM_W::new(self)
    }
    #[doc = "Bit 3 - Transmit FIFO interrupt mask"]
    #[inline(always)]
    pub fn txim(&mut self) -> TXIM_W<3> {
        TXIM_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Interrupt mask set or clear register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [imsc](index.html) module"]
pub struct IMSC_SPEC;
impl crate::RegisterSpec for IMSC_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [imsc::R](R) reader structure"]
impl crate::Readable for IMSC_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [imsc::W](W) writer structure"]
impl crate::Writable for IMSC_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets IMSC to value 0"]
impl crate::Resettable for IMSC_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
