// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `TCR` reader"]
pub struct R(crate::R<TCR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<TCR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<TCR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<TCR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `TCR` writer"]
pub struct W(crate::W<TCR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<TCR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<TCR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<TCR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `ITEN` reader - Integration test enable"]
pub type ITEN_R = crate::BitReader<ITEN_A>;
#[doc = "Integration test enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ITEN_A {
    #[doc = "0: Normal mode"]
    DISABLED = 0,
    #[doc = "1: The PrimeCell SSP is placed in integration test mode."]
    ENABLED = 1,
}
impl From<ITEN_A> for bool {
    #[inline(always)]
    fn from(variant: ITEN_A) -> Self {
        variant as u8 != 0
    }
}
impl ITEN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> ITEN_A {
        match self.bits {
            false => ITEN_A::DISABLED,
            true => ITEN_A::ENABLED,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == ITEN_A::DISABLED
    }
    #[doc = "Checks if the value of the field is `ENABLED`"]
    #[inline(always)]
    pub fn is_enabled(&self) -> bool {
        *self == ITEN_A::ENABLED
    }
}
#[doc = "Field `ITEN` writer - Integration test enable"]
pub type ITEN_W<'a, const O: u8> = crate::BitWriter<'a, u32, TCR_SPEC, ITEN_A, O>;
impl<'a, const O: u8> ITEN_W<'a, O> {
    #[doc = "Normal mode"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(ITEN_A::DISABLED)
    }
    #[doc = "The PrimeCell SSP is placed in integration test mode."]
    #[inline(always)]
    pub fn enabled(self) -> &'a mut W {
        self.variant(ITEN_A::ENABLED)
    }
}
#[doc = "Field `TESTFIFO` reader - Test FIFO enable"]
pub type TESTFIFO_R = crate::BitReader<TESTFIFO_A>;
#[doc = "Test FIFO enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TESTFIFO_A {
    #[doc = "0: Normal operation"]
    DISABLED = 0,
    #[doc = "1: When this bit is 1, a write to the TDR writes data into the receive FIFO, and reads from the TDR reads data out of the transmit FIFO."]
    ENABLED = 1,
}
impl From<TESTFIFO_A> for bool {
    #[inline(always)]
    fn from(variant: TESTFIFO_A) -> Self {
        variant as u8 != 0
    }
}
impl TESTFIFO_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> TESTFIFO_A {
        match self.bits {
            false => TESTFIFO_A::DISABLED,
            true => TESTFIFO_A::ENABLED,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == TESTFIFO_A::DISABLED
    }
    #[doc = "Checks if the value of the field is `ENABLED`"]
    #[inline(always)]
    pub fn is_enabled(&self) -> bool {
        *self == TESTFIFO_A::ENABLED
    }
}
#[doc = "Field `TESTFIFO` writer - Test FIFO enable"]
pub type TESTFIFO_W<'a, const O: u8> = crate::BitWriter<'a, u32, TCR_SPEC, TESTFIFO_A, O>;
impl<'a, const O: u8> TESTFIFO_W<'a, O> {
    #[doc = "Normal operation"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(TESTFIFO_A::DISABLED)
    }
    #[doc = "When this bit is 1, a write to the TDR writes data into the receive FIFO, and reads from the TDR reads data out of the transmit FIFO."]
    #[inline(always)]
    pub fn enabled(self) -> &'a mut W {
        self.variant(TESTFIFO_A::ENABLED)
    }
}
impl R {
    #[doc = "Bit 0 - Integration test enable"]
    #[inline(always)]
    pub fn iten(&self) -> ITEN_R {
        ITEN_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Test FIFO enable"]
    #[inline(always)]
    pub fn testfifo(&self) -> TESTFIFO_R {
        TESTFIFO_R::new(((self.bits >> 1) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Integration test enable"]
    #[inline(always)]
    pub fn iten(&mut self) -> ITEN_W<0> {
        ITEN_W::new(self)
    }
    #[doc = "Bit 1 - Test FIFO enable"]
    #[inline(always)]
    pub fn testfifo(&mut self) -> TESTFIFO_W<1> {
        TESTFIFO_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Test control register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [tcr](index.html) module"]
pub struct TCR_SPEC;
impl crate::RegisterSpec for TCR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [tcr::R](R) reader structure"]
impl crate::Readable for TCR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [tcr::W](W) writer structure"]
impl crate::Writable for TCR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets TCR to value 0"]
impl crate::Resettable for TCR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
