// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ITOP` reader"]
pub struct R(crate::R<ITOP_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ITOP_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ITOP_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ITOP_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `ITOP` writer"]
pub struct W(crate::W<ITOP_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<ITOP_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<ITOP_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<ITOP_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `TXD` reader - Value on the TXD line"]
pub type TXD_R = crate::BitReader<bool>;
#[doc = "Field `TXD` writer - Value on the TXD line"]
pub type TXD_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `FSSOUT` reader - Value on the FSSOUT line"]
pub type FSSOUT_R = crate::BitReader<bool>;
#[doc = "Field `FSSOUT` writer - Value on the FSSOUT line"]
pub type FSSOUT_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `CLKOUT` reader - Value on the CLKOUT line"]
pub type CLKOUT_R = crate::BitReader<bool>;
#[doc = "Field `CLKOUT` writer - Value on the CLKOUT line"]
pub type CLKOUT_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `CTLOE` reader - Value on the OE line"]
pub type CTLOE_R = crate::BitReader<bool>;
#[doc = "Field `CTLOE` writer - Value on the OE line"]
pub type CTLOE_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `OE` reader - Value on the OE line"]
pub type OE_R = crate::BitReader<bool>;
#[doc = "Field `OE` writer - Value on the OE line"]
pub type OE_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `RORINTR` reader - Value on the RORINTR line"]
pub type RORINTR_R = crate::BitReader<bool>;
#[doc = "Field `RORINTR` writer - Value on the RORINTR line"]
pub type RORINTR_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `RTINTR` reader - Value on the RTINTR line"]
pub type RTINTR_R = crate::BitReader<bool>;
#[doc = "Field `RTINTR` writer - Value on the RTINTR line"]
pub type RTINTR_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `RXINTR` reader - Value on the RXINTR line"]
pub type RXINTR_R = crate::BitReader<bool>;
#[doc = "Field `RXINTR` writer - Value on the RXINTR line"]
pub type RXINTR_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `TXINTR` reader - Value on the TXINTR line"]
pub type TXINTR_R = crate::BitReader<bool>;
#[doc = "Field `TXINTR` writer - Value on the TXINTR line"]
pub type TXINTR_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `INTR` reader - Value on the INTR line"]
pub type INTR_R = crate::BitReader<bool>;
#[doc = "Field `INTR` writer - Value on the INTR line"]
pub type INTR_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `RXDMABREQ` reader - Value on the TXDMABREQ line"]
pub type RXDMABREQ_R = crate::BitReader<bool>;
#[doc = "Field `RXDMABREQ` writer - Value on the TXDMABREQ line"]
pub type RXDMABREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `RXDMASREQ` reader - Value on the TXDMASREQ line"]
pub type RXDMASREQ_R = crate::BitReader<bool>;
#[doc = "Field `RXDMASREQ` writer - Value on the TXDMASREQ line"]
pub type RXDMASREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `TXDMABREQ` reader - Value on the TXDMABREQ line"]
pub type TXDMABREQ_R = crate::BitReader<bool>;
#[doc = "Field `TXDMABREQ` writer - Value on the TXDMABREQ line"]
pub type TXDMABREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
#[doc = "Field `TXDMASREQ` reader - Value on the TXDMASREQ line"]
pub type TXDMASREQ_R = crate::BitReader<bool>;
#[doc = "Field `TXDMASREQ` writer - Value on the TXDMASREQ line"]
pub type TXDMASREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Value on the TXD line"]
    #[inline(always)]
    pub fn txd(&self) -> TXD_R {
        TXD_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Value on the FSSOUT line"]
    #[inline(always)]
    pub fn fssout(&self) -> FSSOUT_R {
        FSSOUT_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Value on the CLKOUT line"]
    #[inline(always)]
    pub fn clkout(&self) -> CLKOUT_R {
        CLKOUT_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Value on the OE line"]
    #[inline(always)]
    pub fn ctloe(&self) -> CTLOE_R {
        CTLOE_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - Value on the OE line"]
    #[inline(always)]
    pub fn oe(&self) -> OE_R {
        OE_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5 - Value on the RORINTR line"]
    #[inline(always)]
    pub fn rorintr(&self) -> RORINTR_R {
        RORINTR_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6 - Value on the RTINTR line"]
    #[inline(always)]
    pub fn rtintr(&self) -> RTINTR_R {
        RTINTR_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - Value on the RXINTR line"]
    #[inline(always)]
    pub fn rxintr(&self) -> RXINTR_R {
        RXINTR_R::new(((self.bits >> 7) & 1) != 0)
    }
    #[doc = "Bit 8 - Value on the TXINTR line"]
    #[inline(always)]
    pub fn txintr(&self) -> TXINTR_R {
        TXINTR_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9 - Value on the INTR line"]
    #[inline(always)]
    pub fn intr(&self) -> INTR_R {
        INTR_R::new(((self.bits >> 9) & 1) != 0)
    }
    #[doc = "Bit 10 - Value on the TXDMABREQ line"]
    #[inline(always)]
    pub fn rxdmabreq(&self) -> RXDMABREQ_R {
        RXDMABREQ_R::new(((self.bits >> 10) & 1) != 0)
    }
    #[doc = "Bit 11 - Value on the TXDMASREQ line"]
    #[inline(always)]
    pub fn rxdmasreq(&self) -> RXDMASREQ_R {
        RXDMASREQ_R::new(((self.bits >> 11) & 1) != 0)
    }
    #[doc = "Bit 12 - Value on the TXDMABREQ line"]
    #[inline(always)]
    pub fn txdmabreq(&self) -> TXDMABREQ_R {
        TXDMABREQ_R::new(((self.bits >> 12) & 1) != 0)
    }
    #[doc = "Bit 13 - Value on the TXDMASREQ line"]
    #[inline(always)]
    pub fn txdmasreq(&self) -> TXDMASREQ_R {
        TXDMASREQ_R::new(((self.bits >> 13) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Value on the TXD line"]
    #[inline(always)]
    pub fn txd(&mut self) -> TXD_W<0> {
        TXD_W::new(self)
    }
    #[doc = "Bit 1 - Value on the FSSOUT line"]
    #[inline(always)]
    pub fn fssout(&mut self) -> FSSOUT_W<1> {
        FSSOUT_W::new(self)
    }
    #[doc = "Bit 2 - Value on the CLKOUT line"]
    #[inline(always)]
    pub fn clkout(&mut self) -> CLKOUT_W<2> {
        CLKOUT_W::new(self)
    }
    #[doc = "Bit 3 - Value on the OE line"]
    #[inline(always)]
    pub fn ctloe(&mut self) -> CTLOE_W<3> {
        CTLOE_W::new(self)
    }
    #[doc = "Bit 4 - Value on the OE line"]
    #[inline(always)]
    pub fn oe(&mut self) -> OE_W<4> {
        OE_W::new(self)
    }
    #[doc = "Bit 5 - Value on the RORINTR line"]
    #[inline(always)]
    pub fn rorintr(&mut self) -> RORINTR_W<5> {
        RORINTR_W::new(self)
    }
    #[doc = "Bit 6 - Value on the RTINTR line"]
    #[inline(always)]
    pub fn rtintr(&mut self) -> RTINTR_W<6> {
        RTINTR_W::new(self)
    }
    #[doc = "Bit 7 - Value on the RXINTR line"]
    #[inline(always)]
    pub fn rxintr(&mut self) -> RXINTR_W<7> {
        RXINTR_W::new(self)
    }
    #[doc = "Bit 8 - Value on the TXINTR line"]
    #[inline(always)]
    pub fn txintr(&mut self) -> TXINTR_W<8> {
        TXINTR_W::new(self)
    }
    #[doc = "Bit 9 - Value on the INTR line"]
    #[inline(always)]
    pub fn intr(&mut self) -> INTR_W<9> {
        INTR_W::new(self)
    }
    #[doc = "Bit 10 - Value on the TXDMABREQ line"]
    #[inline(always)]
    pub fn rxdmabreq(&mut self) -> RXDMABREQ_W<10> {
        RXDMABREQ_W::new(self)
    }
    #[doc = "Bit 11 - Value on the TXDMASREQ line"]
    #[inline(always)]
    pub fn rxdmasreq(&mut self) -> RXDMASREQ_W<11> {
        RXDMASREQ_W::new(self)
    }
    #[doc = "Bit 12 - Value on the TXDMABREQ line"]
    #[inline(always)]
    pub fn txdmabreq(&mut self) -> TXDMABREQ_W<12> {
        TXDMABREQ_W::new(self)
    }
    #[doc = "Bit 13 - Value on the TXDMASREQ line"]
    #[inline(always)]
    pub fn txdmasreq(&mut self) -> TXDMASREQ_W<13> {
        TXDMASREQ_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Integration test output register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [itop](index.html) module"]
pub struct ITOP_SPEC;
impl crate::RegisterSpec for ITOP_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [itop::R](R) reader structure"]
impl crate::Readable for ITOP_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [itop::W](W) writer structure"]
impl crate::Writable for ITOP_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets ITOP to value 0"]
impl crate::Resettable for ITOP_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
