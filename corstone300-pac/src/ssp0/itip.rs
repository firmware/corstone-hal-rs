// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ITIP` reader"]
pub struct R(crate::R<ITIP_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ITIP_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ITIP_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ITIP_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `ITIP` writer"]
pub struct W(crate::W<ITIP_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<ITIP_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<ITIP_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<ITIP_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `RXD` reader - Return the value of RXD primary input"]
pub type RXD_R = crate::BitReader<bool>;
#[doc = "Field `RXD` writer - Return the value of RXD primary input"]
pub type RXD_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITIP_SPEC, bool, O>;
#[doc = "Field `FSSIN` reader - Return the value of FSSIN primary input"]
pub type FSSIN_R = crate::BitReader<bool>;
#[doc = "Field `FSSIN` writer - Return the value of FSSIN primary input"]
pub type FSSIN_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITIP_SPEC, bool, O>;
#[doc = "Field `CLKIN` reader - Return the value of CLKIN primary input"]
pub type CLKIN_R = crate::BitReader<bool>;
#[doc = "Field `CLKIN` writer - Return the value of CLKIN primary input"]
pub type CLKIN_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITIP_SPEC, bool, O>;
#[doc = "Field `RXDDMACLR` reader - Value to be driven on the intra-chip input"]
pub type RXDDMACLR_R = crate::BitReader<bool>;
#[doc = "Field `RXDDMACLR` writer - Value to be driven on the intra-chip input"]
pub type RXDDMACLR_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITIP_SPEC, bool, O>;
#[doc = "Field `TXDMACLR` reader - Value to be driven on the intra-chip input"]
pub type TXDMACLR_R = crate::BitReader<bool>;
#[doc = "Field `TXDMACLR` writer - Value to be driven on the intra-chip input"]
pub type TXDMACLR_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITIP_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Return the value of RXD primary input"]
    #[inline(always)]
    pub fn rxd(&self) -> RXD_R {
        RXD_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Return the value of FSSIN primary input"]
    #[inline(always)]
    pub fn fssin(&self) -> FSSIN_R {
        FSSIN_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Return the value of CLKIN primary input"]
    #[inline(always)]
    pub fn clkin(&self) -> CLKIN_R {
        CLKIN_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Value to be driven on the intra-chip input"]
    #[inline(always)]
    pub fn rxddmaclr(&self) -> RXDDMACLR_R {
        RXDDMACLR_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - Value to be driven on the intra-chip input"]
    #[inline(always)]
    pub fn txdmaclr(&self) -> TXDMACLR_R {
        TXDMACLR_R::new(((self.bits >> 4) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Return the value of RXD primary input"]
    #[inline(always)]
    pub fn rxd(&mut self) -> RXD_W<0> {
        RXD_W::new(self)
    }
    #[doc = "Bit 1 - Return the value of FSSIN primary input"]
    #[inline(always)]
    pub fn fssin(&mut self) -> FSSIN_W<1> {
        FSSIN_W::new(self)
    }
    #[doc = "Bit 2 - Return the value of CLKIN primary input"]
    #[inline(always)]
    pub fn clkin(&mut self) -> CLKIN_W<2> {
        CLKIN_W::new(self)
    }
    #[doc = "Bit 3 - Value to be driven on the intra-chip input"]
    #[inline(always)]
    pub fn rxddmaclr(&mut self) -> RXDDMACLR_W<3> {
        RXDDMACLR_W::new(self)
    }
    #[doc = "Bit 4 - Value to be driven on the intra-chip input"]
    #[inline(always)]
    pub fn txdmaclr(&mut self) -> TXDMACLR_W<4> {
        TXDMACLR_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Integration test input register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [itip](index.html) module"]
pub struct ITIP_SPEC;
impl crate::RegisterSpec for ITIP_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [itip::R](R) reader structure"]
impl crate::Readable for ITIP_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [itip::W](W) writer structure"]
impl crate::Writable for ITIP_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets ITIP to value 0"]
impl crate::Resettable for ITIP_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
