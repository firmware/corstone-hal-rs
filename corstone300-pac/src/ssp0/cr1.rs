// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CR1` reader"]
pub struct R(crate::R<CR1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CR1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CR1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CR1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CR1` writer"]
pub struct W(crate::W<CR1_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CR1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CR1_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CR1_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `LBM` reader - Loop back mode"]
pub type LBM_R = crate::BitReader<LBM_A>;
#[doc = "Loop back mode\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LBM_A {
    #[doc = "0: Normal serial port operation enabled"]
    NORMAL = 0,
    #[doc = "1: Output of transmit serial shifter is connected to input of receive serial shifter internally"]
    LOOPBACK = 1,
}
impl From<LBM_A> for bool {
    #[inline(always)]
    fn from(variant: LBM_A) -> Self {
        variant as u8 != 0
    }
}
impl LBM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LBM_A {
        match self.bits {
            false => LBM_A::NORMAL,
            true => LBM_A::LOOPBACK,
        }
    }
    #[doc = "Checks if the value of the field is `NORMAL`"]
    #[inline(always)]
    pub fn is_normal(&self) -> bool {
        *self == LBM_A::NORMAL
    }
    #[doc = "Checks if the value of the field is `LOOPBACK`"]
    #[inline(always)]
    pub fn is_loopback(&self) -> bool {
        *self == LBM_A::LOOPBACK
    }
}
#[doc = "Field `LBM` writer - Loop back mode"]
pub type LBM_W<'a, const O: u8> = crate::BitWriter<'a, u32, CR1_SPEC, LBM_A, O>;
impl<'a, const O: u8> LBM_W<'a, O> {
    #[doc = "Normal serial port operation enabled"]
    #[inline(always)]
    pub fn normal(self) -> &'a mut W {
        self.variant(LBM_A::NORMAL)
    }
    #[doc = "Output of transmit serial shifter is connected to input of receive serial shifter internally"]
    #[inline(always)]
    pub fn loopback(self) -> &'a mut W {
        self.variant(LBM_A::LOOPBACK)
    }
}
#[doc = "Field `SSE` reader - Synchronous serial port enable"]
pub type SSE_R = crate::BitReader<SSE_A>;
#[doc = "Synchronous serial port enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SSE_A {
    #[doc = "0: SSP operation disabled"]
    DISABLED = 0,
    #[doc = "1: SSP operation enabled"]
    ENABLED = 1,
}
impl From<SSE_A> for bool {
    #[inline(always)]
    fn from(variant: SSE_A) -> Self {
        variant as u8 != 0
    }
}
impl SSE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SSE_A {
        match self.bits {
            false => SSE_A::DISABLED,
            true => SSE_A::ENABLED,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == SSE_A::DISABLED
    }
    #[doc = "Checks if the value of the field is `ENABLED`"]
    #[inline(always)]
    pub fn is_enabled(&self) -> bool {
        *self == SSE_A::ENABLED
    }
}
#[doc = "Field `SSE` writer - Synchronous serial port enable"]
pub type SSE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CR1_SPEC, SSE_A, O>;
impl<'a, const O: u8> SSE_W<'a, O> {
    #[doc = "SSP operation disabled"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(SSE_A::DISABLED)
    }
    #[doc = "SSP operation enabled"]
    #[inline(always)]
    pub fn enabled(self) -> &'a mut W {
        self.variant(SSE_A::ENABLED)
    }
}
#[doc = "Field `MS` reader - Master or slave mode select"]
pub type MS_R = crate::BitReader<MS_A>;
#[doc = "Master or slave mode select\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MS_A {
    #[doc = "0: Device configured as master, default"]
    MASTER = 0,
    #[doc = "1: Device configured as slave"]
    SLAVE = 1,
}
impl From<MS_A> for bool {
    #[inline(always)]
    fn from(variant: MS_A) -> Self {
        variant as u8 != 0
    }
}
impl MS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MS_A {
        match self.bits {
            false => MS_A::MASTER,
            true => MS_A::SLAVE,
        }
    }
    #[doc = "Checks if the value of the field is `MASTER`"]
    #[inline(always)]
    pub fn is_master(&self) -> bool {
        *self == MS_A::MASTER
    }
    #[doc = "Checks if the value of the field is `SLAVE`"]
    #[inline(always)]
    pub fn is_slave(&self) -> bool {
        *self == MS_A::SLAVE
    }
}
#[doc = "Field `MS` writer - Master or slave mode select"]
pub type MS_W<'a, const O: u8> = crate::BitWriter<'a, u32, CR1_SPEC, MS_A, O>;
impl<'a, const O: u8> MS_W<'a, O> {
    #[doc = "Device configured as master, default"]
    #[inline(always)]
    pub fn master(self) -> &'a mut W {
        self.variant(MS_A::MASTER)
    }
    #[doc = "Device configured as slave"]
    #[inline(always)]
    pub fn slave(self) -> &'a mut W {
        self.variant(MS_A::SLAVE)
    }
}
#[doc = "Field `SOD` reader - Slave-mode output disable"]
pub type SOD_R = crate::BitReader<SOD_A>;
#[doc = "Slave-mode output disable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SOD_A {
    #[doc = "0: SSP can drive the SSPTXD output in slave mode"]
    ENABLE = 0,
    #[doc = "1: SSP must not drive the SSPTXD output in slave mode"]
    DISABLE = 1,
}
impl From<SOD_A> for bool {
    #[inline(always)]
    fn from(variant: SOD_A) -> Self {
        variant as u8 != 0
    }
}
impl SOD_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SOD_A {
        match self.bits {
            false => SOD_A::ENABLE,
            true => SOD_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == SOD_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == SOD_A::DISABLE
    }
}
#[doc = "Field `SOD` writer - Slave-mode output disable"]
pub type SOD_W<'a, const O: u8> = crate::BitWriter<'a, u32, CR1_SPEC, SOD_A, O>;
impl<'a, const O: u8> SOD_W<'a, O> {
    #[doc = "SSP can drive the SSPTXD output in slave mode"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(SOD_A::ENABLE)
    }
    #[doc = "SSP must not drive the SSPTXD output in slave mode"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(SOD_A::DISABLE)
    }
}
impl R {
    #[doc = "Bit 0 - Loop back mode"]
    #[inline(always)]
    pub fn lbm(&self) -> LBM_R {
        LBM_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Synchronous serial port enable"]
    #[inline(always)]
    pub fn sse(&self) -> SSE_R {
        SSE_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Master or slave mode select"]
    #[inline(always)]
    pub fn ms(&self) -> MS_R {
        MS_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Slave-mode output disable"]
    #[inline(always)]
    pub fn sod(&self) -> SOD_R {
        SOD_R::new(((self.bits >> 3) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Loop back mode"]
    #[inline(always)]
    pub fn lbm(&mut self) -> LBM_W<0> {
        LBM_W::new(self)
    }
    #[doc = "Bit 1 - Synchronous serial port enable"]
    #[inline(always)]
    pub fn sse(&mut self) -> SSE_W<1> {
        SSE_W::new(self)
    }
    #[doc = "Bit 2 - Master or slave mode select"]
    #[inline(always)]
    pub fn ms(&mut self) -> MS_W<2> {
        MS_W::new(self)
    }
    #[doc = "Bit 3 - Slave-mode output disable"]
    #[inline(always)]
    pub fn sod(&mut self) -> SOD_W<3> {
        SOD_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Control register 1\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cr1](index.html) module"]
pub struct CR1_SPEC;
impl crate::RegisterSpec for CR1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cr1::R](R) reader structure"]
impl crate::Readable for CR1_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cr1::W](W) writer structure"]
impl crate::Writable for CR1_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CR1 to value 0"]
impl crate::Resettable for CR1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
