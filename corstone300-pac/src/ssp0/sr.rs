// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SR` reader"]
pub struct R(crate::R<SR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `SR` writer"]
pub struct W(crate::W<SR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<SR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<SR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<SR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `TFE` reader - Transmit FIFO empty"]
pub type TFE_R = crate::BitReader<TFE_A>;
#[doc = "Transmit FIFO empty\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TFE_A {
    #[doc = "0: Receive FIFO is not empty"]
    NE = 0,
    #[doc = "1: Receive FIFO is empty"]
    E = 1,
}
impl From<TFE_A> for bool {
    #[inline(always)]
    fn from(variant: TFE_A) -> Self {
        variant as u8 != 0
    }
}
impl TFE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> TFE_A {
        match self.bits {
            false => TFE_A::NE,
            true => TFE_A::E,
        }
    }
    #[doc = "Checks if the value of the field is `NE`"]
    #[inline(always)]
    pub fn is_ne(&self) -> bool {
        *self == TFE_A::NE
    }
    #[doc = "Checks if the value of the field is `E`"]
    #[inline(always)]
    pub fn is_e(&self) -> bool {
        *self == TFE_A::E
    }
}
#[doc = "Field `TFE` writer - Transmit FIFO empty"]
pub type TFE_W<'a, const O: u8> = crate::BitWriter<'a, u32, SR_SPEC, TFE_A, O>;
impl<'a, const O: u8> TFE_W<'a, O> {
    #[doc = "Receive FIFO is not empty"]
    #[inline(always)]
    pub fn ne(self) -> &'a mut W {
        self.variant(TFE_A::NE)
    }
    #[doc = "Receive FIFO is empty"]
    #[inline(always)]
    pub fn e(self) -> &'a mut W {
        self.variant(TFE_A::E)
    }
}
#[doc = "Field `TNF` reader - Transmit FIFO not full"]
pub type TNF_R = crate::BitReader<TNF_A>;
#[doc = "Transmit FIFO not full\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TNF_A {
    #[doc = "0: Receive FIFO is full"]
    F = 0,
    #[doc = "1: Receive FIFO is not full"]
    NF = 1,
}
impl From<TNF_A> for bool {
    #[inline(always)]
    fn from(variant: TNF_A) -> Self {
        variant as u8 != 0
    }
}
impl TNF_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> TNF_A {
        match self.bits {
            false => TNF_A::F,
            true => TNF_A::NF,
        }
    }
    #[doc = "Checks if the value of the field is `F`"]
    #[inline(always)]
    pub fn is_f(&self) -> bool {
        *self == TNF_A::F
    }
    #[doc = "Checks if the value of the field is `NF`"]
    #[inline(always)]
    pub fn is_nf(&self) -> bool {
        *self == TNF_A::NF
    }
}
#[doc = "Field `TNF` writer - Transmit FIFO not full"]
pub type TNF_W<'a, const O: u8> = crate::BitWriter<'a, u32, SR_SPEC, TNF_A, O>;
impl<'a, const O: u8> TNF_W<'a, O> {
    #[doc = "Receive FIFO is full"]
    #[inline(always)]
    pub fn f(self) -> &'a mut W {
        self.variant(TNF_A::F)
    }
    #[doc = "Receive FIFO is not full"]
    #[inline(always)]
    pub fn nf(self) -> &'a mut W {
        self.variant(TNF_A::NF)
    }
}
#[doc = "Field `RNE` reader - Receive FIFO not empty"]
pub type RNE_R = crate::BitReader<RNE_A>;
#[doc = "Receive FIFO not empty\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RNE_A {
    #[doc = "0: Receive FIFO is empty"]
    E = 0,
    #[doc = "1: Receive FIFO is not empty"]
    NE = 1,
}
impl From<RNE_A> for bool {
    #[inline(always)]
    fn from(variant: RNE_A) -> Self {
        variant as u8 != 0
    }
}
impl RNE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RNE_A {
        match self.bits {
            false => RNE_A::E,
            true => RNE_A::NE,
        }
    }
    #[doc = "Checks if the value of the field is `E`"]
    #[inline(always)]
    pub fn is_e(&self) -> bool {
        *self == RNE_A::E
    }
    #[doc = "Checks if the value of the field is `NE`"]
    #[inline(always)]
    pub fn is_ne(&self) -> bool {
        *self == RNE_A::NE
    }
}
#[doc = "Field `RNE` writer - Receive FIFO not empty"]
pub type RNE_W<'a, const O: u8> = crate::BitWriter<'a, u32, SR_SPEC, RNE_A, O>;
impl<'a, const O: u8> RNE_W<'a, O> {
    #[doc = "Receive FIFO is empty"]
    #[inline(always)]
    pub fn e(self) -> &'a mut W {
        self.variant(RNE_A::E)
    }
    #[doc = "Receive FIFO is not empty"]
    #[inline(always)]
    pub fn ne(self) -> &'a mut W {
        self.variant(RNE_A::NE)
    }
}
#[doc = "Field `RFF` reader - Receive FIFO full"]
pub type RFF_R = crate::BitReader<RFF_A>;
#[doc = "Receive FIFO full\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RFF_A {
    #[doc = "0: Receive FIFO is not full"]
    NF = 0,
    #[doc = "1: Receive FIFO is full"]
    F = 1,
}
impl From<RFF_A> for bool {
    #[inline(always)]
    fn from(variant: RFF_A) -> Self {
        variant as u8 != 0
    }
}
impl RFF_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> RFF_A {
        match self.bits {
            false => RFF_A::NF,
            true => RFF_A::F,
        }
    }
    #[doc = "Checks if the value of the field is `NF`"]
    #[inline(always)]
    pub fn is_nf(&self) -> bool {
        *self == RFF_A::NF
    }
    #[doc = "Checks if the value of the field is `F`"]
    #[inline(always)]
    pub fn is_f(&self) -> bool {
        *self == RFF_A::F
    }
}
#[doc = "Field `RFF` writer - Receive FIFO full"]
pub type RFF_W<'a, const O: u8> = crate::BitWriter<'a, u32, SR_SPEC, RFF_A, O>;
impl<'a, const O: u8> RFF_W<'a, O> {
    #[doc = "Receive FIFO is not full"]
    #[inline(always)]
    pub fn nf(self) -> &'a mut W {
        self.variant(RFF_A::NF)
    }
    #[doc = "Receive FIFO is full"]
    #[inline(always)]
    pub fn f(self) -> &'a mut W {
        self.variant(RFF_A::F)
    }
}
#[doc = "Field `BSY` reader - PrimeCell SSP busy flag"]
pub type BSY_R = crate::BitReader<BSY_A>;
#[doc = "PrimeCell SSP busy flag\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum BSY_A {
    #[doc = "0: SSP is idle"]
    IDLE = 0,
    #[doc = "1: SSP is currently transmitting and/or receiving a frame or the transmit FIFO is not empty"]
    BUSY = 1,
}
impl From<BSY_A> for bool {
    #[inline(always)]
    fn from(variant: BSY_A) -> Self {
        variant as u8 != 0
    }
}
impl BSY_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> BSY_A {
        match self.bits {
            false => BSY_A::IDLE,
            true => BSY_A::BUSY,
        }
    }
    #[doc = "Checks if the value of the field is `IDLE`"]
    #[inline(always)]
    pub fn is_idle(&self) -> bool {
        *self == BSY_A::IDLE
    }
    #[doc = "Checks if the value of the field is `BUSY`"]
    #[inline(always)]
    pub fn is_busy(&self) -> bool {
        *self == BSY_A::BUSY
    }
}
#[doc = "Field `BSY` writer - PrimeCell SSP busy flag"]
pub type BSY_W<'a, const O: u8> = crate::BitWriter<'a, u32, SR_SPEC, BSY_A, O>;
impl<'a, const O: u8> BSY_W<'a, O> {
    #[doc = "SSP is idle"]
    #[inline(always)]
    pub fn idle(self) -> &'a mut W {
        self.variant(BSY_A::IDLE)
    }
    #[doc = "SSP is currently transmitting and/or receiving a frame or the transmit FIFO is not empty"]
    #[inline(always)]
    pub fn busy(self) -> &'a mut W {
        self.variant(BSY_A::BUSY)
    }
}
impl R {
    #[doc = "Bit 0 - Transmit FIFO empty"]
    #[inline(always)]
    pub fn tfe(&self) -> TFE_R {
        TFE_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Transmit FIFO not full"]
    #[inline(always)]
    pub fn tnf(&self) -> TNF_R {
        TNF_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Receive FIFO not empty"]
    #[inline(always)]
    pub fn rne(&self) -> RNE_R {
        RNE_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Receive FIFO full"]
    #[inline(always)]
    pub fn rff(&self) -> RFF_R {
        RFF_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - PrimeCell SSP busy flag"]
    #[inline(always)]
    pub fn bsy(&self) -> BSY_R {
        BSY_R::new(((self.bits >> 4) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Transmit FIFO empty"]
    #[inline(always)]
    pub fn tfe(&mut self) -> TFE_W<0> {
        TFE_W::new(self)
    }
    #[doc = "Bit 1 - Transmit FIFO not full"]
    #[inline(always)]
    pub fn tnf(&mut self) -> TNF_W<1> {
        TNF_W::new(self)
    }
    #[doc = "Bit 2 - Receive FIFO not empty"]
    #[inline(always)]
    pub fn rne(&mut self) -> RNE_W<2> {
        RNE_W::new(self)
    }
    #[doc = "Bit 3 - Receive FIFO full"]
    #[inline(always)]
    pub fn rff(&mut self) -> RFF_W<3> {
        RFF_W::new(self)
    }
    #[doc = "Bit 4 - PrimeCell SSP busy flag"]
    #[inline(always)]
    pub fn bsy(&mut self) -> BSY_W<4> {
        BSY_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Status register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [sr](index.html) module"]
pub struct SR_SPEC;
impl crate::RegisterSpec for SR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [sr::R](R) reader structure"]
impl crate::Readable for SR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [sr::W](W) writer structure"]
impl crate::Writable for SR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets SR to value 0"]
impl crate::Resettable for SR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
