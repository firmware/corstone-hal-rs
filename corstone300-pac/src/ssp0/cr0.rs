// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CR0` reader"]
pub struct R(crate::R<CR0_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CR0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CR0_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CR0_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CR0` writer"]
pub struct W(crate::W<CR0_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CR0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CR0_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CR0_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `DSS` reader - Data Size Select"]
pub type DSS_R = crate::FieldReader<u8, DSS_A>;
#[doc = "Data Size Select\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum DSS_A {
    #[doc = "3: `11`"]
    _4BIT = 3,
    #[doc = "4: `100`"]
    _5BIT = 4,
    #[doc = "5: `101`"]
    _6BIT = 5,
    #[doc = "6: `110`"]
    _7BIT = 6,
    #[doc = "7: `111`"]
    _8BIT = 7,
    #[doc = "8: `1000`"]
    _9BIT = 8,
    #[doc = "9: `1001`"]
    _10BIT = 9,
    #[doc = "10: `1010`"]
    _11BIT = 10,
    #[doc = "11: `1011`"]
    _12BIT = 11,
    #[doc = "12: `1100`"]
    _13BIT = 12,
    #[doc = "13: `1101`"]
    _14BIT = 13,
    #[doc = "14: `1110`"]
    _15BIT = 14,
    #[doc = "15: `1111`"]
    _16BIT = 15,
}
impl From<DSS_A> for u8 {
    #[inline(always)]
    fn from(variant: DSS_A) -> Self {
        variant as _
    }
}
impl DSS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<DSS_A> {
        match self.bits {
            3 => Some(DSS_A::_4BIT),
            4 => Some(DSS_A::_5BIT),
            5 => Some(DSS_A::_6BIT),
            6 => Some(DSS_A::_7BIT),
            7 => Some(DSS_A::_8BIT),
            8 => Some(DSS_A::_9BIT),
            9 => Some(DSS_A::_10BIT),
            10 => Some(DSS_A::_11BIT),
            11 => Some(DSS_A::_12BIT),
            12 => Some(DSS_A::_13BIT),
            13 => Some(DSS_A::_14BIT),
            14 => Some(DSS_A::_15BIT),
            15 => Some(DSS_A::_16BIT),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `_4BIT`"]
    #[inline(always)]
    pub fn is_4bit(&self) -> bool {
        *self == DSS_A::_4BIT
    }
    #[doc = "Checks if the value of the field is `_5BIT`"]
    #[inline(always)]
    pub fn is_5bit(&self) -> bool {
        *self == DSS_A::_5BIT
    }
    #[doc = "Checks if the value of the field is `_6BIT`"]
    #[inline(always)]
    pub fn is_6bit(&self) -> bool {
        *self == DSS_A::_6BIT
    }
    #[doc = "Checks if the value of the field is `_7BIT`"]
    #[inline(always)]
    pub fn is_7bit(&self) -> bool {
        *self == DSS_A::_7BIT
    }
    #[doc = "Checks if the value of the field is `_8BIT`"]
    #[inline(always)]
    pub fn is_8bit(&self) -> bool {
        *self == DSS_A::_8BIT
    }
    #[doc = "Checks if the value of the field is `_9BIT`"]
    #[inline(always)]
    pub fn is_9bit(&self) -> bool {
        *self == DSS_A::_9BIT
    }
    #[doc = "Checks if the value of the field is `_10BIT`"]
    #[inline(always)]
    pub fn is_10bit(&self) -> bool {
        *self == DSS_A::_10BIT
    }
    #[doc = "Checks if the value of the field is `_11BIT`"]
    #[inline(always)]
    pub fn is_11bit(&self) -> bool {
        *self == DSS_A::_11BIT
    }
    #[doc = "Checks if the value of the field is `_12BIT`"]
    #[inline(always)]
    pub fn is_12bit(&self) -> bool {
        *self == DSS_A::_12BIT
    }
    #[doc = "Checks if the value of the field is `_13BIT`"]
    #[inline(always)]
    pub fn is_13bit(&self) -> bool {
        *self == DSS_A::_13BIT
    }
    #[doc = "Checks if the value of the field is `_14BIT`"]
    #[inline(always)]
    pub fn is_14bit(&self) -> bool {
        *self == DSS_A::_14BIT
    }
    #[doc = "Checks if the value of the field is `_15BIT`"]
    #[inline(always)]
    pub fn is_15bit(&self) -> bool {
        *self == DSS_A::_15BIT
    }
    #[doc = "Checks if the value of the field is `_16BIT`"]
    #[inline(always)]
    pub fn is_16bit(&self) -> bool {
        *self == DSS_A::_16BIT
    }
}
#[doc = "Field `DSS` writer - Data Size Select"]
pub type DSS_W<'a, const O: u8> = crate::FieldWriter<'a, u32, CR0_SPEC, u8, DSS_A, 4, O>;
impl<'a, const O: u8> DSS_W<'a, O> {
    #[doc = "`11`"]
    #[inline(always)]
    pub fn _4bit(self) -> &'a mut W {
        self.variant(DSS_A::_4BIT)
    }
    #[doc = "`100`"]
    #[inline(always)]
    pub fn _5bit(self) -> &'a mut W {
        self.variant(DSS_A::_5BIT)
    }
    #[doc = "`101`"]
    #[inline(always)]
    pub fn _6bit(self) -> &'a mut W {
        self.variant(DSS_A::_6BIT)
    }
    #[doc = "`110`"]
    #[inline(always)]
    pub fn _7bit(self) -> &'a mut W {
        self.variant(DSS_A::_7BIT)
    }
    #[doc = "`111`"]
    #[inline(always)]
    pub fn _8bit(self) -> &'a mut W {
        self.variant(DSS_A::_8BIT)
    }
    #[doc = "`1000`"]
    #[inline(always)]
    pub fn _9bit(self) -> &'a mut W {
        self.variant(DSS_A::_9BIT)
    }
    #[doc = "`1001`"]
    #[inline(always)]
    pub fn _10bit(self) -> &'a mut W {
        self.variant(DSS_A::_10BIT)
    }
    #[doc = "`1010`"]
    #[inline(always)]
    pub fn _11bit(self) -> &'a mut W {
        self.variant(DSS_A::_11BIT)
    }
    #[doc = "`1011`"]
    #[inline(always)]
    pub fn _12bit(self) -> &'a mut W {
        self.variant(DSS_A::_12BIT)
    }
    #[doc = "`1100`"]
    #[inline(always)]
    pub fn _13bit(self) -> &'a mut W {
        self.variant(DSS_A::_13BIT)
    }
    #[doc = "`1101`"]
    #[inline(always)]
    pub fn _14bit(self) -> &'a mut W {
        self.variant(DSS_A::_14BIT)
    }
    #[doc = "`1110`"]
    #[inline(always)]
    pub fn _15bit(self) -> &'a mut W {
        self.variant(DSS_A::_15BIT)
    }
    #[doc = "`1111`"]
    #[inline(always)]
    pub fn _16bit(self) -> &'a mut W {
        self.variant(DSS_A::_16BIT)
    }
}
#[doc = "Field `FRF` reader - Frame format"]
pub type FRF_R = crate::FieldReader<u8, FRF_A>;
#[doc = "Frame format\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum FRF_A {
    #[doc = "0: Motorola SPI frame format"]
    MOTOROLA = 0,
    #[doc = "1: TI synchronous serial frame format"]
    TI = 1,
    #[doc = "2: National Microwire frame format"]
    NM = 2,
}
impl From<FRF_A> for u8 {
    #[inline(always)]
    fn from(variant: FRF_A) -> Self {
        variant as _
    }
}
impl FRF_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<FRF_A> {
        match self.bits {
            0 => Some(FRF_A::MOTOROLA),
            1 => Some(FRF_A::TI),
            2 => Some(FRF_A::NM),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `MOTOROLA`"]
    #[inline(always)]
    pub fn is_motorola(&self) -> bool {
        *self == FRF_A::MOTOROLA
    }
    #[doc = "Checks if the value of the field is `TI`"]
    #[inline(always)]
    pub fn is_ti(&self) -> bool {
        *self == FRF_A::TI
    }
    #[doc = "Checks if the value of the field is `NM`"]
    #[inline(always)]
    pub fn is_nm(&self) -> bool {
        *self == FRF_A::NM
    }
}
#[doc = "Field `FRF` writer - Frame format"]
pub type FRF_W<'a, const O: u8> = crate::FieldWriter<'a, u32, CR0_SPEC, u8, FRF_A, 2, O>;
impl<'a, const O: u8> FRF_W<'a, O> {
    #[doc = "Motorola SPI frame format"]
    #[inline(always)]
    pub fn motorola(self) -> &'a mut W {
        self.variant(FRF_A::MOTOROLA)
    }
    #[doc = "TI synchronous serial frame format"]
    #[inline(always)]
    pub fn ti(self) -> &'a mut W {
        self.variant(FRF_A::TI)
    }
    #[doc = "National Microwire frame format"]
    #[inline(always)]
    pub fn nm(self) -> &'a mut W {
        self.variant(FRF_A::NM)
    }
}
#[doc = "Field `SPO` reader - SSPCLKOUT polarity"]
pub type SPO_R = crate::BitReader<bool>;
#[doc = "Field `SPO` writer - SSPCLKOUT polarity"]
pub type SPO_W<'a, const O: u8> = crate::BitWriter<'a, u32, CR0_SPEC, bool, O>;
#[doc = "Field `SPH` reader - SSPCLKOUT phase"]
pub type SPH_R = crate::BitReader<bool>;
#[doc = "Field `SPH` writer - SSPCLKOUT phase"]
pub type SPH_W<'a, const O: u8> = crate::BitWriter<'a, u32, CR0_SPEC, bool, O>;
#[doc = "Field `SCR` reader - Serial clock rate"]
pub type SCR_R = crate::FieldReader<u8, u8>;
#[doc = "Field `SCR` writer - Serial clock rate"]
pub type SCR_W<'a, const O: u8> = crate::FieldWriter<'a, u32, CR0_SPEC, u8, u8, 8, O>;
impl R {
    #[doc = "Bits 0:3 - Data Size Select"]
    #[inline(always)]
    pub fn dss(&self) -> DSS_R {
        DSS_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bits 4:5 - Frame format"]
    #[inline(always)]
    pub fn frf(&self) -> FRF_R {
        FRF_R::new(((self.bits >> 4) & 3) as u8)
    }
    #[doc = "Bit 6 - SSPCLKOUT polarity"]
    #[inline(always)]
    pub fn spo(&self) -> SPO_R {
        SPO_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - SSPCLKOUT phase"]
    #[inline(always)]
    pub fn sph(&self) -> SPH_R {
        SPH_R::new(((self.bits >> 7) & 1) != 0)
    }
    #[doc = "Bits 8:15 - Serial clock rate"]
    #[inline(always)]
    pub fn scr(&self) -> SCR_R {
        SCR_R::new(((self.bits >> 8) & 0xff) as u8)
    }
}
impl W {
    #[doc = "Bits 0:3 - Data Size Select"]
    #[inline(always)]
    pub fn dss(&mut self) -> DSS_W<0> {
        DSS_W::new(self)
    }
    #[doc = "Bits 4:5 - Frame format"]
    #[inline(always)]
    pub fn frf(&mut self) -> FRF_W<4> {
        FRF_W::new(self)
    }
    #[doc = "Bit 6 - SSPCLKOUT polarity"]
    #[inline(always)]
    pub fn spo(&mut self) -> SPO_W<6> {
        SPO_W::new(self)
    }
    #[doc = "Bit 7 - SSPCLKOUT phase"]
    #[inline(always)]
    pub fn sph(&mut self) -> SPH_W<7> {
        SPH_W::new(self)
    }
    #[doc = "Bits 8:15 - Serial clock rate"]
    #[inline(always)]
    pub fn scr(&mut self) -> SCR_W<8> {
        SCR_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Control register 0\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cr0](index.html) module"]
pub struct CR0_SPEC;
impl crate::RegisterSpec for CR0_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cr0::R](R) reader structure"]
impl crate::Readable for CR0_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cr0::W](W) writer structure"]
impl crate::Writable for CR0_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CR0 to value 0"]
impl crate::Resettable for CR0_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
