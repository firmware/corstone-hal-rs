// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - System Identity Register"]
    pub soc_identity: SOC_IDENTITY,
    #[doc = "0x04 - System Hardware Configuration 0 register"]
    pub sys_config0: SYS_CONFIG0,
    #[doc = "0x08 - System Hardware Configuration 0 register"]
    pub sys_config1: SYS_CONFIG1,
    _reserved3: [u8; 0x0fbc],
    #[doc = "0xfc8 - Subsystem Implementation Identity Register."]
    pub iidr: IIDR,
    _reserved4: [u8; 0x04],
    #[doc = "0xfd0 - Peripheral ID 4"]
    pub pidr4: PIDR4,
    _reserved5: [u8; 0x0c],
    #[doc = "0xfe0 - Peripheral ID 0"]
    pub pidr0: PIDR0,
    #[doc = "0xfe4 - Peripheral ID 1"]
    pub pidr1: PIDR1,
    #[doc = "0xfe8 - Peripheral ID 2"]
    pub pidr2: PIDR2,
    #[doc = "0xfec - Peripheral ID 3"]
    pub pidr3: PIDR3,
    #[doc = "0xff0 - Component ID 0"]
    pub cidr0: CIDR0,
    #[doc = "0xff4 - Component ID 1"]
    pub cidr1: CIDR1,
    #[doc = "0xff8 - Component ID 2"]
    pub cidr2: CIDR2,
    #[doc = "0xffc - Component ID 3"]
    pub cidr3: CIDR3,
}
#[doc = "SOC_IDENTITY (r) register accessor: an alias for `Reg<SOC_IDENTITY_SPEC>`"]
pub type SOC_IDENTITY = crate::Reg<soc_identity::SOC_IDENTITY_SPEC>;
#[doc = "System Identity Register"]
pub mod soc_identity;
#[doc = "SYS_CONFIG0 (r) register accessor: an alias for `Reg<SYS_CONFIG0_SPEC>`"]
pub type SYS_CONFIG0 = crate::Reg<sys_config0::SYS_CONFIG0_SPEC>;
#[doc = "System Hardware Configuration 0 register"]
pub mod sys_config0;
#[doc = "SYS_CONFIG1 (r) register accessor: an alias for `Reg<SYS_CONFIG1_SPEC>`"]
pub type SYS_CONFIG1 = crate::Reg<sys_config1::SYS_CONFIG1_SPEC>;
#[doc = "System Hardware Configuration 0 register"]
pub mod sys_config1;
#[doc = "IIDR (r) register accessor: an alias for `Reg<IIDR_SPEC>`"]
pub type IIDR = crate::Reg<iidr::IIDR_SPEC>;
#[doc = "Subsystem Implementation Identity Register."]
pub mod iidr;
#[doc = "PIDR4 (r) register accessor: an alias for `Reg<PIDR4_SPEC>`"]
pub type PIDR4 = crate::Reg<pidr4::PIDR4_SPEC>;
#[doc = "Peripheral ID 4"]
pub mod pidr4;
#[doc = "PIDR0 (r) register accessor: an alias for `Reg<PIDR0_SPEC>`"]
pub type PIDR0 = crate::Reg<pidr0::PIDR0_SPEC>;
#[doc = "Peripheral ID 0"]
pub mod pidr0;
#[doc = "PIDR1 (r) register accessor: an alias for `Reg<PIDR1_SPEC>`"]
pub type PIDR1 = crate::Reg<pidr1::PIDR1_SPEC>;
#[doc = "Peripheral ID 1"]
pub mod pidr1;
#[doc = "PIDR2 (r) register accessor: an alias for `Reg<PIDR2_SPEC>`"]
pub type PIDR2 = crate::Reg<pidr2::PIDR2_SPEC>;
#[doc = "Peripheral ID 2"]
pub mod pidr2;
#[doc = "PIDR3 (r) register accessor: an alias for `Reg<PIDR3_SPEC>`"]
pub type PIDR3 = crate::Reg<pidr3::PIDR3_SPEC>;
#[doc = "Peripheral ID 3"]
pub mod pidr3;
#[doc = "CIDR0 (r) register accessor: an alias for `Reg<CIDR0_SPEC>`"]
pub type CIDR0 = crate::Reg<cidr0::CIDR0_SPEC>;
#[doc = "Component ID 0"]
pub mod cidr0;
#[doc = "CIDR1 (r) register accessor: an alias for `Reg<CIDR1_SPEC>`"]
pub type CIDR1 = crate::Reg<cidr1::CIDR1_SPEC>;
#[doc = "Component ID 1"]
pub mod cidr1;
#[doc = "CIDR2 (r) register accessor: an alias for `Reg<CIDR2_SPEC>`"]
pub type CIDR2 = crate::Reg<cidr2::CIDR2_SPEC>;
#[doc = "Component ID 2"]
pub mod cidr2;
#[doc = "CIDR3 (r) register accessor: an alias for `Reg<CIDR3_SPEC>`"]
pub type CIDR3 = crate::Reg<cidr3::CIDR3_SPEC>;
#[doc = "Component ID 3"]
pub mod cidr3;
