// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - RX Data FIFO Port"]
    pub rx_data_fifo: RX_DATA_FIFO,
    _reserved1: [u8; 0x1c],
    #[doc = "0x20 - TX Data FIFO Port"]
    pub tx_data_fifo: TX_DATA_FIFO,
    _reserved2: [u8; 0x1c],
    #[doc = "0x40 - RX Status FIFO Port"]
    pub rx_status_fifo: RX_STATUS_FIFO,
    #[doc = "0x44 - RX Status FIFO PEEK Port"]
    pub rx_status_fifo_peek: RX_STATUS_FIFO_PEEK,
    #[doc = "0x48 - TX Status FIFO Port"]
    pub tx_status_fifo: TX_STATUS_FIFO,
    #[doc = "0x4c - TX Status FIFO PEEK Port"]
    pub tx_status_fifo_peek: TX_STATUS_FIFO_PEEK,
    #[doc = "0x50 - ID and Revision"]
    pub id_rev: ID_REV,
    #[doc = "0x54 - Main Interrupt Configuration"]
    pub irq_cfg: IRQ_CFG,
    #[doc = "0x58 - Interrupt Status Register"]
    pub int_sts: INT_STS,
    #[doc = "0x5c - Interrupt Enable Register"]
    pub int_en: INT_EN,
    _reserved10: [u8; 0x04],
    #[doc = "0x64 - Byte Order Test Register"]
    pub test_byte: TEST_BYTE,
    #[doc = "0x68 - FIFO Level Interrupt"]
    pub fifo_int: FIFO_INT,
    #[doc = "0x6c - Receive Configuration"]
    pub rx_cfg: RX_CFG,
    #[doc = "0x70 - Transmit Configuration"]
    pub tx_cfg: TX_CFG,
    #[doc = "0x74 - Hardware Configuration"]
    pub hw_cfg: HW_CFG,
    #[doc = "0x78 - RX Datapath Control"]
    pub rx_dp_ctl: RX_DP_CTL,
    #[doc = "0x7c - Receive FIFO Configuration"]
    pub rx_fifo_inf: RX_FIFO_INF,
    #[doc = "0x80 - Transmit FIFO Configuration"]
    pub tx_fifo_inf: TX_FIFO_INF,
    #[doc = "0x84 - Power Management Control"]
    pub pmt_ctrl: PMT_CTRL,
    #[doc = "0x88 - General Purpose IO Configuration"]
    pub gpio_cfg: GPIO_CFG,
    #[doc = "0x8c - General Purpose Timer Configuration"]
    pub gpt_cfg: GPT_CFG,
    #[doc = "0x90 - General Purpose Timer Count"]
    pub gpt_cnt: GPT_CNT,
    _reserved22: [u8; 0x04],
    #[doc = "0x98 - WORD SWAP Register"]
    pub word_swap: WORD_SWAP,
    #[doc = "0x9c - Free Run Counter"]
    pub free_run: FREE_RUN,
    #[doc = "0xa0 - RX Dropped Frames Counter"]
    pub rx_drop: RX_DROP,
    #[doc = "0xa4 - MAC CSR Synchronizer Command"]
    pub mac_csr_cmd: MAC_CSR_CMD,
    #[doc = "0xa8 - MAC CSR Synchronizer Data"]
    pub mac_csr_data: MAC_CSR_DATA,
    #[doc = "0xac - Automatic Flow Control Configuration"]
    pub afc_cfg: AFC_CFG,
    #[doc = "0xb0 - EEPROM Command"]
    pub e2p_cmd: E2P_CMD,
    #[doc = "0xb4 - EEPROM Data"]
    pub e2p_data: E2P_DATA,
}
#[doc = "RxDataFifo (r) register accessor: an alias for `Reg<RX_DATA_FIFO_SPEC>`"]
pub type RX_DATA_FIFO = crate::Reg<rx_data_fifo::RX_DATA_FIFO_SPEC>;
#[doc = "RX Data FIFO Port"]
pub mod rx_data_fifo;
#[doc = "TxDataFifo (w) register accessor: an alias for `Reg<TX_DATA_FIFO_SPEC>`"]
pub type TX_DATA_FIFO = crate::Reg<tx_data_fifo::TX_DATA_FIFO_SPEC>;
#[doc = "TX Data FIFO Port"]
pub mod tx_data_fifo;
#[doc = "RxStatusFifo (r) register accessor: an alias for `Reg<RX_STATUS_FIFO_SPEC>`"]
pub type RX_STATUS_FIFO = crate::Reg<rx_status_fifo::RX_STATUS_FIFO_SPEC>;
#[doc = "RX Status FIFO Port"]
pub mod rx_status_fifo;
#[doc = "RxStatusFifoPeek (r) register accessor: an alias for `Reg<RX_STATUS_FIFO_PEEK_SPEC>`"]
pub type RX_STATUS_FIFO_PEEK = crate::Reg<rx_status_fifo_peek::RX_STATUS_FIFO_PEEK_SPEC>;
#[doc = "RX Status FIFO PEEK Port"]
pub mod rx_status_fifo_peek;
#[doc = "TxStatusFifo (r) register accessor: an alias for `Reg<TX_STATUS_FIFO_SPEC>`"]
pub type TX_STATUS_FIFO = crate::Reg<tx_status_fifo::TX_STATUS_FIFO_SPEC>;
#[doc = "TX Status FIFO Port"]
pub mod tx_status_fifo;
#[doc = "TxStatusFifoPeek (r) register accessor: an alias for `Reg<TX_STATUS_FIFO_PEEK_SPEC>`"]
pub type TX_STATUS_FIFO_PEEK = crate::Reg<tx_status_fifo_peek::TX_STATUS_FIFO_PEEK_SPEC>;
#[doc = "TX Status FIFO PEEK Port"]
pub mod tx_status_fifo_peek;
#[doc = "ID_REV (r) register accessor: an alias for `Reg<ID_REV_SPEC>`"]
pub type ID_REV = crate::Reg<id_rev::ID_REV_SPEC>;
#[doc = "ID and Revision"]
pub mod id_rev;
#[doc = "IRQ_CFG (rw) register accessor: an alias for `Reg<IRQ_CFG_SPEC>`"]
pub type IRQ_CFG = crate::Reg<irq_cfg::IRQ_CFG_SPEC>;
#[doc = "Main Interrupt Configuration"]
pub mod irq_cfg;
#[doc = "INT_STS (rw) register accessor: an alias for `Reg<INT_STS_SPEC>`"]
pub type INT_STS = crate::Reg<int_sts::INT_STS_SPEC>;
#[doc = "Interrupt Status Register"]
pub mod int_sts;
#[doc = "INT_EN (rw) register accessor: an alias for `Reg<INT_EN_SPEC>`"]
pub type INT_EN = crate::Reg<int_en::INT_EN_SPEC>;
#[doc = "Interrupt Enable Register"]
pub mod int_en;
#[doc = "TEST_BYTE (r) register accessor: an alias for `Reg<TEST_BYTE_SPEC>`"]
pub type TEST_BYTE = crate::Reg<test_byte::TEST_BYTE_SPEC>;
#[doc = "Byte Order Test Register"]
pub mod test_byte;
#[doc = "FIFO_INT (rw) register accessor: an alias for `Reg<FIFO_INT_SPEC>`"]
pub type FIFO_INT = crate::Reg<fifo_int::FIFO_INT_SPEC>;
#[doc = "FIFO Level Interrupt"]
pub mod fifo_int;
#[doc = "RX_CFG (rw) register accessor: an alias for `Reg<RX_CFG_SPEC>`"]
pub type RX_CFG = crate::Reg<rx_cfg::RX_CFG_SPEC>;
#[doc = "Receive Configuration"]
pub mod rx_cfg;
#[doc = "TX_CFG (rw) register accessor: an alias for `Reg<TX_CFG_SPEC>`"]
pub type TX_CFG = crate::Reg<tx_cfg::TX_CFG_SPEC>;
#[doc = "Transmit Configuration"]
pub mod tx_cfg;
#[doc = "HW_CFG (rw) register accessor: an alias for `Reg<HW_CFG_SPEC>`"]
pub type HW_CFG = crate::Reg<hw_cfg::HW_CFG_SPEC>;
#[doc = "Hardware Configuration"]
pub mod hw_cfg;
#[doc = "RX_DP_CTL (r) register accessor: an alias for `Reg<RX_DP_CTL_SPEC>`"]
pub type RX_DP_CTL = crate::Reg<rx_dp_ctl::RX_DP_CTL_SPEC>;
#[doc = "RX Datapath Control"]
pub mod rx_dp_ctl;
#[doc = "RX_FIFO_INF (rw) register accessor: an alias for `Reg<RX_FIFO_INF_SPEC>`"]
pub type RX_FIFO_INF = crate::Reg<rx_fifo_inf::RX_FIFO_INF_SPEC>;
#[doc = "Receive FIFO Configuration"]
pub mod rx_fifo_inf;
#[doc = "TX_FIFO_INF (rw) register accessor: an alias for `Reg<TX_FIFO_INF_SPEC>`"]
pub type TX_FIFO_INF = crate::Reg<tx_fifo_inf::TX_FIFO_INF_SPEC>;
#[doc = "Transmit FIFO Configuration"]
pub mod tx_fifo_inf;
#[doc = "PMT_CTRL (rw) register accessor: an alias for `Reg<PMT_CTRL_SPEC>`"]
pub type PMT_CTRL = crate::Reg<pmt_ctrl::PMT_CTRL_SPEC>;
#[doc = "Power Management Control"]
pub mod pmt_ctrl;
#[doc = "GPIO_CFG (rw) register accessor: an alias for `Reg<GPIO_CFG_SPEC>`"]
pub type GPIO_CFG = crate::Reg<gpio_cfg::GPIO_CFG_SPEC>;
#[doc = "General Purpose IO Configuration"]
pub mod gpio_cfg;
#[doc = "GPT_CFG (rw) register accessor: an alias for `Reg<GPT_CFG_SPEC>`"]
pub type GPT_CFG = crate::Reg<gpt_cfg::GPT_CFG_SPEC>;
#[doc = "General Purpose Timer Configuration"]
pub mod gpt_cfg;
#[doc = "GPT_CNT (r) register accessor: an alias for `Reg<GPT_CNT_SPEC>`"]
pub type GPT_CNT = crate::Reg<gpt_cnt::GPT_CNT_SPEC>;
#[doc = "General Purpose Timer Count"]
pub mod gpt_cnt;
#[doc = "WORD_SWAP (rw) register accessor: an alias for `Reg<WORD_SWAP_SPEC>`"]
pub type WORD_SWAP = crate::Reg<word_swap::WORD_SWAP_SPEC>;
#[doc = "WORD SWAP Register"]
pub mod word_swap;
#[doc = "FREE_RUN (r) register accessor: an alias for `Reg<FREE_RUN_SPEC>`"]
pub type FREE_RUN = crate::Reg<free_run::FREE_RUN_SPEC>;
#[doc = "Free Run Counter"]
pub mod free_run;
#[doc = "RX_DROP (r) register accessor: an alias for `Reg<RX_DROP_SPEC>`"]
pub type RX_DROP = crate::Reg<rx_drop::RX_DROP_SPEC>;
#[doc = "RX Dropped Frames Counter"]
pub mod rx_drop;
#[doc = "MAC_CSR_CMD (rw) register accessor: an alias for `Reg<MAC_CSR_CMD_SPEC>`"]
pub type MAC_CSR_CMD = crate::Reg<mac_csr_cmd::MAC_CSR_CMD_SPEC>;
#[doc = "MAC CSR Synchronizer Command"]
pub mod mac_csr_cmd;
#[doc = "MAC_CSR_DATA (rw) register accessor: an alias for `Reg<MAC_CSR_DATA_SPEC>`"]
pub type MAC_CSR_DATA = crate::Reg<mac_csr_data::MAC_CSR_DATA_SPEC>;
#[doc = "MAC CSR Synchronizer Data"]
pub mod mac_csr_data;
#[doc = "AFC_CFG (rw) register accessor: an alias for `Reg<AFC_CFG_SPEC>`"]
pub type AFC_CFG = crate::Reg<afc_cfg::AFC_CFG_SPEC>;
#[doc = "Automatic Flow Control Configuration"]
pub mod afc_cfg;
#[doc = "E2P_CMD (rw) register accessor: an alias for `Reg<E2P_CMD_SPEC>`"]
pub type E2P_CMD = crate::Reg<e2p_cmd::E2P_CMD_SPEC>;
#[doc = "EEPROM Command"]
pub mod e2p_cmd;
#[doc = "E2P_DATA (rw) register accessor: an alias for `Reg<E2P_DATA_SPEC>`"]
pub type E2P_DATA = crate::Reg<e2p_data::E2P_DATA_SPEC>;
#[doc = "EEPROM Data"]
pub mod e2p_data;
