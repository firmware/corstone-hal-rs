// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `TX_CFG` reader"]
pub struct R(crate::R<TX_CFG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<TX_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<TX_CFG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<TX_CFG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `TX_CFG` writer"]
pub struct W(crate::W<TX_CFG_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<TX_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<TX_CFG_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<TX_CFG_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `STOP_TX` reader - Stop Transmitter"]
pub type STOP_TX_R = crate::BitReader<bool>;
#[doc = "Field `STOP_TX` writer - Stop Transmitter"]
pub type STOP_TX_W<'a, const O: u8> = crate::BitWriter<'a, u32, TX_CFG_SPEC, bool, O>;
#[doc = "Field `TX_ON` reader - Transmitter Enable"]
pub type TX_ON_R = crate::BitReader<bool>;
#[doc = "Field `TX_ON` writer - Transmitter Enable"]
pub type TX_ON_W<'a, const O: u8> = crate::BitWriter<'a, u32, TX_CFG_SPEC, bool, O>;
#[doc = "Field `TXSAO` reader - TX Status Allow Overun"]
pub type TXSAO_R = crate::BitReader<bool>;
#[doc = "Field `TXSAO` writer - TX Status Allow Overun"]
pub type TXSAO_W<'a, const O: u8> = crate::BitWriter<'a, u32, TX_CFG_SPEC, bool, O>;
#[doc = "Field `TXD_DUMP` reader - Force TX Data Discard"]
pub type TXD_DUMP_R = crate::BitReader<bool>;
#[doc = "Field `TXD_DUMP` writer - Force TX Data Discard"]
pub type TXD_DUMP_W<'a, const O: u8> = crate::BitWriter<'a, u32, TX_CFG_SPEC, bool, O>;
#[doc = "Field `TXS_DUMP` reader - Force TX Status Discard"]
pub type TXS_DUMP_R = crate::BitReader<bool>;
#[doc = "Field `TXS_DUMP` writer - Force TX Status Discard"]
pub type TXS_DUMP_W<'a, const O: u8> = crate::BitWriter<'a, u32, TX_CFG_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Stop Transmitter"]
    #[inline(always)]
    pub fn stop_tx(&self) -> STOP_TX_R {
        STOP_TX_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Transmitter Enable"]
    #[inline(always)]
    pub fn tx_on(&self) -> TX_ON_R {
        TX_ON_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - TX Status Allow Overun"]
    #[inline(always)]
    pub fn txsao(&self) -> TXSAO_R {
        TXSAO_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 14 - Force TX Data Discard"]
    #[inline(always)]
    pub fn txd_dump(&self) -> TXD_DUMP_R {
        TXD_DUMP_R::new(((self.bits >> 14) & 1) != 0)
    }
    #[doc = "Bit 15 - Force TX Status Discard"]
    #[inline(always)]
    pub fn txs_dump(&self) -> TXS_DUMP_R {
        TXS_DUMP_R::new(((self.bits >> 15) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Stop Transmitter"]
    #[inline(always)]
    pub fn stop_tx(&mut self) -> STOP_TX_W<0> {
        STOP_TX_W::new(self)
    }
    #[doc = "Bit 1 - Transmitter Enable"]
    #[inline(always)]
    pub fn tx_on(&mut self) -> TX_ON_W<1> {
        TX_ON_W::new(self)
    }
    #[doc = "Bit 2 - TX Status Allow Overun"]
    #[inline(always)]
    pub fn txsao(&mut self) -> TXSAO_W<2> {
        TXSAO_W::new(self)
    }
    #[doc = "Bit 14 - Force TX Data Discard"]
    #[inline(always)]
    pub fn txd_dump(&mut self) -> TXD_DUMP_W<14> {
        TXD_DUMP_W::new(self)
    }
    #[doc = "Bit 15 - Force TX Status Discard"]
    #[inline(always)]
    pub fn txs_dump(&mut self) -> TXS_DUMP_W<15> {
        TXS_DUMP_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Transmit Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [tx_cfg](index.html) module"]
pub struct TX_CFG_SPEC;
impl crate::RegisterSpec for TX_CFG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [tx_cfg::R](R) reader structure"]
impl crate::Readable for TX_CFG_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [tx_cfg::W](W) writer structure"]
impl crate::Writable for TX_CFG_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets TX_CFG to value 0"]
impl crate::Resettable for TX_CFG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
