// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `GPT_CFG` reader"]
pub struct R(crate::R<GPT_CFG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<GPT_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<GPT_CFG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<GPT_CFG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `GPT_CFG` writer"]
pub struct W(crate::W<GPT_CFG_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<GPT_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<GPT_CFG_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<GPT_CFG_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `GPT_LOAD` reader - General Purpose Timer Pre-Load"]
pub type GPT_LOAD_R = crate::FieldReader<u16, u16>;
#[doc = "Field `GPT_LOAD` writer - General Purpose Timer Pre-Load"]
pub type GPT_LOAD_W<'a, const O: u8> = crate::FieldWriter<'a, u32, GPT_CFG_SPEC, u16, u16, 16, O>;
#[doc = "Field `TIMER_EN` reader - GP Timer Enable"]
pub type TIMER_EN_R = crate::BitReader<bool>;
#[doc = "Field `TIMER_EN` writer - GP Timer Enable"]
pub type TIMER_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPT_CFG_SPEC, bool, O>;
impl R {
    #[doc = "Bits 0:15 - General Purpose Timer Pre-Load"]
    #[inline(always)]
    pub fn gpt_load(&self) -> GPT_LOAD_R {
        GPT_LOAD_R::new((self.bits & 0xffff) as u16)
    }
    #[doc = "Bit 29 - GP Timer Enable"]
    #[inline(always)]
    pub fn timer_en(&self) -> TIMER_EN_R {
        TIMER_EN_R::new(((self.bits >> 29) & 1) != 0)
    }
}
impl W {
    #[doc = "Bits 0:15 - General Purpose Timer Pre-Load"]
    #[inline(always)]
    pub fn gpt_load(&mut self) -> GPT_LOAD_W<0> {
        GPT_LOAD_W::new(self)
    }
    #[doc = "Bit 29 - GP Timer Enable"]
    #[inline(always)]
    pub fn timer_en(&mut self) -> TIMER_EN_W<29> {
        TIMER_EN_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "General Purpose Timer Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [gpt_cfg](index.html) module"]
pub struct GPT_CFG_SPEC;
impl crate::RegisterSpec for GPT_CFG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [gpt_cfg::R](R) reader structure"]
impl crate::Readable for GPT_CFG_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [gpt_cfg::W](W) writer structure"]
impl crate::Writable for GPT_CFG_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets GPT_CFG to value 0xffff"]
impl crate::Resettable for GPT_CFG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0xffff
    }
}
