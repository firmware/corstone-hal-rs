// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RX_CFG` reader"]
pub struct R(crate::R<RX_CFG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RX_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RX_CFG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RX_CFG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `RX_CFG` writer"]
pub struct W(crate::W<RX_CFG_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<RX_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<RX_CFG_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<RX_CFG_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `RXDOFF` reader - RX Data Offset"]
pub type RXDOFF_R = crate::FieldReader<u8, u8>;
#[doc = "Field `RXDOFF` writer - RX Data Offset"]
pub type RXDOFF_W<'a, const O: u8> = crate::FieldWriter<'a, u32, RX_CFG_SPEC, u8, u8, 5, O>;
#[doc = "Field `RX_DUMP` reader - Force RX Discard"]
pub type RX_DUMP_R = crate::BitReader<bool>;
#[doc = "Field `RX_DUMP` writer - Force RX Discard"]
pub type RX_DUMP_W<'a, const O: u8> = crate::BitWriter<'a, u32, RX_CFG_SPEC, bool, O>;
#[doc = "Field `RX_DMA_CNT` reader - RX DMA Count"]
pub type RX_DMA_CNT_R = crate::FieldReader<u16, u16>;
#[doc = "Field `RX_DMA_CNT` writer - RX DMA Count"]
pub type RX_DMA_CNT_W<'a, const O: u8> = crate::FieldWriter<'a, u32, RX_CFG_SPEC, u16, u16, 12, O>;
#[doc = "Field `RX_END_ALIGN` reader - RX End Alignment"]
pub type RX_END_ALIGN_R = crate::FieldReader<u8, u8>;
#[doc = "Field `RX_END_ALIGN` writer - RX End Alignment"]
pub type RX_END_ALIGN_W<'a, const O: u8> = crate::FieldWriter<'a, u32, RX_CFG_SPEC, u8, u8, 2, O>;
impl R {
    #[doc = "Bits 8:12 - RX Data Offset"]
    #[inline(always)]
    pub fn rxdoff(&self) -> RXDOFF_R {
        RXDOFF_R::new(((self.bits >> 8) & 0x1f) as u8)
    }
    #[doc = "Bit 15 - Force RX Discard"]
    #[inline(always)]
    pub fn rx_dump(&self) -> RX_DUMP_R {
        RX_DUMP_R::new(((self.bits >> 15) & 1) != 0)
    }
    #[doc = "Bits 16:27 - RX DMA Count"]
    #[inline(always)]
    pub fn rx_dma_cnt(&self) -> RX_DMA_CNT_R {
        RX_DMA_CNT_R::new(((self.bits >> 16) & 0x0fff) as u16)
    }
    #[doc = "Bits 30:31 - RX End Alignment"]
    #[inline(always)]
    pub fn rx_end_align(&self) -> RX_END_ALIGN_R {
        RX_END_ALIGN_R::new(((self.bits >> 30) & 3) as u8)
    }
}
impl W {
    #[doc = "Bits 8:12 - RX Data Offset"]
    #[inline(always)]
    pub fn rxdoff(&mut self) -> RXDOFF_W<8> {
        RXDOFF_W::new(self)
    }
    #[doc = "Bit 15 - Force RX Discard"]
    #[inline(always)]
    pub fn rx_dump(&mut self) -> RX_DUMP_W<15> {
        RX_DUMP_W::new(self)
    }
    #[doc = "Bits 16:27 - RX DMA Count"]
    #[inline(always)]
    pub fn rx_dma_cnt(&mut self) -> RX_DMA_CNT_W<16> {
        RX_DMA_CNT_W::new(self)
    }
    #[doc = "Bits 30:31 - RX End Alignment"]
    #[inline(always)]
    pub fn rx_end_align(&mut self) -> RX_END_ALIGN_W<30> {
        RX_END_ALIGN_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Receive Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [rx_cfg](index.html) module"]
pub struct RX_CFG_SPEC;
impl crate::RegisterSpec for RX_CFG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [rx_cfg::R](R) reader structure"]
impl crate::Readable for RX_CFG_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [rx_cfg::W](W) writer structure"]
impl crate::Writable for RX_CFG_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets RX_CFG to value 0"]
impl crate::Resettable for RX_CFG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
