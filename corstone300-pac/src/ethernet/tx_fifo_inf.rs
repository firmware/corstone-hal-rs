// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `TX_FIFO_INF` reader"]
pub struct R(crate::R<TX_FIFO_INF_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<TX_FIFO_INF_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<TX_FIFO_INF_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<TX_FIFO_INF_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `TX_FIFO_INF` writer"]
pub struct W(crate::W<TX_FIFO_INF_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<TX_FIFO_INF_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<TX_FIFO_INF_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<TX_FIFO_INF_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `TDFREE` reader - TX Data FIFO Free Space"]
pub type TDFREE_R = crate::FieldReader<u16, u16>;
#[doc = "Field `TDFREE` writer - TX Data FIFO Free Space"]
pub type TDFREE_W<'a, const O: u8> = crate::FieldWriter<'a, u32, TX_FIFO_INF_SPEC, u16, u16, 16, O>;
#[doc = "Field `TXSUSED` reader - TX Status FIFO Used Space"]
pub type TXSUSED_R = crate::FieldReader<u8, u8>;
#[doc = "Field `TXSUSED` writer - TX Status FIFO Used Space"]
pub type TXSUSED_W<'a, const O: u8> = crate::FieldWriter<'a, u32, TX_FIFO_INF_SPEC, u8, u8, 8, O>;
impl R {
    #[doc = "Bits 0:15 - TX Data FIFO Free Space"]
    #[inline(always)]
    pub fn tdfree(&self) -> TDFREE_R {
        TDFREE_R::new((self.bits & 0xffff) as u16)
    }
    #[doc = "Bits 16:23 - TX Status FIFO Used Space"]
    #[inline(always)]
    pub fn txsused(&self) -> TXSUSED_R {
        TXSUSED_R::new(((self.bits >> 16) & 0xff) as u8)
    }
}
impl W {
    #[doc = "Bits 0:15 - TX Data FIFO Free Space"]
    #[inline(always)]
    pub fn tdfree(&mut self) -> TDFREE_W<0> {
        TDFREE_W::new(self)
    }
    #[doc = "Bits 16:23 - TX Status FIFO Used Space"]
    #[inline(always)]
    pub fn txsused(&mut self) -> TXSUSED_W<16> {
        TXSUSED_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Transmit FIFO Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [tx_fifo_inf](index.html) module"]
pub struct TX_FIFO_INF_SPEC;
impl crate::RegisterSpec for TX_FIFO_INF_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [tx_fifo_inf::R](R) reader structure"]
impl crate::Readable for TX_FIFO_INF_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [tx_fifo_inf::W](W) writer structure"]
impl crate::Writable for TX_FIFO_INF_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets TX_FIFO_INF to value 0x1200"]
impl crate::Resettable for TX_FIFO_INF_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x1200
    }
}
