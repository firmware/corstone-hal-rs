// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INT_STS` reader"]
pub struct R(crate::R<INT_STS_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INT_STS_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INT_STS_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INT_STS_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `INT_STS` writer"]
pub struct W(crate::W<INT_STS_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<INT_STS_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<INT_STS_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<INT_STS_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `GPIO0_INT` reader - "]
pub type GPIO0_INT_R = crate::BitReader<bool>;
#[doc = "Field `GPIO0_INT` writer - "]
pub type GPIO0_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `GPIO1_INT` reader - "]
pub type GPIO1_INT_R = crate::BitReader<bool>;
#[doc = "Field `GPIO1_INT` writer - "]
pub type GPIO1_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `GPIO2_INT` reader - "]
pub type GPIO2_INT_R = crate::BitReader<bool>;
#[doc = "Field `GPIO2_INT` writer - "]
pub type GPIO2_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `RSFL` reader - RX Status FIFO Level Interrupt"]
pub type RSFL_R = crate::BitReader<bool>;
#[doc = "Field `RSFL` writer - RX Status FIFO Level Interrupt"]
pub type RSFL_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `RSFF` reader - RX Status FIFO Full Interrupt"]
pub type RSFF_R = crate::BitReader<bool>;
#[doc = "Field `RSFF` writer - RX Status FIFO Full Interrupt"]
pub type RSFF_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `RXDF_INT` reader - RX Dropped Frame Interrupt"]
pub type RXDF_INT_R = crate::BitReader<bool>;
#[doc = "Field `RXDF_INT` writer - RX Dropped Frame Interrupt"]
pub type RXDF_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `TSFL` reader - TX Status FIFO Level Interrupt"]
pub type TSFL_R = crate::BitReader<bool>;
#[doc = "Field `TSFL` writer - TX Status FIFO Level Interrupt"]
pub type TSFL_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `TSFF` reader - TX Status FIFO Full Interrupt"]
pub type TSFF_R = crate::BitReader<bool>;
#[doc = "Field `TSFF` writer - TX Status FIFO Full Interrupt"]
pub type TSFF_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `TDFA` reader - TX Data Available Interrupt"]
pub type TDFA_R = crate::BitReader<bool>;
#[doc = "Field `TDFA` writer - TX Data Available Interrupt"]
pub type TDFA_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `TDFO` reader - TX Data FIFO Overrun Interrupt"]
pub type TDFO_R = crate::BitReader<bool>;
#[doc = "Field `TDFO` writer - TX Data FIFO Overrun Interrupt"]
pub type TDFO_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `TXE` reader - Transmitter Error"]
pub type TXE_R = crate::BitReader<bool>;
#[doc = "Field `TXE` writer - Transmitter Error"]
pub type TXE_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `RXE` reader - Receiver Error"]
pub type RXE_R = crate::BitReader<bool>;
#[doc = "Field `RXE` writer - Receiver Error"]
pub type RXE_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `RWT` reader - Receive Watchdog Time-out"]
pub type RWT_R = crate::BitReader<bool>;
#[doc = "Field `RWT` writer - Receive Watchdog Time-out"]
pub type RWT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `TXSO` reader - TX Status Fifo Overflow"]
pub type TXSO_R = crate::BitReader<bool>;
#[doc = "Field `TXSO` writer - TX Status Fifo Overflow"]
pub type TXSO_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `PME_INT` reader - Power Management Event Interrupt"]
pub type PME_INT_R = crate::BitReader<bool>;
#[doc = "Field `PME_INT` writer - Power Management Event Interrupt"]
pub type PME_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `PHY_INT` reader - PHY"]
pub type PHY_INT_R = crate::BitReader<bool>;
#[doc = "Field `PHY_INT` writer - PHY"]
pub type PHY_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `GPT_INT` reader - GP Timer"]
pub type GPT_INT_R = crate::BitReader<bool>;
#[doc = "Field `GPT_INT` writer - GP Timer"]
pub type GPT_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `RXD_INT` reader - RX DMA Interrupt"]
pub type RXD_INT_R = crate::BitReader<bool>;
#[doc = "Field `RXD_INT` writer - RX DMA Interrupt"]
pub type RXD_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `TX_IOC` reader - TX IOC Interrupt"]
pub type TX_IOC_R = crate::BitReader<bool>;
#[doc = "Field `TX_IOC` writer - TX IOC Interrupt"]
pub type TX_IOC_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `RXDFH_INT` reader - RX Dropped Frame Counter Halfway"]
pub type RXDFH_INT_R = crate::BitReader<bool>;
#[doc = "Field `RXDFH_INT` writer - RX Dropped Frame Counter Halfway"]
pub type RXDFH_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `RXSTOP_INT` reader - RX Stopped"]
pub type RXSTOP_INT_R = crate::BitReader<bool>;
#[doc = "Field `RXSTOP_INT` writer - RX Stopped"]
pub type RXSTOP_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `TXSTOP_INT` reader - TX Stopped"]
pub type TXSTOP_INT_R = crate::BitReader<bool>;
#[doc = "Field `TXSTOP_INT` writer - TX Stopped"]
pub type TXSTOP_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
#[doc = "Field `SW_INT` reader - Software Interrupt"]
pub type SW_INT_R = crate::BitReader<bool>;
#[doc = "Field `SW_INT` writer - Software Interrupt"]
pub type SW_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_STS_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn gpio0_int(&self) -> GPIO0_INT_R {
        GPIO0_INT_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn gpio1_int(&self) -> GPIO1_INT_R {
        GPIO1_INT_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2"]
    #[inline(always)]
    pub fn gpio2_int(&self) -> GPIO2_INT_R {
        GPIO2_INT_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - RX Status FIFO Level Interrupt"]
    #[inline(always)]
    pub fn rsfl(&self) -> RSFL_R {
        RSFL_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - RX Status FIFO Full Interrupt"]
    #[inline(always)]
    pub fn rsff(&self) -> RSFF_R {
        RSFF_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 6 - RX Dropped Frame Interrupt"]
    #[inline(always)]
    pub fn rxdf_int(&self) -> RXDF_INT_R {
        RXDF_INT_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - TX Status FIFO Level Interrupt"]
    #[inline(always)]
    pub fn tsfl(&self) -> TSFL_R {
        TSFL_R::new(((self.bits >> 7) & 1) != 0)
    }
    #[doc = "Bit 8 - TX Status FIFO Full Interrupt"]
    #[inline(always)]
    pub fn tsff(&self) -> TSFF_R {
        TSFF_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9 - TX Data Available Interrupt"]
    #[inline(always)]
    pub fn tdfa(&self) -> TDFA_R {
        TDFA_R::new(((self.bits >> 9) & 1) != 0)
    }
    #[doc = "Bit 10 - TX Data FIFO Overrun Interrupt"]
    #[inline(always)]
    pub fn tdfo(&self) -> TDFO_R {
        TDFO_R::new(((self.bits >> 10) & 1) != 0)
    }
    #[doc = "Bit 13 - Transmitter Error"]
    #[inline(always)]
    pub fn txe(&self) -> TXE_R {
        TXE_R::new(((self.bits >> 13) & 1) != 0)
    }
    #[doc = "Bit 14 - Receiver Error"]
    #[inline(always)]
    pub fn rxe(&self) -> RXE_R {
        RXE_R::new(((self.bits >> 14) & 1) != 0)
    }
    #[doc = "Bit 15 - Receive Watchdog Time-out"]
    #[inline(always)]
    pub fn rwt(&self) -> RWT_R {
        RWT_R::new(((self.bits >> 15) & 1) != 0)
    }
    #[doc = "Bit 16 - TX Status Fifo Overflow"]
    #[inline(always)]
    pub fn txso(&self) -> TXSO_R {
        TXSO_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - Power Management Event Interrupt"]
    #[inline(always)]
    pub fn pme_int(&self) -> PME_INT_R {
        PME_INT_R::new(((self.bits >> 17) & 1) != 0)
    }
    #[doc = "Bit 18 - PHY"]
    #[inline(always)]
    pub fn phy_int(&self) -> PHY_INT_R {
        PHY_INT_R::new(((self.bits >> 18) & 1) != 0)
    }
    #[doc = "Bit 19 - GP Timer"]
    #[inline(always)]
    pub fn gpt_int(&self) -> GPT_INT_R {
        GPT_INT_R::new(((self.bits >> 19) & 1) != 0)
    }
    #[doc = "Bit 20 - RX DMA Interrupt"]
    #[inline(always)]
    pub fn rxd_int(&self) -> RXD_INT_R {
        RXD_INT_R::new(((self.bits >> 20) & 1) != 0)
    }
    #[doc = "Bit 21 - TX IOC Interrupt"]
    #[inline(always)]
    pub fn tx_ioc(&self) -> TX_IOC_R {
        TX_IOC_R::new(((self.bits >> 21) & 1) != 0)
    }
    #[doc = "Bit 23 - RX Dropped Frame Counter Halfway"]
    #[inline(always)]
    pub fn rxdfh_int(&self) -> RXDFH_INT_R {
        RXDFH_INT_R::new(((self.bits >> 23) & 1) != 0)
    }
    #[doc = "Bit 24 - RX Stopped"]
    #[inline(always)]
    pub fn rxstop_int(&self) -> RXSTOP_INT_R {
        RXSTOP_INT_R::new(((self.bits >> 24) & 1) != 0)
    }
    #[doc = "Bit 25 - TX Stopped"]
    #[inline(always)]
    pub fn txstop_int(&self) -> TXSTOP_INT_R {
        TXSTOP_INT_R::new(((self.bits >> 25) & 1) != 0)
    }
    #[doc = "Bit 31 - Software Interrupt"]
    #[inline(always)]
    pub fn sw_int(&self) -> SW_INT_R {
        SW_INT_R::new(((self.bits >> 31) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn gpio0_int(&mut self) -> GPIO0_INT_W<0> {
        GPIO0_INT_W::new(self)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn gpio1_int(&mut self) -> GPIO1_INT_W<1> {
        GPIO1_INT_W::new(self)
    }
    #[doc = "Bit 2"]
    #[inline(always)]
    pub fn gpio2_int(&mut self) -> GPIO2_INT_W<2> {
        GPIO2_INT_W::new(self)
    }
    #[doc = "Bit 3 - RX Status FIFO Level Interrupt"]
    #[inline(always)]
    pub fn rsfl(&mut self) -> RSFL_W<3> {
        RSFL_W::new(self)
    }
    #[doc = "Bit 4 - RX Status FIFO Full Interrupt"]
    #[inline(always)]
    pub fn rsff(&mut self) -> RSFF_W<4> {
        RSFF_W::new(self)
    }
    #[doc = "Bit 6 - RX Dropped Frame Interrupt"]
    #[inline(always)]
    pub fn rxdf_int(&mut self) -> RXDF_INT_W<6> {
        RXDF_INT_W::new(self)
    }
    #[doc = "Bit 7 - TX Status FIFO Level Interrupt"]
    #[inline(always)]
    pub fn tsfl(&mut self) -> TSFL_W<7> {
        TSFL_W::new(self)
    }
    #[doc = "Bit 8 - TX Status FIFO Full Interrupt"]
    #[inline(always)]
    pub fn tsff(&mut self) -> TSFF_W<8> {
        TSFF_W::new(self)
    }
    #[doc = "Bit 9 - TX Data Available Interrupt"]
    #[inline(always)]
    pub fn tdfa(&mut self) -> TDFA_W<9> {
        TDFA_W::new(self)
    }
    #[doc = "Bit 10 - TX Data FIFO Overrun Interrupt"]
    #[inline(always)]
    pub fn tdfo(&mut self) -> TDFO_W<10> {
        TDFO_W::new(self)
    }
    #[doc = "Bit 13 - Transmitter Error"]
    #[inline(always)]
    pub fn txe(&mut self) -> TXE_W<13> {
        TXE_W::new(self)
    }
    #[doc = "Bit 14 - Receiver Error"]
    #[inline(always)]
    pub fn rxe(&mut self) -> RXE_W<14> {
        RXE_W::new(self)
    }
    #[doc = "Bit 15 - Receive Watchdog Time-out"]
    #[inline(always)]
    pub fn rwt(&mut self) -> RWT_W<15> {
        RWT_W::new(self)
    }
    #[doc = "Bit 16 - TX Status Fifo Overflow"]
    #[inline(always)]
    pub fn txso(&mut self) -> TXSO_W<16> {
        TXSO_W::new(self)
    }
    #[doc = "Bit 17 - Power Management Event Interrupt"]
    #[inline(always)]
    pub fn pme_int(&mut self) -> PME_INT_W<17> {
        PME_INT_W::new(self)
    }
    #[doc = "Bit 18 - PHY"]
    #[inline(always)]
    pub fn phy_int(&mut self) -> PHY_INT_W<18> {
        PHY_INT_W::new(self)
    }
    #[doc = "Bit 19 - GP Timer"]
    #[inline(always)]
    pub fn gpt_int(&mut self) -> GPT_INT_W<19> {
        GPT_INT_W::new(self)
    }
    #[doc = "Bit 20 - RX DMA Interrupt"]
    #[inline(always)]
    pub fn rxd_int(&mut self) -> RXD_INT_W<20> {
        RXD_INT_W::new(self)
    }
    #[doc = "Bit 21 - TX IOC Interrupt"]
    #[inline(always)]
    pub fn tx_ioc(&mut self) -> TX_IOC_W<21> {
        TX_IOC_W::new(self)
    }
    #[doc = "Bit 23 - RX Dropped Frame Counter Halfway"]
    #[inline(always)]
    pub fn rxdfh_int(&mut self) -> RXDFH_INT_W<23> {
        RXDFH_INT_W::new(self)
    }
    #[doc = "Bit 24 - RX Stopped"]
    #[inline(always)]
    pub fn rxstop_int(&mut self) -> RXSTOP_INT_W<24> {
        RXSTOP_INT_W::new(self)
    }
    #[doc = "Bit 25 - TX Stopped"]
    #[inline(always)]
    pub fn txstop_int(&mut self) -> TXSTOP_INT_W<25> {
        TXSTOP_INT_W::new(self)
    }
    #[doc = "Bit 31 - Software Interrupt"]
    #[inline(always)]
    pub fn sw_int(&mut self) -> SW_INT_W<31> {
        SW_INT_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Interrupt Status Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [int_sts](index.html) module"]
pub struct INT_STS_SPEC;
impl crate::RegisterSpec for INT_STS_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [int_sts::R](R) reader structure"]
impl crate::Readable for INT_STS_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [int_sts::W](W) writer structure"]
impl crate::Writable for INT_STS_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets INT_STS to value 0"]
impl crate::Resettable for INT_STS_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
