// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `FIFO_INT` reader"]
pub struct R(crate::R<FIFO_INT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<FIFO_INT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<FIFO_INT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<FIFO_INT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `FIFO_INT` writer"]
pub struct W(crate::W<FIFO_INT_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<FIFO_INT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<FIFO_INT_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<FIFO_INT_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `RSFL` reader - RX Status FIFO Level"]
pub type RSFL_R = crate::FieldReader<u8, u8>;
#[doc = "Field `RSFL` writer - RX Status FIFO Level"]
pub type RSFL_W<'a, const O: u8> = crate::FieldWriter<'a, u32, FIFO_INT_SPEC, u8, u8, 8, O>;
#[doc = "Field `TSFL` reader - TX Status FIFO Level"]
pub type TSFL_R = crate::FieldReader<u8, u8>;
#[doc = "Field `TSFL` writer - TX Status FIFO Level"]
pub type TSFL_W<'a, const O: u8> = crate::FieldWriter<'a, u32, FIFO_INT_SPEC, u8, u8, 8, O>;
#[doc = "Field `TDFL` reader - TX Data FIFO Level"]
pub type TDFL_R = crate::FieldReader<u8, u8>;
#[doc = "Field `TDFL` writer - TX Data FIFO Level"]
pub type TDFL_W<'a, const O: u8> = crate::FieldWriter<'a, u32, FIFO_INT_SPEC, u8, u8, 8, O>;
impl R {
    #[doc = "Bits 0:7 - RX Status FIFO Level"]
    #[inline(always)]
    pub fn rsfl(&self) -> RSFL_R {
        RSFL_R::new((self.bits & 0xff) as u8)
    }
    #[doc = "Bits 16:23 - TX Status FIFO Level"]
    #[inline(always)]
    pub fn tsfl(&self) -> TSFL_R {
        TSFL_R::new(((self.bits >> 16) & 0xff) as u8)
    }
    #[doc = "Bits 24:31 - TX Data FIFO Level"]
    #[inline(always)]
    pub fn tdfl(&self) -> TDFL_R {
        TDFL_R::new(((self.bits >> 24) & 0xff) as u8)
    }
}
impl W {
    #[doc = "Bits 0:7 - RX Status FIFO Level"]
    #[inline(always)]
    pub fn rsfl(&mut self) -> RSFL_W<0> {
        RSFL_W::new(self)
    }
    #[doc = "Bits 16:23 - TX Status FIFO Level"]
    #[inline(always)]
    pub fn tsfl(&mut self) -> TSFL_W<16> {
        TSFL_W::new(self)
    }
    #[doc = "Bits 24:31 - TX Data FIFO Level"]
    #[inline(always)]
    pub fn tdfl(&mut self) -> TDFL_W<24> {
        TDFL_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "FIFO Level Interrupt\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [fifo_int](index.html) module"]
pub struct FIFO_INT_SPEC;
impl crate::RegisterSpec for FIFO_INT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [fifo_int::R](R) reader structure"]
impl crate::Readable for FIFO_INT_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [fifo_int::W](W) writer structure"]
impl crate::Writable for FIFO_INT_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets FIFO_INT to value 0x4800_0000"]
impl crate::Resettable for FIFO_INT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x4800_0000
    }
}
