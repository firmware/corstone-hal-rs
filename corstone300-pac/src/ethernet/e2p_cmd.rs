// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `E2P_CMD` reader"]
pub struct R(crate::R<E2P_CMD_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<E2P_CMD_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<E2P_CMD_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<E2P_CMD_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `E2P_CMD` writer"]
pub struct W(crate::W<E2P_CMD_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<E2P_CMD_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<E2P_CMD_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<E2P_CMD_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `Addr` reader - EPC Address"]
pub type ADDR_R = crate::FieldReader<u8, u8>;
#[doc = "Field `Addr` writer - EPC Address"]
pub type ADDR_W<'a, const O: u8> = crate::FieldWriter<'a, u32, E2P_CMD_SPEC, u8, u8, 8, O>;
#[doc = "Field `MAC_LDR` reader - MAC Address Loaded"]
pub type MAC_LDR_R = crate::BitReader<bool>;
#[doc = "Field `MAC_LDR` writer - MAC Address Loaded"]
pub type MAC_LDR_W<'a, const O: u8> = crate::BitWriter<'a, u32, E2P_CMD_SPEC, bool, O>;
#[doc = "Field `TIME_OUT` reader - EPC Time-out"]
pub type TIME_OUT_R = crate::BitReader<bool>;
#[doc = "Field `TIME_OUT` writer - EPC Time-out"]
pub type TIME_OUT_W<'a, const O: u8> = crate::BitWriter<'a, u32, E2P_CMD_SPEC, bool, O>;
#[doc = "Field `CMD` reader - EPC Command"]
pub type CMD_R = crate::FieldReader<u8, CMD_A>;
#[doc = "EPC Command\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum CMD_A {
    #[doc = "0: `0`"]
    READ = 0,
    #[doc = "1: Erase/Write Disable"]
    EWDS = 1,
    #[doc = "2: Erase/Write Enable"]
    EWEN = 2,
    #[doc = "3: `11`"]
    WRITE = 3,
    #[doc = "4: Write All"]
    WRAL = 4,
    #[doc = "5: `101`"]
    ERASE = 5,
    #[doc = "6: Erase All"]
    ERAL = 6,
    #[doc = "7: MAC Address Reload"]
    RELOAD = 7,
}
impl From<CMD_A> for u8 {
    #[inline(always)]
    fn from(variant: CMD_A) -> Self {
        variant as _
    }
}
impl CMD_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CMD_A {
        match self.bits {
            0 => CMD_A::READ,
            1 => CMD_A::EWDS,
            2 => CMD_A::EWEN,
            3 => CMD_A::WRITE,
            4 => CMD_A::WRAL,
            5 => CMD_A::ERASE,
            6 => CMD_A::ERAL,
            7 => CMD_A::RELOAD,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `READ`"]
    #[inline(always)]
    pub fn is_read(&self) -> bool {
        *self == CMD_A::READ
    }
    #[doc = "Checks if the value of the field is `EWDS`"]
    #[inline(always)]
    pub fn is_ewds(&self) -> bool {
        *self == CMD_A::EWDS
    }
    #[doc = "Checks if the value of the field is `EWEN`"]
    #[inline(always)]
    pub fn is_ewen(&self) -> bool {
        *self == CMD_A::EWEN
    }
    #[doc = "Checks if the value of the field is `WRITE`"]
    #[inline(always)]
    pub fn is_write(&self) -> bool {
        *self == CMD_A::WRITE
    }
    #[doc = "Checks if the value of the field is `WRAL`"]
    #[inline(always)]
    pub fn is_wral(&self) -> bool {
        *self == CMD_A::WRAL
    }
    #[doc = "Checks if the value of the field is `ERASE`"]
    #[inline(always)]
    pub fn is_erase(&self) -> bool {
        *self == CMD_A::ERASE
    }
    #[doc = "Checks if the value of the field is `ERAL`"]
    #[inline(always)]
    pub fn is_eral(&self) -> bool {
        *self == CMD_A::ERAL
    }
    #[doc = "Checks if the value of the field is `RELOAD`"]
    #[inline(always)]
    pub fn is_reload(&self) -> bool {
        *self == CMD_A::RELOAD
    }
}
#[doc = "Field `CMD` writer - EPC Command"]
pub type CMD_W<'a, const O: u8> = crate::FieldWriterSafe<'a, u32, E2P_CMD_SPEC, u8, CMD_A, 3, O>;
impl<'a, const O: u8> CMD_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn read(self) -> &'a mut W {
        self.variant(CMD_A::READ)
    }
    #[doc = "Erase/Write Disable"]
    #[inline(always)]
    pub fn ewds(self) -> &'a mut W {
        self.variant(CMD_A::EWDS)
    }
    #[doc = "Erase/Write Enable"]
    #[inline(always)]
    pub fn ewen(self) -> &'a mut W {
        self.variant(CMD_A::EWEN)
    }
    #[doc = "`11`"]
    #[inline(always)]
    pub fn write(self) -> &'a mut W {
        self.variant(CMD_A::WRITE)
    }
    #[doc = "Write All"]
    #[inline(always)]
    pub fn wral(self) -> &'a mut W {
        self.variant(CMD_A::WRAL)
    }
    #[doc = "`101`"]
    #[inline(always)]
    pub fn erase(self) -> &'a mut W {
        self.variant(CMD_A::ERASE)
    }
    #[doc = "Erase All"]
    #[inline(always)]
    pub fn eral(self) -> &'a mut W {
        self.variant(CMD_A::ERAL)
    }
    #[doc = "MAC Address Reload"]
    #[inline(always)]
    pub fn reload(self) -> &'a mut W {
        self.variant(CMD_A::RELOAD)
    }
}
#[doc = "Field `BSY` reader - EPC Busy"]
pub type BSY_R = crate::BitReader<bool>;
#[doc = "Field `BSY` writer - EPC Busy"]
pub type BSY_W<'a, const O: u8> = crate::BitWriter<'a, u32, E2P_CMD_SPEC, bool, O>;
impl R {
    #[doc = "Bits 0:7 - EPC Address"]
    #[inline(always)]
    pub fn addr(&self) -> ADDR_R {
        ADDR_R::new((self.bits & 0xff) as u8)
    }
    #[doc = "Bit 8 - MAC Address Loaded"]
    #[inline(always)]
    pub fn mac_ldr(&self) -> MAC_LDR_R {
        MAC_LDR_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9 - EPC Time-out"]
    #[inline(always)]
    pub fn time_out(&self) -> TIME_OUT_R {
        TIME_OUT_R::new(((self.bits >> 9) & 1) != 0)
    }
    #[doc = "Bits 28:30 - EPC Command"]
    #[inline(always)]
    pub fn cmd(&self) -> CMD_R {
        CMD_R::new(((self.bits >> 28) & 7) as u8)
    }
    #[doc = "Bit 31 - EPC Busy"]
    #[inline(always)]
    pub fn bsy(&self) -> BSY_R {
        BSY_R::new(((self.bits >> 31) & 1) != 0)
    }
}
impl W {
    #[doc = "Bits 0:7 - EPC Address"]
    #[inline(always)]
    pub fn addr(&mut self) -> ADDR_W<0> {
        ADDR_W::new(self)
    }
    #[doc = "Bit 8 - MAC Address Loaded"]
    #[inline(always)]
    pub fn mac_ldr(&mut self) -> MAC_LDR_W<8> {
        MAC_LDR_W::new(self)
    }
    #[doc = "Bit 9 - EPC Time-out"]
    #[inline(always)]
    pub fn time_out(&mut self) -> TIME_OUT_W<9> {
        TIME_OUT_W::new(self)
    }
    #[doc = "Bits 28:30 - EPC Command"]
    #[inline(always)]
    pub fn cmd(&mut self) -> CMD_W<28> {
        CMD_W::new(self)
    }
    #[doc = "Bit 31 - EPC Busy"]
    #[inline(always)]
    pub fn bsy(&mut self) -> BSY_W<31> {
        BSY_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "EEPROM Command\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [e2p_cmd](index.html) module"]
pub struct E2P_CMD_SPEC;
impl crate::RegisterSpec for E2P_CMD_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [e2p_cmd::R](R) reader structure"]
impl crate::Readable for E2P_CMD_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [e2p_cmd::W](W) writer structure"]
impl crate::Writable for E2P_CMD_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets E2P_CMD to value 0"]
impl crate::Resettable for E2P_CMD_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
