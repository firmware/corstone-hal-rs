// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INT_EN` reader"]
pub struct R(crate::R<INT_EN_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INT_EN_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INT_EN_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INT_EN_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `INT_EN` writer"]
pub struct W(crate::W<INT_EN_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<INT_EN_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<INT_EN_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<INT_EN_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `GPIO0_INT_EN` reader - GPIO 0 Interrupt"]
pub type GPIO0_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `GPIO0_INT_EN` writer - GPIO 0 Interrupt"]
pub type GPIO0_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `GPIO1_INT_EN` reader - GPIO 1 Interrupt"]
pub type GPIO1_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `GPIO1_INT_EN` writer - GPIO 1 Interrupt"]
pub type GPIO1_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `GPIO2_INT_EN` reader - GPIO 2 Interrupt"]
pub type GPIO2_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `GPIO2_INT_EN` writer - GPIO 2 Interrupt"]
pub type GPIO2_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `RSFL_INT_EN` reader - RX Status FIFO Level Interrupt"]
pub type RSFL_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `RSFL_INT_EN` writer - RX Status FIFO Level Interrupt"]
pub type RSFL_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `RSFF_INT_EN` reader - RX Status FIFO Full Interrupt"]
pub type RSFF_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `RSFF_INT_EN` writer - RX Status FIFO Full Interrupt"]
pub type RSFF_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `RXDF_INT_EN` reader - RX Dropped Frame Interrupt Enable"]
pub type RXDF_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `RXDF_INT_EN` writer - RX Dropped Frame Interrupt Enable"]
pub type RXDF_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `TSFL_INT_EN` reader - TX Status FIFO Level Interrupt"]
pub type TSFL_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `TSFL_INT_EN` writer - TX Status FIFO Level Interrupt"]
pub type TSFL_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `TSFF_INT_EN` reader - TX Status FIFO Full Interrupt"]
pub type TSFF_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `TSFF_INT_EN` writer - TX Status FIFO Full Interrupt"]
pub type TSFF_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `TDFA_INT_EN` reader - TX Data FIFO Available Interrupt"]
pub type TDFA_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `TDFA_INT_EN` writer - TX Data FIFO Available Interrupt"]
pub type TDFA_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `TDFO_INT_EN` reader - TX Data FIFO Overrun Interrupt"]
pub type TDFO_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `TDFO_INT_EN` writer - TX Data FIFO Overrun Interrupt"]
pub type TDFO_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `TXE_INT_EN` reader - Transmitter Error Interrupt"]
pub type TXE_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `TXE_INT_EN` writer - Transmitter Error Interrupt"]
pub type TXE_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `RXE_INT_EN` reader - Receiver Error Interrupt"]
pub type RXE_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `RXE_INT_EN` writer - Receiver Error Interrupt"]
pub type RXE_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `RWT_INT_EN` reader - Receive Watchdog Time-out Interrupt"]
pub type RWT_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `RWT_INT_EN` writer - Receive Watchdog Time-out Interrupt"]
pub type RWT_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `TXSO_EN` reader - TX Status FIFO Overflow"]
pub type TXSO_EN_R = crate::BitReader<bool>;
#[doc = "Field `TXSO_EN` writer - TX Status FIFO Overflow"]
pub type TXSO_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `PME_INT_EN` reader - Power Management Event Interrupt Enable"]
pub type PME_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `PME_INT_EN` writer - Power Management Event Interrupt Enable"]
pub type PME_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `PHY_INT_EN` reader - PHY"]
pub type PHY_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `PHY_INT_EN` writer - PHY"]
pub type PHY_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `GPT_INT_EN` reader - GP Timer"]
pub type GPT_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `GPT_INT_EN` writer - GP Timer"]
pub type GPT_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `RXD_INT` reader - RX DMA Interrupt"]
pub type RXD_INT_R = crate::BitReader<bool>;
#[doc = "Field `RXD_INT` writer - RX DMA Interrupt"]
pub type RXD_INT_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `TIOC_INT_EN` reader - TX IOC Interrupt Enable"]
pub type TIOC_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `TIOC_INT_EN` writer - TX IOC Interrupt Enable"]
pub type TIOC_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `RXDFH_INT_EN` reader - RX Dropped Frame Counter Halfway Interrupt Enable"]
pub type RXDFH_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `RXDFH_INT_EN` writer - RX Dropped Frame Counter Halfway Interrupt Enable"]
pub type RXDFH_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `RXSTOP_INT_EN` reader - RX Stopped Interrupt Enable"]
pub type RXSTOP_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `RXSTOP_INT_EN` writer - RX Stopped Interrupt Enable"]
pub type RXSTOP_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `TXSTOP_INT_EN` reader - TX Stopped Interrupt Enable"]
pub type TXSTOP_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `TXSTOP_INT_EN` writer - TX Stopped Interrupt Enable"]
pub type TXSTOP_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
#[doc = "Field `SW_INT_EN` reader - Software Interrupt"]
pub type SW_INT_EN_R = crate::BitReader<bool>;
#[doc = "Field `SW_INT_EN` writer - Software Interrupt"]
pub type SW_INT_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, INT_EN_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - GPIO 0 Interrupt"]
    #[inline(always)]
    pub fn gpio0_int_en(&self) -> GPIO0_INT_EN_R {
        GPIO0_INT_EN_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - GPIO 1 Interrupt"]
    #[inline(always)]
    pub fn gpio1_int_en(&self) -> GPIO1_INT_EN_R {
        GPIO1_INT_EN_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - GPIO 2 Interrupt"]
    #[inline(always)]
    pub fn gpio2_int_en(&self) -> GPIO2_INT_EN_R {
        GPIO2_INT_EN_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - RX Status FIFO Level Interrupt"]
    #[inline(always)]
    pub fn rsfl_int_en(&self) -> RSFL_INT_EN_R {
        RSFL_INT_EN_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - RX Status FIFO Full Interrupt"]
    #[inline(always)]
    pub fn rsff_int_en(&self) -> RSFF_INT_EN_R {
        RSFF_INT_EN_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 6 - RX Dropped Frame Interrupt Enable"]
    #[inline(always)]
    pub fn rxdf_int_en(&self) -> RXDF_INT_EN_R {
        RXDF_INT_EN_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - TX Status FIFO Level Interrupt"]
    #[inline(always)]
    pub fn tsfl_int_en(&self) -> TSFL_INT_EN_R {
        TSFL_INT_EN_R::new(((self.bits >> 7) & 1) != 0)
    }
    #[doc = "Bit 8 - TX Status FIFO Full Interrupt"]
    #[inline(always)]
    pub fn tsff_int_en(&self) -> TSFF_INT_EN_R {
        TSFF_INT_EN_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9 - TX Data FIFO Available Interrupt"]
    #[inline(always)]
    pub fn tdfa_int_en(&self) -> TDFA_INT_EN_R {
        TDFA_INT_EN_R::new(((self.bits >> 9) & 1) != 0)
    }
    #[doc = "Bit 10 - TX Data FIFO Overrun Interrupt"]
    #[inline(always)]
    pub fn tdfo_int_en(&self) -> TDFO_INT_EN_R {
        TDFO_INT_EN_R::new(((self.bits >> 10) & 1) != 0)
    }
    #[doc = "Bit 13 - Transmitter Error Interrupt"]
    #[inline(always)]
    pub fn txe_int_en(&self) -> TXE_INT_EN_R {
        TXE_INT_EN_R::new(((self.bits >> 13) & 1) != 0)
    }
    #[doc = "Bit 14 - Receiver Error Interrupt"]
    #[inline(always)]
    pub fn rxe_int_en(&self) -> RXE_INT_EN_R {
        RXE_INT_EN_R::new(((self.bits >> 14) & 1) != 0)
    }
    #[doc = "Bit 15 - Receive Watchdog Time-out Interrupt"]
    #[inline(always)]
    pub fn rwt_int_en(&self) -> RWT_INT_EN_R {
        RWT_INT_EN_R::new(((self.bits >> 15) & 1) != 0)
    }
    #[doc = "Bit 16 - TX Status FIFO Overflow"]
    #[inline(always)]
    pub fn txso_en(&self) -> TXSO_EN_R {
        TXSO_EN_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - Power Management Event Interrupt Enable"]
    #[inline(always)]
    pub fn pme_int_en(&self) -> PME_INT_EN_R {
        PME_INT_EN_R::new(((self.bits >> 17) & 1) != 0)
    }
    #[doc = "Bit 18 - PHY"]
    #[inline(always)]
    pub fn phy_int_en(&self) -> PHY_INT_EN_R {
        PHY_INT_EN_R::new(((self.bits >> 18) & 1) != 0)
    }
    #[doc = "Bit 19 - GP Timer"]
    #[inline(always)]
    pub fn gpt_int_en(&self) -> GPT_INT_EN_R {
        GPT_INT_EN_R::new(((self.bits >> 19) & 1) != 0)
    }
    #[doc = "Bit 20 - RX DMA Interrupt"]
    #[inline(always)]
    pub fn rxd_int(&self) -> RXD_INT_R {
        RXD_INT_R::new(((self.bits >> 20) & 1) != 0)
    }
    #[doc = "Bit 21 - TX IOC Interrupt Enable"]
    #[inline(always)]
    pub fn tioc_int_en(&self) -> TIOC_INT_EN_R {
        TIOC_INT_EN_R::new(((self.bits >> 21) & 1) != 0)
    }
    #[doc = "Bit 23 - RX Dropped Frame Counter Halfway Interrupt Enable"]
    #[inline(always)]
    pub fn rxdfh_int_en(&self) -> RXDFH_INT_EN_R {
        RXDFH_INT_EN_R::new(((self.bits >> 23) & 1) != 0)
    }
    #[doc = "Bit 24 - RX Stopped Interrupt Enable"]
    #[inline(always)]
    pub fn rxstop_int_en(&self) -> RXSTOP_INT_EN_R {
        RXSTOP_INT_EN_R::new(((self.bits >> 24) & 1) != 0)
    }
    #[doc = "Bit 25 - TX Stopped Interrupt Enable"]
    #[inline(always)]
    pub fn txstop_int_en(&self) -> TXSTOP_INT_EN_R {
        TXSTOP_INT_EN_R::new(((self.bits >> 25) & 1) != 0)
    }
    #[doc = "Bit 31 - Software Interrupt"]
    #[inline(always)]
    pub fn sw_int_en(&self) -> SW_INT_EN_R {
        SW_INT_EN_R::new(((self.bits >> 31) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - GPIO 0 Interrupt"]
    #[inline(always)]
    pub fn gpio0_int_en(&mut self) -> GPIO0_INT_EN_W<0> {
        GPIO0_INT_EN_W::new(self)
    }
    #[doc = "Bit 1 - GPIO 1 Interrupt"]
    #[inline(always)]
    pub fn gpio1_int_en(&mut self) -> GPIO1_INT_EN_W<1> {
        GPIO1_INT_EN_W::new(self)
    }
    #[doc = "Bit 2 - GPIO 2 Interrupt"]
    #[inline(always)]
    pub fn gpio2_int_en(&mut self) -> GPIO2_INT_EN_W<2> {
        GPIO2_INT_EN_W::new(self)
    }
    #[doc = "Bit 3 - RX Status FIFO Level Interrupt"]
    #[inline(always)]
    pub fn rsfl_int_en(&mut self) -> RSFL_INT_EN_W<3> {
        RSFL_INT_EN_W::new(self)
    }
    #[doc = "Bit 4 - RX Status FIFO Full Interrupt"]
    #[inline(always)]
    pub fn rsff_int_en(&mut self) -> RSFF_INT_EN_W<4> {
        RSFF_INT_EN_W::new(self)
    }
    #[doc = "Bit 6 - RX Dropped Frame Interrupt Enable"]
    #[inline(always)]
    pub fn rxdf_int_en(&mut self) -> RXDF_INT_EN_W<6> {
        RXDF_INT_EN_W::new(self)
    }
    #[doc = "Bit 7 - TX Status FIFO Level Interrupt"]
    #[inline(always)]
    pub fn tsfl_int_en(&mut self) -> TSFL_INT_EN_W<7> {
        TSFL_INT_EN_W::new(self)
    }
    #[doc = "Bit 8 - TX Status FIFO Full Interrupt"]
    #[inline(always)]
    pub fn tsff_int_en(&mut self) -> TSFF_INT_EN_W<8> {
        TSFF_INT_EN_W::new(self)
    }
    #[doc = "Bit 9 - TX Data FIFO Available Interrupt"]
    #[inline(always)]
    pub fn tdfa_int_en(&mut self) -> TDFA_INT_EN_W<9> {
        TDFA_INT_EN_W::new(self)
    }
    #[doc = "Bit 10 - TX Data FIFO Overrun Interrupt"]
    #[inline(always)]
    pub fn tdfo_int_en(&mut self) -> TDFO_INT_EN_W<10> {
        TDFO_INT_EN_W::new(self)
    }
    #[doc = "Bit 13 - Transmitter Error Interrupt"]
    #[inline(always)]
    pub fn txe_int_en(&mut self) -> TXE_INT_EN_W<13> {
        TXE_INT_EN_W::new(self)
    }
    #[doc = "Bit 14 - Receiver Error Interrupt"]
    #[inline(always)]
    pub fn rxe_int_en(&mut self) -> RXE_INT_EN_W<14> {
        RXE_INT_EN_W::new(self)
    }
    #[doc = "Bit 15 - Receive Watchdog Time-out Interrupt"]
    #[inline(always)]
    pub fn rwt_int_en(&mut self) -> RWT_INT_EN_W<15> {
        RWT_INT_EN_W::new(self)
    }
    #[doc = "Bit 16 - TX Status FIFO Overflow"]
    #[inline(always)]
    pub fn txso_en(&mut self) -> TXSO_EN_W<16> {
        TXSO_EN_W::new(self)
    }
    #[doc = "Bit 17 - Power Management Event Interrupt Enable"]
    #[inline(always)]
    pub fn pme_int_en(&mut self) -> PME_INT_EN_W<17> {
        PME_INT_EN_W::new(self)
    }
    #[doc = "Bit 18 - PHY"]
    #[inline(always)]
    pub fn phy_int_en(&mut self) -> PHY_INT_EN_W<18> {
        PHY_INT_EN_W::new(self)
    }
    #[doc = "Bit 19 - GP Timer"]
    #[inline(always)]
    pub fn gpt_int_en(&mut self) -> GPT_INT_EN_W<19> {
        GPT_INT_EN_W::new(self)
    }
    #[doc = "Bit 20 - RX DMA Interrupt"]
    #[inline(always)]
    pub fn rxd_int(&mut self) -> RXD_INT_W<20> {
        RXD_INT_W::new(self)
    }
    #[doc = "Bit 21 - TX IOC Interrupt Enable"]
    #[inline(always)]
    pub fn tioc_int_en(&mut self) -> TIOC_INT_EN_W<21> {
        TIOC_INT_EN_W::new(self)
    }
    #[doc = "Bit 23 - RX Dropped Frame Counter Halfway Interrupt Enable"]
    #[inline(always)]
    pub fn rxdfh_int_en(&mut self) -> RXDFH_INT_EN_W<23> {
        RXDFH_INT_EN_W::new(self)
    }
    #[doc = "Bit 24 - RX Stopped Interrupt Enable"]
    #[inline(always)]
    pub fn rxstop_int_en(&mut self) -> RXSTOP_INT_EN_W<24> {
        RXSTOP_INT_EN_W::new(self)
    }
    #[doc = "Bit 25 - TX Stopped Interrupt Enable"]
    #[inline(always)]
    pub fn txstop_int_en(&mut self) -> TXSTOP_INT_EN_W<25> {
        TXSTOP_INT_EN_W::new(self)
    }
    #[doc = "Bit 31 - Software Interrupt"]
    #[inline(always)]
    pub fn sw_int_en(&mut self) -> SW_INT_EN_W<31> {
        SW_INT_EN_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Interrupt Enable Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [int_en](index.html) module"]
pub struct INT_EN_SPEC;
impl crate::RegisterSpec for INT_EN_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [int_en::R](R) reader structure"]
impl crate::Readable for INT_EN_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [int_en::W](W) writer structure"]
impl crate::Writable for INT_EN_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets INT_EN to value 0"]
impl crate::Resettable for INT_EN_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
