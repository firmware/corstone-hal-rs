// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `IRQ_CFG` reader"]
pub struct R(crate::R<IRQ_CFG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<IRQ_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<IRQ_CFG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<IRQ_CFG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `IRQ_CFG` writer"]
pub struct W(crate::W<IRQ_CFG_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<IRQ_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<IRQ_CFG_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<IRQ_CFG_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `IRQ_TYPE` reader - IRQ Buffer Type"]
pub type IRQ_TYPE_R = crate::BitReader<bool>;
#[doc = "Field `IRQ_TYPE` writer - IRQ Buffer Type"]
pub type IRQ_TYPE_W<'a, const O: u8> = crate::BitWriter<'a, u32, IRQ_CFG_SPEC, bool, O>;
#[doc = "Field `IRQ_POL` reader - IRQ Polarity"]
pub type IRQ_POL_R = crate::BitReader<bool>;
#[doc = "Field `IRQ_POL` writer - IRQ Polarity"]
pub type IRQ_POL_W<'a, const O: u8> = crate::BitWriter<'a, u32, IRQ_CFG_SPEC, bool, O>;
#[doc = "Field `IRQ_EN` reader - IRQ Enable"]
pub type IRQ_EN_R = crate::BitReader<bool>;
#[doc = "Field `IRQ_EN` writer - IRQ Enable"]
pub type IRQ_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, IRQ_CFG_SPEC, bool, O>;
#[doc = "Field `IRQ_INT` reader - Master Interrupt"]
pub type IRQ_INT_R = crate::BitReader<bool>;
#[doc = "Field `INT_DEAS_STS` reader - Interrupt Deassertion Status"]
pub type INT_DEAS_STS_R = crate::BitReader<bool>;
#[doc = "Field `INT_DEAS_CLR` reader - Interrupt Deassertion Interval Clear"]
pub type INT_DEAS_CLR_R = crate::BitReader<bool>;
#[doc = "Field `INT_DEAS` reader - Interrupt Deassertion Interval"]
pub type INT_DEAS_R = crate::FieldReader<u8, u8>;
#[doc = "Field `INT_DEAS` writer - Interrupt Deassertion Interval"]
pub type INT_DEAS_W<'a, const O: u8> = crate::FieldWriter<'a, u32, IRQ_CFG_SPEC, u8, u8, 8, O>;
impl R {
    #[doc = "Bit 0 - IRQ Buffer Type"]
    #[inline(always)]
    pub fn irq_type(&self) -> IRQ_TYPE_R {
        IRQ_TYPE_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 4 - IRQ Polarity"]
    #[inline(always)]
    pub fn irq_pol(&self) -> IRQ_POL_R {
        IRQ_POL_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 8 - IRQ Enable"]
    #[inline(always)]
    pub fn irq_en(&self) -> IRQ_EN_R {
        IRQ_EN_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 12 - Master Interrupt"]
    #[inline(always)]
    pub fn irq_int(&self) -> IRQ_INT_R {
        IRQ_INT_R::new(((self.bits >> 12) & 1) != 0)
    }
    #[doc = "Bit 13 - Interrupt Deassertion Status"]
    #[inline(always)]
    pub fn int_deas_sts(&self) -> INT_DEAS_STS_R {
        INT_DEAS_STS_R::new(((self.bits >> 13) & 1) != 0)
    }
    #[doc = "Bit 14 - Interrupt Deassertion Interval Clear"]
    #[inline(always)]
    pub fn int_deas_clr(&self) -> INT_DEAS_CLR_R {
        INT_DEAS_CLR_R::new(((self.bits >> 14) & 1) != 0)
    }
    #[doc = "Bits 24:31 - Interrupt Deassertion Interval"]
    #[inline(always)]
    pub fn int_deas(&self) -> INT_DEAS_R {
        INT_DEAS_R::new(((self.bits >> 24) & 0xff) as u8)
    }
}
impl W {
    #[doc = "Bit 0 - IRQ Buffer Type"]
    #[inline(always)]
    pub fn irq_type(&mut self) -> IRQ_TYPE_W<0> {
        IRQ_TYPE_W::new(self)
    }
    #[doc = "Bit 4 - IRQ Polarity"]
    #[inline(always)]
    pub fn irq_pol(&mut self) -> IRQ_POL_W<4> {
        IRQ_POL_W::new(self)
    }
    #[doc = "Bit 8 - IRQ Enable"]
    #[inline(always)]
    pub fn irq_en(&mut self) -> IRQ_EN_W<8> {
        IRQ_EN_W::new(self)
    }
    #[doc = "Bits 24:31 - Interrupt Deassertion Interval"]
    #[inline(always)]
    pub fn int_deas(&mut self) -> INT_DEAS_W<24> {
        INT_DEAS_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Main Interrupt Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [irq_cfg](index.html) module"]
pub struct IRQ_CFG_SPEC;
impl crate::RegisterSpec for IRQ_CFG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [irq_cfg::R](R) reader structure"]
impl crate::Readable for IRQ_CFG_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [irq_cfg::W](W) writer structure"]
impl crate::Writable for IRQ_CFG_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets IRQ_CFG to value 0"]
impl crate::Resettable for IRQ_CFG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
