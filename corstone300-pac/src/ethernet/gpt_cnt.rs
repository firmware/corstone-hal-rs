// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `GPT_CNT` reader"]
pub struct R(crate::R<GPT_CNT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<GPT_CNT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<GPT_CNT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<GPT_CNT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `CNT` reader - Counter"]
pub type CNT_R = crate::FieldReader<u16, u16>;
impl R {
    #[doc = "Bits 0:15 - Counter"]
    #[inline(always)]
    pub fn cnt(&self) -> CNT_R {
        CNT_R::new((self.bits & 0xffff) as u16)
    }
}
#[doc = "General Purpose Timer Count\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [gpt_cnt](index.html) module"]
pub struct GPT_CNT_SPEC;
impl crate::RegisterSpec for GPT_CNT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [gpt_cnt::R](R) reader structure"]
impl crate::Readable for GPT_CNT_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets GPT_CNT to value 0xffff"]
impl crate::Resettable for GPT_CNT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0xffff
    }
}
