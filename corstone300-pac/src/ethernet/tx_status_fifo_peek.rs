// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `TxStatusFifoPeek` reader"]
pub struct R(crate::R<TX_STATUS_FIFO_PEEK_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<TX_STATUS_FIFO_PEEK_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<TX_STATUS_FIFO_PEEK_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<TX_STATUS_FIFO_PEEK_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "TX Status FIFO PEEK Port\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [tx_status_fifo_peek](index.html) module"]
pub struct TX_STATUS_FIFO_PEEK_SPEC;
impl crate::RegisterSpec for TX_STATUS_FIFO_PEEK_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [tx_status_fifo_peek::R](R) reader structure"]
impl crate::Readable for TX_STATUS_FIFO_PEEK_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets TxStatusFifoPeek to value 0"]
impl crate::Resettable for TX_STATUS_FIFO_PEEK_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
