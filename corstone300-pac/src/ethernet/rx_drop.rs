// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RX_DROP` reader"]
pub struct R(crate::R<RX_DROP_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RX_DROP_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RX_DROP_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RX_DROP_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "RX Dropped Frames Counter\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [rx_drop](index.html) module"]
pub struct RX_DROP_SPEC;
impl crate::RegisterSpec for RX_DROP_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [rx_drop::R](R) reader structure"]
impl crate::Readable for RX_DROP_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets RX_DROP to value 0"]
impl crate::Resettable for RX_DROP_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
