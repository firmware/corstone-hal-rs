// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `TxStatusFifo` reader"]
pub struct R(crate::R<TX_STATUS_FIFO_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<TX_STATUS_FIFO_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<TX_STATUS_FIFO_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<TX_STATUS_FIFO_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "TX Status FIFO Port\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [tx_status_fifo](index.html) module"]
pub struct TX_STATUS_FIFO_SPEC;
impl crate::RegisterSpec for TX_STATUS_FIFO_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [tx_status_fifo::R](R) reader structure"]
impl crate::Readable for TX_STATUS_FIFO_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets TxStatusFifo to value 0"]
impl crate::Resettable for TX_STATUS_FIFO_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
