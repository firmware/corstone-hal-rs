// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `PMT_CTRL` reader"]
pub struct R(crate::R<PMT_CTRL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<PMT_CTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<PMT_CTRL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<PMT_CTRL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `PMT_CTRL` writer"]
pub struct W(crate::W<PMT_CTRL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<PMT_CTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<PMT_CTRL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<PMT_CTRL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `READY` reader - Device Ready"]
pub type READY_R = crate::BitReader<bool>;
#[doc = "Field `PME_EN` reader - PME Enable"]
pub type PME_EN_R = crate::BitReader<bool>;
#[doc = "Field `PME_EN` writer - PME Enable"]
pub type PME_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, PMT_CTRL_SPEC, bool, O>;
#[doc = "Field `PME_POL` reader - PME Polarity"]
pub type PME_POL_R = crate::BitReader<bool>;
#[doc = "Field `PME_POL` writer - PME Polarity"]
pub type PME_POL_W<'a, const O: u8> = crate::BitWriter<'a, u32, PMT_CTRL_SPEC, bool, O>;
#[doc = "Field `PME_IND` reader - PME Indication"]
pub type PME_IND_R = crate::BitReader<bool>;
#[doc = "Field `PME_IND` writer - PME Indication"]
pub type PME_IND_W<'a, const O: u8> = crate::BitWriter<'a, u32, PMT_CTRL_SPEC, bool, O>;
#[doc = "Field `WUPS` reader - WAKE-UP Status"]
pub type WUPS_R = crate::FieldReader<u8, WUPS_A>;
#[doc = "WAKE-UP Status\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum WUPS_A {
    #[doc = "0: `0`"]
    NO_WAKE_UP = 0,
    #[doc = "1: `1`"]
    ENERGY = 1,
    #[doc = "2: `10`"]
    WAKE_UP_OR_MAGIC_PACKET = 2,
    #[doc = "3: `11`"]
    MULTIPLE = 3,
}
impl From<WUPS_A> for u8 {
    #[inline(always)]
    fn from(variant: WUPS_A) -> Self {
        variant as _
    }
}
impl WUPS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> WUPS_A {
        match self.bits {
            0 => WUPS_A::NO_WAKE_UP,
            1 => WUPS_A::ENERGY,
            2 => WUPS_A::WAKE_UP_OR_MAGIC_PACKET,
            3 => WUPS_A::MULTIPLE,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `NO_WAKE_UP`"]
    #[inline(always)]
    pub fn is_no_wake_up(&self) -> bool {
        *self == WUPS_A::NO_WAKE_UP
    }
    #[doc = "Checks if the value of the field is `ENERGY`"]
    #[inline(always)]
    pub fn is_energy(&self) -> bool {
        *self == WUPS_A::ENERGY
    }
    #[doc = "Checks if the value of the field is `WAKE_UP_OR_MAGIC_PACKET`"]
    #[inline(always)]
    pub fn is_wake_up_or_magic_packet(&self) -> bool {
        *self == WUPS_A::WAKE_UP_OR_MAGIC_PACKET
    }
    #[doc = "Checks if the value of the field is `MULTIPLE`"]
    #[inline(always)]
    pub fn is_multiple(&self) -> bool {
        *self == WUPS_A::MULTIPLE
    }
}
#[doc = "Field `WUPS` writer - WAKE-UP Status"]
pub type WUPS_W<'a, const O: u8> = crate::FieldWriterSafe<'a, u32, PMT_CTRL_SPEC, u8, WUPS_A, 2, O>;
impl<'a, const O: u8> WUPS_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn no_wake_up(self) -> &'a mut W {
        self.variant(WUPS_A::NO_WAKE_UP)
    }
    #[doc = "`1`"]
    #[inline(always)]
    pub fn energy(self) -> &'a mut W {
        self.variant(WUPS_A::ENERGY)
    }
    #[doc = "`10`"]
    #[inline(always)]
    pub fn wake_up_or_magic_packet(self) -> &'a mut W {
        self.variant(WUPS_A::WAKE_UP_OR_MAGIC_PACKET)
    }
    #[doc = "`11`"]
    #[inline(always)]
    pub fn multiple(self) -> &'a mut W {
        self.variant(WUPS_A::MULTIPLE)
    }
}
#[doc = "Field `PME_TYPE` reader - PME Buffer Type"]
pub type PME_TYPE_R = crate::BitReader<bool>;
#[doc = "Field `PME_TYPE` writer - PME Buffer Type"]
pub type PME_TYPE_W<'a, const O: u8> = crate::BitWriter<'a, u32, PMT_CTRL_SPEC, bool, O>;
#[doc = "Field `ED_EN` reader - Energy Detect Enable"]
pub type ED_EN_R = crate::BitReader<bool>;
#[doc = "Field `ED_EN` writer - Energy Detect Enable"]
pub type ED_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, PMT_CTRL_SPEC, bool, O>;
#[doc = "Field `WOL_EN` reader - Wake-On-Lan Enable"]
pub type WOL_EN_R = crate::BitReader<bool>;
#[doc = "Field `WOL_EN` writer - Wake-On-Lan Enable"]
pub type WOL_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, PMT_CTRL_SPEC, bool, O>;
#[doc = "Field `PHY_RST` reader - PHY Reset"]
pub type PHY_RST_R = crate::BitReader<bool>;
#[doc = "Field `PHY_RST` writer - PHY Reset"]
pub type PHY_RST_W<'a, const O: u8> = crate::BitWriter<'a, u32, PMT_CTRL_SPEC, bool, O>;
#[doc = "Field `PM_MODE` reader - Power Management Mode"]
pub type PM_MODE_R = crate::FieldReader<u8, PM_MODE_A>;
#[doc = "Power Management Mode\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum PM_MODE_A {
    #[doc = "0: `0`"]
    NORMAL = 0,
    #[doc = "1: `1`"]
    WAKE_UP_OR_MAGIC_PACKET = 1,
    #[doc = "2: `10`"]
    CAN_ENERGY_DETECT = 2,
}
impl From<PM_MODE_A> for u8 {
    #[inline(always)]
    fn from(variant: PM_MODE_A) -> Self {
        variant as _
    }
}
impl PM_MODE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<PM_MODE_A> {
        match self.bits {
            0 => Some(PM_MODE_A::NORMAL),
            1 => Some(PM_MODE_A::WAKE_UP_OR_MAGIC_PACKET),
            2 => Some(PM_MODE_A::CAN_ENERGY_DETECT),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `NORMAL`"]
    #[inline(always)]
    pub fn is_normal(&self) -> bool {
        *self == PM_MODE_A::NORMAL
    }
    #[doc = "Checks if the value of the field is `WAKE_UP_OR_MAGIC_PACKET`"]
    #[inline(always)]
    pub fn is_wake_up_or_magic_packet(&self) -> bool {
        *self == PM_MODE_A::WAKE_UP_OR_MAGIC_PACKET
    }
    #[doc = "Checks if the value of the field is `CAN_ENERGY_DETECT`"]
    #[inline(always)]
    pub fn is_can_energy_detect(&self) -> bool {
        *self == PM_MODE_A::CAN_ENERGY_DETECT
    }
}
#[doc = "Field `PM_MODE` writer - Power Management Mode"]
pub type PM_MODE_W<'a, const O: u8> =
    crate::FieldWriter<'a, u32, PMT_CTRL_SPEC, u8, PM_MODE_A, 2, O>;
impl<'a, const O: u8> PM_MODE_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn normal(self) -> &'a mut W {
        self.variant(PM_MODE_A::NORMAL)
    }
    #[doc = "`1`"]
    #[inline(always)]
    pub fn wake_up_or_magic_packet(self) -> &'a mut W {
        self.variant(PM_MODE_A::WAKE_UP_OR_MAGIC_PACKET)
    }
    #[doc = "`10`"]
    #[inline(always)]
    pub fn can_energy_detect(self) -> &'a mut W {
        self.variant(PM_MODE_A::CAN_ENERGY_DETECT)
    }
}
impl R {
    #[doc = "Bit 0 - Device Ready"]
    #[inline(always)]
    pub fn ready(&self) -> READY_R {
        READY_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - PME Enable"]
    #[inline(always)]
    pub fn pme_en(&self) -> PME_EN_R {
        PME_EN_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - PME Polarity"]
    #[inline(always)]
    pub fn pme_pol(&self) -> PME_POL_R {
        PME_POL_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - PME Indication"]
    #[inline(always)]
    pub fn pme_ind(&self) -> PME_IND_R {
        PME_IND_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bits 4:5 - WAKE-UP Status"]
    #[inline(always)]
    pub fn wups(&self) -> WUPS_R {
        WUPS_R::new(((self.bits >> 4) & 3) as u8)
    }
    #[doc = "Bit 6 - PME Buffer Type"]
    #[inline(always)]
    pub fn pme_type(&self) -> PME_TYPE_R {
        PME_TYPE_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 8 - Energy Detect Enable"]
    #[inline(always)]
    pub fn ed_en(&self) -> ED_EN_R {
        ED_EN_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9 - Wake-On-Lan Enable"]
    #[inline(always)]
    pub fn wol_en(&self) -> WOL_EN_R {
        WOL_EN_R::new(((self.bits >> 9) & 1) != 0)
    }
    #[doc = "Bit 10 - PHY Reset"]
    #[inline(always)]
    pub fn phy_rst(&self) -> PHY_RST_R {
        PHY_RST_R::new(((self.bits >> 10) & 1) != 0)
    }
    #[doc = "Bits 12:13 - Power Management Mode"]
    #[inline(always)]
    pub fn pm_mode(&self) -> PM_MODE_R {
        PM_MODE_R::new(((self.bits >> 12) & 3) as u8)
    }
}
impl W {
    #[doc = "Bit 1 - PME Enable"]
    #[inline(always)]
    pub fn pme_en(&mut self) -> PME_EN_W<1> {
        PME_EN_W::new(self)
    }
    #[doc = "Bit 2 - PME Polarity"]
    #[inline(always)]
    pub fn pme_pol(&mut self) -> PME_POL_W<2> {
        PME_POL_W::new(self)
    }
    #[doc = "Bit 3 - PME Indication"]
    #[inline(always)]
    pub fn pme_ind(&mut self) -> PME_IND_W<3> {
        PME_IND_W::new(self)
    }
    #[doc = "Bits 4:5 - WAKE-UP Status"]
    #[inline(always)]
    pub fn wups(&mut self) -> WUPS_W<4> {
        WUPS_W::new(self)
    }
    #[doc = "Bit 6 - PME Buffer Type"]
    #[inline(always)]
    pub fn pme_type(&mut self) -> PME_TYPE_W<6> {
        PME_TYPE_W::new(self)
    }
    #[doc = "Bit 8 - Energy Detect Enable"]
    #[inline(always)]
    pub fn ed_en(&mut self) -> ED_EN_W<8> {
        ED_EN_W::new(self)
    }
    #[doc = "Bit 9 - Wake-On-Lan Enable"]
    #[inline(always)]
    pub fn wol_en(&mut self) -> WOL_EN_W<9> {
        WOL_EN_W::new(self)
    }
    #[doc = "Bit 10 - PHY Reset"]
    #[inline(always)]
    pub fn phy_rst(&mut self) -> PHY_RST_W<10> {
        PHY_RST_W::new(self)
    }
    #[doc = "Bits 12:13 - Power Management Mode"]
    #[inline(always)]
    pub fn pm_mode(&mut self) -> PM_MODE_W<12> {
        PM_MODE_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Power Management Control\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [pmt_ctrl](index.html) module"]
pub struct PMT_CTRL_SPEC;
impl crate::RegisterSpec for PMT_CTRL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [pmt_ctrl::R](R) reader structure"]
impl crate::Readable for PMT_CTRL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [pmt_ctrl::W](W) writer structure"]
impl crate::Writable for PMT_CTRL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets PMT_CTRL to value 0"]
impl crate::Resettable for PMT_CTRL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
