// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RX_FIFO_INF` reader"]
pub struct R(crate::R<RX_FIFO_INF_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RX_FIFO_INF_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RX_FIFO_INF_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RX_FIFO_INF_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `RX_FIFO_INF` writer"]
pub struct W(crate::W<RX_FIFO_INF_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<RX_FIFO_INF_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<RX_FIFO_INF_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<RX_FIFO_INF_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `RDFREE` reader - RX Data FIFO Free Space"]
pub type RDFREE_R = crate::FieldReader<u16, u16>;
#[doc = "Field `RDFREE` writer - RX Data FIFO Free Space"]
pub type RDFREE_W<'a, const O: u8> = crate::FieldWriter<'a, u32, RX_FIFO_INF_SPEC, u16, u16, 16, O>;
#[doc = "Field `RXSUSED` reader - RX Status FIFO Used Space"]
pub type RXSUSED_R = crate::FieldReader<u8, u8>;
#[doc = "Field `RXSUSED` writer - RX Status FIFO Used Space"]
pub type RXSUSED_W<'a, const O: u8> = crate::FieldWriter<'a, u32, RX_FIFO_INF_SPEC, u8, u8, 8, O>;
impl R {
    #[doc = "Bits 0:15 - RX Data FIFO Free Space"]
    #[inline(always)]
    pub fn rdfree(&self) -> RDFREE_R {
        RDFREE_R::new((self.bits & 0xffff) as u16)
    }
    #[doc = "Bits 16:23 - RX Status FIFO Used Space"]
    #[inline(always)]
    pub fn rxsused(&self) -> RXSUSED_R {
        RXSUSED_R::new(((self.bits >> 16) & 0xff) as u8)
    }
}
impl W {
    #[doc = "Bits 0:15 - RX Data FIFO Free Space"]
    #[inline(always)]
    pub fn rdfree(&mut self) -> RDFREE_W<0> {
        RDFREE_W::new(self)
    }
    #[doc = "Bits 16:23 - RX Status FIFO Used Space"]
    #[inline(always)]
    pub fn rxsused(&mut self) -> RXSUSED_W<16> {
        RXSUSED_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Receive FIFO Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [rx_fifo_inf](index.html) module"]
pub struct RX_FIFO_INF_SPEC;
impl crate::RegisterSpec for RX_FIFO_INF_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [rx_fifo_inf::R](R) reader structure"]
impl crate::Readable for RX_FIFO_INF_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [rx_fifo_inf::W](W) writer structure"]
impl crate::Writable for RX_FIFO_INF_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets RX_FIFO_INF to value 0"]
impl crate::Resettable for RX_FIFO_INF_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
