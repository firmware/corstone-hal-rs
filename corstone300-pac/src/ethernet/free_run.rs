// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `FREE_RUN` reader"]
pub struct R(crate::R<FREE_RUN_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<FREE_RUN_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<FREE_RUN_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<FREE_RUN_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Free Run Counter\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [free_run](index.html) module"]
pub struct FREE_RUN_SPEC;
impl crate::RegisterSpec for FREE_RUN_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [free_run::R](R) reader structure"]
impl crate::Readable for FREE_RUN_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets FREE_RUN to value 0"]
impl crate::Resettable for FREE_RUN_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
