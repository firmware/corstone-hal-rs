// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `GPIO_CFG` reader"]
pub struct R(crate::R<GPIO_CFG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<GPIO_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<GPIO_CFG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<GPIO_CFG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `GPIO_CFG` writer"]
pub struct W(crate::W<GPIO_CFG_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<GPIO_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<GPIO_CFG_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<GPIO_CFG_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `GPIOD0` reader - GPIO Data"]
pub type GPIOD0_R = crate::BitReader<bool>;
#[doc = "Field `GPIOD0` writer - GPIO Data"]
pub type GPIOD0_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPIOD1` reader - GPIO Data"]
pub type GPIOD1_R = crate::BitReader<bool>;
#[doc = "Field `GPIOD1` writer - GPIO Data"]
pub type GPIOD1_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPIOD2` reader - GPIO Data"]
pub type GPIOD2_R = crate::BitReader<bool>;
#[doc = "Field `GPIOD2` writer - GPIO Data"]
pub type GPIOD2_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPOD3` reader - GPO Data"]
pub type GPOD3_R = crate::BitReader<bool>;
#[doc = "Field `GPOD3` writer - GPO Data"]
pub type GPOD3_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPOD4` reader - GPO Data"]
pub type GPOD4_R = crate::BitReader<bool>;
#[doc = "Field `GPOD4` writer - GPO Data"]
pub type GPOD4_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPDIR0` reader - GPIO Direction"]
pub type GPDIR0_R = crate::BitReader<bool>;
#[doc = "Field `GPDIR0` writer - GPIO Direction"]
pub type GPDIR0_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPDIR1` reader - GPIO Direction"]
pub type GPDIR1_R = crate::BitReader<bool>;
#[doc = "Field `GPDIR1` writer - GPIO Direction"]
pub type GPDIR1_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPDIR2` reader - GPIO Direction"]
pub type GPDIR2_R = crate::BitReader<bool>;
#[doc = "Field `GPDIR2` writer - GPIO Direction"]
pub type GPDIR2_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPIOBUF0` reader - GPIO Buffer Type"]
pub type GPIOBUF0_R = crate::BitReader<bool>;
#[doc = "Field `GPIOBUF0` writer - GPIO Buffer Type"]
pub type GPIOBUF0_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPIOBUF1` reader - GPIO Buffer Type"]
pub type GPIOBUF1_R = crate::BitReader<bool>;
#[doc = "Field `GPIOBUF1` writer - GPIO Buffer Type"]
pub type GPIOBUF1_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPIOBUF2` reader - GPIO Buffer Type"]
pub type GPIOBUF2_R = crate::BitReader<bool>;
#[doc = "Field `GPIOBUF2` writer - GPIO Buffer Type"]
pub type GPIOBUF2_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `EEPR_EN` reader - EEPROM Enable"]
pub type EEPR_EN_R = crate::FieldReader<u8, EEPR_EN_A>;
#[doc = "EEPROM Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum EEPR_EN_A {
    #[doc = "0: `0`"]
    EEDIO_EECLK = 0,
    #[doc = "1: `1`"]
    GPO3_GPO4 = 1,
    #[doc = "3: `11`"]
    GPO3_RX_DV = 3,
    #[doc = "5: `101`"]
    TX_EN_GPO4 = 5,
    #[doc = "6: `110`"]
    TX_EN_RX_DV = 6,
    #[doc = "7: `111`"]
    TX_CLK_RX_CLK = 7,
}
impl From<EEPR_EN_A> for u8 {
    #[inline(always)]
    fn from(variant: EEPR_EN_A) -> Self {
        variant as _
    }
}
impl EEPR_EN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<EEPR_EN_A> {
        match self.bits {
            0 => Some(EEPR_EN_A::EEDIO_EECLK),
            1 => Some(EEPR_EN_A::GPO3_GPO4),
            3 => Some(EEPR_EN_A::GPO3_RX_DV),
            5 => Some(EEPR_EN_A::TX_EN_GPO4),
            6 => Some(EEPR_EN_A::TX_EN_RX_DV),
            7 => Some(EEPR_EN_A::TX_CLK_RX_CLK),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `EEDIO_EECLK`"]
    #[inline(always)]
    pub fn is_eedio_eeclk(&self) -> bool {
        *self == EEPR_EN_A::EEDIO_EECLK
    }
    #[doc = "Checks if the value of the field is `GPO3_GPO4`"]
    #[inline(always)]
    pub fn is_gpo3_gpo4(&self) -> bool {
        *self == EEPR_EN_A::GPO3_GPO4
    }
    #[doc = "Checks if the value of the field is `GPO3_RX_DV`"]
    #[inline(always)]
    pub fn is_gpo3_rx_dv(&self) -> bool {
        *self == EEPR_EN_A::GPO3_RX_DV
    }
    #[doc = "Checks if the value of the field is `TX_EN_GPO4`"]
    #[inline(always)]
    pub fn is_tx_en_gpo4(&self) -> bool {
        *self == EEPR_EN_A::TX_EN_GPO4
    }
    #[doc = "Checks if the value of the field is `TX_EN_RX_DV`"]
    #[inline(always)]
    pub fn is_tx_en_rx_dv(&self) -> bool {
        *self == EEPR_EN_A::TX_EN_RX_DV
    }
    #[doc = "Checks if the value of the field is `TX_CLK_RX_CLK`"]
    #[inline(always)]
    pub fn is_tx_clk_rx_clk(&self) -> bool {
        *self == EEPR_EN_A::TX_CLK_RX_CLK
    }
}
#[doc = "Field `EEPR_EN` writer - EEPROM Enable"]
pub type EEPR_EN_W<'a, const O: u8> =
    crate::FieldWriter<'a, u32, GPIO_CFG_SPEC, u8, EEPR_EN_A, 3, O>;
impl<'a, const O: u8> EEPR_EN_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn eedio_eeclk(self) -> &'a mut W {
        self.variant(EEPR_EN_A::EEDIO_EECLK)
    }
    #[doc = "`1`"]
    #[inline(always)]
    pub fn gpo3_gpo4(self) -> &'a mut W {
        self.variant(EEPR_EN_A::GPO3_GPO4)
    }
    #[doc = "`11`"]
    #[inline(always)]
    pub fn gpo3_rx_dv(self) -> &'a mut W {
        self.variant(EEPR_EN_A::GPO3_RX_DV)
    }
    #[doc = "`101`"]
    #[inline(always)]
    pub fn tx_en_gpo4(self) -> &'a mut W {
        self.variant(EEPR_EN_A::TX_EN_GPO4)
    }
    #[doc = "`110`"]
    #[inline(always)]
    pub fn tx_en_rx_dv(self) -> &'a mut W {
        self.variant(EEPR_EN_A::TX_EN_RX_DV)
    }
    #[doc = "`111`"]
    #[inline(always)]
    pub fn tx_clk_rx_clk(self) -> &'a mut W {
        self.variant(EEPR_EN_A::TX_CLK_RX_CLK)
    }
}
#[doc = "Field `GPIO0_INT_POL` reader - GPIO Interrupt Polarity"]
pub type GPIO0_INT_POL_R = crate::BitReader<bool>;
#[doc = "Field `GPIO0_INT_POL` writer - GPIO Interrupt Polarity"]
pub type GPIO0_INT_POL_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPIO1_INT_POL` reader - GPIO Interrupt Polarity"]
pub type GPIO1_INT_POL_R = crate::BitReader<bool>;
#[doc = "Field `GPIO1_INT_POL` writer - GPIO Interrupt Polarity"]
pub type GPIO1_INT_POL_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `GPIO2_INT_POL` reader - GPIO Interrupt Polarity"]
pub type GPIO2_INT_POL_R = crate::BitReader<bool>;
#[doc = "Field `GPIO2_INT_POL` writer - GPIO Interrupt Polarity"]
pub type GPIO2_INT_POL_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `LED0_EN` reader - Led Enable"]
pub type LED0_EN_R = crate::BitReader<bool>;
#[doc = "Field `LED0_EN` writer - Led Enable"]
pub type LED0_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `LED1_EN` reader - Led Enable"]
pub type LED1_EN_R = crate::BitReader<bool>;
#[doc = "Field `LED1_EN` writer - Led Enable"]
pub type LED1_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
#[doc = "Field `LED2_EN` reader - Led Enable"]
pub type LED2_EN_R = crate::BitReader<bool>;
#[doc = "Field `LED2_EN` writer - Led Enable"]
pub type LED2_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, GPIO_CFG_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - GPIO Data"]
    #[inline(always)]
    pub fn gpiod0(&self) -> GPIOD0_R {
        GPIOD0_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - GPIO Data"]
    #[inline(always)]
    pub fn gpiod1(&self) -> GPIOD1_R {
        GPIOD1_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - GPIO Data"]
    #[inline(always)]
    pub fn gpiod2(&self) -> GPIOD2_R {
        GPIOD2_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - GPO Data"]
    #[inline(always)]
    pub fn gpod3(&self) -> GPOD3_R {
        GPOD3_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - GPO Data"]
    #[inline(always)]
    pub fn gpod4(&self) -> GPOD4_R {
        GPOD4_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 8 - GPIO Direction"]
    #[inline(always)]
    pub fn gpdir0(&self) -> GPDIR0_R {
        GPDIR0_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9 - GPIO Direction"]
    #[inline(always)]
    pub fn gpdir1(&self) -> GPDIR1_R {
        GPDIR1_R::new(((self.bits >> 9) & 1) != 0)
    }
    #[doc = "Bit 10 - GPIO Direction"]
    #[inline(always)]
    pub fn gpdir2(&self) -> GPDIR2_R {
        GPDIR2_R::new(((self.bits >> 10) & 1) != 0)
    }
    #[doc = "Bit 16 - GPIO Buffer Type"]
    #[inline(always)]
    pub fn gpiobuf0(&self) -> GPIOBUF0_R {
        GPIOBUF0_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - GPIO Buffer Type"]
    #[inline(always)]
    pub fn gpiobuf1(&self) -> GPIOBUF1_R {
        GPIOBUF1_R::new(((self.bits >> 17) & 1) != 0)
    }
    #[doc = "Bit 18 - GPIO Buffer Type"]
    #[inline(always)]
    pub fn gpiobuf2(&self) -> GPIOBUF2_R {
        GPIOBUF2_R::new(((self.bits >> 18) & 1) != 0)
    }
    #[doc = "Bits 20:22 - EEPROM Enable"]
    #[inline(always)]
    pub fn eepr_en(&self) -> EEPR_EN_R {
        EEPR_EN_R::new(((self.bits >> 20) & 7) as u8)
    }
    #[doc = "Bit 24 - GPIO Interrupt Polarity"]
    #[inline(always)]
    pub fn gpio0_int_pol(&self) -> GPIO0_INT_POL_R {
        GPIO0_INT_POL_R::new(((self.bits >> 24) & 1) != 0)
    }
    #[doc = "Bit 25 - GPIO Interrupt Polarity"]
    #[inline(always)]
    pub fn gpio1_int_pol(&self) -> GPIO1_INT_POL_R {
        GPIO1_INT_POL_R::new(((self.bits >> 25) & 1) != 0)
    }
    #[doc = "Bit 26 - GPIO Interrupt Polarity"]
    #[inline(always)]
    pub fn gpio2_int_pol(&self) -> GPIO2_INT_POL_R {
        GPIO2_INT_POL_R::new(((self.bits >> 26) & 1) != 0)
    }
    #[doc = "Bit 28 - Led Enable"]
    #[inline(always)]
    pub fn led0_en(&self) -> LED0_EN_R {
        LED0_EN_R::new(((self.bits >> 28) & 1) != 0)
    }
    #[doc = "Bit 29 - Led Enable"]
    #[inline(always)]
    pub fn led1_en(&self) -> LED1_EN_R {
        LED1_EN_R::new(((self.bits >> 29) & 1) != 0)
    }
    #[doc = "Bit 30 - Led Enable"]
    #[inline(always)]
    pub fn led2_en(&self) -> LED2_EN_R {
        LED2_EN_R::new(((self.bits >> 30) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - GPIO Data"]
    #[inline(always)]
    pub fn gpiod0(&mut self) -> GPIOD0_W<0> {
        GPIOD0_W::new(self)
    }
    #[doc = "Bit 1 - GPIO Data"]
    #[inline(always)]
    pub fn gpiod1(&mut self) -> GPIOD1_W<1> {
        GPIOD1_W::new(self)
    }
    #[doc = "Bit 2 - GPIO Data"]
    #[inline(always)]
    pub fn gpiod2(&mut self) -> GPIOD2_W<2> {
        GPIOD2_W::new(self)
    }
    #[doc = "Bit 3 - GPO Data"]
    #[inline(always)]
    pub fn gpod3(&mut self) -> GPOD3_W<3> {
        GPOD3_W::new(self)
    }
    #[doc = "Bit 4 - GPO Data"]
    #[inline(always)]
    pub fn gpod4(&mut self) -> GPOD4_W<4> {
        GPOD4_W::new(self)
    }
    #[doc = "Bit 8 - GPIO Direction"]
    #[inline(always)]
    pub fn gpdir0(&mut self) -> GPDIR0_W<8> {
        GPDIR0_W::new(self)
    }
    #[doc = "Bit 9 - GPIO Direction"]
    #[inline(always)]
    pub fn gpdir1(&mut self) -> GPDIR1_W<9> {
        GPDIR1_W::new(self)
    }
    #[doc = "Bit 10 - GPIO Direction"]
    #[inline(always)]
    pub fn gpdir2(&mut self) -> GPDIR2_W<10> {
        GPDIR2_W::new(self)
    }
    #[doc = "Bit 16 - GPIO Buffer Type"]
    #[inline(always)]
    pub fn gpiobuf0(&mut self) -> GPIOBUF0_W<16> {
        GPIOBUF0_W::new(self)
    }
    #[doc = "Bit 17 - GPIO Buffer Type"]
    #[inline(always)]
    pub fn gpiobuf1(&mut self) -> GPIOBUF1_W<17> {
        GPIOBUF1_W::new(self)
    }
    #[doc = "Bit 18 - GPIO Buffer Type"]
    #[inline(always)]
    pub fn gpiobuf2(&mut self) -> GPIOBUF2_W<18> {
        GPIOBUF2_W::new(self)
    }
    #[doc = "Bits 20:22 - EEPROM Enable"]
    #[inline(always)]
    pub fn eepr_en(&mut self) -> EEPR_EN_W<20> {
        EEPR_EN_W::new(self)
    }
    #[doc = "Bit 24 - GPIO Interrupt Polarity"]
    #[inline(always)]
    pub fn gpio0_int_pol(&mut self) -> GPIO0_INT_POL_W<24> {
        GPIO0_INT_POL_W::new(self)
    }
    #[doc = "Bit 25 - GPIO Interrupt Polarity"]
    #[inline(always)]
    pub fn gpio1_int_pol(&mut self) -> GPIO1_INT_POL_W<25> {
        GPIO1_INT_POL_W::new(self)
    }
    #[doc = "Bit 26 - GPIO Interrupt Polarity"]
    #[inline(always)]
    pub fn gpio2_int_pol(&mut self) -> GPIO2_INT_POL_W<26> {
        GPIO2_INT_POL_W::new(self)
    }
    #[doc = "Bit 28 - Led Enable"]
    #[inline(always)]
    pub fn led0_en(&mut self) -> LED0_EN_W<28> {
        LED0_EN_W::new(self)
    }
    #[doc = "Bit 29 - Led Enable"]
    #[inline(always)]
    pub fn led1_en(&mut self) -> LED1_EN_W<29> {
        LED1_EN_W::new(self)
    }
    #[doc = "Bit 30 - Led Enable"]
    #[inline(always)]
    pub fn led2_en(&mut self) -> LED2_EN_W<30> {
        LED2_EN_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "General Purpose IO Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [gpio_cfg](index.html) module"]
pub struct GPIO_CFG_SPEC;
impl crate::RegisterSpec for GPIO_CFG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [gpio_cfg::R](R) reader structure"]
impl crate::Readable for GPIO_CFG_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [gpio_cfg::W](W) writer structure"]
impl crate::Writable for GPIO_CFG_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets GPIO_CFG to value 0"]
impl crate::Resettable for GPIO_CFG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
