// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `AFC_CFG` reader"]
pub struct R(crate::R<AFC_CFG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<AFC_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<AFC_CFG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<AFC_CFG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `AFC_CFG` writer"]
pub struct W(crate::W<AFC_CFG_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<AFC_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<AFC_CFG_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<AFC_CFG_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `FCANY` reader - Flow Control on Any Frame"]
pub type FCANY_R = crate::BitReader<bool>;
#[doc = "Field `FCANY` writer - Flow Control on Any Frame"]
pub type FCANY_W<'a, const O: u8> = crate::BitWriter<'a, u32, AFC_CFG_SPEC, bool, O>;
#[doc = "Field `FCADD` reader - Flow Control on Address Decode"]
pub type FCADD_R = crate::BitReader<bool>;
#[doc = "Field `FCADD` writer - Flow Control on Address Decode"]
pub type FCADD_W<'a, const O: u8> = crate::BitWriter<'a, u32, AFC_CFG_SPEC, bool, O>;
#[doc = "Field `FCBRD` reader - Flow Control on Broadcast Frame"]
pub type FCBRD_R = crate::BitReader<bool>;
#[doc = "Field `FCBRD` writer - Flow Control on Broadcast Frame"]
pub type FCBRD_W<'a, const O: u8> = crate::BitWriter<'a, u32, AFC_CFG_SPEC, bool, O>;
#[doc = "Field `FCMULT` reader - Flow Control on Multicast Frame"]
pub type FCMULT_R = crate::BitReader<bool>;
#[doc = "Field `FCMULT` writer - Flow Control on Multicast Frame"]
pub type FCMULT_W<'a, const O: u8> = crate::BitWriter<'a, u32, AFC_CFG_SPEC, bool, O>;
#[doc = "Field `BACK_DUR` reader - Backpresure duration"]
pub type BACK_DUR_R = crate::FieldReader<u8, u8>;
#[doc = "Field `BACK_DUR` writer - Backpresure duration"]
pub type BACK_DUR_W<'a, const O: u8> = crate::FieldWriter<'a, u32, AFC_CFG_SPEC, u8, u8, 4, O>;
#[doc = "Field `AFC_LO` reader - Automatic Flow Control Low Level"]
pub type AFC_LO_R = crate::FieldReader<u8, u8>;
#[doc = "Field `AFC_LO` writer - Automatic Flow Control Low Level"]
pub type AFC_LO_W<'a, const O: u8> = crate::FieldWriter<'a, u32, AFC_CFG_SPEC, u8, u8, 8, O>;
#[doc = "Field `AFC_HI` reader - Automatic Flow Control High Level"]
pub type AFC_HI_R = crate::FieldReader<u8, u8>;
#[doc = "Field `AFC_HI` writer - Automatic Flow Control High Level"]
pub type AFC_HI_W<'a, const O: u8> = crate::FieldWriter<'a, u32, AFC_CFG_SPEC, u8, u8, 8, O>;
impl R {
    #[doc = "Bit 0 - Flow Control on Any Frame"]
    #[inline(always)]
    pub fn fcany(&self) -> FCANY_R {
        FCANY_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Flow Control on Address Decode"]
    #[inline(always)]
    pub fn fcadd(&self) -> FCADD_R {
        FCADD_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Flow Control on Broadcast Frame"]
    #[inline(always)]
    pub fn fcbrd(&self) -> FCBRD_R {
        FCBRD_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Flow Control on Multicast Frame"]
    #[inline(always)]
    pub fn fcmult(&self) -> FCMULT_R {
        FCMULT_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bits 4:7 - Backpresure duration"]
    #[inline(always)]
    pub fn back_dur(&self) -> BACK_DUR_R {
        BACK_DUR_R::new(((self.bits >> 4) & 0x0f) as u8)
    }
    #[doc = "Bits 8:15 - Automatic Flow Control Low Level"]
    #[inline(always)]
    pub fn afc_lo(&self) -> AFC_LO_R {
        AFC_LO_R::new(((self.bits >> 8) & 0xff) as u8)
    }
    #[doc = "Bits 16:23 - Automatic Flow Control High Level"]
    #[inline(always)]
    pub fn afc_hi(&self) -> AFC_HI_R {
        AFC_HI_R::new(((self.bits >> 16) & 0xff) as u8)
    }
}
impl W {
    #[doc = "Bit 0 - Flow Control on Any Frame"]
    #[inline(always)]
    pub fn fcany(&mut self) -> FCANY_W<0> {
        FCANY_W::new(self)
    }
    #[doc = "Bit 1 - Flow Control on Address Decode"]
    #[inline(always)]
    pub fn fcadd(&mut self) -> FCADD_W<1> {
        FCADD_W::new(self)
    }
    #[doc = "Bit 2 - Flow Control on Broadcast Frame"]
    #[inline(always)]
    pub fn fcbrd(&mut self) -> FCBRD_W<2> {
        FCBRD_W::new(self)
    }
    #[doc = "Bit 3 - Flow Control on Multicast Frame"]
    #[inline(always)]
    pub fn fcmult(&mut self) -> FCMULT_W<3> {
        FCMULT_W::new(self)
    }
    #[doc = "Bits 4:7 - Backpresure duration"]
    #[inline(always)]
    pub fn back_dur(&mut self) -> BACK_DUR_W<4> {
        BACK_DUR_W::new(self)
    }
    #[doc = "Bits 8:15 - Automatic Flow Control Low Level"]
    #[inline(always)]
    pub fn afc_lo(&mut self) -> AFC_LO_W<8> {
        AFC_LO_W::new(self)
    }
    #[doc = "Bits 16:23 - Automatic Flow Control High Level"]
    #[inline(always)]
    pub fn afc_hi(&mut self) -> AFC_HI_W<16> {
        AFC_HI_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Automatic Flow Control Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [afc_cfg](index.html) module"]
pub struct AFC_CFG_SPEC;
impl crate::RegisterSpec for AFC_CFG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [afc_cfg::R](R) reader structure"]
impl crate::Readable for AFC_CFG_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [afc_cfg::W](W) writer structure"]
impl crate::Writable for AFC_CFG_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets AFC_CFG to value 0"]
impl crate::Resettable for AFC_CFG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
