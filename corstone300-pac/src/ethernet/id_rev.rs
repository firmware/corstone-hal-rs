// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ID_REV` reader"]
pub struct R(crate::R<ID_REV_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ID_REV_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ID_REV_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ID_REV_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `REV` reader - "]
pub type REV_R = crate::FieldReader<u16, u16>;
#[doc = "Field `ID` reader - "]
pub type ID_R = crate::FieldReader<u16, u16>;
impl R {
    #[doc = "Bits 0:15"]
    #[inline(always)]
    pub fn rev(&self) -> REV_R {
        REV_R::new((self.bits & 0xffff) as u16)
    }
    #[doc = "Bits 16:31"]
    #[inline(always)]
    pub fn id(&self) -> ID_R {
        ID_R::new(((self.bits >> 16) & 0xffff) as u16)
    }
}
#[doc = "ID and Revision\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [id_rev](index.html) module"]
pub struct ID_REV_SPEC;
impl crate::RegisterSpec for ID_REV_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [id_rev::R](R) reader structure"]
impl crate::Readable for ID_REV_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets ID_REV to value 0x9220_0000"]
impl crate::Resettable for ID_REV_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x9220_0000
    }
}
