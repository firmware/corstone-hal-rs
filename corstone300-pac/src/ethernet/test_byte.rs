// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `TEST_BYTE` reader"]
pub struct R(crate::R<TEST_BYTE_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<TEST_BYTE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<TEST_BYTE_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<TEST_BYTE_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Byte Order Test Register\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [test_byte](index.html) module"]
pub struct TEST_BYTE_SPEC;
impl crate::RegisterSpec for TEST_BYTE_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [test_byte::R](R) reader structure"]
impl crate::Readable for TEST_BYTE_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets TEST_BYTE to value 0x8765_4321"]
impl crate::Resettable for TEST_BYTE_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x8765_4321
    }
}
