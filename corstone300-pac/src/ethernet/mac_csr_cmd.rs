// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `MAC_CSR_CMD` reader"]
pub struct R(crate::R<MAC_CSR_CMD_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<MAC_CSR_CMD_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<MAC_CSR_CMD_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<MAC_CSR_CMD_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `MAC_CSR_CMD` writer"]
pub struct W(crate::W<MAC_CSR_CMD_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<MAC_CSR_CMD_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<MAC_CSR_CMD_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<MAC_CSR_CMD_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `Address` reader - CSR Address"]
pub type ADDRESS_R = crate::FieldReader<u8, u8>;
#[doc = "Field `Address` writer - CSR Address"]
pub type ADDRESS_W<'a, const O: u8> = crate::FieldWriter<'a, u32, MAC_CSR_CMD_SPEC, u8, u8, 8, O>;
#[doc = "Field `RnW` reader - R/nW"]
pub type RN_W_R = crate::BitReader<bool>;
#[doc = "Field `RnW` writer - R/nW"]
pub type RN_W_W<'a, const O: u8> = crate::BitWriter<'a, u32, MAC_CSR_CMD_SPEC, bool, O>;
#[doc = "Field `BSY` reader - CSR Busy"]
pub type BSY_R = crate::BitReader<bool>;
#[doc = "Field `BSY` writer - CSR Busy"]
pub type BSY_W<'a, const O: u8> = crate::BitWriter<'a, u32, MAC_CSR_CMD_SPEC, bool, O>;
impl R {
    #[doc = "Bits 0:7 - CSR Address"]
    #[inline(always)]
    pub fn address(&self) -> ADDRESS_R {
        ADDRESS_R::new((self.bits & 0xff) as u8)
    }
    #[doc = "Bit 30 - R/nW"]
    #[inline(always)]
    pub fn rn_w(&self) -> RN_W_R {
        RN_W_R::new(((self.bits >> 30) & 1) != 0)
    }
    #[doc = "Bit 31 - CSR Busy"]
    #[inline(always)]
    pub fn bsy(&self) -> BSY_R {
        BSY_R::new(((self.bits >> 31) & 1) != 0)
    }
}
impl W {
    #[doc = "Bits 0:7 - CSR Address"]
    #[inline(always)]
    pub fn address(&mut self) -> ADDRESS_W<0> {
        ADDRESS_W::new(self)
    }
    #[doc = "Bit 30 - R/nW"]
    #[inline(always)]
    pub fn rn_w(&mut self) -> RN_W_W<30> {
        RN_W_W::new(self)
    }
    #[doc = "Bit 31 - CSR Busy"]
    #[inline(always)]
    pub fn bsy(&mut self) -> BSY_W<31> {
        BSY_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "MAC CSR Synchronizer Command\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [mac_csr_cmd](index.html) module"]
pub struct MAC_CSR_CMD_SPEC;
impl crate::RegisterSpec for MAC_CSR_CMD_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [mac_csr_cmd::R](R) reader structure"]
impl crate::Readable for MAC_CSR_CMD_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [mac_csr_cmd::W](W) writer structure"]
impl crate::Writable for MAC_CSR_CMD_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets MAC_CSR_CMD to value 0"]
impl crate::Resettable for MAC_CSR_CMD_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
