// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `HW_CFG` reader"]
pub struct R(crate::R<HW_CFG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<HW_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<HW_CFG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<HW_CFG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `HW_CFG` writer"]
pub struct W(crate::W<HW_CFG_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<HW_CFG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<HW_CFG_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<HW_CFG_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `SRST` reader - Soft Reset"]
pub type SRST_R = crate::BitReader<bool>;
#[doc = "Field `SRST` writer - Soft Reset"]
pub type SRST_W<'a, const O: u8> = crate::BitWriter<'a, u32, HW_CFG_SPEC, bool, O>;
#[doc = "Field `SRST_TO` reader - Soft Reset Timeout"]
pub type SRST_TO_R = crate::BitReader<bool>;
#[doc = "Field `TX_FIF_SZ` reader - TX FIFO Size"]
pub type TX_FIF_SZ_R = crate::FieldReader<u8, u8>;
#[doc = "Field `TX_FIF_SZ` writer - TX FIFO Size"]
pub type TX_FIF_SZ_W<'a, const O: u8> = crate::FieldWriter<'a, u32, HW_CFG_SPEC, u8, u8, 4, O>;
#[doc = "Field `MBO` reader - Must Be One"]
pub type MBO_R = crate::BitReader<MBO_A>;
#[doc = "Must Be One\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MBO_A {
    #[doc = "1: `1`"]
    ONE = 1,
}
impl From<MBO_A> for bool {
    #[inline(always)]
    fn from(variant: MBO_A) -> Self {
        variant as u8 != 0
    }
}
impl MBO_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<MBO_A> {
        match self.bits {
            true => Some(MBO_A::ONE),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `ONE`"]
    #[inline(always)]
    pub fn is_one(&self) -> bool {
        *self == MBO_A::ONE
    }
}
#[doc = "Field `MBO` writer - Must Be One"]
pub type MBO_W<'a, const O: u8> = crate::BitWriter<'a, u32, HW_CFG_SPEC, MBO_A, O>;
impl<'a, const O: u8> MBO_W<'a, O> {
    #[doc = "`1`"]
    #[inline(always)]
    pub fn one(self) -> &'a mut W {
        self.variant(MBO_A::ONE)
    }
}
#[doc = "Field `AMDIX_EN` reader - AMDIX_EN Strap state"]
pub type AMDIX_EN_R = crate::BitReader<bool>;
#[doc = "Field `AMDIX_EN` writer - AMDIX_EN Strap state"]
pub type AMDIX_EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, HW_CFG_SPEC, bool, O>;
#[doc = "Field `FSELEND` reader - Direct FIFO Access Endian Ordering"]
pub type FSELEND_R = crate::BitReader<FSELEND_A>;
#[doc = "Direct FIFO Access Endian Ordering\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum FSELEND_A {
    #[doc = "0: Little Endian"]
    LE = 0,
    #[doc = "1: Big Endian"]
    BE = 1,
}
impl From<FSELEND_A> for bool {
    #[inline(always)]
    fn from(variant: FSELEND_A) -> Self {
        variant as u8 != 0
    }
}
impl FSELEND_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> FSELEND_A {
        match self.bits {
            false => FSELEND_A::LE,
            true => FSELEND_A::BE,
        }
    }
    #[doc = "Checks if the value of the field is `LE`"]
    #[inline(always)]
    pub fn is_le(&self) -> bool {
        *self == FSELEND_A::LE
    }
    #[doc = "Checks if the value of the field is `BE`"]
    #[inline(always)]
    pub fn is_be(&self) -> bool {
        *self == FSELEND_A::BE
    }
}
#[doc = "Field `FSELEND` writer - Direct FIFO Access Endian Ordering"]
pub type FSELEND_W<'a, const O: u8> = crate::BitWriter<'a, u32, HW_CFG_SPEC, FSELEND_A, O>;
impl<'a, const O: u8> FSELEND_W<'a, O> {
    #[doc = "Little Endian"]
    #[inline(always)]
    pub fn le(self) -> &'a mut W {
        self.variant(FSELEND_A::LE)
    }
    #[doc = "Big Endian"]
    #[inline(always)]
    pub fn be(self) -> &'a mut W {
        self.variant(FSELEND_A::BE)
    }
}
#[doc = "Field `FPORTEND` reader - FIFO Port Endian Ordering"]
pub type FPORTEND_R = crate::BitReader<FPORTEND_A>;
#[doc = "FIFO Port Endian Ordering\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum FPORTEND_A {
    #[doc = "0: Little Endian"]
    LE = 0,
    #[doc = "1: Big Endian"]
    BE = 1,
}
impl From<FPORTEND_A> for bool {
    #[inline(always)]
    fn from(variant: FPORTEND_A) -> Self {
        variant as u8 != 0
    }
}
impl FPORTEND_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> FPORTEND_A {
        match self.bits {
            false => FPORTEND_A::LE,
            true => FPORTEND_A::BE,
        }
    }
    #[doc = "Checks if the value of the field is `LE`"]
    #[inline(always)]
    pub fn is_le(&self) -> bool {
        *self == FPORTEND_A::LE
    }
    #[doc = "Checks if the value of the field is `BE`"]
    #[inline(always)]
    pub fn is_be(&self) -> bool {
        *self == FPORTEND_A::BE
    }
}
#[doc = "Field `FPORTEND` writer - FIFO Port Endian Ordering"]
pub type FPORTEND_W<'a, const O: u8> = crate::BitWriter<'a, u32, HW_CFG_SPEC, FPORTEND_A, O>;
impl<'a, const O: u8> FPORTEND_W<'a, O> {
    #[doc = "Little Endian"]
    #[inline(always)]
    pub fn le(self) -> &'a mut W {
        self.variant(FPORTEND_A::LE)
    }
    #[doc = "Big Endian"]
    #[inline(always)]
    pub fn be(self) -> &'a mut W {
        self.variant(FPORTEND_A::BE)
    }
}
impl R {
    #[doc = "Bit 0 - Soft Reset"]
    #[inline(always)]
    pub fn srst(&self) -> SRST_R {
        SRST_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Soft Reset Timeout"]
    #[inline(always)]
    pub fn srst_to(&self) -> SRST_TO_R {
        SRST_TO_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bits 16:19 - TX FIFO Size"]
    #[inline(always)]
    pub fn tx_fif_sz(&self) -> TX_FIF_SZ_R {
        TX_FIF_SZ_R::new(((self.bits >> 16) & 0x0f) as u8)
    }
    #[doc = "Bit 20 - Must Be One"]
    #[inline(always)]
    pub fn mbo(&self) -> MBO_R {
        MBO_R::new(((self.bits >> 20) & 1) != 0)
    }
    #[doc = "Bit 24 - AMDIX_EN Strap state"]
    #[inline(always)]
    pub fn amdix_en(&self) -> AMDIX_EN_R {
        AMDIX_EN_R::new(((self.bits >> 24) & 1) != 0)
    }
    #[doc = "Bit 28 - Direct FIFO Access Endian Ordering"]
    #[inline(always)]
    pub fn fselend(&self) -> FSELEND_R {
        FSELEND_R::new(((self.bits >> 28) & 1) != 0)
    }
    #[doc = "Bit 29 - FIFO Port Endian Ordering"]
    #[inline(always)]
    pub fn fportend(&self) -> FPORTEND_R {
        FPORTEND_R::new(((self.bits >> 29) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Soft Reset"]
    #[inline(always)]
    pub fn srst(&mut self) -> SRST_W<0> {
        SRST_W::new(self)
    }
    #[doc = "Bits 16:19 - TX FIFO Size"]
    #[inline(always)]
    pub fn tx_fif_sz(&mut self) -> TX_FIF_SZ_W<16> {
        TX_FIF_SZ_W::new(self)
    }
    #[doc = "Bit 20 - Must Be One"]
    #[inline(always)]
    pub fn mbo(&mut self) -> MBO_W<20> {
        MBO_W::new(self)
    }
    #[doc = "Bit 24 - AMDIX_EN Strap state"]
    #[inline(always)]
    pub fn amdix_en(&mut self) -> AMDIX_EN_W<24> {
        AMDIX_EN_W::new(self)
    }
    #[doc = "Bit 28 - Direct FIFO Access Endian Ordering"]
    #[inline(always)]
    pub fn fselend(&mut self) -> FSELEND_W<28> {
        FSELEND_W::new(self)
    }
    #[doc = "Bit 29 - FIFO Port Endian Ordering"]
    #[inline(always)]
    pub fn fportend(&mut self) -> FPORTEND_W<29> {
        FPORTEND_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Hardware Configuration\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [hw_cfg](index.html) module"]
pub struct HW_CFG_SPEC;
impl crate::RegisterSpec for HW_CFG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [hw_cfg::R](R) reader structure"]
impl crate::Readable for HW_CFG_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [hw_cfg::W](W) writer structure"]
impl crate::Writable for HW_CFG_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets HW_CFG to value 0x0005_0000"]
impl crate::Resettable for HW_CFG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x0005_0000
    }
}
