// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RX_DP_CTL` reader"]
pub struct R(crate::R<RX_DP_CTL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RX_DP_CTL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RX_DP_CTL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RX_DP_CTL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `RX_FFWD` reader - RX Data FIFO Fast Forward"]
pub type RX_FFWD_R = crate::BitReader<bool>;
impl R {
    #[doc = "Bit 31 - RX Data FIFO Fast Forward"]
    #[inline(always)]
    pub fn rx_ffwd(&self) -> RX_FFWD_R {
        RX_FFWD_R::new(((self.bits >> 31) & 1) != 0)
    }
}
#[doc = "RX Datapath Control\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [rx_dp_ctl](index.html) module"]
pub struct RX_DP_CTL_SPEC;
impl crate::RegisterSpec for RX_DP_CTL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [rx_dp_ctl::R](R) reader structure"]
impl crate::Readable for RX_DP_CTL_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets RX_DP_CTL to value 0"]
impl crate::Resettable for RX_DP_CTL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
