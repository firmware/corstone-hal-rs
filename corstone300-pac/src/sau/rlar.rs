// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RLAR` reader"]
pub struct R(crate::R<RLAR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RLAR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RLAR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RLAR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `RLAR` writer"]
pub struct W(crate::W<RLAR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<RLAR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<RLAR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<RLAR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `ENABLE` reader - SAU Region enabled"]
pub type ENABLE_R = crate::BitReader<bool>;
#[doc = "Field `ENABLE` writer - SAU Region enabled"]
pub type ENABLE_W<'a, const O: u8> = crate::BitWriter<'a, u32, RLAR_SPEC, bool, O>;
#[doc = "Field `NSC` reader - Non-Secure Callable"]
pub type NSC_R = crate::BitReader<bool>;
#[doc = "Field `NSC` writer - Non-Secure Callable"]
pub type NSC_W<'a, const O: u8> = crate::BitWriter<'a, u32, RLAR_SPEC, bool, O>;
#[doc = "Field `LADDR` reader - Limit Address"]
pub type LADDR_R = crate::FieldReader<u32, u32>;
#[doc = "Field `LADDR` writer - Limit Address"]
pub type LADDR_W<'a, const O: u8> = crate::FieldWriter<'a, u32, RLAR_SPEC, u32, u32, 27, O>;
impl R {
    #[doc = "Bit 0 - SAU Region enabled"]
    #[inline(always)]
    pub fn enable(&self) -> ENABLE_R {
        ENABLE_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Non-Secure Callable"]
    #[inline(always)]
    pub fn nsc(&self) -> NSC_R {
        NSC_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bits 5:31 - Limit Address"]
    #[inline(always)]
    pub fn laddr(&self) -> LADDR_R {
        LADDR_R::new(((self.bits >> 5) & 0x07ff_ffff) as u32)
    }
}
impl W {
    #[doc = "Bit 0 - SAU Region enabled"]
    #[inline(always)]
    pub fn enable(&mut self) -> ENABLE_W<0> {
        ENABLE_W::new(self)
    }
    #[doc = "Bit 1 - Non-Secure Callable"]
    #[inline(always)]
    pub fn nsc(&mut self) -> NSC_W<1> {
        NSC_W::new(self)
    }
    #[doc = "Bits 5:31 - Limit Address"]
    #[inline(always)]
    pub fn laddr(&mut self) -> LADDR_W<5> {
        LADDR_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Region Limit Address Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [rlar](index.html) module"]
pub struct RLAR_SPEC;
impl crate::RegisterSpec for RLAR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [rlar::R](R) reader structure"]
impl crate::Readable for RLAR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [rlar::W](W) writer structure"]
impl crate::Writable for RLAR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets RLAR to value 0"]
impl crate::Resettable for RLAR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
