// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RNR` reader"]
pub struct R(crate::R<RNR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RNR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RNR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RNR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `RNR` writer"]
pub struct W(crate::W<RNR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<RNR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<RNR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<RNR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `REGION` reader - Currently selected SAU region"]
pub type REGION_R = crate::FieldReader<u8, REGION_A>;
#[doc = "Currently selected SAU region\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum REGION_A {
    #[doc = "0: Select SAU Region 0"]
    SAU_REGION_0 = 0,
    #[doc = "1: Select SAU Region 1"]
    SAU_REGION_1 = 1,
    #[doc = "2: Select SAU Region 2"]
    SAU_REGION_2 = 2,
    #[doc = "3: Select SAU Region 3"]
    SAU_REGION_3 = 3,
}
impl From<REGION_A> for u8 {
    #[inline(always)]
    fn from(variant: REGION_A) -> Self {
        variant as _
    }
}
impl REGION_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<REGION_A> {
        match self.bits {
            0 => Some(REGION_A::SAU_REGION_0),
            1 => Some(REGION_A::SAU_REGION_1),
            2 => Some(REGION_A::SAU_REGION_2),
            3 => Some(REGION_A::SAU_REGION_3),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `SAU_REGION_0`"]
    #[inline(always)]
    pub fn is_sau_region_0(&self) -> bool {
        *self == REGION_A::SAU_REGION_0
    }
    #[doc = "Checks if the value of the field is `SAU_REGION_1`"]
    #[inline(always)]
    pub fn is_sau_region_1(&self) -> bool {
        *self == REGION_A::SAU_REGION_1
    }
    #[doc = "Checks if the value of the field is `SAU_REGION_2`"]
    #[inline(always)]
    pub fn is_sau_region_2(&self) -> bool {
        *self == REGION_A::SAU_REGION_2
    }
    #[doc = "Checks if the value of the field is `SAU_REGION_3`"]
    #[inline(always)]
    pub fn is_sau_region_3(&self) -> bool {
        *self == REGION_A::SAU_REGION_3
    }
}
#[doc = "Field `REGION` writer - Currently selected SAU region"]
pub type REGION_W<'a, const O: u8> = crate::FieldWriter<'a, u32, RNR_SPEC, u8, REGION_A, 8, O>;
impl<'a, const O: u8> REGION_W<'a, O> {
    #[doc = "Select SAU Region 0"]
    #[inline(always)]
    pub fn sau_region_0(self) -> &'a mut W {
        self.variant(REGION_A::SAU_REGION_0)
    }
    #[doc = "Select SAU Region 1"]
    #[inline(always)]
    pub fn sau_region_1(self) -> &'a mut W {
        self.variant(REGION_A::SAU_REGION_1)
    }
    #[doc = "Select SAU Region 2"]
    #[inline(always)]
    pub fn sau_region_2(self) -> &'a mut W {
        self.variant(REGION_A::SAU_REGION_2)
    }
    #[doc = "Select SAU Region 3"]
    #[inline(always)]
    pub fn sau_region_3(self) -> &'a mut W {
        self.variant(REGION_A::SAU_REGION_3)
    }
}
impl R {
    #[doc = "Bits 0:7 - Currently selected SAU region"]
    #[inline(always)]
    pub fn region(&self) -> REGION_R {
        REGION_R::new((self.bits & 0xff) as u8)
    }
}
impl W {
    #[doc = "Bits 0:7 - Currently selected SAU region"]
    #[inline(always)]
    pub fn region(&mut self) -> REGION_W<0> {
        REGION_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Region Number Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [rnr](index.html) module"]
pub struct RNR_SPEC;
impl crate::RegisterSpec for RNR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [rnr::R](R) reader structure"]
impl crate::Readable for RNR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [rnr::W](W) writer structure"]
impl crate::Writable for RNR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets RNR to value 0"]
impl crate::Resettable for RNR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
