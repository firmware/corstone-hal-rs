// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SFSR` reader"]
pub struct R(crate::R<SFSR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SFSR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SFSR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SFSR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `SFSR` writer"]
pub struct W(crate::W<SFSR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<SFSR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<SFSR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<SFSR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `INVEP` reader - Invalid entry pointd"]
pub type INVEP_R = crate::BitReader<bool>;
#[doc = "Field `INVEP` writer - Invalid entry pointd"]
pub type INVEP_W<'a, const O: u8> = crate::BitWriter<'a, u32, SFSR_SPEC, bool, O>;
#[doc = "Field `INVIS` reader - Invalid integrity signature flag"]
pub type INVIS_R = crate::BitReader<bool>;
#[doc = "Field `INVIS` writer - Invalid integrity signature flag"]
pub type INVIS_W<'a, const O: u8> = crate::BitWriter<'a, u32, SFSR_SPEC, bool, O>;
#[doc = "Field `INVER` reader - Invalid exception return flag"]
pub type INVER_R = crate::BitReader<bool>;
#[doc = "Field `INVER` writer - Invalid exception return flag"]
pub type INVER_W<'a, const O: u8> = crate::BitWriter<'a, u32, SFSR_SPEC, bool, O>;
#[doc = "Field `AUVIOL` reader - Attribution unit violation flag"]
pub type AUVIOL_R = crate::BitReader<bool>;
#[doc = "Field `AUVIOL` writer - Attribution unit violation flag"]
pub type AUVIOL_W<'a, const O: u8> = crate::BitWriter<'a, u32, SFSR_SPEC, bool, O>;
#[doc = "Field `INVTRAN` reader - Invalid transition flag"]
pub type INVTRAN_R = crate::BitReader<bool>;
#[doc = "Field `INVTRAN` writer - Invalid transition flag"]
pub type INVTRAN_W<'a, const O: u8> = crate::BitWriter<'a, u32, SFSR_SPEC, bool, O>;
#[doc = "Field `LSPERR` reader - Lazy state preservation error flag"]
pub type LSPERR_R = crate::BitReader<bool>;
#[doc = "Field `LSPERR` writer - Lazy state preservation error flag"]
pub type LSPERR_W<'a, const O: u8> = crate::BitWriter<'a, u32, SFSR_SPEC, bool, O>;
#[doc = "Field `SFARVALID` reader - Secure fault address valid"]
pub type SFARVALID_R = crate::BitReader<bool>;
#[doc = "Field `SFARVALID` writer - Secure fault address valid"]
pub type SFARVALID_W<'a, const O: u8> = crate::BitWriter<'a, u32, SFSR_SPEC, bool, O>;
#[doc = "Field `LSERR` reader - Lazy state error flag"]
pub type LSERR_R = crate::BitReader<bool>;
#[doc = "Field `LSERR` writer - Lazy state error flag"]
pub type LSERR_W<'a, const O: u8> = crate::BitWriter<'a, u32, SFSR_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Invalid entry pointd"]
    #[inline(always)]
    pub fn invep(&self) -> INVEP_R {
        INVEP_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Invalid integrity signature flag"]
    #[inline(always)]
    pub fn invis(&self) -> INVIS_R {
        INVIS_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Invalid exception return flag"]
    #[inline(always)]
    pub fn inver(&self) -> INVER_R {
        INVER_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Attribution unit violation flag"]
    #[inline(always)]
    pub fn auviol(&self) -> AUVIOL_R {
        AUVIOL_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - Invalid transition flag"]
    #[inline(always)]
    pub fn invtran(&self) -> INVTRAN_R {
        INVTRAN_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5 - Lazy state preservation error flag"]
    #[inline(always)]
    pub fn lsperr(&self) -> LSPERR_R {
        LSPERR_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6 - Secure fault address valid"]
    #[inline(always)]
    pub fn sfarvalid(&self) -> SFARVALID_R {
        SFARVALID_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - Lazy state error flag"]
    #[inline(always)]
    pub fn lserr(&self) -> LSERR_R {
        LSERR_R::new(((self.bits >> 7) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Invalid entry pointd"]
    #[inline(always)]
    pub fn invep(&mut self) -> INVEP_W<0> {
        INVEP_W::new(self)
    }
    #[doc = "Bit 1 - Invalid integrity signature flag"]
    #[inline(always)]
    pub fn invis(&mut self) -> INVIS_W<1> {
        INVIS_W::new(self)
    }
    #[doc = "Bit 2 - Invalid exception return flag"]
    #[inline(always)]
    pub fn inver(&mut self) -> INVER_W<2> {
        INVER_W::new(self)
    }
    #[doc = "Bit 3 - Attribution unit violation flag"]
    #[inline(always)]
    pub fn auviol(&mut self) -> AUVIOL_W<3> {
        AUVIOL_W::new(self)
    }
    #[doc = "Bit 4 - Invalid transition flag"]
    #[inline(always)]
    pub fn invtran(&mut self) -> INVTRAN_W<4> {
        INVTRAN_W::new(self)
    }
    #[doc = "Bit 5 - Lazy state preservation error flag"]
    #[inline(always)]
    pub fn lsperr(&mut self) -> LSPERR_W<5> {
        LSPERR_W::new(self)
    }
    #[doc = "Bit 6 - Secure fault address valid"]
    #[inline(always)]
    pub fn sfarvalid(&mut self) -> SFARVALID_W<6> {
        SFARVALID_W::new(self)
    }
    #[doc = "Bit 7 - Lazy state error flag"]
    #[inline(always)]
    pub fn lserr(&mut self) -> LSERR_W<7> {
        LSERR_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Secure Fault Status Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [sfsr](index.html) module"]
pub struct SFSR_SPEC;
impl crate::RegisterSpec for SFSR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [sfsr::R](R) reader structure"]
impl crate::Readable for SFSR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [sfsr::W](W) writer structure"]
impl crate::Writable for SFSR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets SFSR to value 0"]
impl crate::Resettable for SFSR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
