// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Control Register"]
    pub ctrl: CTRL,
    #[doc = "0x04 - Type Register"]
    pub type_: TYPE,
    #[doc = "0x08 - Region Number Register"]
    pub rnr: RNR,
    #[doc = "0x0c - Region Base Address Register"]
    pub rbar: RBAR,
    #[doc = "0x10 - Region Limit Address Register"]
    pub rlar: RLAR,
    #[doc = "0x14 - Secure Fault Status Register"]
    pub sfsr: SFSR,
}
#[doc = "CTRL (rw) register accessor: an alias for `Reg<CTRL_SPEC>`"]
pub type CTRL = crate::Reg<ctrl::CTRL_SPEC>;
#[doc = "Control Register"]
pub mod ctrl;
#[doc = "TYPE (r) register accessor: an alias for `Reg<TYPE_SPEC>`"]
pub type TYPE = crate::Reg<type_::TYPE_SPEC>;
#[doc = "Type Register"]
pub mod type_;
#[doc = "RNR (rw) register accessor: an alias for `Reg<RNR_SPEC>`"]
pub type RNR = crate::Reg<rnr::RNR_SPEC>;
#[doc = "Region Number Register"]
pub mod rnr;
#[doc = "RBAR (rw) register accessor: an alias for `Reg<RBAR_SPEC>`"]
pub type RBAR = crate::Reg<rbar::RBAR_SPEC>;
#[doc = "Region Base Address Register"]
pub mod rbar;
#[doc = "RLAR (rw) register accessor: an alias for `Reg<RLAR_SPEC>`"]
pub type RLAR = crate::Reg<rlar::RLAR_SPEC>;
#[doc = "Region Limit Address Register"]
pub mod rlar;
#[doc = "SFSR (rw) register accessor: an alias for `Reg<SFSR_SPEC>`"]
pub type SFSR = crate::Reg<sfsr::SFSR_SPEC>;
#[doc = "Secure Fault Status Register"]
pub mod sfsr;
