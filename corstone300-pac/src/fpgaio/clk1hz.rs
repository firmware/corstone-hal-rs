// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CLK1HZ` reader"]
pub struct R(crate::R<CLK1HZ_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CLK1HZ_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CLK1HZ_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CLK1HZ_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "1Hz Up Counter\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [clk1hz](index.html) module"]
pub struct CLK1HZ_SPEC;
impl crate::RegisterSpec for CLK1HZ_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [clk1hz::R](R) reader structure"]
impl crate::Readable for CLK1HZ_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CLK1HZ to value 0"]
impl crate::Resettable for CLK1HZ_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
