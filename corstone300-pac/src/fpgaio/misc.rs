// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `MISC` reader"]
pub struct R(crate::R<MISC_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<MISC_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<MISC_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<MISC_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `MISC` writer"]
pub struct W(crate::W<MISC_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<MISC_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<MISC_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<MISC_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `CLCD_CS` reader - "]
pub type CLCD_CS_R = crate::BitReader<bool>;
#[doc = "Field `CLCD_CS` writer - "]
pub type CLCD_CS_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
#[doc = "Field `SPI_nSS` reader - "]
pub type SPI_N_SS_R = crate::BitReader<bool>;
#[doc = "Field `SPI_nSS` writer - "]
pub type SPI_N_SS_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
#[doc = "Field `CLCD_RESET` reader - "]
pub type CLCD_RESET_R = crate::BitReader<bool>;
#[doc = "Field `CLCD_RESET` writer - "]
pub type CLCD_RESET_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
#[doc = "Field `CLCD_RS` reader - "]
pub type CLCD_RS_R = crate::BitReader<bool>;
#[doc = "Field `CLCD_RS` writer - "]
pub type CLCD_RS_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
#[doc = "Field `CLCD_RD` reader - "]
pub type CLCD_RD_R = crate::BitReader<bool>;
#[doc = "Field `CLCD_RD` writer - "]
pub type CLCD_RD_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
#[doc = "Field `CLCD_BL_CTRL` reader - "]
pub type CLCD_BL_CTRL_R = crate::BitReader<bool>;
#[doc = "Field `CLCD_BL_CTRL` writer - "]
pub type CLCD_BL_CTRL_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
#[doc = "Field `ADC_SPI_nCS` reader - "]
pub type ADC_SPI_N_CS_R = crate::BitReader<bool>;
#[doc = "Field `ADC_SPI_nCS` writer - "]
pub type ADC_SPI_N_CS_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
#[doc = "Field `SHIELD0_SPI_nCS` reader - "]
pub type SHIELD0_SPI_N_CS_R = crate::BitReader<bool>;
#[doc = "Field `SHIELD0_SPI_nCS` writer - "]
pub type SHIELD0_SPI_N_CS_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
#[doc = "Field `SHIELD1_SPI_nCS` reader - "]
pub type SHIELD1_SPI_N_CS_R = crate::BitReader<bool>;
#[doc = "Field `SHIELD1_SPI_nCS` writer - "]
pub type SHIELD1_SPI_N_CS_W<'a, const O: u8> = crate::BitWriter<'a, u32, MISC_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn clcd_cs(&self) -> CLCD_CS_R {
        CLCD_CS_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn spi_n_ss(&self) -> SPI_N_SS_R {
        SPI_N_SS_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 3"]
    #[inline(always)]
    pub fn clcd_reset(&self) -> CLCD_RESET_R {
        CLCD_RESET_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4"]
    #[inline(always)]
    pub fn clcd_rs(&self) -> CLCD_RS_R {
        CLCD_RS_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5"]
    #[inline(always)]
    pub fn clcd_rd(&self) -> CLCD_RD_R {
        CLCD_RD_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6"]
    #[inline(always)]
    pub fn clcd_bl_ctrl(&self) -> CLCD_BL_CTRL_R {
        CLCD_BL_CTRL_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7"]
    #[inline(always)]
    pub fn adc_spi_n_cs(&self) -> ADC_SPI_N_CS_R {
        ADC_SPI_N_CS_R::new(((self.bits >> 7) & 1) != 0)
    }
    #[doc = "Bit 8"]
    #[inline(always)]
    pub fn shield0_spi_n_cs(&self) -> SHIELD0_SPI_N_CS_R {
        SHIELD0_SPI_N_CS_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9"]
    #[inline(always)]
    pub fn shield1_spi_n_cs(&self) -> SHIELD1_SPI_N_CS_R {
        SHIELD1_SPI_N_CS_R::new(((self.bits >> 9) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn clcd_cs(&mut self) -> CLCD_CS_W<0> {
        CLCD_CS_W::new(self)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn spi_n_ss(&mut self) -> SPI_N_SS_W<1> {
        SPI_N_SS_W::new(self)
    }
    #[doc = "Bit 3"]
    #[inline(always)]
    pub fn clcd_reset(&mut self) -> CLCD_RESET_W<3> {
        CLCD_RESET_W::new(self)
    }
    #[doc = "Bit 4"]
    #[inline(always)]
    pub fn clcd_rs(&mut self) -> CLCD_RS_W<4> {
        CLCD_RS_W::new(self)
    }
    #[doc = "Bit 5"]
    #[inline(always)]
    pub fn clcd_rd(&mut self) -> CLCD_RD_W<5> {
        CLCD_RD_W::new(self)
    }
    #[doc = "Bit 6"]
    #[inline(always)]
    pub fn clcd_bl_ctrl(&mut self) -> CLCD_BL_CTRL_W<6> {
        CLCD_BL_CTRL_W::new(self)
    }
    #[doc = "Bit 7"]
    #[inline(always)]
    pub fn adc_spi_n_cs(&mut self) -> ADC_SPI_N_CS_W<7> {
        ADC_SPI_N_CS_W::new(self)
    }
    #[doc = "Bit 8"]
    #[inline(always)]
    pub fn shield0_spi_n_cs(&mut self) -> SHIELD0_SPI_N_CS_W<8> {
        SHIELD0_SPI_N_CS_W::new(self)
    }
    #[doc = "Bit 9"]
    #[inline(always)]
    pub fn shield1_spi_n_cs(&mut self) -> SHIELD1_SPI_N_CS_W<9> {
        SHIELD1_SPI_N_CS_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Misc. Control\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [misc](index.html) module"]
pub struct MISC_SPEC;
impl crate::RegisterSpec for MISC_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [misc::R](R) reader structure"]
impl crate::Readable for MISC_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [misc::W](W) writer structure"]
impl crate::Writable for MISC_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets MISC to value 0"]
impl crate::Resettable for MISC_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
