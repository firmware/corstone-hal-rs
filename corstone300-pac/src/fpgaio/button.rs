// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `BUTTON` reader"]
pub struct R(crate::R<BUTTON_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<BUTTON_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<BUTTON_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<BUTTON_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `BUTTON` writer"]
pub struct W(crate::W<BUTTON_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<BUTTON_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<BUTTON_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<BUTTON_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `BUTTON0` reader - "]
pub type BUTTON0_R = crate::BitReader<BUTTON0_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum BUTTON0_A {
    #[doc = "0: BUTTON is off"]
    OFF = 0,
    #[doc = "1: BUTTON is on"]
    ON = 1,
}
impl From<BUTTON0_A> for bool {
    #[inline(always)]
    fn from(variant: BUTTON0_A) -> Self {
        variant as u8 != 0
    }
}
impl BUTTON0_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> BUTTON0_A {
        match self.bits {
            false => BUTTON0_A::OFF,
            true => BUTTON0_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == BUTTON0_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == BUTTON0_A::ON
    }
}
#[doc = "Field `BUTTON0` writer - "]
pub type BUTTON0_W<'a, const O: u8> = crate::BitWriter<'a, u32, BUTTON_SPEC, BUTTON0_A, O>;
impl<'a, const O: u8> BUTTON0_W<'a, O> {
    #[doc = "BUTTON is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(BUTTON0_A::OFF)
    }
    #[doc = "BUTTON is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(BUTTON0_A::ON)
    }
}
#[doc = "Field `BUTTON1` reader - "]
pub type BUTTON1_R = crate::BitReader<BUTTON1_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum BUTTON1_A {
    #[doc = "0: BUTTON is off"]
    OFF = 0,
    #[doc = "1: BUTTON is on"]
    ON = 1,
}
impl From<BUTTON1_A> for bool {
    #[inline(always)]
    fn from(variant: BUTTON1_A) -> Self {
        variant as u8 != 0
    }
}
impl BUTTON1_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> BUTTON1_A {
        match self.bits {
            false => BUTTON1_A::OFF,
            true => BUTTON1_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == BUTTON1_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == BUTTON1_A::ON
    }
}
#[doc = "Field `BUTTON1` writer - "]
pub type BUTTON1_W<'a, const O: u8> = crate::BitWriter<'a, u32, BUTTON_SPEC, BUTTON1_A, O>;
impl<'a, const O: u8> BUTTON1_W<'a, O> {
    #[doc = "BUTTON is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(BUTTON1_A::OFF)
    }
    #[doc = "BUTTON is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(BUTTON1_A::ON)
    }
}
impl R {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn button0(&self) -> BUTTON0_R {
        BUTTON0_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn button1(&self) -> BUTTON1_R {
        BUTTON1_R::new(((self.bits >> 1) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn button0(&mut self) -> BUTTON0_W<0> {
        BUTTON0_W::new(self)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn button1(&mut self) -> BUTTON1_W<1> {
        BUTTON1_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Button Connections\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [button](index.html) module"]
pub struct BUTTON_SPEC;
impl crate::RegisterSpec for BUTTON_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [button::R](R) reader structure"]
impl crate::Readable for BUTTON_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [button::W](W) writer structure"]
impl crate::Writable for BUTTON_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets BUTTON to value 0"]
impl crate::Resettable for BUTTON_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
