// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SWITCHES` reader"]
pub struct R(crate::R<SWITCHES_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SWITCHES_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SWITCHES_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SWITCHES_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `SW0` reader - "]
pub type SW0_R = crate::BitReader<SW0_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SW0_A {
    #[doc = "0: Switch is Off"]
    OFF = 0,
    #[doc = "1: Switch is On"]
    ON = 1,
}
impl From<SW0_A> for bool {
    #[inline(always)]
    fn from(variant: SW0_A) -> Self {
        variant as u8 != 0
    }
}
impl SW0_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SW0_A {
        match self.bits {
            false => SW0_A::OFF,
            true => SW0_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == SW0_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == SW0_A::ON
    }
}
#[doc = "Field `SW1` reader - "]
pub type SW1_R = crate::BitReader<SW1_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SW1_A {
    #[doc = "0: Switch is Off"]
    OFF = 0,
    #[doc = "1: Switch is On"]
    ON = 1,
}
impl From<SW1_A> for bool {
    #[inline(always)]
    fn from(variant: SW1_A) -> Self {
        variant as u8 != 0
    }
}
impl SW1_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SW1_A {
        match self.bits {
            false => SW1_A::OFF,
            true => SW1_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == SW1_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == SW1_A::ON
    }
}
#[doc = "Field `SW2` reader - "]
pub type SW2_R = crate::BitReader<SW2_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SW2_A {
    #[doc = "0: Switch is Off"]
    OFF = 0,
    #[doc = "1: Switch is On"]
    ON = 1,
}
impl From<SW2_A> for bool {
    #[inline(always)]
    fn from(variant: SW2_A) -> Self {
        variant as u8 != 0
    }
}
impl SW2_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SW2_A {
        match self.bits {
            false => SW2_A::OFF,
            true => SW2_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == SW2_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == SW2_A::ON
    }
}
#[doc = "Field `SW3` reader - "]
pub type SW3_R = crate::BitReader<SW3_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SW3_A {
    #[doc = "0: Switch is Off"]
    OFF = 0,
    #[doc = "1: Switch is On"]
    ON = 1,
}
impl From<SW3_A> for bool {
    #[inline(always)]
    fn from(variant: SW3_A) -> Self {
        variant as u8 != 0
    }
}
impl SW3_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SW3_A {
        match self.bits {
            false => SW3_A::OFF,
            true => SW3_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == SW3_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == SW3_A::ON
    }
}
#[doc = "Field `SW4` reader - "]
pub type SW4_R = crate::BitReader<SW4_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SW4_A {
    #[doc = "0: Switch is Off"]
    OFF = 0,
    #[doc = "1: Switch is On"]
    ON = 1,
}
impl From<SW4_A> for bool {
    #[inline(always)]
    fn from(variant: SW4_A) -> Self {
        variant as u8 != 0
    }
}
impl SW4_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SW4_A {
        match self.bits {
            false => SW4_A::OFF,
            true => SW4_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == SW4_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == SW4_A::ON
    }
}
#[doc = "Field `SW5` reader - "]
pub type SW5_R = crate::BitReader<SW5_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SW5_A {
    #[doc = "0: Switch is Off"]
    OFF = 0,
    #[doc = "1: Switch is On"]
    ON = 1,
}
impl From<SW5_A> for bool {
    #[inline(always)]
    fn from(variant: SW5_A) -> Self {
        variant as u8 != 0
    }
}
impl SW5_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SW5_A {
        match self.bits {
            false => SW5_A::OFF,
            true => SW5_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == SW5_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == SW5_A::ON
    }
}
#[doc = "Field `SW6` reader - "]
pub type SW6_R = crate::BitReader<SW6_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SW6_A {
    #[doc = "0: Switch is Off"]
    OFF = 0,
    #[doc = "1: Switch is On"]
    ON = 1,
}
impl From<SW6_A> for bool {
    #[inline(always)]
    fn from(variant: SW6_A) -> Self {
        variant as u8 != 0
    }
}
impl SW6_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SW6_A {
        match self.bits {
            false => SW6_A::OFF,
            true => SW6_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == SW6_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == SW6_A::ON
    }
}
#[doc = "Field `SW7` reader - "]
pub type SW7_R = crate::BitReader<SW7_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SW7_A {
    #[doc = "0: Switch is Off"]
    OFF = 0,
    #[doc = "1: Switch is On"]
    ON = 1,
}
impl From<SW7_A> for bool {
    #[inline(always)]
    fn from(variant: SW7_A) -> Self {
        variant as u8 != 0
    }
}
impl SW7_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SW7_A {
        match self.bits {
            false => SW7_A::OFF,
            true => SW7_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == SW7_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == SW7_A::ON
    }
}
impl R {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn sw0(&self) -> SW0_R {
        SW0_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn sw1(&self) -> SW1_R {
        SW1_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2"]
    #[inline(always)]
    pub fn sw2(&self) -> SW2_R {
        SW2_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3"]
    #[inline(always)]
    pub fn sw3(&self) -> SW3_R {
        SW3_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4"]
    #[inline(always)]
    pub fn sw4(&self) -> SW4_R {
        SW4_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5"]
    #[inline(always)]
    pub fn sw5(&self) -> SW5_R {
        SW5_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6"]
    #[inline(always)]
    pub fn sw6(&self) -> SW6_R {
        SW6_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7"]
    #[inline(always)]
    pub fn sw7(&self) -> SW7_R {
        SW7_R::new(((self.bits >> 7) & 1) != 0)
    }
}
#[doc = "User Switches\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [switches](index.html) module"]
pub struct SWITCHES_SPEC;
impl crate::RegisterSpec for SWITCHES_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [switches::R](R) reader structure"]
impl crate::Readable for SWITCHES_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets SWITCHES to value 0"]
impl crate::Resettable for SWITCHES_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
