// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `LED` reader"]
pub struct R(crate::R<LED_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<LED_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<LED_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<LED_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `LED` writer"]
pub struct W(crate::W<LED_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<LED_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<LED_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<LED_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `LED0` reader - "]
pub type LED0_R = crate::BitReader<LED0_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED0_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED0_A> for bool {
    #[inline(always)]
    fn from(variant: LED0_A) -> Self {
        variant as u8 != 0
    }
}
impl LED0_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED0_A {
        match self.bits {
            false => LED0_A::OFF,
            true => LED0_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED0_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED0_A::ON
    }
}
#[doc = "Field `LED0` writer - "]
pub type LED0_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED0_A, O>;
impl<'a, const O: u8> LED0_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED0_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED0_A::ON)
    }
}
#[doc = "Field `LED1` reader - "]
pub type LED1_R = crate::BitReader<LED1_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED1_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED1_A> for bool {
    #[inline(always)]
    fn from(variant: LED1_A) -> Self {
        variant as u8 != 0
    }
}
impl LED1_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED1_A {
        match self.bits {
            false => LED1_A::OFF,
            true => LED1_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED1_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED1_A::ON
    }
}
#[doc = "Field `LED1` writer - "]
pub type LED1_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED1_A, O>;
impl<'a, const O: u8> LED1_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED1_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED1_A::ON)
    }
}
#[doc = "Field `LED2` reader - "]
pub type LED2_R = crate::BitReader<LED2_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED2_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED2_A> for bool {
    #[inline(always)]
    fn from(variant: LED2_A) -> Self {
        variant as u8 != 0
    }
}
impl LED2_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED2_A {
        match self.bits {
            false => LED2_A::OFF,
            true => LED2_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED2_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED2_A::ON
    }
}
#[doc = "Field `LED2` writer - "]
pub type LED2_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED2_A, O>;
impl<'a, const O: u8> LED2_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED2_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED2_A::ON)
    }
}
#[doc = "Field `LED3` reader - "]
pub type LED3_R = crate::BitReader<LED3_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED3_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED3_A> for bool {
    #[inline(always)]
    fn from(variant: LED3_A) -> Self {
        variant as u8 != 0
    }
}
impl LED3_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED3_A {
        match self.bits {
            false => LED3_A::OFF,
            true => LED3_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED3_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED3_A::ON
    }
}
#[doc = "Field `LED3` writer - "]
pub type LED3_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED3_A, O>;
impl<'a, const O: u8> LED3_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED3_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED3_A::ON)
    }
}
#[doc = "Field `LED4` reader - "]
pub type LED4_R = crate::BitReader<LED4_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED4_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED4_A> for bool {
    #[inline(always)]
    fn from(variant: LED4_A) -> Self {
        variant as u8 != 0
    }
}
impl LED4_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED4_A {
        match self.bits {
            false => LED4_A::OFF,
            true => LED4_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED4_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED4_A::ON
    }
}
#[doc = "Field `LED4` writer - "]
pub type LED4_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED4_A, O>;
impl<'a, const O: u8> LED4_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED4_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED4_A::ON)
    }
}
#[doc = "Field `LED5` reader - "]
pub type LED5_R = crate::BitReader<LED5_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED5_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED5_A> for bool {
    #[inline(always)]
    fn from(variant: LED5_A) -> Self {
        variant as u8 != 0
    }
}
impl LED5_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED5_A {
        match self.bits {
            false => LED5_A::OFF,
            true => LED5_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED5_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED5_A::ON
    }
}
#[doc = "Field `LED5` writer - "]
pub type LED5_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED5_A, O>;
impl<'a, const O: u8> LED5_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED5_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED5_A::ON)
    }
}
#[doc = "Field `LED6` reader - "]
pub type LED6_R = crate::BitReader<LED6_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED6_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED6_A> for bool {
    #[inline(always)]
    fn from(variant: LED6_A) -> Self {
        variant as u8 != 0
    }
}
impl LED6_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED6_A {
        match self.bits {
            false => LED6_A::OFF,
            true => LED6_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED6_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED6_A::ON
    }
}
#[doc = "Field `LED6` writer - "]
pub type LED6_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED6_A, O>;
impl<'a, const O: u8> LED6_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED6_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED6_A::ON)
    }
}
#[doc = "Field `LED7` reader - "]
pub type LED7_R = crate::BitReader<LED7_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED7_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED7_A> for bool {
    #[inline(always)]
    fn from(variant: LED7_A) -> Self {
        variant as u8 != 0
    }
}
impl LED7_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED7_A {
        match self.bits {
            false => LED7_A::OFF,
            true => LED7_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED7_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED7_A::ON
    }
}
#[doc = "Field `LED7` writer - "]
pub type LED7_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED7_A, O>;
impl<'a, const O: u8> LED7_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED7_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED7_A::ON)
    }
}
#[doc = "Field `LED8` reader - "]
pub type LED8_R = crate::BitReader<LED8_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED8_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED8_A> for bool {
    #[inline(always)]
    fn from(variant: LED8_A) -> Self {
        variant as u8 != 0
    }
}
impl LED8_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED8_A {
        match self.bits {
            false => LED8_A::OFF,
            true => LED8_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED8_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED8_A::ON
    }
}
#[doc = "Field `LED8` writer - "]
pub type LED8_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED8_A, O>;
impl<'a, const O: u8> LED8_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED8_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED8_A::ON)
    }
}
#[doc = "Field `LED9` reader - "]
pub type LED9_R = crate::BitReader<LED9_A>;
#[doc = "\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LED9_A {
    #[doc = "0: LED is off"]
    OFF = 0,
    #[doc = "1: LED is on"]
    ON = 1,
}
impl From<LED9_A> for bool {
    #[inline(always)]
    fn from(variant: LED9_A) -> Self {
        variant as u8 != 0
    }
}
impl LED9_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> LED9_A {
        match self.bits {
            false => LED9_A::OFF,
            true => LED9_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == LED9_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == LED9_A::ON
    }
}
#[doc = "Field `LED9` writer - "]
pub type LED9_W<'a, const O: u8> = crate::BitWriter<'a, u32, LED_SPEC, LED9_A, O>;
impl<'a, const O: u8> LED9_W<'a, O> {
    #[doc = "LED is off"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(LED9_A::OFF)
    }
    #[doc = "LED is on"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(LED9_A::ON)
    }
}
impl R {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn led0(&self) -> LED0_R {
        LED0_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn led1(&self) -> LED1_R {
        LED1_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2"]
    #[inline(always)]
    pub fn led2(&self) -> LED2_R {
        LED2_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3"]
    #[inline(always)]
    pub fn led3(&self) -> LED3_R {
        LED3_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4"]
    #[inline(always)]
    pub fn led4(&self) -> LED4_R {
        LED4_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5"]
    #[inline(always)]
    pub fn led5(&self) -> LED5_R {
        LED5_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6"]
    #[inline(always)]
    pub fn led6(&self) -> LED6_R {
        LED6_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7"]
    #[inline(always)]
    pub fn led7(&self) -> LED7_R {
        LED7_R::new(((self.bits >> 7) & 1) != 0)
    }
    #[doc = "Bit 8"]
    #[inline(always)]
    pub fn led8(&self) -> LED8_R {
        LED8_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9"]
    #[inline(always)]
    pub fn led9(&self) -> LED9_R {
        LED9_R::new(((self.bits >> 9) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0"]
    #[inline(always)]
    pub fn led0(&mut self) -> LED0_W<0> {
        LED0_W::new(self)
    }
    #[doc = "Bit 1"]
    #[inline(always)]
    pub fn led1(&mut self) -> LED1_W<1> {
        LED1_W::new(self)
    }
    #[doc = "Bit 2"]
    #[inline(always)]
    pub fn led2(&mut self) -> LED2_W<2> {
        LED2_W::new(self)
    }
    #[doc = "Bit 3"]
    #[inline(always)]
    pub fn led3(&mut self) -> LED3_W<3> {
        LED3_W::new(self)
    }
    #[doc = "Bit 4"]
    #[inline(always)]
    pub fn led4(&mut self) -> LED4_W<4> {
        LED4_W::new(self)
    }
    #[doc = "Bit 5"]
    #[inline(always)]
    pub fn led5(&mut self) -> LED5_W<5> {
        LED5_W::new(self)
    }
    #[doc = "Bit 6"]
    #[inline(always)]
    pub fn led6(&mut self) -> LED6_W<6> {
        LED6_W::new(self)
    }
    #[doc = "Bit 7"]
    #[inline(always)]
    pub fn led7(&mut self) -> LED7_W<7> {
        LED7_W::new(self)
    }
    #[doc = "Bit 8"]
    #[inline(always)]
    pub fn led8(&mut self) -> LED8_W<8> {
        LED8_W::new(self)
    }
    #[doc = "Bit 9"]
    #[inline(always)]
    pub fn led9(&mut self) -> LED9_W<9> {
        LED9_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "LED Connections\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [led](index.html) module"]
pub struct LED_SPEC;
impl crate::RegisterSpec for LED_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [led::R](R) reader structure"]
impl crate::Readable for LED_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [led::W](W) writer structure"]
impl crate::Writable for LED_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets LED to value 0"]
impl crate::Resettable for LED_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
