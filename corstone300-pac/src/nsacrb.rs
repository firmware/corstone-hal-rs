// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    _reserved0: [u8; 0x90],
    #[doc = "0x90 - Non-Secure Unprivileged Access AHB slave Peripheral Protection Control #0"]
    pub ahbnspppc0: AHBNSPPPC0,
    _reserved1: [u8; 0x0c],
    #[doc = "0xa0 - Expansion 0 Non_Secure Unprivileged Access AHB slave Peripheral Protection Control"]
    pub ahbnspppcexp0: AHBNSPPPCEXP0,
    #[doc = "0xa4 - Expansion 1 Non_Secure Unprivileged Access AHB slave Peripheral Protection Control"]
    pub ahbnspppcexp1: AHBNSPPPCEXP1,
    #[doc = "0xa8 - Expansion 2 Non_Secure Unprivileged Access AHB slave Peripheral Protection Control"]
    pub ahbnspppcexp2: AHBNSPPPCEXP2,
    #[doc = "0xac - Expansion 3 Non_Secure Unprivileged Access AHB slave Peripheral Protection Control"]
    pub ahbnspppcexp3: AHBNSPPPCEXP3,
    #[doc = "0xb0 - Non-Secure Unprivileged Access APB slave Peripheral Protection Control 0"]
    pub apbnspppc0: APBNSPPPC0,
    #[doc = "0xb4 - Non-Secure Unprivileged Access APB slave Peripheral Protection Control 1"]
    pub apbnspppc1: APBNSPPPC1,
    _reserved7: [u8; 0x08],
    #[doc = "0xc0 - Expansion 0 Non_Secure Unprivileged Access APB slave Peripheral Protection Control"]
    pub apbnspppcexp0: APBNSPPPCEXP0,
    #[doc = "0xc4 - Expansion 1 Non_Secure Unprivileged Access APB slave Peripheral Protection Control"]
    pub apbnspppcexp1: APBNSPPPCEXP1,
    #[doc = "0xc8 - Expansion 2 Non_Secure Unprivileged Access APB slave Peripheral Protection Control"]
    pub apbnspppcexp2: APBNSPPPCEXP2,
    #[doc = "0xcc - Expansion 3 Non_Secure Unprivileged Access APB slave Peripheral Protection Control"]
    pub apbnspppcexp3: APBNSPPPCEXP3,
    _reserved11: [u8; 0x0f00],
    #[doc = "0xfd0 - Peripheral ID 4"]
    pub pidr4: PIDR4,
    _reserved12: [u8; 0x0c],
    #[doc = "0xfe0 - Peripheral ID 0"]
    pub pidr0: PIDR0,
    #[doc = "0xfe4 - Peripheral ID 1"]
    pub pidr1: PIDR1,
    #[doc = "0xfe8 - Peripheral ID 2"]
    pub pidr2: PIDR2,
    #[doc = "0xfec - Peripheral ID 3"]
    pub pidr3: PIDR3,
    #[doc = "0xff0 - Component ID 0"]
    pub cidr0: CIDR0,
    #[doc = "0xff4 - Component ID 1"]
    pub cidr1: CIDR1,
    #[doc = "0xff8 - Component ID 2"]
    pub cidr2: CIDR2,
    #[doc = "0xffc - Component ID 3"]
    pub cidr3: CIDR3,
}
#[doc = "AHBNSPPPC0 (rw) register accessor: an alias for `Reg<AHBNSPPPC0_SPEC>`"]
pub type AHBNSPPPC0 = crate::Reg<ahbnspppc0::AHBNSPPPC0_SPEC>;
#[doc = "Non-Secure Unprivileged Access AHB slave Peripheral Protection Control #0"]
pub mod ahbnspppc0;
#[doc = "AHBNSPPPCEXP0 (rw) register accessor: an alias for `Reg<AHBNSPPPCEXP0_SPEC>`"]
pub type AHBNSPPPCEXP0 = crate::Reg<ahbnspppcexp0::AHBNSPPPCEXP0_SPEC>;
#[doc = "Expansion 0 Non_Secure Unprivileged Access AHB slave Peripheral Protection Control"]
pub mod ahbnspppcexp0;
#[doc = "AHBNSPPPCEXP1 (rw) register accessor: an alias for `Reg<AHBNSPPPCEXP1_SPEC>`"]
pub type AHBNSPPPCEXP1 = crate::Reg<ahbnspppcexp1::AHBNSPPPCEXP1_SPEC>;
#[doc = "Expansion 1 Non_Secure Unprivileged Access AHB slave Peripheral Protection Control"]
pub mod ahbnspppcexp1;
#[doc = "AHBNSPPPCEXP2 (rw) register accessor: an alias for `Reg<AHBNSPPPCEXP2_SPEC>`"]
pub type AHBNSPPPCEXP2 = crate::Reg<ahbnspppcexp2::AHBNSPPPCEXP2_SPEC>;
#[doc = "Expansion 2 Non_Secure Unprivileged Access AHB slave Peripheral Protection Control"]
pub mod ahbnspppcexp2;
#[doc = "AHBNSPPPCEXP3 (rw) register accessor: an alias for `Reg<AHBNSPPPCEXP3_SPEC>`"]
pub type AHBNSPPPCEXP3 = crate::Reg<ahbnspppcexp3::AHBNSPPPCEXP3_SPEC>;
#[doc = "Expansion 3 Non_Secure Unprivileged Access AHB slave Peripheral Protection Control"]
pub mod ahbnspppcexp3;
#[doc = "APBNSPPPC0 (rw) register accessor: an alias for `Reg<APBNSPPPC0_SPEC>`"]
pub type APBNSPPPC0 = crate::Reg<apbnspppc0::APBNSPPPC0_SPEC>;
#[doc = "Non-Secure Unprivileged Access APB slave Peripheral Protection Control 0"]
pub mod apbnspppc0;
#[doc = "APBNSPPPC1 (rw) register accessor: an alias for `Reg<APBNSPPPC1_SPEC>`"]
pub type APBNSPPPC1 = crate::Reg<apbnspppc1::APBNSPPPC1_SPEC>;
#[doc = "Non-Secure Unprivileged Access APB slave Peripheral Protection Control 1"]
pub mod apbnspppc1;
#[doc = "APBNSPPPCEXP0 (rw) register accessor: an alias for `Reg<APBNSPPPCEXP0_SPEC>`"]
pub type APBNSPPPCEXP0 = crate::Reg<apbnspppcexp0::APBNSPPPCEXP0_SPEC>;
#[doc = "Expansion 0 Non_Secure Unprivileged Access APB slave Peripheral Protection Control"]
pub mod apbnspppcexp0;
#[doc = "APBNSPPPCEXP1 (rw) register accessor: an alias for `Reg<APBNSPPPCEXP1_SPEC>`"]
pub type APBNSPPPCEXP1 = crate::Reg<apbnspppcexp1::APBNSPPPCEXP1_SPEC>;
#[doc = "Expansion 1 Non_Secure Unprivileged Access APB slave Peripheral Protection Control"]
pub mod apbnspppcexp1;
#[doc = "APBNSPPPCEXP2 (rw) register accessor: an alias for `Reg<APBNSPPPCEXP2_SPEC>`"]
pub type APBNSPPPCEXP2 = crate::Reg<apbnspppcexp2::APBNSPPPCEXP2_SPEC>;
#[doc = "Expansion 2 Non_Secure Unprivileged Access APB slave Peripheral Protection Control"]
pub mod apbnspppcexp2;
#[doc = "APBNSPPPCEXP3 (rw) register accessor: an alias for `Reg<APBNSPPPCEXP3_SPEC>`"]
pub type APBNSPPPCEXP3 = crate::Reg<apbnspppcexp3::APBNSPPPCEXP3_SPEC>;
#[doc = "Expansion 3 Non_Secure Unprivileged Access APB slave Peripheral Protection Control"]
pub mod apbnspppcexp3;
#[doc = "PIDR4 (r) register accessor: an alias for `Reg<PIDR4_SPEC>`"]
pub type PIDR4 = crate::Reg<pidr4::PIDR4_SPEC>;
#[doc = "Peripheral ID 4"]
pub mod pidr4;
#[doc = "PIDR0 (r) register accessor: an alias for `Reg<PIDR0_SPEC>`"]
pub type PIDR0 = crate::Reg<pidr0::PIDR0_SPEC>;
#[doc = "Peripheral ID 0"]
pub mod pidr0;
#[doc = "PIDR1 (r) register accessor: an alias for `Reg<PIDR1_SPEC>`"]
pub type PIDR1 = crate::Reg<pidr1::PIDR1_SPEC>;
#[doc = "Peripheral ID 1"]
pub mod pidr1;
#[doc = "PIDR2 (r) register accessor: an alias for `Reg<PIDR2_SPEC>`"]
pub type PIDR2 = crate::Reg<pidr2::PIDR2_SPEC>;
#[doc = "Peripheral ID 2"]
pub mod pidr2;
#[doc = "PIDR3 (r) register accessor: an alias for `Reg<PIDR3_SPEC>`"]
pub type PIDR3 = crate::Reg<pidr3::PIDR3_SPEC>;
#[doc = "Peripheral ID 3"]
pub mod pidr3;
#[doc = "CIDR0 (r) register accessor: an alias for `Reg<CIDR0_SPEC>`"]
pub type CIDR0 = crate::Reg<cidr0::CIDR0_SPEC>;
#[doc = "Component ID 0"]
pub mod cidr0;
#[doc = "CIDR1 (r) register accessor: an alias for `Reg<CIDR1_SPEC>`"]
pub type CIDR1 = crate::Reg<cidr1::CIDR1_SPEC>;
#[doc = "Component ID 1"]
pub mod cidr1;
#[doc = "CIDR2 (r) register accessor: an alias for `Reg<CIDR2_SPEC>`"]
pub type CIDR2 = crate::Reg<cidr2::CIDR2_SPEC>;
#[doc = "Component ID 2"]
pub mod cidr2;
#[doc = "CIDR3 (r) register accessor: an alias for `Reg<CIDR3_SPEC>`"]
pub type CIDR3 = crate::Reg<cidr3::CIDR3_SPEC>;
#[doc = "Component ID 3"]
pub mod cidr3;
