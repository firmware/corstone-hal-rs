// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTP_AIVAL_CTL` reader"]
pub struct R(crate::R<CNTP_AIVAL_CTL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTP_AIVAL_CTL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTP_AIVAL_CTL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTP_AIVAL_CTL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CNTP_AIVAL_CTL` writer"]
pub struct W(crate::W<CNTP_AIVAL_CTL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CNTP_AIVAL_CTL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CNTP_AIVAL_CTL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CNTP_AIVAL_CTL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `En` reader - Enable AutoIncrement."]
pub type EN_R = crate::BitReader<EN_A>;
#[doc = "Enable AutoIncrement.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum EN_A {
    #[doc = "0: `0`"]
    OFF = 0,
    #[doc = "1: `1`"]
    ON = 1,
}
impl From<EN_A> for bool {
    #[inline(always)]
    fn from(variant: EN_A) -> Self {
        variant as u8 != 0
    }
}
impl EN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> EN_A {
        match self.bits {
            false => EN_A::OFF,
            true => EN_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == EN_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == EN_A::ON
    }
}
#[doc = "Field `En` writer - Enable AutoIncrement."]
pub type EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTP_AIVAL_CTL_SPEC, EN_A, O>;
impl<'a, const O: u8> EN_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(EN_A::OFF)
    }
    #[doc = "`1`"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(EN_A::ON)
    }
}
#[doc = "Field `IRQ_CLR` reader - Interrupt Clear"]
pub type IRQ_CLR_R = crate::BitReader<IRQ_CLR_A>;
#[doc = "Interrupt Clear\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum IRQ_CLR_A {
    #[doc = "0: `0`"]
    NOP = 0,
    #[doc = "1: `1`"]
    CLEAR = 1,
}
impl From<IRQ_CLR_A> for bool {
    #[inline(always)]
    fn from(variant: IRQ_CLR_A) -> Self {
        variant as u8 != 0
    }
}
impl IRQ_CLR_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> IRQ_CLR_A {
        match self.bits {
            false => IRQ_CLR_A::NOP,
            true => IRQ_CLR_A::CLEAR,
        }
    }
    #[doc = "Checks if the value of the field is `NOP`"]
    #[inline(always)]
    pub fn is_nop(&self) -> bool {
        *self == IRQ_CLR_A::NOP
    }
    #[doc = "Checks if the value of the field is `CLEAR`"]
    #[inline(always)]
    pub fn is_clear(&self) -> bool {
        *self == IRQ_CLR_A::CLEAR
    }
}
#[doc = "Field `IRQ_CLR` writer - Interrupt Clear"]
pub type IRQ_CLR_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTP_AIVAL_CTL_SPEC, IRQ_CLR_A, O>;
impl<'a, const O: u8> IRQ_CLR_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn nop(self) -> &'a mut W {
        self.variant(IRQ_CLR_A::NOP)
    }
    #[doc = "`1`"]
    #[inline(always)]
    pub fn clear(self) -> &'a mut W {
        self.variant(IRQ_CLR_A::CLEAR)
    }
}
impl R {
    #[doc = "Bit 0 - Enable AutoIncrement."]
    #[inline(always)]
    pub fn en(&self) -> EN_R {
        EN_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Interrupt Clear"]
    #[inline(always)]
    pub fn irq_clr(&self) -> IRQ_CLR_R {
        IRQ_CLR_R::new(((self.bits >> 1) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Enable AutoIncrement."]
    #[inline(always)]
    pub fn en(&mut self) -> EN_W<0> {
        EN_W::new(self)
    }
    #[doc = "Bit 1 - Interrupt Clear"]
    #[inline(always)]
    pub fn irq_clr(&mut self) -> IRQ_CLR_W<1> {
        IRQ_CLR_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "AutoIncrValue Control register.\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntp_aival_ctl](index.html) module"]
pub struct CNTP_AIVAL_CTL_SPEC;
impl crate::RegisterSpec for CNTP_AIVAL_CTL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntp_aival_ctl::R](R) reader structure"]
impl crate::Readable for CNTP_AIVAL_CTL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cntp_aival_ctl::W](W) writer structure"]
impl crate::Writable for CNTP_AIVAL_CTL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CNTP_AIVAL_CTL to value 0"]
impl crate::Resettable for CNTP_AIVAL_CTL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
