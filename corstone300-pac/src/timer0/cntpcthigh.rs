// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTPCTHIGH` reader"]
pub struct R(crate::R<CNTPCTHIGH_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTPCTHIGH_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTPCTHIGH_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTPCTHIGH_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Physical Count Register Higher Word.\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntpcthigh](index.html) module"]
pub struct CNTPCTHIGH_SPEC;
impl crate::RegisterSpec for CNTPCTHIGH_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntpcthigh::R](R) reader structure"]
impl crate::Readable for CNTPCTHIGH_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTPCTHIGH to value 0"]
impl crate::Resettable for CNTPCTHIGH_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
