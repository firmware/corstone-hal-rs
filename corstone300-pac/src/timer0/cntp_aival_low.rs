// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTP_AIVAL_LOW` reader"]
pub struct R(crate::R<CNTP_AIVAL_LOW_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTP_AIVAL_LOW_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTP_AIVAL_LOW_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTP_AIVAL_LOW_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "AutoIncrValue Lower Word Register.\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntp_aival_low](index.html) module"]
pub struct CNTP_AIVAL_LOW_SPEC;
impl crate::RegisterSpec for CNTP_AIVAL_LOW_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntp_aival_low::R](R) reader structure"]
impl crate::Readable for CNTP_AIVAL_LOW_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTP_AIVAL_LOW to value 0"]
impl crate::Resettable for CNTP_AIVAL_LOW_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
