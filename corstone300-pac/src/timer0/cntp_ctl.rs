// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTP_CTL` reader"]
pub struct R(crate::R<CNTP_CTL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTP_CTL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTP_CTL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTP_CTL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CNTP_CTL` writer"]
pub struct W(crate::W<CNTP_CTL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CNTP_CTL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CNTP_CTL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CNTP_CTL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `En` reader - Enable Counter"]
pub type EN_R = crate::BitReader<EN_A>;
#[doc = "Enable Counter\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum EN_A {
    #[doc = "0: `0`"]
    OFF = 0,
    #[doc = "1: `1`"]
    ON = 1,
}
impl From<EN_A> for bool {
    #[inline(always)]
    fn from(variant: EN_A) -> Self {
        variant as u8 != 0
    }
}
impl EN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> EN_A {
        match self.bits {
            false => EN_A::OFF,
            true => EN_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == EN_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == EN_A::ON
    }
}
#[doc = "Field `En` writer - Enable Counter"]
pub type EN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTP_CTL_SPEC, EN_A, O>;
impl<'a, const O: u8> EN_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(EN_A::OFF)
    }
    #[doc = "`1`"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(EN_A::ON)
    }
}
#[doc = "Field `IMASK` reader - Interrupt Mask"]
pub type IMASK_R = crate::BitReader<IMASK_A>;
#[doc = "Interrupt Mask\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum IMASK_A {
    #[doc = "0: `0`"]
    OFF = 0,
    #[doc = "1: `1`"]
    ON = 1,
}
impl From<IMASK_A> for bool {
    #[inline(always)]
    fn from(variant: IMASK_A) -> Self {
        variant as u8 != 0
    }
}
impl IMASK_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> IMASK_A {
        match self.bits {
            false => IMASK_A::OFF,
            true => IMASK_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == IMASK_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == IMASK_A::ON
    }
}
#[doc = "Field `IMASK` writer - Interrupt Mask"]
pub type IMASK_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTP_CTL_SPEC, IMASK_A, O>;
impl<'a, const O: u8> IMASK_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(IMASK_A::OFF)
    }
    #[doc = "`1`"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(IMASK_A::ON)
    }
}
#[doc = "Field `ISTATUS` reader - Interrupt Status"]
pub type ISTATUS_R = crate::BitReader<ISTATUS_A>;
#[doc = "Interrupt Status\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ISTATUS_A {
    #[doc = "0: `0`"]
    OFF = 0,
    #[doc = "1: `1`"]
    ON = 1,
}
impl From<ISTATUS_A> for bool {
    #[inline(always)]
    fn from(variant: ISTATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl ISTATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> ISTATUS_A {
        match self.bits {
            false => ISTATUS_A::OFF,
            true => ISTATUS_A::ON,
        }
    }
    #[doc = "Checks if the value of the field is `OFF`"]
    #[inline(always)]
    pub fn is_off(&self) -> bool {
        *self == ISTATUS_A::OFF
    }
    #[doc = "Checks if the value of the field is `ON`"]
    #[inline(always)]
    pub fn is_on(&self) -> bool {
        *self == ISTATUS_A::ON
    }
}
#[doc = "Field `ISTATUS` writer - Interrupt Status"]
pub type ISTATUS_W<'a, const O: u8> = crate::BitWriter<'a, u32, CNTP_CTL_SPEC, ISTATUS_A, O>;
impl<'a, const O: u8> ISTATUS_W<'a, O> {
    #[doc = "`0`"]
    #[inline(always)]
    pub fn off(self) -> &'a mut W {
        self.variant(ISTATUS_A::OFF)
    }
    #[doc = "`1`"]
    #[inline(always)]
    pub fn on(self) -> &'a mut W {
        self.variant(ISTATUS_A::ON)
    }
}
impl R {
    #[doc = "Bit 0 - Enable Counter"]
    #[inline(always)]
    pub fn en(&self) -> EN_R {
        EN_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Interrupt Mask"]
    #[inline(always)]
    pub fn imask(&self) -> IMASK_R {
        IMASK_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Interrupt Status"]
    #[inline(always)]
    pub fn istatus(&self) -> ISTATUS_R {
        ISTATUS_R::new(((self.bits >> 2) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Enable Counter"]
    #[inline(always)]
    pub fn en(&mut self) -> EN_W<0> {
        EN_W::new(self)
    }
    #[doc = "Bit 1 - Interrupt Mask"]
    #[inline(always)]
    pub fn imask(&mut self) -> IMASK_W<1> {
        IMASK_W::new(self)
    }
    #[doc = "Bit 2 - Interrupt Status"]
    #[inline(always)]
    pub fn istatus(&mut self) -> ISTATUS_W<2> {
        ISTATUS_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Timer Control register.\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntp_ctl](index.html) module"]
pub struct CNTP_CTL_SPEC;
impl crate::RegisterSpec for CNTP_CTL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntp_ctl::R](R) reader structure"]
impl crate::Readable for CNTP_CTL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cntp_ctl::W](W) writer structure"]
impl crate::Writable for CNTP_CTL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CNTP_CTL to value 0"]
impl crate::Resettable for CNTP_CTL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
