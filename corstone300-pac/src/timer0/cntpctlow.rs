// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTPCTLOW` reader"]
pub struct R(crate::R<CNTPCTLOW_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTPCTLOW_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTPCTLOW_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTPCTLOW_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Physical Count Register Lower Word.\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntpctlow](index.html) module"]
pub struct CNTPCTLOW_SPEC;
impl crate::RegisterSpec for CNTPCTLOW_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntpctlow::R](R) reader structure"]
impl crate::Readable for CNTPCTLOW_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTPCTLOW to value 0"]
impl crate::Resettable for CNTPCTLOW_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
