// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RESET_MASK` reader"]
pub struct R(crate::R<RESET_MASK_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RESET_MASK_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RESET_MASK_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RESET_MASK_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `RESET_MASK` writer"]
pub struct W(crate::W<RESET_MASK_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<RESET_MASK_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<RESET_MASK_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<RESET_MASK_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `NSWDRSTREQEN` reader - Enable NON-SECURE WATCHDOG Reset"]
pub type NSWDRSTREQEN_R = crate::BitReader<NSWDRSTREQEN_A>;
#[doc = "Enable NON-SECURE WATCHDOG Reset\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum NSWDRSTREQEN_A {
    #[doc = "1: Enable NON-SECURE WATCHDOG Reset"]
    ENABLED = 1,
    #[doc = "0: Disabled NON-SECURE WATCHDOG Reset"]
    DISABLED = 0,
}
impl From<NSWDRSTREQEN_A> for bool {
    #[inline(always)]
    fn from(variant: NSWDRSTREQEN_A) -> Self {
        variant as u8 != 0
    }
}
impl NSWDRSTREQEN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> NSWDRSTREQEN_A {
        match self.bits {
            true => NSWDRSTREQEN_A::ENABLED,
            false => NSWDRSTREQEN_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLED`"]
    #[inline(always)]
    pub fn is_enabled(&self) -> bool {
        *self == NSWDRSTREQEN_A::ENABLED
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == NSWDRSTREQEN_A::DISABLED
    }
}
#[doc = "Field `NSWDRSTREQEN` writer - Enable NON-SECURE WATCHDOG Reset"]
pub type NSWDRSTREQEN_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, RESET_MASK_SPEC, NSWDRSTREQEN_A, O>;
impl<'a, const O: u8> NSWDRSTREQEN_W<'a, O> {
    #[doc = "Enable NON-SECURE WATCHDOG Reset"]
    #[inline(always)]
    pub fn enabled(self) -> &'a mut W {
        self.variant(NSWDRSTREQEN_A::ENABLED)
    }
    #[doc = "Disabled NON-SECURE WATCHDOG Reset"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(NSWDRSTREQEN_A::DISABLED)
    }
}
#[doc = "Field `CPU0RSTREQENRST` reader - CPU 0 Warm Reset Request Enable."]
pub type CPU0RSTREQENRST_R = crate::BitReader<CPU0RSTREQENRST_A>;
#[doc = "CPU 0 Warm Reset Request Enable.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU0RSTREQENRST_A {
    #[doc = "1: Enable Merging CPU 0 System Reset Request"]
    ENABLED = 1,
    #[doc = "0: Disabled Merging CPU 0 System Reset Request"]
    DISABLED = 0,
}
impl From<CPU0RSTREQENRST_A> for bool {
    #[inline(always)]
    fn from(variant: CPU0RSTREQENRST_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU0RSTREQENRST_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU0RSTREQENRST_A {
        match self.bits {
            true => CPU0RSTREQENRST_A::ENABLED,
            false => CPU0RSTREQENRST_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLED`"]
    #[inline(always)]
    pub fn is_enabled(&self) -> bool {
        *self == CPU0RSTREQENRST_A::ENABLED
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == CPU0RSTREQENRST_A::DISABLED
    }
}
#[doc = "Field `CPU0RSTREQENRST` writer - CPU 0 Warm Reset Request Enable."]
pub type CPU0RSTREQENRST_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, RESET_MASK_SPEC, CPU0RSTREQENRST_A, O>;
impl<'a, const O: u8> CPU0RSTREQENRST_W<'a, O> {
    #[doc = "Enable Merging CPU 0 System Reset Request"]
    #[inline(always)]
    pub fn enabled(self) -> &'a mut W {
        self.variant(CPU0RSTREQENRST_A::ENABLED)
    }
    #[doc = "Disabled Merging CPU 0 System Reset Request"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(CPU0RSTREQENRST_A::DISABLED)
    }
}
impl R {
    #[doc = "Bit 1 - Enable NON-SECURE WATCHDOG Reset"]
    #[inline(always)]
    pub fn nswdrstreqen(&self) -> NSWDRSTREQEN_R {
        NSWDRSTREQEN_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 8 - CPU 0 Warm Reset Request Enable."]
    #[inline(always)]
    pub fn cpu0rstreqenrst(&self) -> CPU0RSTREQENRST_R {
        CPU0RSTREQENRST_R::new(((self.bits >> 8) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 1 - Enable NON-SECURE WATCHDOG Reset"]
    #[inline(always)]
    pub fn nswdrstreqen(&mut self) -> NSWDRSTREQEN_W<1> {
        NSWDRSTREQEN_W::new(self)
    }
    #[doc = "Bit 8 - CPU 0 Warm Reset Request Enable."]
    #[inline(always)]
    pub fn cpu0rstreqenrst(&mut self) -> CPU0RSTREQENRST_W<8> {
        CPU0RSTREQENRST_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Reset Mask\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [reset_mask](index.html) module"]
pub struct RESET_MASK_SPEC;
impl crate::RegisterSpec for RESET_MASK_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [reset_mask::R](R) reader structure"]
impl crate::Readable for RESET_MASK_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [reset_mask::W](W) writer structure"]
impl crate::Writable for RESET_MASK_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets RESET_MASK to value 0"]
impl crate::Resettable for RESET_MASK_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
