// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `PDCM_PD_SYS_SENSE` reader"]
pub struct R(crate::R<PDCM_PD_SYS_SENSE_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<PDCM_PD_SYS_SENSE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<PDCM_PD_SYS_SENSE_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<PDCM_PD_SYS_SENSE_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `PDCM_PD_SYS_SENSE` writer"]
pub struct W(crate::W<PDCM_PD_SYS_SENSE_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<PDCM_PD_SYS_SENSE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<PDCM_PD_SYS_SENSE_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<PDCM_PD_SYS_SENSE_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `S_PD_SYS_ON` reader - Enable PD_SYS ON Sensitivity"]
pub type S_PD_SYS_ON_R = crate::BitReader<S_PD_SYS_ON_A>;
#[doc = "Enable PD_SYS ON Sensitivity\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum S_PD_SYS_ON_A {
    #[doc = "1: Keep PD_SYS awake after powered ON"]
    ENABLE = 1,
}
impl From<S_PD_SYS_ON_A> for bool {
    #[inline(always)]
    fn from(variant: S_PD_SYS_ON_A) -> Self {
        variant as u8 != 0
    }
}
impl S_PD_SYS_ON_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<S_PD_SYS_ON_A> {
        match self.bits {
            true => Some(S_PD_SYS_ON_A::ENABLE),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == S_PD_SYS_ON_A::ENABLE
    }
}
#[doc = "Field `S_PD_SYS_ON` writer - Enable PD_SYS ON Sensitivity"]
pub type S_PD_SYS_ON_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_SYS_SENSE_SPEC, S_PD_SYS_ON_A, O>;
impl<'a, const O: u8> S_PD_SYS_ON_W<'a, O> {
    #[doc = "Keep PD_SYS awake after powered ON"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(S_PD_SYS_ON_A::ENABLE)
    }
}
#[doc = "Field `S_PD_CPU0CORE_ON` reader - Tied to HIGH"]
pub type S_PD_CPU0CORE_ON_R = crate::BitReader<S_PD_CPU0CORE_ON_A>;
#[doc = "Tied to HIGH\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum S_PD_CPU0CORE_ON_A {
    #[doc = "1: PD_SYS always tries to stay ON if PD_CPU0CORE is ON"]
    HIGH = 1,
}
impl From<S_PD_CPU0CORE_ON_A> for bool {
    #[inline(always)]
    fn from(variant: S_PD_CPU0CORE_ON_A) -> Self {
        variant as u8 != 0
    }
}
impl S_PD_CPU0CORE_ON_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<S_PD_CPU0CORE_ON_A> {
        match self.bits {
            true => Some(S_PD_CPU0CORE_ON_A::HIGH),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `HIGH`"]
    #[inline(always)]
    pub fn is_high(&self) -> bool {
        *self == S_PD_CPU0CORE_ON_A::HIGH
    }
}
#[doc = "Field `S_PD_DEBUG_ON` reader - Tied to LOW"]
pub type S_PD_DEBUG_ON_R = crate::BitReader<bool>;
#[doc = "Field `S_PDCMQREQ0` reader - Enable PDCMQREQn\\[0\\]
signal Sensitivity"]
pub type S_PDCMQREQ0_R = crate::BitReader<S_PDCMQREQ0_A>;
#[doc = "Enable PDCMQREQn\\[0\\]
signal Sensitivity\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum S_PDCMQREQ0_A {
    #[doc = "1: Enable PDCMQREQn\\[0\\]
signal Sensitivity."]
    ENABLE = 1,
    #[doc = "0: Disable PDCMQREQn\\[0\\]
signal Sensitivity."]
    DISABLED = 0,
}
impl From<S_PDCMQREQ0_A> for bool {
    #[inline(always)]
    fn from(variant: S_PDCMQREQ0_A) -> Self {
        variant as u8 != 0
    }
}
impl S_PDCMQREQ0_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> S_PDCMQREQ0_A {
        match self.bits {
            true => S_PDCMQREQ0_A::ENABLE,
            false => S_PDCMQREQ0_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == S_PDCMQREQ0_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == S_PDCMQREQ0_A::DISABLED
    }
}
#[doc = "Field `S_PDCMQREQ0` writer - Enable PDCMQREQn\\[0\\]
signal Sensitivity"]
pub type S_PDCMQREQ0_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_SYS_SENSE_SPEC, S_PDCMQREQ0_A, O>;
impl<'a, const O: u8> S_PDCMQREQ0_W<'a, O> {
    #[doc = "Enable PDCMQREQn\\[0\\]
signal Sensitivity."]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(S_PDCMQREQ0_A::ENABLE)
    }
    #[doc = "Disable PDCMQREQn\\[0\\]
signal Sensitivity."]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(S_PDCMQREQ0_A::DISABLED)
    }
}
#[doc = "Field `S_PDCMQREQ1` reader - Enable PDCMQREQn\\[1\\]
signal Sensitivity"]
pub type S_PDCMQREQ1_R = crate::BitReader<S_PDCMQREQ1_A>;
#[doc = "Enable PDCMQREQn\\[1\\]
signal Sensitivity\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum S_PDCMQREQ1_A {
    #[doc = "1: Enable PDCMQREQn\\[1\\]
signal Sensitivity."]
    ENABLE = 1,
    #[doc = "0: Disable PDCMQREQn\\[1\\]
signal Sensitivity."]
    DISABLED = 0,
}
impl From<S_PDCMQREQ1_A> for bool {
    #[inline(always)]
    fn from(variant: S_PDCMQREQ1_A) -> Self {
        variant as u8 != 0
    }
}
impl S_PDCMQREQ1_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> S_PDCMQREQ1_A {
        match self.bits {
            true => S_PDCMQREQ1_A::ENABLE,
            false => S_PDCMQREQ1_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == S_PDCMQREQ1_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == S_PDCMQREQ1_A::DISABLED
    }
}
#[doc = "Field `S_PDCMQREQ1` writer - Enable PDCMQREQn\\[1\\]
signal Sensitivity"]
pub type S_PDCMQREQ1_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_SYS_SENSE_SPEC, S_PDCMQREQ1_A, O>;
impl<'a, const O: u8> S_PDCMQREQ1_W<'a, O> {
    #[doc = "Enable PDCMQREQn\\[1\\]
signal Sensitivity."]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(S_PDCMQREQ1_A::ENABLE)
    }
    #[doc = "Disable PDCMQREQn\\[1\\]
signal Sensitivity."]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(S_PDCMQREQ1_A::DISABLED)
    }
}
#[doc = "Field `S_PDCMQREQ2` reader - Enable PDCMQREQn\\[2\\]
signal Sensitivity"]
pub type S_PDCMQREQ2_R = crate::BitReader<S_PDCMQREQ2_A>;
#[doc = "Enable PDCMQREQn\\[2\\]
signal Sensitivity\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum S_PDCMQREQ2_A {
    #[doc = "1: Enable PDCMQREQn\\[2\\]
signal Sensitivity."]
    ENABLE = 1,
    #[doc = "0: Disable PDCMQREQn\\[2\\]
signal Sensitivity."]
    DISABLED = 0,
}
impl From<S_PDCMQREQ2_A> for bool {
    #[inline(always)]
    fn from(variant: S_PDCMQREQ2_A) -> Self {
        variant as u8 != 0
    }
}
impl S_PDCMQREQ2_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> S_PDCMQREQ2_A {
        match self.bits {
            true => S_PDCMQREQ2_A::ENABLE,
            false => S_PDCMQREQ2_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == S_PDCMQREQ2_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == S_PDCMQREQ2_A::DISABLED
    }
}
#[doc = "Field `S_PDCMQREQ2` writer - Enable PDCMQREQn\\[2\\]
signal Sensitivity"]
pub type S_PDCMQREQ2_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_SYS_SENSE_SPEC, S_PDCMQREQ2_A, O>;
impl<'a, const O: u8> S_PDCMQREQ2_W<'a, O> {
    #[doc = "Enable PDCMQREQn\\[2\\]
signal Sensitivity."]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(S_PDCMQREQ2_A::ENABLE)
    }
    #[doc = "Disable PDCMQREQn\\[2\\]
signal Sensitivity."]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(S_PDCMQREQ2_A::DISABLED)
    }
}
#[doc = "Field `S_PDCMQREQ3` reader - Enable PDCMQREQn\\[3\\]
signal Sensitivity"]
pub type S_PDCMQREQ3_R = crate::BitReader<S_PDCMQREQ3_A>;
#[doc = "Enable PDCMQREQn\\[3\\]
signal Sensitivity\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum S_PDCMQREQ3_A {
    #[doc = "1: Enable PDCMQREQn\\[3\\]
signal Sensitivity."]
    ENABLE = 1,
    #[doc = "0: Disable PDCMQREQn\\[3\\]
signal Sensitivity."]
    DISABLED = 0,
}
impl From<S_PDCMQREQ3_A> for bool {
    #[inline(always)]
    fn from(variant: S_PDCMQREQ3_A) -> Self {
        variant as u8 != 0
    }
}
impl S_PDCMQREQ3_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> S_PDCMQREQ3_A {
        match self.bits {
            true => S_PDCMQREQ3_A::ENABLE,
            false => S_PDCMQREQ3_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == S_PDCMQREQ3_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == S_PDCMQREQ3_A::DISABLED
    }
}
#[doc = "Field `S_PDCMQREQ3` writer - Enable PDCMQREQn\\[3\\]
signal Sensitivity"]
pub type S_PDCMQREQ3_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_SYS_SENSE_SPEC, S_PDCMQREQ3_A, O>;
impl<'a, const O: u8> S_PDCMQREQ3_W<'a, O> {
    #[doc = "Enable PDCMQREQn\\[3\\]
signal Sensitivity."]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(S_PDCMQREQ3_A::ENABLE)
    }
    #[doc = "Disable PDCMQREQn\\[3\\]
signal Sensitivity."]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(S_PDCMQREQ3_A::DISABLED)
    }
}
#[doc = "Field `MIN_PWR_STATE` reader - Defines the Minimum Power State, when PD_SYS is trying to enter a lower power state."]
pub type MIN_PWR_STATE_R = crate::FieldReader<u8, u8>;
#[doc = "Field `MIN_PWR_STATE` writer - Defines the Minimum Power State, when PD_SYS is trying to enter a lower power state."]
pub type MIN_PWR_STATE_W<'a, const O: u8> =
    crate::FieldWriter<'a, u32, PDCM_PD_SYS_SENSE_SPEC, u8, u8, 2, O>;
impl R {
    #[doc = "Bit 0 - Enable PD_SYS ON Sensitivity"]
    #[inline(always)]
    pub fn s_pd_sys_on(&self) -> S_PD_SYS_ON_R {
        S_PD_SYS_ON_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Tied to HIGH"]
    #[inline(always)]
    pub fn s_pd_cpu0core_on(&self) -> S_PD_CPU0CORE_ON_R {
        S_PD_CPU0CORE_ON_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 13 - Tied to LOW"]
    #[inline(always)]
    pub fn s_pd_debug_on(&self) -> S_PD_DEBUG_ON_R {
        S_PD_DEBUG_ON_R::new(((self.bits >> 13) & 1) != 0)
    }
    #[doc = "Bit 16 - Enable PDCMQREQn\\[0\\]
signal Sensitivity"]
    #[inline(always)]
    pub fn s_pdcmqreq0(&self) -> S_PDCMQREQ0_R {
        S_PDCMQREQ0_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - Enable PDCMQREQn\\[1\\]
signal Sensitivity"]
    #[inline(always)]
    pub fn s_pdcmqreq1(&self) -> S_PDCMQREQ1_R {
        S_PDCMQREQ1_R::new(((self.bits >> 17) & 1) != 0)
    }
    #[doc = "Bit 18 - Enable PDCMQREQn\\[2\\]
signal Sensitivity"]
    #[inline(always)]
    pub fn s_pdcmqreq2(&self) -> S_PDCMQREQ2_R {
        S_PDCMQREQ2_R::new(((self.bits >> 18) & 1) != 0)
    }
    #[doc = "Bit 19 - Enable PDCMQREQn\\[3\\]
signal Sensitivity"]
    #[inline(always)]
    pub fn s_pdcmqreq3(&self) -> S_PDCMQREQ3_R {
        S_PDCMQREQ3_R::new(((self.bits >> 19) & 1) != 0)
    }
    #[doc = "Bits 30:31 - Defines the Minimum Power State, when PD_SYS is trying to enter a lower power state."]
    #[inline(always)]
    pub fn min_pwr_state(&self) -> MIN_PWR_STATE_R {
        MIN_PWR_STATE_R::new(((self.bits >> 30) & 3) as u8)
    }
}
impl W {
    #[doc = "Bit 0 - Enable PD_SYS ON Sensitivity"]
    #[inline(always)]
    pub fn s_pd_sys_on(&mut self) -> S_PD_SYS_ON_W<0> {
        S_PD_SYS_ON_W::new(self)
    }
    #[doc = "Bit 16 - Enable PDCMQREQn\\[0\\]
signal Sensitivity"]
    #[inline(always)]
    pub fn s_pdcmqreq0(&mut self) -> S_PDCMQREQ0_W<16> {
        S_PDCMQREQ0_W::new(self)
    }
    #[doc = "Bit 17 - Enable PDCMQREQn\\[1\\]
signal Sensitivity"]
    #[inline(always)]
    pub fn s_pdcmqreq1(&mut self) -> S_PDCMQREQ1_W<17> {
        S_PDCMQREQ1_W::new(self)
    }
    #[doc = "Bit 18 - Enable PDCMQREQn\\[2\\]
signal Sensitivity"]
    #[inline(always)]
    pub fn s_pdcmqreq2(&mut self) -> S_PDCMQREQ2_W<18> {
        S_PDCMQREQ2_W::new(self)
    }
    #[doc = "Bit 19 - Enable PDCMQREQn\\[3\\]
signal Sensitivity"]
    #[inline(always)]
    pub fn s_pdcmqreq3(&mut self) -> S_PDCMQREQ3_W<19> {
        S_PDCMQREQ3_W::new(self)
    }
    #[doc = "Bits 30:31 - Defines the Minimum Power State, when PD_SYS is trying to enter a lower power state."]
    #[inline(always)]
    pub fn min_pwr_state(&mut self) -> MIN_PWR_STATE_W<30> {
        MIN_PWR_STATE_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "External Wakeup Control\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [pdcm_pd_sys_sense](index.html) module"]
pub struct PDCM_PD_SYS_SENSE_SPEC;
impl crate::RegisterSpec for PDCM_PD_SYS_SENSE_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [pdcm_pd_sys_sense::R](R) reader structure"]
impl crate::Readable for PDCM_PD_SYS_SENSE_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [pdcm_pd_sys_sense::W](W) writer structure"]
impl crate::Writable for PDCM_PD_SYS_SENSE_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets PDCM_PD_SYS_SENSE to value 0"]
impl crate::Resettable for PDCM_PD_SYS_SENSE_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
