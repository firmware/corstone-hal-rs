// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CPUWAIT` reader"]
pub struct R(crate::R<CPUWAIT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CPUWAIT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CPUWAIT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CPUWAIT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CPUWAIT` writer"]
pub struct W(crate::W<CPUWAIT_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CPUWAIT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CPUWAIT_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CPUWAIT_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `CPU0WAIT` reader - CPU 0 waits at boot and whether CPU1 powers up"]
pub type CPU0WAIT_R = crate::BitReader<CPU0WAIT_A>;
#[doc = "CPU 0 waits at boot and whether CPU1 powers up\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU0WAIT_A {
    #[doc = "0: CPU0 boot normally. From Power ON reset, nSRST reset or Watchdog Reset, CPU 1 powers up"]
    NORMALLY_OR_POWERUP = 0,
    #[doc = "1: CPU0 wait. From Power ON reset, nSRST reset or Watchdog Reset, CPU 1 do not power up"]
    WAIT_OR_NO_POWERUP = 1,
}
impl From<CPU0WAIT_A> for bool {
    #[inline(always)]
    fn from(variant: CPU0WAIT_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU0WAIT_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU0WAIT_A {
        match self.bits {
            false => CPU0WAIT_A::NORMALLY_OR_POWERUP,
            true => CPU0WAIT_A::WAIT_OR_NO_POWERUP,
        }
    }
    #[doc = "Checks if the value of the field is `NORMALLY_OR_POWERUP`"]
    #[inline(always)]
    pub fn is_normally_or_powerup(&self) -> bool {
        *self == CPU0WAIT_A::NORMALLY_OR_POWERUP
    }
    #[doc = "Checks if the value of the field is `WAIT_OR_NO_POWERUP`"]
    #[inline(always)]
    pub fn is_wait_or_no_powerup(&self) -> bool {
        *self == CPU0WAIT_A::WAIT_OR_NO_POWERUP
    }
}
#[doc = "Field `CPU0WAIT` writer - CPU 0 waits at boot and whether CPU1 powers up"]
pub type CPU0WAIT_W<'a, const O: u8> = crate::BitWriter<'a, u32, CPUWAIT_SPEC, CPU0WAIT_A, O>;
impl<'a, const O: u8> CPU0WAIT_W<'a, O> {
    #[doc = "CPU0 boot normally. From Power ON reset, nSRST reset or Watchdog Reset, CPU 1 powers up"]
    #[inline(always)]
    pub fn normally_or_powerup(self) -> &'a mut W {
        self.variant(CPU0WAIT_A::NORMALLY_OR_POWERUP)
    }
    #[doc = "CPU0 wait. From Power ON reset, nSRST reset or Watchdog Reset, CPU 1 do not power up"]
    #[inline(always)]
    pub fn wait_or_no_powerup(self) -> &'a mut W {
        self.variant(CPU0WAIT_A::WAIT_OR_NO_POWERUP)
    }
}
impl R {
    #[doc = "Bit 0 - CPU 0 waits at boot and whether CPU1 powers up"]
    #[inline(always)]
    pub fn cpu0wait(&self) -> CPU0WAIT_R {
        CPU0WAIT_R::new((self.bits & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - CPU 0 waits at boot and whether CPU1 powers up"]
    #[inline(always)]
    pub fn cpu0wait(&mut self) -> CPU0WAIT_W<0> {
        CPU0WAIT_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "CPU Boot wait control after reset\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cpuwait](index.html) module"]
pub struct CPUWAIT_SPEC;
impl crate::RegisterSpec for CPUWAIT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cpuwait::R](R) reader structure"]
impl crate::Readable for CPUWAIT_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cpuwait::W](W) writer structure"]
impl crate::Writable for CPUWAIT_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CPUWAIT to value 0"]
impl crate::Resettable for CPUWAIT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
