// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SWRESET` writer"]
pub struct W(crate::W<SWRESET_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<SWRESET_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<SWRESET_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<SWRESET_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `SWRESETREQ` writer - High Active Software Reset Request"]
pub type SWRESETREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, SWRESET_SPEC, bool, O>;
impl W {
    #[doc = "Bit 9 - High Active Software Reset Request"]
    #[inline(always)]
    pub fn swresetreq(&mut self) -> SWRESETREQ_W<9> {
        SWRESETREQ_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Software Reset\n\nThis register you can [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [swreset](index.html) module"]
pub struct SWRESET_SPEC;
impl crate::RegisterSpec for SWRESET_SPEC {
    type Ux = u32;
}
#[doc = "`write(|w| ..)` method takes [swreset::W](W) writer structure"]
impl crate::Writable for SWRESET_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets SWRESET to value 0"]
impl crate::Resettable for SWRESET_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
