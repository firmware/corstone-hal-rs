// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INITSVRTOR0` reader"]
pub struct R(crate::R<INITSVRTOR0_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INITSVRTOR0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INITSVRTOR0_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INITSVRTOR0_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `INITSVRTOR0` writer"]
pub struct W(crate::W<INITSVRTOR0_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<INITSVRTOR0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<INITSVRTOR0_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<INITSVRTOR0_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `INITSVTOR0LOCK` writer - Lock INITSVTOR0. When set to 1, will stop any further writes to INITSVTOR0 and INITSVTOR0LOCK fields. Cleared only by Warm reset."]
pub type INITSVTOR0LOCK_W<'a, const O: u8> = crate::BitWriter<'a, u32, INITSVRTOR0_SPEC, bool, O>;
#[doc = "Field `INITSVTOR0` reader - Default Secure Vector table offset at reset for CPU 0"]
pub type INITSVTOR0_R = crate::FieldReader<u32, u32>;
#[doc = "Field `INITSVTOR0` writer - Default Secure Vector table offset at reset for CPU 0"]
pub type INITSVTOR0_W<'a, const O: u8> =
    crate::FieldWriter<'a, u32, INITSVRTOR0_SPEC, u32, u32, 25, O>;
impl R {
    #[doc = "Bits 7:31 - Default Secure Vector table offset at reset for CPU 0"]
    #[inline(always)]
    pub fn initsvtor0(&self) -> INITSVTOR0_R {
        INITSVTOR0_R::new(((self.bits >> 7) & 0x01ff_ffff) as u32)
    }
}
impl W {
    #[doc = "Bit 0 - Lock INITSVTOR0. When set to 1, will stop any further writes to INITSVTOR0 and INITSVTOR0LOCK fields. Cleared only by Warm reset."]
    #[inline(always)]
    pub fn initsvtor0lock(&mut self) -> INITSVTOR0LOCK_W<0> {
        INITSVTOR0LOCK_W::new(self)
    }
    #[doc = "Bits 7:31 - Default Secure Vector table offset at reset for CPU 0"]
    #[inline(always)]
    pub fn initsvtor0(&mut self) -> INITSVTOR0_W<7> {
        INITSVTOR0_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Initial Secure Reset Vector Register For CPU 0\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [initsvrtor0](index.html) module"]
pub struct INITSVRTOR0_SPEC;
impl crate::RegisterSpec for INITSVRTOR0_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [initsvrtor0::R](R) reader structure"]
impl crate::Readable for INITSVRTOR0_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [initsvrtor0::W](W) writer structure"]
impl crate::Writable for INITSVRTOR0_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets INITSVRTOR0 to value 0"]
impl crate::Resettable for INITSVRTOR0_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
