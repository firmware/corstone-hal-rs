// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CLOCK_FORCE` reader"]
pub struct R(crate::R<CLOCK_FORCE_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CLOCK_FORCE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CLOCK_FORCE_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CLOCK_FORCE_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CLOCK_FORCE` writer"]
pub struct W(crate::W<CLOCK_FORCE_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CLOCK_FORCE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CLOCK_FORCE_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CLOCK_FORCE_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `MGMT_CLKFORCE` reader - Set HIGH to force all clocks in PD_AON domain that runs on SYSCLK to run."]
pub type MGMT_CLKFORCE_R = crate::BitReader<bool>;
#[doc = "Field `MGMT_CLKFORCE` writer - Set HIGH to force all clocks in PD_AON domain that runs on SYSCLK to run."]
pub type MGMT_CLKFORCE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CLOCK_FORCE_SPEC, bool, O>;
#[doc = "Field `SYS_CLKFORCE` reader - Set HIGH to force all clocks in PD_SYS to run."]
pub type SYS_CLKFORCE_R = crate::BitReader<bool>;
#[doc = "Field `SYS_CLKFORCE` writer - Set HIGH to force all clocks in PD_SYS to run."]
pub type SYS_CLKFORCE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CLOCK_FORCE_SPEC, bool, O>;
#[doc = "Field `DEBUG_CLKFORCE` reader - Set HIGH to force all clocks in PD_DEBUG to run."]
pub type DEBUG_CLKFORCE_R = crate::BitReader<bool>;
#[doc = "Field `DEBUG_CLKFORCE` writer - Set HIGH to force all clocks in PD_DEBUG to run."]
pub type DEBUG_CLKFORCE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CLOCK_FORCE_SPEC, bool, O>;
#[doc = "Field `CPU0_CLKFORCE` reader - Set HIGH to force PD_CPU0 Local clocks to run."]
pub type CPU0_CLKFORCE_R = crate::BitReader<bool>;
#[doc = "Field `CPU0_CLKFORCE` writer - Set HIGH to force PD_CPU0 Local clocks to run."]
pub type CPU0_CLKFORCE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CLOCK_FORCE_SPEC, bool, O>;
#[doc = "Field `AONCLK_FORCE` reader - Set HIGH to request the input AONCLK source to stay ON."]
pub type AONCLK_FORCE_R = crate::BitReader<bool>;
#[doc = "Field `AONCLK_FORCE` writer - Set HIGH to request the input AONCLK source to stay ON."]
pub type AONCLK_FORCE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CLOCK_FORCE_SPEC, bool, O>;
#[doc = "Field `SYSCLK_FORCE` reader - Set HIGH to request the input SYSCLK source to stay ON."]
pub type SYSCLK_FORCE_R = crate::BitReader<bool>;
#[doc = "Field `SYSCLK_FORCE` writer - Set HIGH to request the input SYSCLK source to stay ON."]
pub type SYSCLK_FORCE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CLOCK_FORCE_SPEC, bool, O>;
#[doc = "Field `CPU0CLK_FORCE` reader - Set HIGH to request the input CPU0CLK source to stay ON.."]
pub type CPU0CLK_FORCE_R = crate::BitReader<bool>;
#[doc = "Field `CPU0CLK_FORCE` writer - Set HIGH to request the input CPU0CLK source to stay ON.."]
pub type CPU0CLK_FORCE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CLOCK_FORCE_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Set HIGH to force all clocks in PD_AON domain that runs on SYSCLK to run."]
    #[inline(always)]
    pub fn mgmt_clkforce(&self) -> MGMT_CLKFORCE_R {
        MGMT_CLKFORCE_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Set HIGH to force all clocks in PD_SYS to run."]
    #[inline(always)]
    pub fn sys_clkforce(&self) -> SYS_CLKFORCE_R {
        SYS_CLKFORCE_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Set HIGH to force all clocks in PD_DEBUG to run."]
    #[inline(always)]
    pub fn debug_clkforce(&self) -> DEBUG_CLKFORCE_R {
        DEBUG_CLKFORCE_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 4 - Set HIGH to force PD_CPU0 Local clocks to run."]
    #[inline(always)]
    pub fn cpu0_clkforce(&self) -> CPU0_CLKFORCE_R {
        CPU0_CLKFORCE_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 16 - Set HIGH to request the input AONCLK source to stay ON."]
    #[inline(always)]
    pub fn aonclk_force(&self) -> AONCLK_FORCE_R {
        AONCLK_FORCE_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - Set HIGH to request the input SYSCLK source to stay ON."]
    #[inline(always)]
    pub fn sysclk_force(&self) -> SYSCLK_FORCE_R {
        SYSCLK_FORCE_R::new(((self.bits >> 17) & 1) != 0)
    }
    #[doc = "Bit 19 - Set HIGH to request the input CPU0CLK source to stay ON.."]
    #[inline(always)]
    pub fn cpu0clk_force(&self) -> CPU0CLK_FORCE_R {
        CPU0CLK_FORCE_R::new(((self.bits >> 19) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Set HIGH to force all clocks in PD_AON domain that runs on SYSCLK to run."]
    #[inline(always)]
    pub fn mgmt_clkforce(&mut self) -> MGMT_CLKFORCE_W<0> {
        MGMT_CLKFORCE_W::new(self)
    }
    #[doc = "Bit 1 - Set HIGH to force all clocks in PD_SYS to run."]
    #[inline(always)]
    pub fn sys_clkforce(&mut self) -> SYS_CLKFORCE_W<1> {
        SYS_CLKFORCE_W::new(self)
    }
    #[doc = "Bit 2 - Set HIGH to force all clocks in PD_DEBUG to run."]
    #[inline(always)]
    pub fn debug_clkforce(&mut self) -> DEBUG_CLKFORCE_W<2> {
        DEBUG_CLKFORCE_W::new(self)
    }
    #[doc = "Bit 4 - Set HIGH to force PD_CPU0 Local clocks to run."]
    #[inline(always)]
    pub fn cpu0_clkforce(&mut self) -> CPU0_CLKFORCE_W<4> {
        CPU0_CLKFORCE_W::new(self)
    }
    #[doc = "Bit 16 - Set HIGH to request the input AONCLK source to stay ON."]
    #[inline(always)]
    pub fn aonclk_force(&mut self) -> AONCLK_FORCE_W<16> {
        AONCLK_FORCE_W::new(self)
    }
    #[doc = "Bit 17 - Set HIGH to request the input SYSCLK source to stay ON."]
    #[inline(always)]
    pub fn sysclk_force(&mut self) -> SYSCLK_FORCE_W<17> {
        SYSCLK_FORCE_W::new(self)
    }
    #[doc = "Bit 19 - Set HIGH to request the input CPU0CLK source to stay ON.."]
    #[inline(always)]
    pub fn cpu0clk_force(&mut self) -> CPU0CLK_FORCE_W<19> {
        CPU0CLK_FORCE_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Clock Force\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [clock_force](index.html) module"]
pub struct CLOCK_FORCE_SPEC;
impl crate::RegisterSpec for CLOCK_FORCE_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [clock_force::R](R) reader structure"]
impl crate::Readable for CLOCK_FORCE_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [clock_force::W](W) writer structure"]
impl crate::Writable for CLOCK_FORCE_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CLOCK_FORCE to value 0"]
impl crate::Resettable for CLOCK_FORCE_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
