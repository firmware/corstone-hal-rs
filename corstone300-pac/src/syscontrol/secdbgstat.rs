// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SECDBGSTAT` reader"]
pub struct R(crate::R<SECDBGSTAT_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SECDBGSTAT_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SECDBGSTAT_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SECDBGSTAT_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `DBGEN_I_STATUS` reader - Debug enable value"]
pub type DBGEN_I_STATUS_R = crate::BitReader<DBGEN_I_STATUS_A>;
#[doc = "Debug enable value\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DBGEN_I_STATUS_A {
    #[doc = "1: debug enable"]
    ENABLE = 1,
    #[doc = "0: debug disable"]
    DISABLE = 0,
}
impl From<DBGEN_I_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DBGEN_I_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DBGEN_I_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DBGEN_I_STATUS_A {
        match self.bits {
            true => DBGEN_I_STATUS_A::ENABLE,
            false => DBGEN_I_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DBGEN_I_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DBGEN_I_STATUS_A::DISABLE
    }
}
#[doc = "Field `DBGEN_SEL_STATUS` reader - Debug enable selector value"]
pub type DBGEN_SEL_STATUS_R = crate::BitReader<DBGEN_SEL_STATUS_A>;
#[doc = "Debug enable selector value\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DBGEN_SEL_STATUS_A {
    #[doc = "1: debug enable selector"]
    ENABLE = 1,
    #[doc = "0: debug disable selector"]
    DISABLE = 0,
}
impl From<DBGEN_SEL_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DBGEN_SEL_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DBGEN_SEL_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DBGEN_SEL_STATUS_A {
        match self.bits {
            true => DBGEN_SEL_STATUS_A::ENABLE,
            false => DBGEN_SEL_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DBGEN_SEL_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DBGEN_SEL_STATUS_A::DISABLE
    }
}
#[doc = "Field `NIDEN_I_STATUS` reader - Non-invasive debug enable value"]
pub type NIDEN_I_STATUS_R = crate::BitReader<NIDEN_I_STATUS_A>;
#[doc = "Non-invasive debug enable value\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum NIDEN_I_STATUS_A {
    #[doc = "1: non-invasive debug enable"]
    ENABLE = 1,
    #[doc = "0: non-invasive debug disable"]
    DISABLE = 0,
}
impl From<NIDEN_I_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: NIDEN_I_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl NIDEN_I_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> NIDEN_I_STATUS_A {
        match self.bits {
            true => NIDEN_I_STATUS_A::ENABLE,
            false => NIDEN_I_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == NIDEN_I_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == NIDEN_I_STATUS_A::DISABLE
    }
}
#[doc = "Field `NIDEN_SEL_STATUS` reader - Non-invasive debug enable selector value"]
pub type NIDEN_SEL_STATUS_R = crate::BitReader<NIDEN_SEL_STATUS_A>;
#[doc = "Non-invasive debug enable selector value\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum NIDEN_SEL_STATUS_A {
    #[doc = "1: non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<NIDEN_SEL_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: NIDEN_SEL_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl NIDEN_SEL_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> NIDEN_SEL_STATUS_A {
        match self.bits {
            true => NIDEN_SEL_STATUS_A::ENABLE,
            false => NIDEN_SEL_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == NIDEN_SEL_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == NIDEN_SEL_STATUS_A::DISABLE
    }
}
#[doc = "Field `SPIDEN_I_STATUS` reader - Secure privilege invasive debug enable value"]
pub type SPIDEN_I_STATUS_R = crate::BitReader<SPIDEN_I_STATUS_A>;
#[doc = "Secure privilege invasive debug enable value\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPIDEN_I_STATUS_A {
    #[doc = "1: Secure privilege invasive debug enable"]
    ENABLE = 1,
    #[doc = "0: Secure privilege invasive debug disable"]
    DISABLE = 0,
}
impl From<SPIDEN_I_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: SPIDEN_I_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl SPIDEN_I_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SPIDEN_I_STATUS_A {
        match self.bits {
            true => SPIDEN_I_STATUS_A::ENABLE,
            false => SPIDEN_I_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == SPIDEN_I_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == SPIDEN_I_STATUS_A::DISABLE
    }
}
#[doc = "Field `SPIDEN_SEL_STATUS` reader - Secure privilege invasive debug enable selector value"]
pub type SPIDEN_SEL_STATUS_R = crate::BitReader<SPIDEN_SEL_STATUS_A>;
#[doc = "Secure privilege invasive debug enable selector value\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPIDEN_SEL_STATUS_A {
    #[doc = "1: Secure privilege invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege invasive debug disable selector"]
    DISABLE = 0,
}
impl From<SPIDEN_SEL_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: SPIDEN_SEL_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl SPIDEN_SEL_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SPIDEN_SEL_STATUS_A {
        match self.bits {
            true => SPIDEN_SEL_STATUS_A::ENABLE,
            false => SPIDEN_SEL_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == SPIDEN_SEL_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == SPIDEN_SEL_STATUS_A::DISABLE
    }
}
#[doc = "Field `SPNIDEN_STATUS` reader - Secure privilege non-invasive debug enable value"]
pub type SPNIDEN_STATUS_R = crate::BitReader<SPNIDEN_STATUS_A>;
#[doc = "Secure privilege non-invasive debug enable value\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPNIDEN_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable"]
    DISABLE = 0,
}
impl From<SPNIDEN_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: SPNIDEN_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl SPNIDEN_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SPNIDEN_STATUS_A {
        match self.bits {
            true => SPNIDEN_STATUS_A::ENABLE,
            false => SPNIDEN_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == SPNIDEN_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == SPNIDEN_STATUS_A::DISABLE
    }
}
#[doc = "Field `SPNIDEN_SEL_STATUS` reader - Secure privilege non-invasive debug enable selector value"]
pub type SPNIDEN_SEL_STATUS_R = crate::BitReader<SPNIDEN_SEL_STATUS_A>;
#[doc = "Secure privilege non-invasive debug enable selector value\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPNIDEN_SEL_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<SPNIDEN_SEL_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: SPNIDEN_SEL_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl SPNIDEN_SEL_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SPNIDEN_SEL_STATUS_A {
        match self.bits {
            true => SPNIDEN_SEL_STATUS_A::ENABLE,
            false => SPNIDEN_SEL_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == SPNIDEN_SEL_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == SPNIDEN_SEL_STATUS_A::DISABLE
    }
}
#[doc = "Field `DAPACCEN_STATUS` reader - Active High DAP Access Enable Value. This bit reflects the value on the DAPACCEN pin."]
pub type DAPACCEN_STATUS_R = crate::BitReader<DAPACCEN_STATUS_A>;
#[doc = "Active High DAP Access Enable Value. This bit reflects the value on the DAPACCEN pin.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPACCEN_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPACCEN_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DAPACCEN_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DAPACCEN_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DAPACCEN_STATUS_A {
        match self.bits {
            true => DAPACCEN_STATUS_A::ENABLE,
            false => DAPACCEN_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DAPACCEN_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DAPACCEN_STATUS_A::DISABLE
    }
}
#[doc = "Field `DAPACCEN_SEL_STATUS` reader - Active High DAP Access Enable Selector Value. This bit returns the DAPACCEN_SEL value. Forced to Zero if DAPACCENSELDIS = 1."]
pub type DAPACCEN_SEL_STATUS_R = crate::BitReader<DAPACCEN_SEL_STATUS_A>;
#[doc = "Active High DAP Access Enable Selector Value. This bit returns the DAPACCEN_SEL value. Forced to Zero if DAPACCENSELDIS = 1.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPACCEN_SEL_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPACCEN_SEL_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DAPACCEN_SEL_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DAPACCEN_SEL_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DAPACCEN_SEL_STATUS_A {
        match self.bits {
            true => DAPACCEN_SEL_STATUS_A::ENABLE,
            false => DAPACCEN_SEL_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DAPACCEN_SEL_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DAPACCEN_SEL_STATUS_A::DISABLE
    }
}
#[doc = "Field `DAPDSSACCEN_STATUS` reader - Active High DAP to Debug Subsystem Access Enable Value. This bit reflects the value on the DAPDSSACCEN pin."]
pub type DAPDSSACCEN_STATUS_R = crate::BitReader<DAPDSSACCEN_STATUS_A>;
#[doc = "Active High DAP to Debug Subsystem Access Enable Value. This bit reflects the value on the DAPDSSACCEN pin.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPDSSACCEN_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPDSSACCEN_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DAPDSSACCEN_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DAPDSSACCEN_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DAPDSSACCEN_STATUS_A {
        match self.bits {
            true => DAPDSSACCEN_STATUS_A::ENABLE,
            false => DAPDSSACCEN_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DAPDSSACCEN_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DAPDSSACCEN_STATUS_A::DISABLE
    }
}
#[doc = "Field `DAPDSSACCEN_SEL_STATUS` reader - Active High DAP to Debug Subsystem Access Enable Selector Value. This bit returns the DAPDSSACCEN_SEL value. Forced to Zero if DAPDSSACCENSELDIS = 1."]
pub type DAPDSSACCEN_SEL_STATUS_R = crate::BitReader<DAPDSSACCEN_SEL_STATUS_A>;
#[doc = "Active High DAP to Debug Subsystem Access Enable Selector Value. This bit returns the DAPDSSACCEN_SEL value. Forced to Zero if DAPDSSACCENSELDIS = 1.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPDSSACCEN_SEL_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPDSSACCEN_SEL_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DAPDSSACCEN_SEL_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DAPDSSACCEN_SEL_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DAPDSSACCEN_SEL_STATUS_A {
        match self.bits {
            true => DAPDSSACCEN_SEL_STATUS_A::ENABLE,
            false => DAPDSSACCEN_SEL_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DAPDSSACCEN_SEL_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DAPDSSACCEN_SEL_STATUS_A::DISABLE
    }
}
#[doc = "Field `DBGENSELDIS_STATUS` reader - Returns the DBGENSELDIS configuration value when read."]
pub type DBGENSELDIS_STATUS_R = crate::BitReader<DBGENSELDIS_STATUS_A>;
#[doc = "Returns the DBGENSELDIS configuration value when read.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DBGENSELDIS_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DBGENSELDIS_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DBGENSELDIS_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DBGENSELDIS_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DBGENSELDIS_STATUS_A {
        match self.bits {
            true => DBGENSELDIS_STATUS_A::ENABLE,
            false => DBGENSELDIS_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DBGENSELDIS_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DBGENSELDIS_STATUS_A::DISABLE
    }
}
#[doc = "Field `NIDENSELDIS_STATUS` reader - Returns the NIDENSELDIS configuration value when read."]
pub type NIDENSELDIS_STATUS_R = crate::BitReader<NIDENSELDIS_STATUS_A>;
#[doc = "Returns the NIDENSELDIS configuration value when read.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum NIDENSELDIS_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<NIDENSELDIS_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: NIDENSELDIS_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl NIDENSELDIS_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> NIDENSELDIS_STATUS_A {
        match self.bits {
            true => NIDENSELDIS_STATUS_A::ENABLE,
            false => NIDENSELDIS_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == NIDENSELDIS_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == NIDENSELDIS_STATUS_A::DISABLE
    }
}
#[doc = "Field `SPIDENSELDIS_STATUS` reader - Returns the SPIDENSELDIS configuration value when read."]
pub type SPIDENSELDIS_STATUS_R = crate::BitReader<SPIDENSELDIS_STATUS_A>;
#[doc = "Returns the SPIDENSELDIS configuration value when read.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPIDENSELDIS_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<SPIDENSELDIS_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: SPIDENSELDIS_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl SPIDENSELDIS_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SPIDENSELDIS_STATUS_A {
        match self.bits {
            true => SPIDENSELDIS_STATUS_A::ENABLE,
            false => SPIDENSELDIS_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == SPIDENSELDIS_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == SPIDENSELDIS_STATUS_A::DISABLE
    }
}
#[doc = "Field `SPNIDENSELDIS_STATUS` reader - Returns the SPNIDENSELDIS configuration value when read."]
pub type SPNIDENSELDIS_STATUS_R = crate::BitReader<SPNIDENSELDIS_STATUS_A>;
#[doc = "Returns the SPNIDENSELDIS configuration value when read.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPNIDENSELDIS_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<SPNIDENSELDIS_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: SPNIDENSELDIS_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl SPNIDENSELDIS_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SPNIDENSELDIS_STATUS_A {
        match self.bits {
            true => SPNIDENSELDIS_STATUS_A::ENABLE,
            false => SPNIDENSELDIS_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == SPNIDENSELDIS_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == SPNIDENSELDIS_STATUS_A::DISABLE
    }
}
#[doc = "Field `DAPACCENSELDIS_STATUS` reader - Returns the DAPACCENSELDIS configuration value when read."]
pub type DAPACCENSELDIS_STATUS_R = crate::BitReader<DAPACCENSELDIS_STATUS_A>;
#[doc = "Returns the DAPACCENSELDIS configuration value when read.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPACCENSELDIS_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPACCENSELDIS_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DAPACCENSELDIS_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DAPACCENSELDIS_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DAPACCENSELDIS_STATUS_A {
        match self.bits {
            true => DAPACCENSELDIS_STATUS_A::ENABLE,
            false => DAPACCENSELDIS_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DAPACCENSELDIS_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DAPACCENSELDIS_STATUS_A::DISABLE
    }
}
#[doc = "Field `DAPDSSACCENSELDIS_STATUS` reader - Returns the DAPDSSACCENSELDIS configuration value when read."]
pub type DAPDSSACCENSELDIS_STATUS_R = crate::BitReader<DAPDSSACCENSELDIS_STATUS_A>;
#[doc = "Returns the DAPDSSACCENSELDIS configuration value when read.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPDSSACCENSELDIS_STATUS_A {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPDSSACCENSELDIS_STATUS_A> for bool {
    #[inline(always)]
    fn from(variant: DAPDSSACCENSELDIS_STATUS_A) -> Self {
        variant as u8 != 0
    }
}
impl DAPDSSACCENSELDIS_STATUS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> DAPDSSACCENSELDIS_STATUS_A {
        match self.bits {
            true => DAPDSSACCENSELDIS_STATUS_A::ENABLE,
            false => DAPDSSACCENSELDIS_STATUS_A::DISABLE,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == DAPDSSACCENSELDIS_STATUS_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == DAPDSSACCENSELDIS_STATUS_A::DISABLE
    }
}
impl R {
    #[doc = "Bit 0 - Debug enable value"]
    #[inline(always)]
    pub fn dbgen_i_status(&self) -> DBGEN_I_STATUS_R {
        DBGEN_I_STATUS_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Debug enable selector value"]
    #[inline(always)]
    pub fn dbgen_sel_status(&self) -> DBGEN_SEL_STATUS_R {
        DBGEN_SEL_STATUS_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Non-invasive debug enable value"]
    #[inline(always)]
    pub fn niden_i_status(&self) -> NIDEN_I_STATUS_R {
        NIDEN_I_STATUS_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Non-invasive debug enable selector value"]
    #[inline(always)]
    pub fn niden_sel_status(&self) -> NIDEN_SEL_STATUS_R {
        NIDEN_SEL_STATUS_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - Secure privilege invasive debug enable value"]
    #[inline(always)]
    pub fn spiden_i_status(&self) -> SPIDEN_I_STATUS_R {
        SPIDEN_I_STATUS_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5 - Secure privilege invasive debug enable selector value"]
    #[inline(always)]
    pub fn spiden_sel_status(&self) -> SPIDEN_SEL_STATUS_R {
        SPIDEN_SEL_STATUS_R::new(((self.bits >> 5) & 1) != 0)
    }
    #[doc = "Bit 6 - Secure privilege non-invasive debug enable value"]
    #[inline(always)]
    pub fn spniden_status(&self) -> SPNIDEN_STATUS_R {
        SPNIDEN_STATUS_R::new(((self.bits >> 6) & 1) != 0)
    }
    #[doc = "Bit 7 - Secure privilege non-invasive debug enable selector value"]
    #[inline(always)]
    pub fn spniden_sel_status(&self) -> SPNIDEN_SEL_STATUS_R {
        SPNIDEN_SEL_STATUS_R::new(((self.bits >> 7) & 1) != 0)
    }
    #[doc = "Bit 8 - Active High DAP Access Enable Value. This bit reflects the value on the DAPACCEN pin."]
    #[inline(always)]
    pub fn dapaccen_status(&self) -> DAPACCEN_STATUS_R {
        DAPACCEN_STATUS_R::new(((self.bits >> 8) & 1) != 0)
    }
    #[doc = "Bit 9 - Active High DAP Access Enable Selector Value. This bit returns the DAPACCEN_SEL value. Forced to Zero if DAPACCENSELDIS = 1."]
    #[inline(always)]
    pub fn dapaccen_sel_status(&self) -> DAPACCEN_SEL_STATUS_R {
        DAPACCEN_SEL_STATUS_R::new(((self.bits >> 9) & 1) != 0)
    }
    #[doc = "Bit 10 - Active High DAP to Debug Subsystem Access Enable Value. This bit reflects the value on the DAPDSSACCEN pin."]
    #[inline(always)]
    pub fn dapdssaccen_status(&self) -> DAPDSSACCEN_STATUS_R {
        DAPDSSACCEN_STATUS_R::new(((self.bits >> 10) & 1) != 0)
    }
    #[doc = "Bit 11 - Active High DAP to Debug Subsystem Access Enable Selector Value. This bit returns the DAPDSSACCEN_SEL value. Forced to Zero if DAPDSSACCENSELDIS = 1."]
    #[inline(always)]
    pub fn dapdssaccen_sel_status(&self) -> DAPDSSACCEN_SEL_STATUS_R {
        DAPDSSACCEN_SEL_STATUS_R::new(((self.bits >> 11) & 1) != 0)
    }
    #[doc = "Bit 24 - Returns the DBGENSELDIS configuration value when read."]
    #[inline(always)]
    pub fn dbgenseldis_status(&self) -> DBGENSELDIS_STATUS_R {
        DBGENSELDIS_STATUS_R::new(((self.bits >> 24) & 1) != 0)
    }
    #[doc = "Bit 25 - Returns the NIDENSELDIS configuration value when read."]
    #[inline(always)]
    pub fn nidenseldis_status(&self) -> NIDENSELDIS_STATUS_R {
        NIDENSELDIS_STATUS_R::new(((self.bits >> 25) & 1) != 0)
    }
    #[doc = "Bit 26 - Returns the SPIDENSELDIS configuration value when read."]
    #[inline(always)]
    pub fn spidenseldis_status(&self) -> SPIDENSELDIS_STATUS_R {
        SPIDENSELDIS_STATUS_R::new(((self.bits >> 26) & 1) != 0)
    }
    #[doc = "Bit 27 - Returns the SPNIDENSELDIS configuration value when read."]
    #[inline(always)]
    pub fn spnidenseldis_status(&self) -> SPNIDENSELDIS_STATUS_R {
        SPNIDENSELDIS_STATUS_R::new(((self.bits >> 27) & 1) != 0)
    }
    #[doc = "Bit 28 - Returns the DAPACCENSELDIS configuration value when read."]
    #[inline(always)]
    pub fn dapaccenseldis_status(&self) -> DAPACCENSELDIS_STATUS_R {
        DAPACCENSELDIS_STATUS_R::new(((self.bits >> 28) & 1) != 0)
    }
    #[doc = "Bit 29 - Returns the DAPDSSACCENSELDIS configuration value when read."]
    #[inline(always)]
    pub fn dapdssaccenseldis_status(&self) -> DAPDSSACCENSELDIS_STATUS_R {
        DAPDSSACCENSELDIS_STATUS_R::new(((self.bits >> 29) & 1) != 0)
    }
}
#[doc = "Secure Debug Configuration Status\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [secdbgstat](index.html) module"]
pub struct SECDBGSTAT_SPEC;
impl crate::RegisterSpec for SECDBGSTAT_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [secdbgstat::R](R) reader structure"]
impl crate::Readable for SECDBGSTAT_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets SECDBGSTAT to value 0"]
impl crate::Resettable for SECDBGSTAT_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
