// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `PWRCTRL` reader"]
pub struct R(crate::R<PWRCTRL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<PWRCTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<PWRCTRL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<PWRCTRL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `PWRCTRL` writer"]
pub struct W(crate::W<PWRCTRL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<PWRCTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<PWRCTRL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<PWRCTRL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `PPU_ACCESS_UNLOCK` writer - PPU_ACCESS_FILTER write unlock. When 1, Both PPU_ACCESS_FILTER and this register bits can be written. When set to '0' the PPU_ACCESS_FILTER and this register bit will no longer writable, and PPU_ACCESS_UNLOCK will stay '0'."]
pub type PPU_ACCESS_UNLOCK_W<'a, const O: u8> = crate::BitWriter<'a, u32, PWRCTRL_SPEC, bool, O>;
#[doc = "Field `PPU_ACCESS_FILTER` reader - Filter Access to PPU Registers. When set to '1' only key PPU interrupt handling registers are open to write access, and all other PPU registers are read only. When set to '0' releases all PPU register to full access."]
pub type PPU_ACCESS_FILTER_R = crate::BitReader<bool>;
#[doc = "Field `PPU_ACCESS_FILTER` writer - Filter Access to PPU Registers. When set to '1' only key PPU interrupt handling registers are open to write access, and all other PPU registers are read only. When set to '0' releases all PPU register to full access."]
pub type PPU_ACCESS_FILTER_W<'a, const O: u8> = crate::BitWriter<'a, u32, PWRCTRL_SPEC, bool, O>;
impl R {
    #[doc = "Bit 1 - Filter Access to PPU Registers. When set to '1' only key PPU interrupt handling registers are open to write access, and all other PPU registers are read only. When set to '0' releases all PPU register to full access."]
    #[inline(always)]
    pub fn ppu_access_filter(&self) -> PPU_ACCESS_FILTER_R {
        PPU_ACCESS_FILTER_R::new(((self.bits >> 1) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - PPU_ACCESS_FILTER write unlock. When 1, Both PPU_ACCESS_FILTER and this register bits can be written. When set to '0' the PPU_ACCESS_FILTER and this register bit will no longer writable, and PPU_ACCESS_UNLOCK will stay '0'."]
    #[inline(always)]
    pub fn ppu_access_unlock(&mut self) -> PPU_ACCESS_UNLOCK_W<0> {
        PPU_ACCESS_UNLOCK_W::new(self)
    }
    #[doc = "Bit 1 - Filter Access to PPU Registers. When set to '1' only key PPU interrupt handling registers are open to write access, and all other PPU registers are read only. When set to '0' releases all PPU register to full access."]
    #[inline(always)]
    pub fn ppu_access_filter(&mut self) -> PPU_ACCESS_FILTER_W<1> {
        PPU_ACCESS_FILTER_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Power Configuration and Control.\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [pwrctrl](index.html) module"]
pub struct PWRCTRL_SPEC;
impl crate::RegisterSpec for PWRCTRL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [pwrctrl::R](R) reader structure"]
impl crate::Readable for PWRCTRL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [pwrctrl::W](W) writer structure"]
impl crate::Writable for PWRCTRL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets PWRCTRL to value 0x03"]
impl crate::Resettable for PWRCTRL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x03
    }
}
