// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `NMI_ENABLE` reader"]
pub struct R(crate::R<NMI_ENABLE_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<NMI_ENABLE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<NMI_ENABLE_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<NMI_ENABLE_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `NMI_ENABLE` writer"]
pub struct W(crate::W<NMI_ENABLE_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<NMI_ENABLE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<NMI_ENABLE_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<NMI_ENABLE_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `CPU0_INTNMI_ENABLE` reader - CPU0 Internally Sourced NMI Enable"]
pub type CPU0_INTNMI_ENABLE_R = crate::BitReader<CPU0_INTNMI_ENABLE_A>;
#[doc = "CPU0 Internally Sourced NMI Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU0_INTNMI_ENABLE_A {
    #[doc = "1: CPU0 Internally Sourced NMI Enabled"]
    ENABLE = 1,
    #[doc = "0: CPU0 Internally Sourced NMI Disabled"]
    DISABLED = 0,
}
impl From<CPU0_INTNMI_ENABLE_A> for bool {
    #[inline(always)]
    fn from(variant: CPU0_INTNMI_ENABLE_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU0_INTNMI_ENABLE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU0_INTNMI_ENABLE_A {
        match self.bits {
            true => CPU0_INTNMI_ENABLE_A::ENABLE,
            false => CPU0_INTNMI_ENABLE_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == CPU0_INTNMI_ENABLE_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == CPU0_INTNMI_ENABLE_A::DISABLED
    }
}
#[doc = "Field `CPU0_INTNMI_ENABLE` writer - CPU0 Internally Sourced NMI Enable"]
pub type CPU0_INTNMI_ENABLE_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, NMI_ENABLE_SPEC, CPU0_INTNMI_ENABLE_A, O>;
impl<'a, const O: u8> CPU0_INTNMI_ENABLE_W<'a, O> {
    #[doc = "CPU0 Internally Sourced NMI Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(CPU0_INTNMI_ENABLE_A::ENABLE)
    }
    #[doc = "CPU0 Internally Sourced NMI Disabled"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(CPU0_INTNMI_ENABLE_A::DISABLED)
    }
}
#[doc = "Field `CPU1_INTNMI_ENABLE` reader - CPU1 Internally Sourced NMI Enable"]
pub type CPU1_INTNMI_ENABLE_R = crate::BitReader<CPU1_INTNMI_ENABLE_A>;
#[doc = "CPU1 Internally Sourced NMI Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU1_INTNMI_ENABLE_A {
    #[doc = "1: CPU1 Internally Sourced NMI Enabled"]
    ENABLE = 1,
    #[doc = "0: CPU1 Internally Sourced NMI Disabled"]
    DISABLED = 0,
}
impl From<CPU1_INTNMI_ENABLE_A> for bool {
    #[inline(always)]
    fn from(variant: CPU1_INTNMI_ENABLE_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU1_INTNMI_ENABLE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU1_INTNMI_ENABLE_A {
        match self.bits {
            true => CPU1_INTNMI_ENABLE_A::ENABLE,
            false => CPU1_INTNMI_ENABLE_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == CPU1_INTNMI_ENABLE_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == CPU1_INTNMI_ENABLE_A::DISABLED
    }
}
#[doc = "Field `CPU1_INTNMI_ENABLE` writer - CPU1 Internally Sourced NMI Enable"]
pub type CPU1_INTNMI_ENABLE_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, NMI_ENABLE_SPEC, CPU1_INTNMI_ENABLE_A, O>;
impl<'a, const O: u8> CPU1_INTNMI_ENABLE_W<'a, O> {
    #[doc = "CPU1 Internally Sourced NMI Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(CPU1_INTNMI_ENABLE_A::ENABLE)
    }
    #[doc = "CPU1 Internally Sourced NMI Disabled"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(CPU1_INTNMI_ENABLE_A::DISABLED)
    }
}
#[doc = "Field `CPU0_EXPNMI_ENABLE` reader - CPU0 Externally Sourced NMI Enable"]
pub type CPU0_EXPNMI_ENABLE_R = crate::BitReader<CPU0_EXPNMI_ENABLE_A>;
#[doc = "CPU0 Externally Sourced NMI Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU0_EXPNMI_ENABLE_A {
    #[doc = "1: CPU0 Externally Sourced NMI Enabled"]
    ENABLE = 1,
    #[doc = "0: CPU0 Externally Sourced NMI Disabled"]
    DISABLED = 0,
}
impl From<CPU0_EXPNMI_ENABLE_A> for bool {
    #[inline(always)]
    fn from(variant: CPU0_EXPNMI_ENABLE_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU0_EXPNMI_ENABLE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU0_EXPNMI_ENABLE_A {
        match self.bits {
            true => CPU0_EXPNMI_ENABLE_A::ENABLE,
            false => CPU0_EXPNMI_ENABLE_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == CPU0_EXPNMI_ENABLE_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == CPU0_EXPNMI_ENABLE_A::DISABLED
    }
}
#[doc = "Field `CPU0_EXPNMI_ENABLE` writer - CPU0 Externally Sourced NMI Enable"]
pub type CPU0_EXPNMI_ENABLE_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, NMI_ENABLE_SPEC, CPU0_EXPNMI_ENABLE_A, O>;
impl<'a, const O: u8> CPU0_EXPNMI_ENABLE_W<'a, O> {
    #[doc = "CPU0 Externally Sourced NMI Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(CPU0_EXPNMI_ENABLE_A::ENABLE)
    }
    #[doc = "CPU0 Externally Sourced NMI Disabled"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(CPU0_EXPNMI_ENABLE_A::DISABLED)
    }
}
#[doc = "Field `CPU1_EXPNMI_ENABLE` reader - CPU1 Externally Sourced NMI Enable"]
pub type CPU1_EXPNMI_ENABLE_R = crate::BitReader<CPU1_EXPNMI_ENABLE_A>;
#[doc = "CPU1 Externally Sourced NMI Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU1_EXPNMI_ENABLE_A {
    #[doc = "1: CPU1 Externally Sourced NMI Enabled"]
    ENABLE = 1,
    #[doc = "0: CPU1 Externally Sourced NMI Disabled"]
    DISABLED = 0,
}
impl From<CPU1_EXPNMI_ENABLE_A> for bool {
    #[inline(always)]
    fn from(variant: CPU1_EXPNMI_ENABLE_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU1_EXPNMI_ENABLE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU1_EXPNMI_ENABLE_A {
        match self.bits {
            true => CPU1_EXPNMI_ENABLE_A::ENABLE,
            false => CPU1_EXPNMI_ENABLE_A::DISABLED,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == CPU1_EXPNMI_ENABLE_A::ENABLE
    }
    #[doc = "Checks if the value of the field is `DISABLED`"]
    #[inline(always)]
    pub fn is_disabled(&self) -> bool {
        *self == CPU1_EXPNMI_ENABLE_A::DISABLED
    }
}
#[doc = "Field `CPU1_EXPNMI_ENABLE` writer - CPU1 Externally Sourced NMI Enable"]
pub type CPU1_EXPNMI_ENABLE_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, NMI_ENABLE_SPEC, CPU1_EXPNMI_ENABLE_A, O>;
impl<'a, const O: u8> CPU1_EXPNMI_ENABLE_W<'a, O> {
    #[doc = "CPU1 Externally Sourced NMI Enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(CPU1_EXPNMI_ENABLE_A::ENABLE)
    }
    #[doc = "CPU1 Externally Sourced NMI Disabled"]
    #[inline(always)]
    pub fn disabled(self) -> &'a mut W {
        self.variant(CPU1_EXPNMI_ENABLE_A::DISABLED)
    }
}
impl R {
    #[doc = "Bit 0 - CPU0 Internally Sourced NMI Enable"]
    #[inline(always)]
    pub fn cpu0_intnmi_enable(&self) -> CPU0_INTNMI_ENABLE_R {
        CPU0_INTNMI_ENABLE_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - CPU1 Internally Sourced NMI Enable"]
    #[inline(always)]
    pub fn cpu1_intnmi_enable(&self) -> CPU1_INTNMI_ENABLE_R {
        CPU1_INTNMI_ENABLE_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 16 - CPU0 Externally Sourced NMI Enable"]
    #[inline(always)]
    pub fn cpu0_expnmi_enable(&self) -> CPU0_EXPNMI_ENABLE_R {
        CPU0_EXPNMI_ENABLE_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - CPU1 Externally Sourced NMI Enable"]
    #[inline(always)]
    pub fn cpu1_expnmi_enable(&self) -> CPU1_EXPNMI_ENABLE_R {
        CPU1_EXPNMI_ENABLE_R::new(((self.bits >> 17) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - CPU0 Internally Sourced NMI Enable"]
    #[inline(always)]
    pub fn cpu0_intnmi_enable(&mut self) -> CPU0_INTNMI_ENABLE_W<0> {
        CPU0_INTNMI_ENABLE_W::new(self)
    }
    #[doc = "Bit 1 - CPU1 Internally Sourced NMI Enable"]
    #[inline(always)]
    pub fn cpu1_intnmi_enable(&mut self) -> CPU1_INTNMI_ENABLE_W<1> {
        CPU1_INTNMI_ENABLE_W::new(self)
    }
    #[doc = "Bit 16 - CPU0 Externally Sourced NMI Enable"]
    #[inline(always)]
    pub fn cpu0_expnmi_enable(&mut self) -> CPU0_EXPNMI_ENABLE_W<16> {
        CPU0_EXPNMI_ENABLE_W::new(self)
    }
    #[doc = "Bit 17 - CPU1 Externally Sourced NMI Enable"]
    #[inline(always)]
    pub fn cpu1_expnmi_enable(&mut self) -> CPU1_EXPNMI_ENABLE_W<17> {
        CPU1_EXPNMI_ENABLE_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "NMI Enable Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [nmi_enable](index.html) module"]
pub struct NMI_ENABLE_SPEC;
impl crate::RegisterSpec for NMI_ENABLE_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [nmi_enable::R](R) reader structure"]
impl crate::Readable for NMI_ENABLE_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [nmi_enable::W](W) writer structure"]
impl crate::Writable for NMI_ENABLE_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets NMI_ENABLE to value 0"]
impl crate::Resettable for NMI_ENABLE_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
