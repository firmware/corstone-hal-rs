// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CLK_CFG0` reader"]
pub struct R(crate::R<CLK_CFG0_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CLK_CFG0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CLK_CFG0_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CLK_CFG0_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CLK_CFG0` writer"]
pub struct W(crate::W<CLK_CFG0_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CLK_CFG0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CLK_CFG0_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CLK_CFG0_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `CPU0CLKCFG` reader - Clock Configuration value that drives CPU0CLKCFG signals."]
pub type CPU0CLKCFG_R = crate::FieldReader<u8, u8>;
#[doc = "Field `CPU0CLKCFG` writer - Clock Configuration value that drives CPU0CLKCFG signals."]
pub type CPU0CLKCFG_W<'a, const O: u8> = crate::FieldWriter<'a, u32, CLK_CFG0_SPEC, u8, u8, 4, O>;
#[doc = "Field `CPU0CLKCFGSTATUS` reader - Clock Configuration Status value that reports the status of clock control for CPU0CLK."]
pub type CPU0CLKCFGSTATUS_R = crate::FieldReader<u8, u8>;
impl R {
    #[doc = "Bits 0:3 - Clock Configuration value that drives CPU0CLKCFG signals."]
    #[inline(always)]
    pub fn cpu0clkcfg(&self) -> CPU0CLKCFG_R {
        CPU0CLKCFG_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bits 16:19 - Clock Configuration Status value that reports the status of clock control for CPU0CLK."]
    #[inline(always)]
    pub fn cpu0clkcfgstatus(&self) -> CPU0CLKCFGSTATUS_R {
        CPU0CLKCFGSTATUS_R::new(((self.bits >> 16) & 0x0f) as u8)
    }
}
impl W {
    #[doc = "Bits 0:3 - Clock Configuration value that drives CPU0CLKCFG signals."]
    #[inline(always)]
    pub fn cpu0clkcfg(&mut self) -> CPU0CLKCFG_W<0> {
        CPU0CLKCFG_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Clock Configuration Register 0.\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [clk_cfg0](index.html) module"]
pub struct CLK_CFG0_SPEC;
impl crate::RegisterSpec for CLK_CFG0_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [clk_cfg0::R](R) reader structure"]
impl crate::Readable for CLK_CFG0_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [clk_cfg0::W](W) writer structure"]
impl crate::Writable for CLK_CFG0_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CLK_CFG0 to value 0"]
impl crate::Resettable for CLK_CFG0_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
