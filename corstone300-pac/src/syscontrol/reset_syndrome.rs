// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RESET_SYNDROME` reader"]
pub struct R(crate::R<RESET_SYNDROME_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RESET_SYNDROME_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RESET_SYNDROME_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RESET_SYNDROME_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `RESET_SYNDROME` writer"]
pub struct W(crate::W<RESET_SYNDROME_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<RESET_SYNDROME_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<RESET_SYNDROME_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<RESET_SYNDROME_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `PoR` writer - Power-on"]
pub type PO_R_W<'a, const O: u8> = crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
#[doc = "Field `NSWDRSTREQ` writer - Non-Secure Watchdog Cold Reset Request."]
pub type NSWDRSTREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
#[doc = "Field `SWDRSTREQ` writer - Secure Watchdog Cold Reset Request."]
pub type SWDRSTREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
#[doc = "Field `SLOWCLKWDRSTREQ` writer - SLOWCLK Watchdog Cold Reset Request."]
pub type SLOWCLKWDRSTREQ_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
#[doc = "Field `RESETREQ` writer - Subsystem Hardware Cold Reset Request Input."]
pub type RESETREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
#[doc = "Field `SWRESETREQ` writer - Software Cold Reset Request."]
pub type SWRESETREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
#[doc = "Field `HOSTRESETREQ` writer - Host Level Cold Reset Request Input."]
pub type HOSTRESETREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
#[doc = "Field `CPU0RSTREQ` writer - CPU 0 Warm Reset Request."]
pub type CPU0RSTREQ_W<'a, const O: u8> = crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
#[doc = "Field `CPU0LOCKUP` writer - CPU 0 Lockup Status."]
pub type CPU0LOCKUP_W<'a, const O: u8> = crate::BitWriter<'a, u32, RESET_SYNDROME_SPEC, bool, O>;
impl W {
    #[doc = "Bit 0 - Power-on"]
    #[inline(always)]
    pub fn po_r(&mut self) -> PO_R_W<0> {
        PO_R_W::new(self)
    }
    #[doc = "Bit 1 - Non-Secure Watchdog Cold Reset Request."]
    #[inline(always)]
    pub fn nswdrstreq(&mut self) -> NSWDRSTREQ_W<1> {
        NSWDRSTREQ_W::new(self)
    }
    #[doc = "Bit 2 - Secure Watchdog Cold Reset Request."]
    #[inline(always)]
    pub fn swdrstreq(&mut self) -> SWDRSTREQ_W<2> {
        SWDRSTREQ_W::new(self)
    }
    #[doc = "Bit 3 - SLOWCLK Watchdog Cold Reset Request."]
    #[inline(always)]
    pub fn slowclkwdrstreq(&mut self) -> SLOWCLKWDRSTREQ_W<3> {
        SLOWCLKWDRSTREQ_W::new(self)
    }
    #[doc = "Bit 4 - Subsystem Hardware Cold Reset Request Input."]
    #[inline(always)]
    pub fn resetreq(&mut self) -> RESETREQ_W<4> {
        RESETREQ_W::new(self)
    }
    #[doc = "Bit 5 - Software Cold Reset Request."]
    #[inline(always)]
    pub fn swresetreq(&mut self) -> SWRESETREQ_W<5> {
        SWRESETREQ_W::new(self)
    }
    #[doc = "Bit 7 - Host Level Cold Reset Request Input."]
    #[inline(always)]
    pub fn hostresetreq(&mut self) -> HOSTRESETREQ_W<7> {
        HOSTRESETREQ_W::new(self)
    }
    #[doc = "Bit 8 - CPU 0 Warm Reset Request."]
    #[inline(always)]
    pub fn cpu0rstreq(&mut self) -> CPU0RSTREQ_W<8> {
        CPU0RSTREQ_W::new(self)
    }
    #[doc = "Bit 12 - CPU 0 Lockup Status."]
    #[inline(always)]
    pub fn cpu0lockup(&mut self) -> CPU0LOCKUP_W<12> {
        CPU0LOCKUP_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Reset Syndrome\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [reset_syndrome](index.html) module"]
pub struct RESET_SYNDROME_SPEC;
impl crate::RegisterSpec for RESET_SYNDROME_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [reset_syndrome::R](R) reader structure"]
impl crate::Readable for RESET_SYNDROME_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [reset_syndrome::W](W) writer structure"]
impl crate::Writable for RESET_SYNDROME_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets RESET_SYNDROME to value 0x01"]
impl crate::Resettable for RESET_SYNDROME_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x01
    }
}
