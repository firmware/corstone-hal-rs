// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `PDCM_PD_CPU0_SENSE` reader"]
pub struct R(crate::R<PDCM_PD_CPU0_SENSE_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<PDCM_PD_CPU0_SENSE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<PDCM_PD_CPU0_SENSE_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<PDCM_PD_CPU0_SENSE_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `PDCM_PD_CPU0_SENSE` writer"]
pub struct W(crate::W<PDCM_PD_CPU0_SENSE_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<PDCM_PD_CPU0_SENSE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<PDCM_PD_CPU0_SENSE_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<PDCM_PD_CPU0_SENSE_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `S_PD_SYS_ON` reader - Enable PD_SYS ON Sensitivity"]
pub type S_PD_SYS_ON_R = crate::BitReader<S_PD_SYS_ON_A>;
#[doc = "Enable PD_SYS ON Sensitivity\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum S_PD_SYS_ON_A {
    #[doc = "1: Keep PD_SYS awake after powered ON"]
    ENABLE = 1,
}
impl From<S_PD_SYS_ON_A> for bool {
    #[inline(always)]
    fn from(variant: S_PD_SYS_ON_A) -> Self {
        variant as u8 != 0
    }
}
impl S_PD_SYS_ON_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<S_PD_SYS_ON_A> {
        match self.bits {
            true => Some(S_PD_SYS_ON_A::ENABLE),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == S_PD_SYS_ON_A::ENABLE
    }
}
#[doc = "Field `S_PD_SYS_ON` writer - Enable PD_SYS ON Sensitivity"]
pub type S_PD_SYS_ON_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_CPU0_SENSE_SPEC, S_PD_SYS_ON_A, O>;
impl<'a, const O: u8> S_PD_SYS_ON_W<'a, O> {
    #[doc = "Keep PD_SYS awake after powered ON"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(S_PD_SYS_ON_A::ENABLE)
    }
}
#[doc = "Field `S_PD_CPU0_ON` reader - Tied to LOW"]
pub type S_PD_CPU0_ON_R = crate::BitReader<bool>;
#[doc = "Field `S_PD_DEBUG_ON` reader - Tied to LOW"]
pub type S_PD_DEBUG_ON_R = crate::BitReader<bool>;
#[doc = "Field `S_PDCMQREQ0` reader - PDCMQREQn\\[0\\]
Tied to LOW"]
pub type S_PDCMQREQ0_R = crate::BitReader<bool>;
#[doc = "Field `S_PDCMQREQ0` writer - PDCMQREQn\\[0\\]
Tied to LOW"]
pub type S_PDCMQREQ0_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_CPU0_SENSE_SPEC, bool, O>;
#[doc = "Field `S_PDCMQREQ1` reader - PDCMQREQn\\[1\\]
Tied to LOW"]
pub type S_PDCMQREQ1_R = crate::BitReader<bool>;
#[doc = "Field `S_PDCMQREQ1` writer - PDCMQREQn\\[1\\]
Tied to LOW"]
pub type S_PDCMQREQ1_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_CPU0_SENSE_SPEC, bool, O>;
#[doc = "Field `S_PDCMQREQ2` reader - PDCMQREQn\\[2\\]
Tied to LOW"]
pub type S_PDCMQREQ2_R = crate::BitReader<bool>;
#[doc = "Field `S_PDCMQREQ2` writer - PDCMQREQn\\[2\\]
Tied to LOW"]
pub type S_PDCMQREQ2_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_CPU0_SENSE_SPEC, bool, O>;
#[doc = "Field `S_PDCMQREQ3` reader - PDCMQREQn\\[3\\]
Tied to LOW"]
pub type S_PDCMQREQ3_R = crate::BitReader<bool>;
#[doc = "Field `S_PDCMQREQ3` writer - PDCMQREQn\\[3\\]
Tied to LOW"]
pub type S_PDCMQREQ3_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, PDCM_PD_CPU0_SENSE_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Enable PD_SYS ON Sensitivity"]
    #[inline(always)]
    pub fn s_pd_sys_on(&self) -> S_PD_SYS_ON_R {
        S_PD_SYS_ON_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Tied to LOW"]
    #[inline(always)]
    pub fn s_pd_cpu0_on(&self) -> S_PD_CPU0_ON_R {
        S_PD_CPU0_ON_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 13 - Tied to LOW"]
    #[inline(always)]
    pub fn s_pd_debug_on(&self) -> S_PD_DEBUG_ON_R {
        S_PD_DEBUG_ON_R::new(((self.bits >> 13) & 1) != 0)
    }
    #[doc = "Bit 16 - PDCMQREQn\\[0\\]
Tied to LOW"]
    #[inline(always)]
    pub fn s_pdcmqreq0(&self) -> S_PDCMQREQ0_R {
        S_PDCMQREQ0_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - PDCMQREQn\\[1\\]
Tied to LOW"]
    #[inline(always)]
    pub fn s_pdcmqreq1(&self) -> S_PDCMQREQ1_R {
        S_PDCMQREQ1_R::new(((self.bits >> 17) & 1) != 0)
    }
    #[doc = "Bit 18 - PDCMQREQn\\[2\\]
Tied to LOW"]
    #[inline(always)]
    pub fn s_pdcmqreq2(&self) -> S_PDCMQREQ2_R {
        S_PDCMQREQ2_R::new(((self.bits >> 18) & 1) != 0)
    }
    #[doc = "Bit 19 - PDCMQREQn\\[3\\]
Tied to LOW"]
    #[inline(always)]
    pub fn s_pdcmqreq3(&self) -> S_PDCMQREQ3_R {
        S_PDCMQREQ3_R::new(((self.bits >> 19) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Enable PD_SYS ON Sensitivity"]
    #[inline(always)]
    pub fn s_pd_sys_on(&mut self) -> S_PD_SYS_ON_W<0> {
        S_PD_SYS_ON_W::new(self)
    }
    #[doc = "Bit 16 - PDCMQREQn\\[0\\]
Tied to LOW"]
    #[inline(always)]
    pub fn s_pdcmqreq0(&mut self) -> S_PDCMQREQ0_W<16> {
        S_PDCMQREQ0_W::new(self)
    }
    #[doc = "Bit 17 - PDCMQREQn\\[1\\]
Tied to LOW"]
    #[inline(always)]
    pub fn s_pdcmqreq1(&mut self) -> S_PDCMQREQ1_W<17> {
        S_PDCMQREQ1_W::new(self)
    }
    #[doc = "Bit 18 - PDCMQREQn\\[2\\]
Tied to LOW"]
    #[inline(always)]
    pub fn s_pdcmqreq2(&mut self) -> S_PDCMQREQ2_W<18> {
        S_PDCMQREQ2_W::new(self)
    }
    #[doc = "Bit 19 - PDCMQREQn\\[3\\]
Tied to LOW"]
    #[inline(always)]
    pub fn s_pdcmqreq3(&mut self) -> S_PDCMQREQ3_W<19> {
        S_PDCMQREQ3_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "PDCM PD_CPU0 Sensitivity.\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [pdcm_pd_cpu0_sense](index.html) module"]
pub struct PDCM_PD_CPU0_SENSE_SPEC;
impl crate::RegisterSpec for PDCM_PD_CPU0_SENSE_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [pdcm_pd_cpu0_sense::R](R) reader structure"]
impl crate::Readable for PDCM_PD_CPU0_SENSE_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [pdcm_pd_cpu0_sense::W](W) writer structure"]
impl crate::Writable for PDCM_PD_CPU0_SENSE_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets PDCM_PD_CPU0_SENSE to value 0"]
impl crate::Resettable for PDCM_PD_CPU0_SENSE_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
