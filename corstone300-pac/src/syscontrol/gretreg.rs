// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `GRETREG` reader"]
pub struct R(crate::R<GRETREG_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<GRETREG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<GRETREG_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<GRETREG_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `GRETREG` writer"]
pub struct W(crate::W<GRETREG_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<GRETREG_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<GRETREG_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<GRETREG_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `GRETREG` reader - General Purpose Retention Register"]
pub type GRETREG_R = crate::FieldReader<u16, u16>;
#[doc = "Field `GRETREG` writer - General Purpose Retention Register"]
pub type GRETREG_W<'a, const O: u8> = crate::FieldWriter<'a, u32, GRETREG_SPEC, u16, u16, 16, O>;
impl R {
    #[doc = "Bits 0:15 - General Purpose Retention Register"]
    #[inline(always)]
    pub fn gretreg(&self) -> GRETREG_R {
        GRETREG_R::new((self.bits & 0xffff) as u16)
    }
}
impl W {
    #[doc = "Bits 0:15 - General Purpose Retention Register"]
    #[inline(always)]
    pub fn gretreg(&mut self) -> GRETREG_W<0> {
        GRETREG_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "General Purpose Retention\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [gretreg](index.html) module"]
pub struct GRETREG_SPEC;
impl crate::RegisterSpec for GRETREG_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [gretreg::R](R) reader structure"]
impl crate::Readable for GRETREG_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [gretreg::W](W) writer structure"]
impl crate::Writable for GRETREG_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets GRETREG to value 0"]
impl crate::Resettable for GRETREG_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
