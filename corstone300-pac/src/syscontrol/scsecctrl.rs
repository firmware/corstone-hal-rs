// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SCSECCTRL` reader"]
pub struct R(crate::R<SCSECCTRL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SCSECCTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SCSECCTRL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SCSECCTRL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `SCSECCTRL` writer"]
pub struct W(crate::W<SCSECCTRL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<SCSECCTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<SCSECCTRL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<SCSECCTRL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `SCSECCFGLOCK` reader - Control to disable writes to security-related control registers in this register block"]
pub type SCSECCFGLOCK_R = crate::BitReader<SCSECCFGLOCK_A>;
#[doc = "Control to disable writes to security-related control registers in this register block\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SCSECCFGLOCK_A {
    #[doc = "1: control to disable writes to security-related control registers in this register block"]
    DISABLE = 1,
    #[doc = "0: control to enable writes to security-related control registers in this register block"]
    ENABLE = 0,
}
impl From<SCSECCFGLOCK_A> for bool {
    #[inline(always)]
    fn from(variant: SCSECCFGLOCK_A) -> Self {
        variant as u8 != 0
    }
}
impl SCSECCFGLOCK_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SCSECCFGLOCK_A {
        match self.bits {
            true => SCSECCFGLOCK_A::DISABLE,
            false => SCSECCFGLOCK_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == SCSECCFGLOCK_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == SCSECCFGLOCK_A::ENABLE
    }
}
#[doc = "Field `SCSECCFGLOCK` writer - Control to disable writes to security-related control registers in this register block"]
pub type SCSECCFGLOCK_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SCSECCTRL_SPEC, SCSECCFGLOCK_A, O>;
impl<'a, const O: u8> SCSECCFGLOCK_W<'a, O> {
    #[doc = "control to disable writes to security-related control registers in this register block"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(SCSECCFGLOCK_A::DISABLE)
    }
    #[doc = "control to enable writes to security-related control registers in this register block"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(SCSECCFGLOCK_A::ENABLE)
    }
}
impl R {
    #[doc = "Bit 2 - Control to disable writes to security-related control registers in this register block"]
    #[inline(always)]
    pub fn scseccfglock(&self) -> SCSECCFGLOCK_R {
        SCSECCFGLOCK_R::new(((self.bits >> 2) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 2 - Control to disable writes to security-related control registers in this register block"]
    #[inline(always)]
    pub fn scseccfglock(&mut self) -> SCSECCFGLOCK_W<2> {
        SCSECCFGLOCK_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "System Security Control\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [scsecctrl](index.html) module"]
pub struct SCSECCTRL_SPEC;
impl crate::RegisterSpec for SCSECCTRL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [scsecctrl::R](R) reader structure"]
impl crate::Readable for SCSECCTRL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [scsecctrl::W](W) writer structure"]
impl crate::Writable for SCSECCTRL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets SCSECCTRL to value 0"]
impl crate::Resettable for SCSECCTRL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
