// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CLK_CFG1` reader"]
pub struct R(crate::R<CLK_CFG1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CLK_CFG1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CLK_CFG1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CLK_CFG1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CLK_CFG1` writer"]
pub struct W(crate::W<CLK_CFG1_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CLK_CFG1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CLK_CFG1_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CLK_CFG1_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `SYSCLKCFG` reader - Clock Configuration value that drives SYSCLKCFG signals."]
pub type SYSCLKCFG_R = crate::FieldReader<u8, u8>;
#[doc = "Field `SYSCLKCFG` writer - Clock Configuration value that drives SYSCLKCFG signals."]
pub type SYSCLKCFG_W<'a, const O: u8> = crate::FieldWriter<'a, u32, CLK_CFG1_SPEC, u8, u8, 4, O>;
#[doc = "Field `AONCLKCFG` reader - Clock Configuration value that drives AONCLKCFG signals."]
pub type AONCLKCFG_R = crate::FieldReader<u8, u8>;
#[doc = "Field `AONCLKCFG` writer - Clock Configuration value that drives AONCLKCFG signals."]
pub type AONCLKCFG_W<'a, const O: u8> = crate::FieldWriter<'a, u32, CLK_CFG1_SPEC, u8, u8, 4, O>;
#[doc = "Field `SYSCLKCFGSTATUS` reader - Clock Configuration Status value that reports the status of clock control for SYSCLK."]
pub type SYSCLKCFGSTATUS_R = crate::FieldReader<u8, u8>;
#[doc = "Field `AONCLKCFGSTATUS` reader - Clock Configuration Status value that reports the status of clock control for AONCLK."]
pub type AONCLKCFGSTATUS_R = crate::FieldReader<u8, u8>;
#[doc = "Field `AONCLKCFGSTATUS` writer - Clock Configuration Status value that reports the status of clock control for AONCLK."]
pub type AONCLKCFGSTATUS_W<'a, const O: u8> =
    crate::FieldWriter<'a, u32, CLK_CFG1_SPEC, u8, u8, 4, O>;
impl R {
    #[doc = "Bits 0:3 - Clock Configuration value that drives SYSCLKCFG signals."]
    #[inline(always)]
    pub fn sysclkcfg(&self) -> SYSCLKCFG_R {
        SYSCLKCFG_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bits 4:7 - Clock Configuration value that drives AONCLKCFG signals."]
    #[inline(always)]
    pub fn aonclkcfg(&self) -> AONCLKCFG_R {
        AONCLKCFG_R::new(((self.bits >> 4) & 0x0f) as u8)
    }
    #[doc = "Bits 16:19 - Clock Configuration Status value that reports the status of clock control for SYSCLK."]
    #[inline(always)]
    pub fn sysclkcfgstatus(&self) -> SYSCLKCFGSTATUS_R {
        SYSCLKCFGSTATUS_R::new(((self.bits >> 16) & 0x0f) as u8)
    }
    #[doc = "Bits 20:23 - Clock Configuration Status value that reports the status of clock control for AONCLK."]
    #[inline(always)]
    pub fn aonclkcfgstatus(&self) -> AONCLKCFGSTATUS_R {
        AONCLKCFGSTATUS_R::new(((self.bits >> 20) & 0x0f) as u8)
    }
}
impl W {
    #[doc = "Bits 0:3 - Clock Configuration value that drives SYSCLKCFG signals."]
    #[inline(always)]
    pub fn sysclkcfg(&mut self) -> SYSCLKCFG_W<0> {
        SYSCLKCFG_W::new(self)
    }
    #[doc = "Bits 4:7 - Clock Configuration value that drives AONCLKCFG signals."]
    #[inline(always)]
    pub fn aonclkcfg(&mut self) -> AONCLKCFG_W<4> {
        AONCLKCFG_W::new(self)
    }
    #[doc = "Bits 20:23 - Clock Configuration Status value that reports the status of clock control for AONCLK."]
    #[inline(always)]
    pub fn aonclkcfgstatus(&mut self) -> AONCLKCFGSTATUS_W<20> {
        AONCLKCFGSTATUS_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Clock Configuration Register 1.\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [clk_cfg1](index.html) module"]
pub struct CLK_CFG1_SPEC;
impl crate::RegisterSpec for CLK_CFG1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [clk_cfg1::R](R) reader structure"]
impl crate::Readable for CLK_CFG1_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [clk_cfg1::W](W) writer structure"]
impl crate::Writable for CLK_CFG1_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CLK_CFG1 to value 0"]
impl crate::Resettable for CLK_CFG1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
