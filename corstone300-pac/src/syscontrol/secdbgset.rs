// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SECDBGSET` writer"]
pub struct W(crate::W<SECDBGSET_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<SECDBGSET_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<SECDBGSET_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<SECDBGSET_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `DBGEN_I_SET` writer - High active debug enable set control"]
pub type DBGEN_I_SET_W<'a, const O: u8> = crate::BitWriter<'a, u32, SECDBGSET_SPEC, bool, O>;
#[doc = "Debug enable selector set control\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DBGEN_SEL_SET_AW {
    #[doc = "1: debug enable selector set control"]
    ENABLE = 1,
    #[doc = "0: debug disable selector set control"]
    DISABLE = 0,
}
impl From<DBGEN_SEL_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: DBGEN_SEL_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `DBGEN_SEL_SET` writer - Debug enable selector set control"]
pub type DBGEN_SEL_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, DBGEN_SEL_SET_AW, O>;
impl<'a, const O: u8> DBGEN_SEL_SET_W<'a, O> {
    #[doc = "debug enable selector set control"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(DBGEN_SEL_SET_AW::ENABLE)
    }
    #[doc = "debug disable selector set control"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(DBGEN_SEL_SET_AW::DISABLE)
    }
}
#[doc = "Non-invasive debug enable set control\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum NIDEN_I_SET_AW {
    #[doc = "1: non-invasive debug enable set control"]
    ENABLE = 1,
    #[doc = "0: non-invasive debug disable set control"]
    DISABLE = 0,
}
impl From<NIDEN_I_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: NIDEN_I_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `NIDEN_I_SET` writer - Non-invasive debug enable set control"]
pub type NIDEN_I_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, NIDEN_I_SET_AW, O>;
impl<'a, const O: u8> NIDEN_I_SET_W<'a, O> {
    #[doc = "non-invasive debug enable set control"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(NIDEN_I_SET_AW::ENABLE)
    }
    #[doc = "non-invasive debug disable set control"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(NIDEN_I_SET_AW::DISABLE)
    }
}
#[doc = "Non-invasive debug enable selector set control\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum NIDEN_SEL_SET_AW {
    #[doc = "1: non-invasive debug enable selector set control"]
    ENABLE = 1,
    #[doc = "0: non-invasive debug disable selector set control"]
    DISABLE = 0,
}
impl From<NIDEN_SEL_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: NIDEN_SEL_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `NIDEN_SEL_SET` writer - Non-invasive debug enable selector set control"]
pub type NIDEN_SEL_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, NIDEN_SEL_SET_AW, O>;
impl<'a, const O: u8> NIDEN_SEL_SET_W<'a, O> {
    #[doc = "non-invasive debug enable selector set control"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(NIDEN_SEL_SET_AW::ENABLE)
    }
    #[doc = "non-invasive debug disable selector set control"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(NIDEN_SEL_SET_AW::DISABLE)
    }
}
#[doc = "Secure privilege invasive debug enable set control\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPIDEN_I_SET_AW {
    #[doc = "1: Secure privilege invasive debug enable set control"]
    ENABLE = 1,
    #[doc = "0: Secure privilege invasive debug disable set control"]
    DISABLE = 0,
}
impl From<SPIDEN_I_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: SPIDEN_I_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `SPIDEN_I_SET` writer - Secure privilege invasive debug enable set control"]
pub type SPIDEN_I_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, SPIDEN_I_SET_AW, O>;
impl<'a, const O: u8> SPIDEN_I_SET_W<'a, O> {
    #[doc = "Secure privilege invasive debug enable set control"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(SPIDEN_I_SET_AW::ENABLE)
    }
    #[doc = "Secure privilege invasive debug disable set control"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(SPIDEN_I_SET_AW::DISABLE)
    }
}
#[doc = "Secure privilege invasive debug enable selector set control\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPIDEN_SEL_SET_AW {
    #[doc = "1: Secure privilege invasive debug enable selector set control"]
    ENABLE = 1,
    #[doc = "0: Secure privilege invasive debug disable selector set control"]
    DISABLE = 0,
}
impl From<SPIDEN_SEL_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: SPIDEN_SEL_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `SPIDEN_SEL_SET` writer - Secure privilege invasive debug enable selector set control"]
pub type SPIDEN_SEL_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, SPIDEN_SEL_SET_AW, O>;
impl<'a, const O: u8> SPIDEN_SEL_SET_W<'a, O> {
    #[doc = "Secure privilege invasive debug enable selector set control"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(SPIDEN_SEL_SET_AW::ENABLE)
    }
    #[doc = "Secure privilege invasive debug disable selector set control"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(SPIDEN_SEL_SET_AW::DISABLE)
    }
}
#[doc = "Secure privilege non-invasive debug enable set control\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPNIDEN_I_SET_AW {
    #[doc = "1: Secure privilege non-invasive debug enable set control"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable set control"]
    DISABLE = 0,
}
impl From<SPNIDEN_I_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: SPNIDEN_I_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `SPNIDEN_I_SET` writer - Secure privilege non-invasive debug enable set control"]
pub type SPNIDEN_I_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, SPNIDEN_I_SET_AW, O>;
impl<'a, const O: u8> SPNIDEN_I_SET_W<'a, O> {
    #[doc = "Secure privilege non-invasive debug enable set control"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(SPNIDEN_I_SET_AW::ENABLE)
    }
    #[doc = "Secure privilege non-invasive debug disable set control"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(SPNIDEN_I_SET_AW::DISABLE)
    }
}
#[doc = "Secure privilege non-invasive debug enable selector set control\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SPNIDEN_SEL_SET_AW {
    #[doc = "1: Secure privilege non-invasive debug enable selector set control"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector set control"]
    DISABLE = 0,
}
impl From<SPNIDEN_SEL_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: SPNIDEN_SEL_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `SPNIDEN_SEL_SET` writer - Secure privilege non-invasive debug enable selector set control"]
pub type SPNIDEN_SEL_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, SPNIDEN_SEL_SET_AW, O>;
impl<'a, const O: u8> SPNIDEN_SEL_SET_W<'a, O> {
    #[doc = "Secure privilege non-invasive debug enable selector set control"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(SPNIDEN_SEL_SET_AW::ENABLE)
    }
    #[doc = "Secure privilege non-invasive debug disable selector set control"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(SPNIDEN_SEL_SET_AW::DISABLE)
    }
}
#[doc = "Set internal version of Active High DAP Access Enable. Write HIGH to set DAPACCEN_I. When read returns DAPACCEN_I. RAZWI if DAPACCENSELDIS = 1.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPACCEN_I_SET_AW {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPACCEN_I_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: DAPACCEN_I_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `DAPACCEN_I_SET` writer - Set internal version of Active High DAP Access Enable. Write HIGH to set DAPACCEN_I. When read returns DAPACCEN_I. RAZWI if DAPACCENSELDIS = 1."]
pub type DAPACCEN_I_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, DAPACCEN_I_SET_AW, O>;
impl<'a, const O: u8> DAPACCEN_I_SET_W<'a, O> {
    #[doc = "Secure privilege non-invasive debug enable selector"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(DAPACCEN_I_SET_AW::ENABLE)
    }
    #[doc = "Secure privilege non-invasive debug disable selector"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(DAPACCEN_I_SET_AW::DISABLE)
    }
}
#[doc = "Set Active High DAP Access Enable Selector. Write HIGH to set DAPACCEN_SEL. RAZWI if DAPACCENSELDIS = 1.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPACCEN_SEL_SET_AW {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPACCEN_SEL_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: DAPACCEN_SEL_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `DAPACCEN_SEL_SET` writer - Set Active High DAP Access Enable Selector. Write HIGH to set DAPACCEN_SEL. RAZWI if DAPACCENSELDIS = 1."]
pub type DAPACCEN_SEL_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, DAPACCEN_SEL_SET_AW, O>;
impl<'a, const O: u8> DAPACCEN_SEL_SET_W<'a, O> {
    #[doc = "Secure privilege non-invasive debug enable selector"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(DAPACCEN_SEL_SET_AW::ENABLE)
    }
    #[doc = "Secure privilege non-invasive debug disable selector"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(DAPACCEN_SEL_SET_AW::DISABLE)
    }
}
#[doc = "Set internal version of Active High DAP to Debug Subsystem Access Enable. Write HIGH to set DAPDSSACCEN_I. When read returns DAPDSSACCEN_I. RAZWI if DAPDSSACCENSELDIS = 1.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPDSSACCEN_I_SET_AW {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPDSSACCEN_I_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: DAPDSSACCEN_I_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `DAPDSSACCEN_I_SET` writer - Set internal version of Active High DAP to Debug Subsystem Access Enable. Write HIGH to set DAPDSSACCEN_I. When read returns DAPDSSACCEN_I. RAZWI if DAPDSSACCENSELDIS = 1."]
pub type DAPDSSACCEN_I_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, DAPDSSACCEN_I_SET_AW, O>;
impl<'a, const O: u8> DAPDSSACCEN_I_SET_W<'a, O> {
    #[doc = "Secure privilege non-invasive debug enable selector"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(DAPDSSACCEN_I_SET_AW::ENABLE)
    }
    #[doc = "Secure privilege non-invasive debug disable selector"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(DAPDSSACCEN_I_SET_AW::DISABLE)
    }
}
#[doc = "Set Active High DAP to Debug Subsystem Access Enable Selector. Write HIGH to set DAPDSSACCEN_SEL. RAZWI if DAPDSSACCENSELDIS = 1.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DAPDSSACCEN_SEL_SET_AW {
    #[doc = "1: Secure privilege non-invasive debug enable selector"]
    ENABLE = 1,
    #[doc = "0: Secure privilege non-invasive debug disable selector"]
    DISABLE = 0,
}
impl From<DAPDSSACCEN_SEL_SET_AW> for bool {
    #[inline(always)]
    fn from(variant: DAPDSSACCEN_SEL_SET_AW) -> Self {
        variant as u8 != 0
    }
}
#[doc = "Field `DAPDSSACCEN_SEL_SET` writer - Set Active High DAP to Debug Subsystem Access Enable Selector. Write HIGH to set DAPDSSACCEN_SEL. RAZWI if DAPDSSACCENSELDIS = 1."]
pub type DAPDSSACCEN_SEL_SET_W<'a, const O: u8> =
    crate::BitWriter<'a, u32, SECDBGSET_SPEC, DAPDSSACCEN_SEL_SET_AW, O>;
impl<'a, const O: u8> DAPDSSACCEN_SEL_SET_W<'a, O> {
    #[doc = "Secure privilege non-invasive debug enable selector"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(DAPDSSACCEN_SEL_SET_AW::ENABLE)
    }
    #[doc = "Secure privilege non-invasive debug disable selector"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(DAPDSSACCEN_SEL_SET_AW::DISABLE)
    }
}
impl W {
    #[doc = "Bit 0 - High active debug enable set control"]
    #[inline(always)]
    pub fn dbgen_i_set(&mut self) -> DBGEN_I_SET_W<0> {
        DBGEN_I_SET_W::new(self)
    }
    #[doc = "Bit 1 - Debug enable selector set control"]
    #[inline(always)]
    pub fn dbgen_sel_set(&mut self) -> DBGEN_SEL_SET_W<1> {
        DBGEN_SEL_SET_W::new(self)
    }
    #[doc = "Bit 2 - Non-invasive debug enable set control"]
    #[inline(always)]
    pub fn niden_i_set(&mut self) -> NIDEN_I_SET_W<2> {
        NIDEN_I_SET_W::new(self)
    }
    #[doc = "Bit 3 - Non-invasive debug enable selector set control"]
    #[inline(always)]
    pub fn niden_sel_set(&mut self) -> NIDEN_SEL_SET_W<3> {
        NIDEN_SEL_SET_W::new(self)
    }
    #[doc = "Bit 4 - Secure privilege invasive debug enable set control"]
    #[inline(always)]
    pub fn spiden_i_set(&mut self) -> SPIDEN_I_SET_W<4> {
        SPIDEN_I_SET_W::new(self)
    }
    #[doc = "Bit 5 - Secure privilege invasive debug enable selector set control"]
    #[inline(always)]
    pub fn spiden_sel_set(&mut self) -> SPIDEN_SEL_SET_W<5> {
        SPIDEN_SEL_SET_W::new(self)
    }
    #[doc = "Bit 6 - Secure privilege non-invasive debug enable set control"]
    #[inline(always)]
    pub fn spniden_i_set(&mut self) -> SPNIDEN_I_SET_W<6> {
        SPNIDEN_I_SET_W::new(self)
    }
    #[doc = "Bit 7 - Secure privilege non-invasive debug enable selector set control"]
    #[inline(always)]
    pub fn spniden_sel_set(&mut self) -> SPNIDEN_SEL_SET_W<7> {
        SPNIDEN_SEL_SET_W::new(self)
    }
    #[doc = "Bit 8 - Set internal version of Active High DAP Access Enable. Write HIGH to set DAPACCEN_I. When read returns DAPACCEN_I. RAZWI if DAPACCENSELDIS = 1."]
    #[inline(always)]
    pub fn dapaccen_i_set(&mut self) -> DAPACCEN_I_SET_W<8> {
        DAPACCEN_I_SET_W::new(self)
    }
    #[doc = "Bit 9 - Set Active High DAP Access Enable Selector. Write HIGH to set DAPACCEN_SEL. RAZWI if DAPACCENSELDIS = 1."]
    #[inline(always)]
    pub fn dapaccen_sel_set(&mut self) -> DAPACCEN_SEL_SET_W<9> {
        DAPACCEN_SEL_SET_W::new(self)
    }
    #[doc = "Bit 10 - Set internal version of Active High DAP to Debug Subsystem Access Enable. Write HIGH to set DAPDSSACCEN_I. When read returns DAPDSSACCEN_I. RAZWI if DAPDSSACCENSELDIS = 1."]
    #[inline(always)]
    pub fn dapdssaccen_i_set(&mut self) -> DAPDSSACCEN_I_SET_W<10> {
        DAPDSSACCEN_I_SET_W::new(self)
    }
    #[doc = "Bit 11 - Set Active High DAP to Debug Subsystem Access Enable Selector. Write HIGH to set DAPDSSACCEN_SEL. RAZWI if DAPDSSACCENSELDIS = 1."]
    #[inline(always)]
    pub fn dapdssaccen_sel_set(&mut self) -> DAPDSSACCEN_SEL_SET_W<11> {
        DAPDSSACCEN_SEL_SET_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Secure Debug Configuration Set\n\nThis register you can [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [secdbgset](index.html) module"]
pub struct SECDBGSET_SPEC;
impl crate::RegisterSpec for SECDBGSET_SPEC {
    type Ux = u32;
}
#[doc = "`write(|w| ..)` method takes [secdbgset::W](W) writer structure"]
impl crate::Writable for SECDBGSET_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets SECDBGSET to value 0"]
impl crate::Resettable for SECDBGSET_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
