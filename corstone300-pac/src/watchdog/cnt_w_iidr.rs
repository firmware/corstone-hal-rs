// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNT_W_IIDR` reader"]
pub struct R(crate::R<CNT_W_IIDR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNT_W_IIDR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNT_W_IIDR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNT_W_IIDR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Control Frame Watchdog Interface Identification Register.\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cnt_w_iidr](index.html) module"]
pub struct CNT_W_IIDR_SPEC;
impl crate::RegisterSpec for CNT_W_IIDR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cnt_w_iidr::R](R) reader structure"]
impl crate::Readable for CNT_W_IIDR_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNT_W_IIDR to value 0x143b"]
impl crate::Resettable for CNT_W_IIDR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x143b
    }
}
