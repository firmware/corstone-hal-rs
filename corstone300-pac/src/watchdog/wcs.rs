// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `WCS` reader"]
pub struct R(crate::R<WCS_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<WCS_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<WCS_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<WCS_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `WCS` writer"]
pub struct W(crate::W<WCS_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<WCS_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<WCS_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<WCS_SPEC>) -> Self {
        W(writer)
    }
}
impl W {
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Watchdog control and Status.\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [wcs](index.html) module"]
pub struct WCS_SPEC;
impl crate::RegisterSpec for WCS_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [wcs::R](R) reader structure"]
impl crate::Readable for WCS_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [wcs::W](W) writer structure"]
impl crate::Writable for WCS_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets WCS to value 0"]
impl crate::Resettable for WCS_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
