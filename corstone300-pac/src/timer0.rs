// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Physical Count Register Lower Word."]
    pub cntpctlow: CNTPCTLOW,
    #[doc = "0x04 - Physical Count Register Higher Word."]
    pub cntpcthigh: CNTPCTHIGH,
    _reserved2: [u8; 0x08],
    #[doc = "0x10 - Counter Frequency Register."]
    pub cntfrq: CNTFRQ,
    _reserved3: [u8; 0x0c],
    #[doc = "0x20 - Timer Compare Value Lower Word Register."]
    pub cntp_cval_low: CNTP_CVAL_LOW,
    #[doc = "0x24 - Timer Compare Value Higher Word Register."]
    pub cntp_cval_high: CNTP_CVAL_HIGH,
    #[doc = "0x28 - Timer Value register."]
    pub cntp_tval: CNTP_TVAL,
    #[doc = "0x2c - Timer Control register."]
    pub cntp_ctl: CNTP_CTL,
    _reserved7: [u8; 0x10],
    #[doc = "0x40 - AutoIncrValue Lower Word Register."]
    pub cntp_aival_low: CNTP_AIVAL_LOW,
    #[doc = "0x44 - AutoIncrValue Higher Word Register."]
    pub cntp_aival_high: CNTP_AIVAL_HIGH,
    #[doc = "0x48 - AutoIncrValue Reload register."]
    pub cntp_aival_reload: CNTP_AIVAL_RELOAD,
    #[doc = "0x4c - AutoIncrValue Control register."]
    pub cntp_aival_ctl: CNTP_AIVAL_CTL,
    #[doc = "0x50 - Timer Configuration register."]
    pub cntp_cfg: CNTP_CFG,
    _reserved12: [u8; 0x0f7c],
    #[doc = "0xfd0 - Peripheral ID 4"]
    pub pidr4: PIDR4,
    _reserved13: [u8; 0x0c],
    #[doc = "0xfe0 - Peripheral ID 0"]
    pub pidr0: PIDR0,
    #[doc = "0xfe4 - Peripheral ID 1"]
    pub pidr1: PIDR1,
    #[doc = "0xfe8 - Peripheral ID 2"]
    pub pidr2: PIDR2,
    #[doc = "0xfec - Peripheral ID 3"]
    pub pidr3: PIDR3,
    #[doc = "0xff0 - Component ID 0"]
    pub cidr0: CIDR0,
    #[doc = "0xff4 - Component ID 1"]
    pub cidr1: CIDR1,
    #[doc = "0xff8 - Component ID 2"]
    pub cidr2: CIDR2,
    #[doc = "0xffc - Component ID 3"]
    pub cidr3: CIDR3,
}
#[doc = "CNTPCTLOW (r) register accessor: an alias for `Reg<CNTPCTLOW_SPEC>`"]
pub type CNTPCTLOW = crate::Reg<cntpctlow::CNTPCTLOW_SPEC>;
#[doc = "Physical Count Register Lower Word."]
pub mod cntpctlow;
#[doc = "CNTPCTHIGH (r) register accessor: an alias for `Reg<CNTPCTHIGH_SPEC>`"]
pub type CNTPCTHIGH = crate::Reg<cntpcthigh::CNTPCTHIGH_SPEC>;
#[doc = "Physical Count Register Higher Word."]
pub mod cntpcthigh;
#[doc = "CNTFRQ (rw) register accessor: an alias for `Reg<CNTFRQ_SPEC>`"]
pub type CNTFRQ = crate::Reg<cntfrq::CNTFRQ_SPEC>;
#[doc = "Counter Frequency Register."]
pub mod cntfrq;
#[doc = "CNTP_CVAL_LOW (rw) register accessor: an alias for `Reg<CNTP_CVAL_LOW_SPEC>`"]
pub type CNTP_CVAL_LOW = crate::Reg<cntp_cval_low::CNTP_CVAL_LOW_SPEC>;
#[doc = "Timer Compare Value Lower Word Register."]
pub mod cntp_cval_low;
#[doc = "CNTP_CVAL_HIGH (rw) register accessor: an alias for `Reg<CNTP_CVAL_HIGH_SPEC>`"]
pub type CNTP_CVAL_HIGH = crate::Reg<cntp_cval_high::CNTP_CVAL_HIGH_SPEC>;
#[doc = "Timer Compare Value Higher Word Register."]
pub mod cntp_cval_high;
#[doc = "CNTP_TVAL (rw) register accessor: an alias for `Reg<CNTP_TVAL_SPEC>`"]
pub type CNTP_TVAL = crate::Reg<cntp_tval::CNTP_TVAL_SPEC>;
#[doc = "Timer Value register."]
pub mod cntp_tval;
#[doc = "CNTP_CTL (rw) register accessor: an alias for `Reg<CNTP_CTL_SPEC>`"]
pub type CNTP_CTL = crate::Reg<cntp_ctl::CNTP_CTL_SPEC>;
#[doc = "Timer Control register."]
pub mod cntp_ctl;
#[doc = "CNTP_AIVAL_LOW (r) register accessor: an alias for `Reg<CNTP_AIVAL_LOW_SPEC>`"]
pub type CNTP_AIVAL_LOW = crate::Reg<cntp_aival_low::CNTP_AIVAL_LOW_SPEC>;
#[doc = "AutoIncrValue Lower Word Register."]
pub mod cntp_aival_low;
#[doc = "CNTP_AIVAL_HIGH (r) register accessor: an alias for `Reg<CNTP_AIVAL_HIGH_SPEC>`"]
pub type CNTP_AIVAL_HIGH = crate::Reg<cntp_aival_high::CNTP_AIVAL_HIGH_SPEC>;
#[doc = "AutoIncrValue Higher Word Register."]
pub mod cntp_aival_high;
#[doc = "CNTP_AIVAL_RELOAD (rw) register accessor: an alias for `Reg<CNTP_AIVAL_RELOAD_SPEC>`"]
pub type CNTP_AIVAL_RELOAD = crate::Reg<cntp_aival_reload::CNTP_AIVAL_RELOAD_SPEC>;
#[doc = "AutoIncrValue Reload register."]
pub mod cntp_aival_reload;
#[doc = "CNTP_AIVAL_CTL (rw) register accessor: an alias for `Reg<CNTP_AIVAL_CTL_SPEC>`"]
pub type CNTP_AIVAL_CTL = crate::Reg<cntp_aival_ctl::CNTP_AIVAL_CTL_SPEC>;
#[doc = "AutoIncrValue Control register."]
pub mod cntp_aival_ctl;
#[doc = "CNTP_CFG (rw) register accessor: an alias for `Reg<CNTP_CFG_SPEC>`"]
pub type CNTP_CFG = crate::Reg<cntp_cfg::CNTP_CFG_SPEC>;
#[doc = "Timer Configuration register."]
pub mod cntp_cfg;
#[doc = "PIDR4 (r) register accessor: an alias for `Reg<PIDR4_SPEC>`"]
pub type PIDR4 = crate::Reg<pidr4::PIDR4_SPEC>;
#[doc = "Peripheral ID 4"]
pub mod pidr4;
#[doc = "PIDR0 (r) register accessor: an alias for `Reg<PIDR0_SPEC>`"]
pub type PIDR0 = crate::Reg<pidr0::PIDR0_SPEC>;
#[doc = "Peripheral ID 0"]
pub mod pidr0;
#[doc = "PIDR1 (r) register accessor: an alias for `Reg<PIDR1_SPEC>`"]
pub type PIDR1 = crate::Reg<pidr1::PIDR1_SPEC>;
#[doc = "Peripheral ID 1"]
pub mod pidr1;
#[doc = "PIDR2 (r) register accessor: an alias for `Reg<PIDR2_SPEC>`"]
pub type PIDR2 = crate::Reg<pidr2::PIDR2_SPEC>;
#[doc = "Peripheral ID 2"]
pub mod pidr2;
#[doc = "PIDR3 (r) register accessor: an alias for `Reg<PIDR3_SPEC>`"]
pub type PIDR3 = crate::Reg<pidr3::PIDR3_SPEC>;
#[doc = "Peripheral ID 3"]
pub mod pidr3;
#[doc = "CIDR0 (r) register accessor: an alias for `Reg<CIDR0_SPEC>`"]
pub type CIDR0 = crate::Reg<cidr0::CIDR0_SPEC>;
#[doc = "Component ID 0"]
pub mod cidr0;
#[doc = "CIDR1 (r) register accessor: an alias for `Reg<CIDR1_SPEC>`"]
pub type CIDR1 = crate::Reg<cidr1::CIDR1_SPEC>;
#[doc = "Component ID 1"]
pub mod cidr1;
#[doc = "CIDR2 (r) register accessor: an alias for `Reg<CIDR2_SPEC>`"]
pub type CIDR2 = crate::Reg<cidr2::CIDR2_SPEC>;
#[doc = "Component ID 2"]
pub mod cidr2;
#[doc = "CIDR3 (r) register accessor: an alias for `Reg<CIDR3_SPEC>`"]
pub type CIDR3 = crate::Reg<cidr3::CIDR3_SPEC>;
#[doc = "Component ID 3"]
pub mod cidr3;
