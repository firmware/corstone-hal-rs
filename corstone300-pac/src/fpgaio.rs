// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - LED Connections"]
    pub led: LED,
    _reserved1: [u8; 0x04],
    #[doc = "0x08 - Button Connections"]
    pub button: BUTTON,
    _reserved2: [u8; 0x04],
    #[doc = "0x10 - 1Hz Up Counter"]
    pub clk1hz: CLK1HZ,
    #[doc = "0x14 - 100Hz Up Counter"]
    pub clk100hz: CLK100HZ,
    #[doc = "0x18 - Cycle up counter"]
    pub counter: COUNTER,
    #[doc = "0x1c - Reload value for prescaler counter"]
    pub prescaler: PRESCALER,
    #[doc = "0x20 - Prescale Counter"]
    pub pscntr: PSCNTR,
    _reserved7: [u8; 0x04],
    #[doc = "0x28 - User Switches"]
    pub switches: SWITCHES,
    _reserved8: [u8; 0x20],
    #[doc = "0x4c - Misc. Control"]
    pub misc: MISC,
}
#[doc = "LED (rw) register accessor: an alias for `Reg<LED_SPEC>`"]
pub type LED = crate::Reg<led::LED_SPEC>;
#[doc = "LED Connections"]
pub mod led;
#[doc = "BUTTON (rw) register accessor: an alias for `Reg<BUTTON_SPEC>`"]
pub type BUTTON = crate::Reg<button::BUTTON_SPEC>;
#[doc = "Button Connections"]
pub mod button;
#[doc = "CLK1HZ (r) register accessor: an alias for `Reg<CLK1HZ_SPEC>`"]
pub type CLK1HZ = crate::Reg<clk1hz::CLK1HZ_SPEC>;
#[doc = "1Hz Up Counter"]
pub mod clk1hz;
#[doc = "CLK100HZ (r) register accessor: an alias for `Reg<CLK100HZ_SPEC>`"]
pub type CLK100HZ = crate::Reg<clk100hz::CLK100HZ_SPEC>;
#[doc = "100Hz Up Counter"]
pub mod clk100hz;
#[doc = "COUNTER (rw) register accessor: an alias for `Reg<COUNTER_SPEC>`"]
pub type COUNTER = crate::Reg<counter::COUNTER_SPEC>;
#[doc = "Cycle up counter"]
pub mod counter;
#[doc = "PRESCALER (rw) register accessor: an alias for `Reg<PRESCALER_SPEC>`"]
pub type PRESCALER = crate::Reg<prescaler::PRESCALER_SPEC>;
#[doc = "Reload value for prescaler counter"]
pub mod prescaler;
#[doc = "PSCNTR (rw) register accessor: an alias for `Reg<PSCNTR_SPEC>`"]
pub type PSCNTR = crate::Reg<pscntr::PSCNTR_SPEC>;
#[doc = "Prescale Counter"]
pub mod pscntr;
#[doc = "SWITCHES (r) register accessor: an alias for `Reg<SWITCHES_SPEC>`"]
pub type SWITCHES = crate::Reg<switches::SWITCHES_SPEC>;
#[doc = "User Switches"]
pub mod switches;
#[doc = "MISC (rw) register accessor: an alias for `Reg<MISC_SPEC>`"]
pub type MISC = crate::Reg<misc::MISC_SPEC>;
#[doc = "Misc. Control"]
pub mod misc;
