// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ERROR` reader"]
pub struct R(crate::R<ERROR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ERROR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ERROR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ERROR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `ERROR` writer"]
pub struct W(crate::W<ERROR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<ERROR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<ERROR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<ERROR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `TxOverrun` reader - Transmit buffer ovverrun or underrun. Set this bit to clear."]
pub type TX_OVERRUN_R = crate::BitReader<bool>;
#[doc = "Field `TxOverrun` writer - Transmit buffer ovverrun or underrun. Set this bit to clear."]
pub type TX_OVERRUN_W<'a, const O: u8> = crate::BitWriter<'a, u32, ERROR_SPEC, bool, O>;
#[doc = "Field `RxOverrun` reader - Receive buffer ovverrun. Set this bit to clear."]
pub type RX_OVERRUN_R = crate::BitReader<bool>;
#[doc = "Field `RxOverrun` writer - Receive buffer ovverrun. Set this bit to clear."]
pub type RX_OVERRUN_W<'a, const O: u8> = crate::BitWriter<'a, u32, ERROR_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Transmit buffer ovverrun or underrun. Set this bit to clear."]
    #[inline(always)]
    pub fn tx_overrun(&self) -> TX_OVERRUN_R {
        TX_OVERRUN_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Receive buffer ovverrun. Set this bit to clear."]
    #[inline(always)]
    pub fn rx_overrun(&self) -> RX_OVERRUN_R {
        RX_OVERRUN_R::new(((self.bits >> 1) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Transmit buffer ovverrun or underrun. Set this bit to clear."]
    #[inline(always)]
    pub fn tx_overrun(&mut self) -> TX_OVERRUN_W<0> {
        TX_OVERRUN_W::new(self)
    }
    #[doc = "Bit 1 - Receive buffer ovverrun. Set this bit to clear."]
    #[inline(always)]
    pub fn rx_overrun(&mut self) -> RX_OVERRUN_W<1> {
        RX_OVERRUN_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Error status register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [error](index.html) module"]
pub struct ERROR_SPEC;
impl crate::RegisterSpec for ERROR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [error::R](R) reader structure"]
impl crate::Readable for ERROR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [error::W](W) writer structure"]
impl crate::Writable for ERROR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets ERROR to value 0"]
impl crate::Resettable for ERROR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
