// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ITIP1` reader"]
pub struct R(crate::R<ITIP1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ITIP1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ITIP1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ITIP1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `ITIP1` writer"]
pub struct W(crate::W<ITIP1_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<ITIP1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<ITIP1_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<ITIP1_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `SDIN` reader - SDIN pin value"]
pub type SDIN_R = crate::BitReader<bool>;
#[doc = "Field `SDIN` writer - SDIN pin value"]
pub type SDIN_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITIP1_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - SDIN pin value"]
    #[inline(always)]
    pub fn sdin(&self) -> SDIN_R {
        SDIN_R::new((self.bits & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - SDIN pin value"]
    #[inline(always)]
    pub fn sdin(&mut self) -> SDIN_W<0> {
        SDIN_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Integration Test Input register 1\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [itip1](index.html) module"]
pub struct ITIP1_SPEC;
impl crate::RegisterSpec for ITIP1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [itip1::R](R) reader structure"]
impl crate::Readable for ITIP1_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [itip1::W](W) writer structure"]
impl crate::Writable for ITIP1_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets ITIP1 to value 0"]
impl crate::Resettable for ITIP1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
