// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ITCR` reader"]
pub struct R(crate::R<ITCR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ITCR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ITCR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ITCR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `ITCR` writer"]
pub struct W(crate::W<ITCR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<ITCR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<ITCR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<ITCR_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `ITCR` reader - Integration test control bit"]
pub type ITCR_R = crate::BitReader<bool>;
#[doc = "Field `ITCR` writer - Integration test control bit"]
pub type ITCR_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITCR_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Integration test control bit"]
    #[inline(always)]
    pub fn itcr(&self) -> ITCR_R {
        ITCR_R::new((self.bits & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Integration test control bit"]
    #[inline(always)]
    pub fn itcr(&mut self) -> ITCR_W<0> {
        ITCR_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Integration Test Control register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [itcr](index.html) module"]
pub struct ITCR_SPEC;
impl crate::RegisterSpec for ITCR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [itcr::R](R) reader structure"]
impl crate::Readable for ITCR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [itcr::W](W) writer structure"]
impl crate::Writable for ITCR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets ITCR to value 0"]
impl crate::Resettable for ITCR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
