// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ITOP1` reader"]
pub struct R(crate::R<ITOP1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ITOP1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ITOP1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ITOP1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `ITOP1` writer"]
pub struct W(crate::W<ITOP1_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<ITOP1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<ITOP1_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<ITOP1_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `SDOUT` reader - SDOUT signal value"]
pub type SDOUT_R = crate::BitReader<bool>;
#[doc = "Field `SDOUT` writer - SDOUT signal value"]
pub type SDOUT_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP1_SPEC, bool, O>;
#[doc = "Field `SCLK` reader - SLCK signal value"]
pub type SCLK_R = crate::BitReader<bool>;
#[doc = "Field `SCLK` writer - SLCK signal value"]
pub type SCLK_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP1_SPEC, bool, O>;
#[doc = "Field `LRCK` reader - LRCK signal value"]
pub type LRCK_R = crate::BitReader<bool>;
#[doc = "Field `LRCK` writer - LRCK signal value"]
pub type LRCK_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP1_SPEC, bool, O>;
#[doc = "Field `IRQOUT` reader - IRQOUT signal value"]
pub type IRQOUT_R = crate::BitReader<bool>;
#[doc = "Field `IRQOUT` writer - IRQOUT signal value"]
pub type IRQOUT_W<'a, const O: u8> = crate::BitWriter<'a, u32, ITOP1_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - SDOUT signal value"]
    #[inline(always)]
    pub fn sdout(&self) -> SDOUT_R {
        SDOUT_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - SLCK signal value"]
    #[inline(always)]
    pub fn sclk(&self) -> SCLK_R {
        SCLK_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - LRCK signal value"]
    #[inline(always)]
    pub fn lrck(&self) -> LRCK_R {
        LRCK_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - IRQOUT signal value"]
    #[inline(always)]
    pub fn irqout(&self) -> IRQOUT_R {
        IRQOUT_R::new(((self.bits >> 3) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - SDOUT signal value"]
    #[inline(always)]
    pub fn sdout(&mut self) -> SDOUT_W<0> {
        SDOUT_W::new(self)
    }
    #[doc = "Bit 1 - SLCK signal value"]
    #[inline(always)]
    pub fn sclk(&mut self) -> SCLK_W<1> {
        SCLK_W::new(self)
    }
    #[doc = "Bit 2 - LRCK signal value"]
    #[inline(always)]
    pub fn lrck(&mut self) -> LRCK_W<2> {
        LRCK_W::new(self)
    }
    #[doc = "Bit 3 - IRQOUT signal value"]
    #[inline(always)]
    pub fn irqout(&mut self) -> IRQOUT_W<3> {
        IRQOUT_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Integration Test Output register 1\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [itop1](index.html) module"]
pub struct ITOP1_SPEC;
impl crate::RegisterSpec for ITOP1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [itop1::R](R) reader structure"]
impl crate::Readable for ITOP1_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [itop1::W](W) writer structure"]
impl crate::Writable for ITOP1_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets ITOP1 to value 0"]
impl crate::Resettable for ITOP1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
