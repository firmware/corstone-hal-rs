// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `DIVIDE` reader"]
pub struct R(crate::R<DIVIDE_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<DIVIDE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<DIVIDE_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<DIVIDE_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `DIVIDE` writer"]
pub struct W(crate::W<DIVIDE_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<DIVIDE_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<DIVIDE_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<DIVIDE_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `LRDIV` reader - Left/Right. The default value is 0x80"]
pub type LRDIV_R = crate::FieldReader<u16, u16>;
#[doc = "Field `LRDIV` writer - Left/Right. The default value is 0x80"]
pub type LRDIV_W<'a, const O: u8> = crate::FieldWriter<'a, u32, DIVIDE_SPEC, u16, u16, 10, O>;
impl R {
    #[doc = "Bits 0:9 - Left/Right. The default value is 0x80"]
    #[inline(always)]
    pub fn lrdiv(&self) -> LRDIV_R {
        LRDIV_R::new((self.bits & 0x03ff) as u16)
    }
}
impl W {
    #[doc = "Bits 0:9 - Left/Right. The default value is 0x80"]
    #[inline(always)]
    pub fn lrdiv(&mut self) -> LRDIV_W<0> {
        LRDIV_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Clock Divide Ratio register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [divide](index.html) module"]
pub struct DIVIDE_SPEC;
impl crate::RegisterSpec for DIVIDE_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [divide::R](R) reader structure"]
impl crate::Readable for DIVIDE_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [divide::W](W) writer structure"]
impl crate::Writable for DIVIDE_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets DIVIDE to value 0"]
impl crate::Resettable for DIVIDE_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
