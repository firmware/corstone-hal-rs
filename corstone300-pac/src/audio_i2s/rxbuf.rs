// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `RXBUF` reader"]
pub struct R(crate::R<RXBUF_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<RXBUF_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<RXBUF_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<RXBUF_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `RightChannel` reader - Right channel value"]
pub type RIGHT_CHANNEL_R = crate::FieldReader<u16, u16>;
#[doc = "Field `LeftChannel` reader - Left channel value"]
pub type LEFT_CHANNEL_R = crate::FieldReader<u16, u16>;
impl R {
    #[doc = "Bits 0:15 - Right channel value"]
    #[inline(always)]
    pub fn right_channel(&self) -> RIGHT_CHANNEL_R {
        RIGHT_CHANNEL_R::new((self.bits & 0xffff) as u16)
    }
    #[doc = "Bits 16:31 - Left channel value"]
    #[inline(always)]
    pub fn left_channel(&self) -> LEFT_CHANNEL_R {
        LEFT_CHANNEL_R::new(((self.bits >> 16) & 0xffff) as u16)
    }
}
#[doc = "Receive Buffer FIFO Data register\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [rxbuf](index.html) module"]
pub struct RXBUF_SPEC;
impl crate::RegisterSpec for RXBUF_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [rxbuf::R](R) reader structure"]
impl crate::Readable for RXBUF_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets RXBUF to value 0"]
impl crate::Resettable for RXBUF_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
