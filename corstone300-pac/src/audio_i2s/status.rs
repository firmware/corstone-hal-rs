// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `STATUS` reader"]
pub struct R(crate::R<STATUS_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<STATUS_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<STATUS_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<STATUS_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `TxBuffAlert` reader - Transmit Buffer Alert (Depends on Water level)"]
pub type TX_BUFF_ALERT_R = crate::BitReader<bool>;
#[doc = "Field `RxBuffAlert` reader - Receive Buffer Alert (Depends on Water level)"]
pub type RX_BUFF_ALERT_R = crate::BitReader<bool>;
#[doc = "Field `TxBuffEmpty` reader - Transmit Buffer Empty"]
pub type TX_BUFF_EMPTY_R = crate::BitReader<bool>;
#[doc = "Field `TxBuffFull` reader - Transmit Buffer Full"]
pub type TX_BUFF_FULL_R = crate::BitReader<bool>;
#[doc = "Field `RxBuffEmpty` reader - Receive Buffer Empty"]
pub type RX_BUFF_EMPTY_R = crate::BitReader<bool>;
#[doc = "Field `RxBuffFull` reader - Receive Buffer Full"]
pub type RX_BUFF_FULL_R = crate::BitReader<bool>;
impl R {
    #[doc = "Bit 0 - Transmit Buffer Alert (Depends on Water level)"]
    #[inline(always)]
    pub fn tx_buff_alert(&self) -> TX_BUFF_ALERT_R {
        TX_BUFF_ALERT_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Receive Buffer Alert (Depends on Water level)"]
    #[inline(always)]
    pub fn rx_buff_alert(&self) -> RX_BUFF_ALERT_R {
        RX_BUFF_ALERT_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Transmit Buffer Empty"]
    #[inline(always)]
    pub fn tx_buff_empty(&self) -> TX_BUFF_EMPTY_R {
        TX_BUFF_EMPTY_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Transmit Buffer Full"]
    #[inline(always)]
    pub fn tx_buff_full(&self) -> TX_BUFF_FULL_R {
        TX_BUFF_FULL_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bit 4 - Receive Buffer Empty"]
    #[inline(always)]
    pub fn rx_buff_empty(&self) -> RX_BUFF_EMPTY_R {
        RX_BUFF_EMPTY_R::new(((self.bits >> 4) & 1) != 0)
    }
    #[doc = "Bit 5 - Receive Buffer Full"]
    #[inline(always)]
    pub fn rx_buff_full(&self) -> RX_BUFF_FULL_R {
        RX_BUFF_FULL_R::new(((self.bits >> 5) & 1) != 0)
    }
}
#[doc = "Status register\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [status](index.html) module"]
pub struct STATUS_SPEC;
impl crate::RegisterSpec for STATUS_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [status::R](R) reader structure"]
impl crate::Readable for STATUS_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets STATUS to value 0"]
impl crate::Resettable for STATUS_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
