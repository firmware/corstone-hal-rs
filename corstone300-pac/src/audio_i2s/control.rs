// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CONTROL` reader"]
pub struct R(crate::R<CONTROL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CONTROL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CONTROL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CONTROL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CONTROL` writer"]
pub struct W(crate::W<CONTROL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CONTROL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CONTROL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CONTROL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `TxEnable` reader - Enable Transfer Buffer"]
pub type TX_ENABLE_R = crate::BitReader<bool>;
#[doc = "Field `TxEnable` writer - Enable Transfer Buffer"]
pub type TX_ENABLE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CONTROL_SPEC, bool, O>;
#[doc = "Field `TxIRQEnable` reader - Enable Interrupt on Transmit Buffer"]
pub type TX_IRQENABLE_R = crate::BitReader<bool>;
#[doc = "Field `TxIRQEnable` writer - Enable Interrupt on Transmit Buffer"]
pub type TX_IRQENABLE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CONTROL_SPEC, bool, O>;
#[doc = "Field `RxEnable` reader - Enable Receive Buffer"]
pub type RX_ENABLE_R = crate::BitReader<bool>;
#[doc = "Field `RxEnable` writer - Enable Receive Buffer"]
pub type RX_ENABLE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CONTROL_SPEC, bool, O>;
#[doc = "Field `RxIRQEnable` reader - Enable Interrupt on Receive Buffer"]
pub type RX_IRQENABLE_R = crate::BitReader<bool>;
#[doc = "Field `RxIRQEnable` writer - Enable Interrupt on Receive Buffer"]
pub type RX_IRQENABLE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CONTROL_SPEC, bool, O>;
#[doc = "Field `TxBufIRQLevel` reader - Transmit Buffer IRQ Water level"]
pub type TX_BUF_IRQLEVEL_R = crate::FieldReader<u8, u8>;
#[doc = "Field `TxBufIRQLevel` writer - Transmit Buffer IRQ Water level"]
pub type TX_BUF_IRQLEVEL_W<'a, const O: u8> =
    crate::FieldWriter<'a, u32, CONTROL_SPEC, u8, u8, 3, O>;
#[doc = "Field `RxBufIRQLevel` reader - Receive Buffer IRQ Water level"]
pub type RX_BUF_IRQLEVEL_R = crate::FieldReader<u8, u8>;
#[doc = "Field `RxBufIRQLevel` writer - Receive Buffer IRQ Water level"]
pub type RX_BUF_IRQLEVEL_W<'a, const O: u8> =
    crate::FieldWriter<'a, u32, CONTROL_SPEC, u8, u8, 3, O>;
#[doc = "Field `FIFOReset` reader - FIFO reset"]
pub type FIFORESET_R = crate::BitReader<bool>;
#[doc = "Field `FIFOReset` writer - FIFO reset"]
pub type FIFORESET_W<'a, const O: u8> = crate::BitWriter<'a, u32, CONTROL_SPEC, bool, O>;
#[doc = "Field `CodecReset` reader - Audio codec reset control"]
pub type CODEC_RESET_R = crate::BitReader<bool>;
#[doc = "Field `CodecReset` writer - Audio codec reset control"]
pub type CODEC_RESET_W<'a, const O: u8> = crate::BitWriter<'a, u32, CONTROL_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Enable Transfer Buffer"]
    #[inline(always)]
    pub fn tx_enable(&self) -> TX_ENABLE_R {
        TX_ENABLE_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Enable Interrupt on Transmit Buffer"]
    #[inline(always)]
    pub fn tx_irqenable(&self) -> TX_IRQENABLE_R {
        TX_IRQENABLE_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - Enable Receive Buffer"]
    #[inline(always)]
    pub fn rx_enable(&self) -> RX_ENABLE_R {
        RX_ENABLE_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Enable Interrupt on Receive Buffer"]
    #[inline(always)]
    pub fn rx_irqenable(&self) -> RX_IRQENABLE_R {
        RX_IRQENABLE_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bits 8:10 - Transmit Buffer IRQ Water level"]
    #[inline(always)]
    pub fn tx_buf_irqlevel(&self) -> TX_BUF_IRQLEVEL_R {
        TX_BUF_IRQLEVEL_R::new(((self.bits >> 8) & 7) as u8)
    }
    #[doc = "Bits 12:14 - Receive Buffer IRQ Water level"]
    #[inline(always)]
    pub fn rx_buf_irqlevel(&self) -> RX_BUF_IRQLEVEL_R {
        RX_BUF_IRQLEVEL_R::new(((self.bits >> 12) & 7) as u8)
    }
    #[doc = "Bit 16 - FIFO reset"]
    #[inline(always)]
    pub fn fiforeset(&self) -> FIFORESET_R {
        FIFORESET_R::new(((self.bits >> 16) & 1) != 0)
    }
    #[doc = "Bit 17 - Audio codec reset control"]
    #[inline(always)]
    pub fn codec_reset(&self) -> CODEC_RESET_R {
        CODEC_RESET_R::new(((self.bits >> 17) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Enable Transfer Buffer"]
    #[inline(always)]
    pub fn tx_enable(&mut self) -> TX_ENABLE_W<0> {
        TX_ENABLE_W::new(self)
    }
    #[doc = "Bit 1 - Enable Interrupt on Transmit Buffer"]
    #[inline(always)]
    pub fn tx_irqenable(&mut self) -> TX_IRQENABLE_W<1> {
        TX_IRQENABLE_W::new(self)
    }
    #[doc = "Bit 2 - Enable Receive Buffer"]
    #[inline(always)]
    pub fn rx_enable(&mut self) -> RX_ENABLE_W<2> {
        RX_ENABLE_W::new(self)
    }
    #[doc = "Bit 3 - Enable Interrupt on Receive Buffer"]
    #[inline(always)]
    pub fn rx_irqenable(&mut self) -> RX_IRQENABLE_W<3> {
        RX_IRQENABLE_W::new(self)
    }
    #[doc = "Bits 8:10 - Transmit Buffer IRQ Water level"]
    #[inline(always)]
    pub fn tx_buf_irqlevel(&mut self) -> TX_BUF_IRQLEVEL_W<8> {
        TX_BUF_IRQLEVEL_W::new(self)
    }
    #[doc = "Bits 12:14 - Receive Buffer IRQ Water level"]
    #[inline(always)]
    pub fn rx_buf_irqlevel(&mut self) -> RX_BUF_IRQLEVEL_W<12> {
        RX_BUF_IRQLEVEL_W::new(self)
    }
    #[doc = "Bit 16 - FIFO reset"]
    #[inline(always)]
    pub fn fiforeset(&mut self) -> FIFORESET_W<16> {
        FIFORESET_W::new(self)
    }
    #[doc = "Bit 17 - Audio codec reset control"]
    #[inline(always)]
    pub fn codec_reset(&mut self) -> CODEC_RESET_W<17> {
        CODEC_RESET_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Control register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [control](index.html) module"]
pub struct CONTROL_SPEC;
impl crate::RegisterSpec for CONTROL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [control::R](R) reader structure"]
impl crate::Readable for CONTROL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [control::W](W) writer structure"]
impl crate::Writable for CONTROL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CONTROL to value 0"]
impl crate::Resettable for CONTROL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
