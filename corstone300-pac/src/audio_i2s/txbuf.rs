// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `TXBUF` writer"]
pub struct W(crate::W<TXBUF_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<TXBUF_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<TXBUF_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<TXBUF_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `RightChannel` writer - Right channel value"]
pub type RIGHT_CHANNEL_W<'a, const O: u8> =
    crate::FieldWriter<'a, u32, TXBUF_SPEC, u16, u16, 16, O>;
#[doc = "Field `LeftChannel` writer - Left channel value"]
pub type LEFT_CHANNEL_W<'a, const O: u8> = crate::FieldWriter<'a, u32, TXBUF_SPEC, u16, u16, 16, O>;
impl W {
    #[doc = "Bits 0:15 - Right channel value"]
    #[inline(always)]
    pub fn right_channel(&mut self) -> RIGHT_CHANNEL_W<0> {
        RIGHT_CHANNEL_W::new(self)
    }
    #[doc = "Bits 16:31 - Left channel value"]
    #[inline(always)]
    pub fn left_channel(&mut self) -> LEFT_CHANNEL_W<16> {
        LEFT_CHANNEL_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Transmit Buffer FIFO Data register\n\nThis register you can [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [txbuf](index.html) module"]
pub struct TXBUF_SPEC;
impl crate::RegisterSpec for TXBUF_SPEC {
    type Ux = u32;
}
#[doc = "`write(|w| ..)` method takes [txbuf::W](W) writer structure"]
impl crate::Writable for TXBUF_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets TXBUF to value 0"]
impl crate::Resettable for TXBUF_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
