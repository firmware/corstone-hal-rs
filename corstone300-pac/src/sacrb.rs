// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Secure Privilege Controller Secure Configuration Control register"]
    pub spcsectrl: SPCSECTRL,
    #[doc = "0x04 - Bus Access wait control after reset"]
    pub buswait: BUSWAIT,
    _reserved2: [u8; 0x08],
    #[doc = "0x10 - Security Violation Response Configuration register"]
    pub secrespcfg: SECRESPCFG,
    #[doc = "0x14 - Non Secure Callable Configuration for IDAU"]
    pub nsccfg: NSCCFG,
    _reserved4: [u8; 0x04],
    #[doc = "0x1c - Secure MPC Interrupt Status"]
    pub secmpcintstatus: SECMPCINTSTATUS,
    #[doc = "0x20 - Secure PPC Interrupt Status"]
    pub secppcintstat: SECPPCINTSTAT,
    #[doc = "0x24 - Secure PPC Interrupt Clear"]
    pub secppcintclr: SECPPCINTCLR,
    #[doc = "0x28 - Secure PPC Interrupt Enable"]
    pub secppcinten: SECPPCINTEN,
    _reserved8: [u8; 0x04],
    #[doc = "0x30 - Secure MSC Interrupt Status"]
    pub secmscintstat: SECMSCINTSTAT,
    #[doc = "0x34 - Secure MSC Interrupt Clear"]
    pub secmscintclr: SECMSCINTCLR,
    #[doc = "0x38 - Secure MSC Interrupt Enable"]
    pub secmscinten: SECMSCINTEN,
    _reserved11: [u8; 0x04],
    #[doc = "0x40 - Bridge Buffer Error Interrupt Status"]
    pub brgintstat: BRGINTSTAT,
    #[doc = "0x44 - Bridge Buffer Error Interrupt Clear"]
    pub brgintclr: BRGINTCLR,
    #[doc = "0x48 - Bridge Buffer Error Interrupt Enable"]
    pub brginten: BRGINTEN,
    _reserved14: [u8; 0x04],
    #[doc = "0x50 - Non-Secure Access AHB slave Peripheral Protection Control 0"]
    pub ahbnsppc0: AHBNSPPC0,
    _reserved15: [u8; 0x0c],
    #[doc = "0x60 - Expansion 0 Non_Secure Access AHB slave Peripheral Protection Control"]
    pub ahbnsppcexp0: AHBNSPPCEXP0,
    #[doc = "0x64 - Expansion 1 Non_Secure Access AHB slave Peripheral Protection Control"]
    pub ahbnsppcexp1: AHBNSPPCEXP1,
    #[doc = "0x68 - Expansion 2 Non_Secure Access AHB slave Peripheral Protection Control"]
    pub ahbnsppcexp2: AHBNSPPCEXP2,
    #[doc = "0x6c - Expansion 3 Non_Secure Access AHB slave Peripheral Protection Control"]
    pub ahbnsppcexp3: AHBNSPPCEXP3,
    #[doc = "0x70 - Non-Secure Access APB slave Peripheral Protection Control 0"]
    pub apbnsppc0: APBNSPPC0,
    #[doc = "0x74 - Non-Secure Access APB slave Peripheral Protection Control 1"]
    pub apbnsppc1: APBNSPPC1,
    _reserved21: [u8; 0x08],
    #[doc = "0x80 - Expansion 0 Non_Secure Access APB slave Peripheral Protection Control"]
    pub apbnsppcexp0: APBNSPPCEXP0,
    #[doc = "0x84 - Expansion 1 Non_Secure Access APB slave Peripheral Protection Control"]
    pub apbnsppcexp1: APBNSPPCEXP1,
    #[doc = "0x88 - Expansion 2 Non_Secure Access APB slave Peripheral Protection Control"]
    pub apbnsppcexp2: APBNSPPCEXP2,
    #[doc = "0x8c - Expansion 3 Non_Secure Access APB slave Peripheral Protection Control"]
    pub apbnsppcexp3: APBNSPPCEXP3,
    #[doc = "0x90 - Secure Unprivileged Access AHB slave Peripheral Protection Control 0"]
    pub ahbspppc0: AHBSPPPC0,
    _reserved26: [u8; 0x0c],
    #[doc = "0xa0 - Expansion 0 Secure Unprivileged Access AHB slave Peripheral Protection Control"]
    pub ahbspppcexp0: AHBSPPPCEXP0,
    #[doc = "0xa4 - Expansion 1 Secure Unprivileged Access AHB slave Peripheral Protection Control"]
    pub ahbspppcexp1: AHBSPPPCEXP1,
    #[doc = "0xa8 - Expansion 2 Secure Unprivileged Access AHB slave Peripheral Protection Control"]
    pub ahbspppcexp2: AHBSPPPCEXP2,
    #[doc = "0xac - Expansion 3 Secure Unprivileged Access AHB slave Peripheral Protection Control"]
    pub ahbspppcexp3: AHBSPPPCEXP3,
    #[doc = "0xb0 - Secure Unprivileged Access APB slave Peripheral Protection Control 0"]
    pub apbspppc0: APBSPPPC0,
    #[doc = "0xb4 - Secure Unprivileged Access APB slave Peripheral Protection Control 1"]
    pub apbspppc1: APBSPPPC1,
    _reserved32: [u8; 0x08],
    #[doc = "0xc0 - Expansion 0 Secure Unprivileged Access APB slave Peripheral Protection Control"]
    pub apbspppcexp0: APBSPPPCEXP0,
    #[doc = "0xc4 - Expansion 1 Secure Unprivileged Access APB slave Peripheral Protection Control"]
    pub apbspppcexp1: APBSPPPCEXP1,
    #[doc = "0xc8 - Expansion 2 Secure Unprivileged Access APB slave Peripheral Protection Control"]
    pub apbspppcexp2: APBSPPPCEXP2,
    #[doc = "0xcc - Expansion 3 Secure Unprivileged Access APB slave Peripheral Protection Control"]
    pub apbspppcexp3: APBSPPPCEXP3,
    #[doc = "0xd0 - Expansion MSC Non-Secure Configuration"]
    pub nsmscexp: NSMSCEXP,
    _reserved37: [u8; 0x0efc],
    #[doc = "0xfd0 - Peripheral ID 4"]
    pub pid4: PID4,
    _reserved38: [u8; 0x0c],
    #[doc = "0xfe0 - Peripheral ID 0"]
    pub pid0: PID0,
    #[doc = "0xfe4 - Peripheral ID 1"]
    pub pid1: PID1,
    #[doc = "0xfe8 - Peripheral ID 2"]
    pub pid2: PID2,
    #[doc = "0xfec - Peripheral ID 3"]
    pub pid3: PID3,
    #[doc = "0xff0 - Component ID 0"]
    pub cidr0: CIDR0,
    #[doc = "0xff4 - Component ID 1"]
    pub cidr1: CIDR1,
    #[doc = "0xff8 - Component ID 2"]
    pub cidr2: CIDR2,
    #[doc = "0xffc - Component ID 3"]
    pub cidr3: CIDR3,
}
#[doc = "SPCSECTRL (rw) register accessor: an alias for `Reg<SPCSECTRL_SPEC>`"]
pub type SPCSECTRL = crate::Reg<spcsectrl::SPCSECTRL_SPEC>;
#[doc = "Secure Privilege Controller Secure Configuration Control register"]
pub mod spcsectrl;
#[doc = "BUSWAIT (rw) register accessor: an alias for `Reg<BUSWAIT_SPEC>`"]
pub type BUSWAIT = crate::Reg<buswait::BUSWAIT_SPEC>;
#[doc = "Bus Access wait control after reset"]
pub mod buswait;
#[doc = "SECRESPCFG (rw) register accessor: an alias for `Reg<SECRESPCFG_SPEC>`"]
pub type SECRESPCFG = crate::Reg<secrespcfg::SECRESPCFG_SPEC>;
#[doc = "Security Violation Response Configuration register"]
pub mod secrespcfg;
#[doc = "NSCCFG (rw) register accessor: an alias for `Reg<NSCCFG_SPEC>`"]
pub type NSCCFG = crate::Reg<nsccfg::NSCCFG_SPEC>;
#[doc = "Non Secure Callable Configuration for IDAU"]
pub mod nsccfg;
#[doc = "SECMPCINTSTATUS (r) register accessor: an alias for `Reg<SECMPCINTSTATUS_SPEC>`"]
pub type SECMPCINTSTATUS = crate::Reg<secmpcintstatus::SECMPCINTSTATUS_SPEC>;
#[doc = "Secure MPC Interrupt Status"]
pub mod secmpcintstatus;
#[doc = "SECPPCINTSTAT (r) register accessor: an alias for `Reg<SECPPCINTSTAT_SPEC>`"]
pub type SECPPCINTSTAT = crate::Reg<secppcintstat::SECPPCINTSTAT_SPEC>;
#[doc = "Secure PPC Interrupt Status"]
pub mod secppcintstat;
#[doc = "SECPPCINTCLR (w) register accessor: an alias for `Reg<SECPPCINTCLR_SPEC>`"]
pub type SECPPCINTCLR = crate::Reg<secppcintclr::SECPPCINTCLR_SPEC>;
#[doc = "Secure PPC Interrupt Clear"]
pub mod secppcintclr;
#[doc = "SECPPCINTEN (rw) register accessor: an alias for `Reg<SECPPCINTEN_SPEC>`"]
pub type SECPPCINTEN = crate::Reg<secppcinten::SECPPCINTEN_SPEC>;
#[doc = "Secure PPC Interrupt Enable"]
pub mod secppcinten;
#[doc = "SECMSCINTSTAT (r) register accessor: an alias for `Reg<SECMSCINTSTAT_SPEC>`"]
pub type SECMSCINTSTAT = crate::Reg<secmscintstat::SECMSCINTSTAT_SPEC>;
#[doc = "Secure MSC Interrupt Status"]
pub mod secmscintstat;
#[doc = "SECMSCINTCLR (rw) register accessor: an alias for `Reg<SECMSCINTCLR_SPEC>`"]
pub type SECMSCINTCLR = crate::Reg<secmscintclr::SECMSCINTCLR_SPEC>;
#[doc = "Secure MSC Interrupt Clear"]
pub mod secmscintclr;
#[doc = "SECMSCINTEN (rw) register accessor: an alias for `Reg<SECMSCINTEN_SPEC>`"]
pub type SECMSCINTEN = crate::Reg<secmscinten::SECMSCINTEN_SPEC>;
#[doc = "Secure MSC Interrupt Enable"]
pub mod secmscinten;
#[doc = "BRGINTSTAT (r) register accessor: an alias for `Reg<BRGINTSTAT_SPEC>`"]
pub type BRGINTSTAT = crate::Reg<brgintstat::BRGINTSTAT_SPEC>;
#[doc = "Bridge Buffer Error Interrupt Status"]
pub mod brgintstat;
#[doc = "BRGINTCLR (w) register accessor: an alias for `Reg<BRGINTCLR_SPEC>`"]
pub type BRGINTCLR = crate::Reg<brgintclr::BRGINTCLR_SPEC>;
#[doc = "Bridge Buffer Error Interrupt Clear"]
pub mod brgintclr;
#[doc = "BRGINTEN (rw) register accessor: an alias for `Reg<BRGINTEN_SPEC>`"]
pub type BRGINTEN = crate::Reg<brginten::BRGINTEN_SPEC>;
#[doc = "Bridge Buffer Error Interrupt Enable"]
pub mod brginten;
#[doc = "AHBNSPPC0 (rw) register accessor: an alias for `Reg<AHBNSPPC0_SPEC>`"]
pub type AHBNSPPC0 = crate::Reg<ahbnsppc0::AHBNSPPC0_SPEC>;
#[doc = "Non-Secure Access AHB slave Peripheral Protection Control 0"]
pub mod ahbnsppc0;
#[doc = "AHBNSPPCEXP0 (rw) register accessor: an alias for `Reg<AHBNSPPCEXP0_SPEC>`"]
pub type AHBNSPPCEXP0 = crate::Reg<ahbnsppcexp0::AHBNSPPCEXP0_SPEC>;
#[doc = "Expansion 0 Non_Secure Access AHB slave Peripheral Protection Control"]
pub mod ahbnsppcexp0;
#[doc = "AHBNSPPCEXP1 (rw) register accessor: an alias for `Reg<AHBNSPPCEXP1_SPEC>`"]
pub type AHBNSPPCEXP1 = crate::Reg<ahbnsppcexp1::AHBNSPPCEXP1_SPEC>;
#[doc = "Expansion 1 Non_Secure Access AHB slave Peripheral Protection Control"]
pub mod ahbnsppcexp1;
#[doc = "AHBNSPPCEXP2 (rw) register accessor: an alias for `Reg<AHBNSPPCEXP2_SPEC>`"]
pub type AHBNSPPCEXP2 = crate::Reg<ahbnsppcexp2::AHBNSPPCEXP2_SPEC>;
#[doc = "Expansion 2 Non_Secure Access AHB slave Peripheral Protection Control"]
pub mod ahbnsppcexp2;
#[doc = "AHBNSPPCEXP3 (rw) register accessor: an alias for `Reg<AHBNSPPCEXP3_SPEC>`"]
pub type AHBNSPPCEXP3 = crate::Reg<ahbnsppcexp3::AHBNSPPCEXP3_SPEC>;
#[doc = "Expansion 3 Non_Secure Access AHB slave Peripheral Protection Control"]
pub mod ahbnsppcexp3;
#[doc = "APBNSPPC0 (rw) register accessor: an alias for `Reg<APBNSPPC0_SPEC>`"]
pub type APBNSPPC0 = crate::Reg<apbnsppc0::APBNSPPC0_SPEC>;
#[doc = "Non-Secure Access APB slave Peripheral Protection Control 0"]
pub mod apbnsppc0;
#[doc = "APBNSPPC1 (rw) register accessor: an alias for `Reg<APBNSPPC1_SPEC>`"]
pub type APBNSPPC1 = crate::Reg<apbnsppc1::APBNSPPC1_SPEC>;
#[doc = "Non-Secure Access APB slave Peripheral Protection Control 1"]
pub mod apbnsppc1;
#[doc = "APBNSPPCEXP0 (rw) register accessor: an alias for `Reg<APBNSPPCEXP0_SPEC>`"]
pub type APBNSPPCEXP0 = crate::Reg<apbnsppcexp0::APBNSPPCEXP0_SPEC>;
#[doc = "Expansion 0 Non_Secure Access APB slave Peripheral Protection Control"]
pub mod apbnsppcexp0;
#[doc = "APBNSPPCEXP1 (rw) register accessor: an alias for `Reg<APBNSPPCEXP1_SPEC>`"]
pub type APBNSPPCEXP1 = crate::Reg<apbnsppcexp1::APBNSPPCEXP1_SPEC>;
#[doc = "Expansion 1 Non_Secure Access APB slave Peripheral Protection Control"]
pub mod apbnsppcexp1;
#[doc = "APBNSPPCEXP2 (rw) register accessor: an alias for `Reg<APBNSPPCEXP2_SPEC>`"]
pub type APBNSPPCEXP2 = crate::Reg<apbnsppcexp2::APBNSPPCEXP2_SPEC>;
#[doc = "Expansion 2 Non_Secure Access APB slave Peripheral Protection Control"]
pub mod apbnsppcexp2;
#[doc = "APBNSPPCEXP3 (rw) register accessor: an alias for `Reg<APBNSPPCEXP3_SPEC>`"]
pub type APBNSPPCEXP3 = crate::Reg<apbnsppcexp3::APBNSPPCEXP3_SPEC>;
#[doc = "Expansion 3 Non_Secure Access APB slave Peripheral Protection Control"]
pub mod apbnsppcexp3;
#[doc = "AHBSPPPC0 (r) register accessor: an alias for `Reg<AHBSPPPC0_SPEC>`"]
pub type AHBSPPPC0 = crate::Reg<ahbspppc0::AHBSPPPC0_SPEC>;
#[doc = "Secure Unprivileged Access AHB slave Peripheral Protection Control 0"]
pub mod ahbspppc0;
#[doc = "AHBSPPPCEXP0 (rw) register accessor: an alias for `Reg<AHBSPPPCEXP0_SPEC>`"]
pub type AHBSPPPCEXP0 = crate::Reg<ahbspppcexp0::AHBSPPPCEXP0_SPEC>;
#[doc = "Expansion 0 Secure Unprivileged Access AHB slave Peripheral Protection Control"]
pub mod ahbspppcexp0;
#[doc = "AHBSPPPCEXP1 (rw) register accessor: an alias for `Reg<AHBSPPPCEXP1_SPEC>`"]
pub type AHBSPPPCEXP1 = crate::Reg<ahbspppcexp1::AHBSPPPCEXP1_SPEC>;
#[doc = "Expansion 1 Secure Unprivileged Access AHB slave Peripheral Protection Control"]
pub mod ahbspppcexp1;
#[doc = "AHBSPPPCEXP2 (rw) register accessor: an alias for `Reg<AHBSPPPCEXP2_SPEC>`"]
pub type AHBSPPPCEXP2 = crate::Reg<ahbspppcexp2::AHBSPPPCEXP2_SPEC>;
#[doc = "Expansion 2 Secure Unprivileged Access AHB slave Peripheral Protection Control"]
pub mod ahbspppcexp2;
#[doc = "AHBSPPPCEXP3 (rw) register accessor: an alias for `Reg<AHBSPPPCEXP3_SPEC>`"]
pub type AHBSPPPCEXP3 = crate::Reg<ahbspppcexp3::AHBSPPPCEXP3_SPEC>;
#[doc = "Expansion 3 Secure Unprivileged Access AHB slave Peripheral Protection Control"]
pub mod ahbspppcexp3;
#[doc = "APBSPPPC0 (rw) register accessor: an alias for `Reg<APBSPPPC0_SPEC>`"]
pub type APBSPPPC0 = crate::Reg<apbspppc0::APBSPPPC0_SPEC>;
#[doc = "Secure Unprivileged Access APB slave Peripheral Protection Control 0"]
pub mod apbspppc0;
#[doc = "APBSPPPC1 (rw) register accessor: an alias for `Reg<APBSPPPC1_SPEC>`"]
pub type APBSPPPC1 = crate::Reg<apbspppc1::APBSPPPC1_SPEC>;
#[doc = "Secure Unprivileged Access APB slave Peripheral Protection Control 1"]
pub mod apbspppc1;
#[doc = "APBSPPPCEXP0 (rw) register accessor: an alias for `Reg<APBSPPPCEXP0_SPEC>`"]
pub type APBSPPPCEXP0 = crate::Reg<apbspppcexp0::APBSPPPCEXP0_SPEC>;
#[doc = "Expansion 0 Secure Unprivileged Access APB slave Peripheral Protection Control"]
pub mod apbspppcexp0;
#[doc = "APBSPPPCEXP1 (rw) register accessor: an alias for `Reg<APBSPPPCEXP1_SPEC>`"]
pub type APBSPPPCEXP1 = crate::Reg<apbspppcexp1::APBSPPPCEXP1_SPEC>;
#[doc = "Expansion 1 Secure Unprivileged Access APB slave Peripheral Protection Control"]
pub mod apbspppcexp1;
#[doc = "APBSPPPCEXP2 (rw) register accessor: an alias for `Reg<APBSPPPCEXP2_SPEC>`"]
pub type APBSPPPCEXP2 = crate::Reg<apbspppcexp2::APBSPPPCEXP2_SPEC>;
#[doc = "Expansion 2 Secure Unprivileged Access APB slave Peripheral Protection Control"]
pub mod apbspppcexp2;
#[doc = "APBSPPPCEXP3 (rw) register accessor: an alias for `Reg<APBSPPPCEXP3_SPEC>`"]
pub type APBSPPPCEXP3 = crate::Reg<apbspppcexp3::APBSPPPCEXP3_SPEC>;
#[doc = "Expansion 3 Secure Unprivileged Access APB slave Peripheral Protection Control"]
pub mod apbspppcexp3;
#[doc = "NSMSCEXP (r) register accessor: an alias for `Reg<NSMSCEXP_SPEC>`"]
pub type NSMSCEXP = crate::Reg<nsmscexp::NSMSCEXP_SPEC>;
#[doc = "Expansion MSC Non-Secure Configuration"]
pub mod nsmscexp;
#[doc = "PID4 (r) register accessor: an alias for `Reg<PID4_SPEC>`"]
pub type PID4 = crate::Reg<pid4::PID4_SPEC>;
#[doc = "Peripheral ID 4"]
pub mod pid4;
#[doc = "PID0 (r) register accessor: an alias for `Reg<PID0_SPEC>`"]
pub type PID0 = crate::Reg<pid0::PID0_SPEC>;
#[doc = "Peripheral ID 0"]
pub mod pid0;
#[doc = "PID1 (r) register accessor: an alias for `Reg<PID1_SPEC>`"]
pub type PID1 = crate::Reg<pid1::PID1_SPEC>;
#[doc = "Peripheral ID 1"]
pub mod pid1;
#[doc = "PID2 (r) register accessor: an alias for `Reg<PID2_SPEC>`"]
pub type PID2 = crate::Reg<pid2::PID2_SPEC>;
#[doc = "Peripheral ID 2"]
pub mod pid2;
#[doc = "PID3 (r) register accessor: an alias for `Reg<PID3_SPEC>`"]
pub type PID3 = crate::Reg<pid3::PID3_SPEC>;
#[doc = "Peripheral ID 3"]
pub mod pid3;
#[doc = "CIDR0 (r) register accessor: an alias for `Reg<CIDR0_SPEC>`"]
pub type CIDR0 = crate::Reg<cidr0::CIDR0_SPEC>;
#[doc = "Component ID 0"]
pub mod cidr0;
#[doc = "CIDR1 (r) register accessor: an alias for `Reg<CIDR1_SPEC>`"]
pub type CIDR1 = crate::Reg<cidr1::CIDR1_SPEC>;
#[doc = "Component ID 1"]
pub mod cidr1;
#[doc = "CIDR2 (r) register accessor: an alias for `Reg<CIDR2_SPEC>`"]
pub type CIDR2 = crate::Reg<cidr2::CIDR2_SPEC>;
#[doc = "Component ID 2"]
pub mod cidr2;
#[doc = "CIDR3 (r) register accessor: an alias for `Reg<CIDR3_SPEC>`"]
pub type CIDR3 = crate::Reg<cidr3::CIDR3_SPEC>;
#[doc = "Component ID 3"]
pub mod cidr3;
