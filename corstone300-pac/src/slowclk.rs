// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Control Register"]
    pub ctrl: CTRL,
    #[doc = "0x04 - Current Timer Counter Value"]
    pub value: VALUE,
    #[doc = "0x08 - Counter Reload Value"]
    pub reload: RELOAD,
    _reserved_3_intclear: [u8; 0x04],
}
impl RegisterBlock {
    #[doc = "0x0c - Timer Interrupt clear register"]
    #[inline(always)]
    pub fn intclear(&self) -> &INTCLEAR {
        unsafe { &*(((self as *const Self) as *const u8).add(12usize) as *const INTCLEAR) }
    }
    #[doc = "0x0c - Timer Interrupt status register"]
    #[inline(always)]
    pub fn intstatus(&self) -> &INTSTATUS {
        unsafe { &*(((self as *const Self) as *const u8).add(12usize) as *const INTSTATUS) }
    }
}
#[doc = "CTRL (rw) register accessor: an alias for `Reg<CTRL_SPEC>`"]
pub type CTRL = crate::Reg<ctrl::CTRL_SPEC>;
#[doc = "Control Register"]
pub mod ctrl;
#[doc = "VALUE (rw) register accessor: an alias for `Reg<VALUE_SPEC>`"]
pub type VALUE = crate::Reg<value::VALUE_SPEC>;
#[doc = "Current Timer Counter Value"]
pub mod value;
#[doc = "RELOAD (rw) register accessor: an alias for `Reg<RELOAD_SPEC>`"]
pub type RELOAD = crate::Reg<reload::RELOAD_SPEC>;
#[doc = "Counter Reload Value"]
pub mod reload;
#[doc = "INTSTATUS (r) register accessor: an alias for `Reg<INTSTATUS_SPEC>`"]
pub type INTSTATUS = crate::Reg<intstatus::INTSTATUS_SPEC>;
#[doc = "Timer Interrupt status register"]
pub mod intstatus;
#[doc = "INTCLEAR (w) register accessor: an alias for `Reg<INTCLEAR_SPEC>`"]
pub type INTCLEAR = crate::Reg<intclear::INTCLEAR_SPEC>;
#[doc = "Timer Interrupt clear register"]
pub mod intclear;
