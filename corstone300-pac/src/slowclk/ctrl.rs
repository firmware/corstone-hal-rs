// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CTRL` reader"]
pub struct R(crate::R<CTRL_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CTRL_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CTRL_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CTRL` writer"]
pub struct W(crate::W<CTRL_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CTRL_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CTRL_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CTRL_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `ENABLE` reader - Enable"]
pub type ENABLE_R = crate::BitReader<ENABLE_A>;
#[doc = "Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ENABLE_A {
    #[doc = "0: Timer is disabled"]
    DISABLE = 0,
    #[doc = "1: Timer is enabled"]
    ENABLE = 1,
}
impl From<ENABLE_A> for bool {
    #[inline(always)]
    fn from(variant: ENABLE_A) -> Self {
        variant as u8 != 0
    }
}
impl ENABLE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> ENABLE_A {
        match self.bits {
            false => ENABLE_A::DISABLE,
            true => ENABLE_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == ENABLE_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == ENABLE_A::ENABLE
    }
}
#[doc = "Field `ENABLE` writer - Enable"]
pub type ENABLE_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, ENABLE_A, O>;
impl<'a, const O: u8> ENABLE_W<'a, O> {
    #[doc = "Timer is disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(ENABLE_A::DISABLE)
    }
    #[doc = "Timer is enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(ENABLE_A::ENABLE)
    }
}
#[doc = "Field `EXTIN` reader - External Input as Enable"]
pub type EXTIN_R = crate::BitReader<EXTIN_A>;
#[doc = "External Input as Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum EXTIN_A {
    #[doc = "0: External Input as Enable is disabled"]
    DISABLE = 0,
    #[doc = "1: External Input as Enable is enabled"]
    ENABLE = 1,
}
impl From<EXTIN_A> for bool {
    #[inline(always)]
    fn from(variant: EXTIN_A) -> Self {
        variant as u8 != 0
    }
}
impl EXTIN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> EXTIN_A {
        match self.bits {
            false => EXTIN_A::DISABLE,
            true => EXTIN_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == EXTIN_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == EXTIN_A::ENABLE
    }
}
#[doc = "Field `EXTIN` writer - External Input as Enable"]
pub type EXTIN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, EXTIN_A, O>;
impl<'a, const O: u8> EXTIN_W<'a, O> {
    #[doc = "External Input as Enable is disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(EXTIN_A::DISABLE)
    }
    #[doc = "External Input as Enable is enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(EXTIN_A::ENABLE)
    }
}
#[doc = "Field `EXTCLK` reader - External Clock Enable"]
pub type EXTCLK_R = crate::BitReader<EXTCLK_A>;
#[doc = "External Clock Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum EXTCLK_A {
    #[doc = "0: External Clock is disabled"]
    DISABLE = 0,
    #[doc = "1: External Clock is enabled"]
    ENABLE = 1,
}
impl From<EXTCLK_A> for bool {
    #[inline(always)]
    fn from(variant: EXTCLK_A) -> Self {
        variant as u8 != 0
    }
}
impl EXTCLK_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> EXTCLK_A {
        match self.bits {
            false => EXTCLK_A::DISABLE,
            true => EXTCLK_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == EXTCLK_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == EXTCLK_A::ENABLE
    }
}
#[doc = "Field `EXTCLK` writer - External Clock Enable"]
pub type EXTCLK_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, EXTCLK_A, O>;
impl<'a, const O: u8> EXTCLK_W<'a, O> {
    #[doc = "External Clock is disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(EXTCLK_A::DISABLE)
    }
    #[doc = "External Clock is enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(EXTCLK_A::ENABLE)
    }
}
#[doc = "Field `INTEN` reader - Interrupt Enable"]
pub type INTEN_R = crate::BitReader<INTEN_A>;
#[doc = "Interrupt Enable\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum INTEN_A {
    #[doc = "0: Interrupt is disabled"]
    DISABLE = 0,
    #[doc = "1: Interrupt is enabled"]
    ENABLE = 1,
}
impl From<INTEN_A> for bool {
    #[inline(always)]
    fn from(variant: INTEN_A) -> Self {
        variant as u8 != 0
    }
}
impl INTEN_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> INTEN_A {
        match self.bits {
            false => INTEN_A::DISABLE,
            true => INTEN_A::ENABLE,
        }
    }
    #[doc = "Checks if the value of the field is `DISABLE`"]
    #[inline(always)]
    pub fn is_disable(&self) -> bool {
        *self == INTEN_A::DISABLE
    }
    #[doc = "Checks if the value of the field is `ENABLE`"]
    #[inline(always)]
    pub fn is_enable(&self) -> bool {
        *self == INTEN_A::ENABLE
    }
}
#[doc = "Field `INTEN` writer - Interrupt Enable"]
pub type INTEN_W<'a, const O: u8> = crate::BitWriter<'a, u32, CTRL_SPEC, INTEN_A, O>;
impl<'a, const O: u8> INTEN_W<'a, O> {
    #[doc = "Interrupt is disabled"]
    #[inline(always)]
    pub fn disable(self) -> &'a mut W {
        self.variant(INTEN_A::DISABLE)
    }
    #[doc = "Interrupt is enabled"]
    #[inline(always)]
    pub fn enable(self) -> &'a mut W {
        self.variant(INTEN_A::ENABLE)
    }
}
impl R {
    #[doc = "Bit 0 - Enable"]
    #[inline(always)]
    pub fn enable(&self) -> ENABLE_R {
        ENABLE_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - External Input as Enable"]
    #[inline(always)]
    pub fn extin(&self) -> EXTIN_R {
        EXTIN_R::new(((self.bits >> 1) & 1) != 0)
    }
    #[doc = "Bit 2 - External Clock Enable"]
    #[inline(always)]
    pub fn extclk(&self) -> EXTCLK_R {
        EXTCLK_R::new(((self.bits >> 2) & 1) != 0)
    }
    #[doc = "Bit 3 - Interrupt Enable"]
    #[inline(always)]
    pub fn inten(&self) -> INTEN_R {
        INTEN_R::new(((self.bits >> 3) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Enable"]
    #[inline(always)]
    pub fn enable(&mut self) -> ENABLE_W<0> {
        ENABLE_W::new(self)
    }
    #[doc = "Bit 1 - External Input as Enable"]
    #[inline(always)]
    pub fn extin(&mut self) -> EXTIN_W<1> {
        EXTIN_W::new(self)
    }
    #[doc = "Bit 2 - External Clock Enable"]
    #[inline(always)]
    pub fn extclk(&mut self) -> EXTCLK_W<2> {
        EXTCLK_W::new(self)
    }
    #[doc = "Bit 3 - Interrupt Enable"]
    #[inline(always)]
    pub fn inten(&mut self) -> INTEN_W<3> {
        INTEN_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Control Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [ctrl](index.html) module"]
pub struct CTRL_SPEC;
impl crate::RegisterSpec for CTRL_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [ctrl::R](R) reader structure"]
impl crate::Readable for CTRL_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [ctrl::W](W) writer structure"]
impl crate::Writable for CTRL_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CTRL to value 0"]
impl crate::Resettable for CTRL_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
