// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - "]
    pub cfg_reg0: CFG_REG0,
    #[doc = "0x04 - "]
    pub cfg_reg1: CFG_REG1,
    #[doc = "0x08 - "]
    pub cfg_reg2: CFG_REG2,
    #[doc = "0x0c - "]
    pub cfg_reg3: CFG_REG3,
    #[doc = "0x10 - "]
    pub cfg_reg4: CFG_REG4,
    #[doc = "0x14 - "]
    pub cfg_reg5: CFG_REG5,
    #[doc = "0x18 - "]
    pub cfg_reg6: CFG_REG6,
    #[doc = "0x1c - "]
    pub cfg_reg7: CFG_REG7,
    _reserved8: [u8; 0x80],
    #[doc = "0xa0 - "]
    pub sys_cfgdata_rtn: SYS_CFGDATA_RTN,
    #[doc = "0xa4 - "]
    pub sys_cfgdata_out: SYS_CFGDATA_OUT,
    #[doc = "0xa8 - "]
    pub sys_cfgctrl: SYS_CFGCTRL,
    #[doc = "0xac - "]
    pub sys_cfgstat: SYS_CFGSTAT,
    _reserved12: [u8; 0x50],
    #[doc = "0x100 - DLL Lock Register"]
    pub dll: DLL,
    _reserved13: [u8; 0x0ef4],
    #[doc = "0xff8 - "]
    pub aid: AID,
    #[doc = "0xffc - "]
    pub id: ID,
}
#[doc = "CFG_REG0 (rw) register accessor: an alias for `Reg<CFG_REG0_SPEC>`"]
pub type CFG_REG0 = crate::Reg<cfg_reg0::CFG_REG0_SPEC>;
#[doc = ""]
pub mod cfg_reg0;
#[doc = "CFG_REG1 (rw) register accessor: an alias for `Reg<CFG_REG1_SPEC>`"]
pub type CFG_REG1 = crate::Reg<cfg_reg1::CFG_REG1_SPEC>;
#[doc = ""]
pub mod cfg_reg1;
#[doc = "CFG_REG2 (r) register accessor: an alias for `Reg<CFG_REG2_SPEC>`"]
pub type CFG_REG2 = crate::Reg<cfg_reg2::CFG_REG2_SPEC>;
#[doc = ""]
pub mod cfg_reg2;
#[doc = "CFG_REG3 (r) register accessor: an alias for `Reg<CFG_REG3_SPEC>`"]
pub type CFG_REG3 = crate::Reg<cfg_reg3::CFG_REG3_SPEC>;
#[doc = ""]
pub mod cfg_reg3;
#[doc = "CFG_REG4 (r) register accessor: an alias for `Reg<CFG_REG4_SPEC>`"]
pub type CFG_REG4 = crate::Reg<cfg_reg4::CFG_REG4_SPEC>;
#[doc = ""]
pub mod cfg_reg4;
#[doc = "CFG_REG5 (rw) register accessor: an alias for `Reg<CFG_REG5_SPEC>`"]
pub type CFG_REG5 = crate::Reg<cfg_reg5::CFG_REG5_SPEC>;
#[doc = ""]
pub mod cfg_reg5;
#[doc = "CFG_REG6 (r) register accessor: an alias for `Reg<CFG_REG6_SPEC>`"]
pub type CFG_REG6 = crate::Reg<cfg_reg6::CFG_REG6_SPEC>;
#[doc = ""]
pub mod cfg_reg6;
#[doc = "CFG_REG7 (r) register accessor: an alias for `Reg<CFG_REG7_SPEC>`"]
pub type CFG_REG7 = crate::Reg<cfg_reg7::CFG_REG7_SPEC>;
#[doc = ""]
pub mod cfg_reg7;
#[doc = "SYS_CFGDATA_RTN (rw) register accessor: an alias for `Reg<SYS_CFGDATA_RTN_SPEC>`"]
pub type SYS_CFGDATA_RTN = crate::Reg<sys_cfgdata_rtn::SYS_CFGDATA_RTN_SPEC>;
#[doc = ""]
pub mod sys_cfgdata_rtn;
#[doc = "SYS_CFGDATA_OUT (rw) register accessor: an alias for `Reg<SYS_CFGDATA_OUT_SPEC>`"]
pub type SYS_CFGDATA_OUT = crate::Reg<sys_cfgdata_out::SYS_CFGDATA_OUT_SPEC>;
#[doc = ""]
pub mod sys_cfgdata_out;
#[doc = "SYS_CFGCTRL (rw) register accessor: an alias for `Reg<SYS_CFGCTRL_SPEC>`"]
pub type SYS_CFGCTRL = crate::Reg<sys_cfgctrl::SYS_CFGCTRL_SPEC>;
#[doc = ""]
pub mod sys_cfgctrl;
#[doc = "SYS_CFGSTAT (rw) register accessor: an alias for `Reg<SYS_CFGSTAT_SPEC>`"]
pub type SYS_CFGSTAT = crate::Reg<sys_cfgstat::SYS_CFGSTAT_SPEC>;
#[doc = ""]
pub mod sys_cfgstat;
#[doc = "DLL (rw) register accessor: an alias for `Reg<DLL_SPEC>`"]
pub type DLL = crate::Reg<dll::DLL_SPEC>;
#[doc = "DLL Lock Register"]
pub mod dll;
#[doc = "AID (r) register accessor: an alias for `Reg<AID_SPEC>`"]
pub type AID = crate::Reg<aid::AID_SPEC>;
#[doc = ""]
pub mod aid;
#[doc = "ID (r) register accessor: an alias for `Reg<ID_SPEC>`"]
pub type ID = crate::Reg<id::ID_SPEC>;
#[doc = ""]
pub mod id;
