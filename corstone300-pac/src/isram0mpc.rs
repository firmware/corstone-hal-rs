// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - MPC Control register"]
    pub ctrl: CTRL,
    _reserved1: [u8; 0x0c],
    #[doc = "0x10 - Maximum value of block based index register"]
    pub blk_max: BLK_MAX,
    #[doc = "0x14 - Block Configuration"]
    pub blk_cfg: BLK_CFG,
    #[doc = "0x18 - Index value for accessing block based look up table"]
    pub blk_idx: BLK_IDX,
    #[doc = "0x1c - Block based gating Look Up Table"]
    pub blk_lut: BLK_LUT,
    #[doc = "0x20 - Interrupt state"]
    pub int_stat: INT_STAT,
    #[doc = "0x24 - Interrupt clear"]
    pub int_clear: INT_CLEAR,
    #[doc = "0x28 - Interrupt enable"]
    pub int_en: INT_EN,
    #[doc = "0x2c - Interrupt information 1"]
    pub int_info1: INT_INFO1,
    #[doc = "0x30 - Interrupt information 2"]
    pub int_info2: INT_INFO2,
    #[doc = "0x34 - Interrupt set. Debug purpose only"]
    pub int_set: INT_SET,
    _reserved11: [u8; 0x0f98],
    #[doc = "0xfd0 - Peripheral ID 4"]
    pub pidr4: PIDR4,
    #[doc = "0xfd4 - Peripheral ID 5"]
    pub pidr5: PIDR5,
    #[doc = "0xfd8 - Peripheral ID 6"]
    pub pidr6: PIDR6,
    #[doc = "0xfdc - Peripheral ID 7"]
    pub pidr7: PIDR7,
    #[doc = "0xfe0 - Peripheral ID 0"]
    pub pidr0: PIDR0,
    #[doc = "0xfe4 - Peripheral ID 1"]
    pub pidr1: PIDR1,
    #[doc = "0xfe8 - Peripheral ID 2"]
    pub pidr2: PIDR2,
    #[doc = "0xfec - Peripheral ID 3"]
    pub pidr3: PIDR3,
    #[doc = "0xff0 - Component ID 0"]
    pub cidr0: CIDR0,
    #[doc = "0xff4 - Component ID 1"]
    pub cidr1: CIDR1,
    #[doc = "0xff8 - Component ID 2"]
    pub cidr2: CIDR2,
    #[doc = "0xffc - Component ID 3"]
    pub cidr3: CIDR3,
}
#[doc = "CTRL (rw) register accessor: an alias for `Reg<CTRL_SPEC>`"]
pub type CTRL = crate::Reg<ctrl::CTRL_SPEC>;
#[doc = "MPC Control register"]
pub mod ctrl;
#[doc = "BLK_MAX (r) register accessor: an alias for `Reg<BLK_MAX_SPEC>`"]
pub type BLK_MAX = crate::Reg<blk_max::BLK_MAX_SPEC>;
#[doc = "Maximum value of block based index register"]
pub mod blk_max;
#[doc = "BLK_CFG (r) register accessor: an alias for `Reg<BLK_CFG_SPEC>`"]
pub type BLK_CFG = crate::Reg<blk_cfg::BLK_CFG_SPEC>;
#[doc = "Block Configuration"]
pub mod blk_cfg;
#[doc = "BLK_IDX (rw) register accessor: an alias for `Reg<BLK_IDX_SPEC>`"]
pub type BLK_IDX = crate::Reg<blk_idx::BLK_IDX_SPEC>;
#[doc = "Index value for accessing block based look up table"]
pub mod blk_idx;
#[doc = "BLK_LUT (rw) register accessor: an alias for `Reg<BLK_LUT_SPEC>`"]
pub type BLK_LUT = crate::Reg<blk_lut::BLK_LUT_SPEC>;
#[doc = "Block based gating Look Up Table"]
pub mod blk_lut;
#[doc = "INT_STAT (r) register accessor: an alias for `Reg<INT_STAT_SPEC>`"]
pub type INT_STAT = crate::Reg<int_stat::INT_STAT_SPEC>;
#[doc = "Interrupt state"]
pub mod int_stat;
#[doc = "INT_CLEAR (w) register accessor: an alias for `Reg<INT_CLEAR_SPEC>`"]
pub type INT_CLEAR = crate::Reg<int_clear::INT_CLEAR_SPEC>;
#[doc = "Interrupt clear"]
pub mod int_clear;
#[doc = "INT_EN (rw) register accessor: an alias for `Reg<INT_EN_SPEC>`"]
pub type INT_EN = crate::Reg<int_en::INT_EN_SPEC>;
#[doc = "Interrupt enable"]
pub mod int_en;
#[doc = "INT_INFO1 (r) register accessor: an alias for `Reg<INT_INFO1_SPEC>`"]
pub type INT_INFO1 = crate::Reg<int_info1::INT_INFO1_SPEC>;
#[doc = "Interrupt information 1"]
pub mod int_info1;
#[doc = "INT_INFO2 (r) register accessor: an alias for `Reg<INT_INFO2_SPEC>`"]
pub type INT_INFO2 = crate::Reg<int_info2::INT_INFO2_SPEC>;
#[doc = "Interrupt information 2"]
pub mod int_info2;
#[doc = "INT_SET (w) register accessor: an alias for `Reg<INT_SET_SPEC>`"]
pub type INT_SET = crate::Reg<int_set::INT_SET_SPEC>;
#[doc = "Interrupt set. Debug purpose only"]
pub mod int_set;
#[doc = "PIDR4 (r) register accessor: an alias for `Reg<PIDR4_SPEC>`"]
pub type PIDR4 = crate::Reg<pidr4::PIDR4_SPEC>;
#[doc = "Peripheral ID 4"]
pub mod pidr4;
#[doc = "PIDR5 (r) register accessor: an alias for `Reg<PIDR5_SPEC>`"]
pub type PIDR5 = crate::Reg<pidr5::PIDR5_SPEC>;
#[doc = "Peripheral ID 5"]
pub mod pidr5;
#[doc = "PIDR6 (r) register accessor: an alias for `Reg<PIDR6_SPEC>`"]
pub type PIDR6 = crate::Reg<pidr6::PIDR6_SPEC>;
#[doc = "Peripheral ID 6"]
pub mod pidr6;
#[doc = "PIDR7 (r) register accessor: an alias for `Reg<PIDR7_SPEC>`"]
pub type PIDR7 = crate::Reg<pidr7::PIDR7_SPEC>;
#[doc = "Peripheral ID 7"]
pub mod pidr7;
#[doc = "PIDR0 (r) register accessor: an alias for `Reg<PIDR0_SPEC>`"]
pub type PIDR0 = crate::Reg<pidr0::PIDR0_SPEC>;
#[doc = "Peripheral ID 0"]
pub mod pidr0;
#[doc = "PIDR1 (r) register accessor: an alias for `Reg<PIDR1_SPEC>`"]
pub type PIDR1 = crate::Reg<pidr1::PIDR1_SPEC>;
#[doc = "Peripheral ID 1"]
pub mod pidr1;
#[doc = "PIDR2 (r) register accessor: an alias for `Reg<PIDR2_SPEC>`"]
pub type PIDR2 = crate::Reg<pidr2::PIDR2_SPEC>;
#[doc = "Peripheral ID 2"]
pub mod pidr2;
#[doc = "PIDR3 (r) register accessor: an alias for `Reg<PIDR3_SPEC>`"]
pub type PIDR3 = crate::Reg<pidr3::PIDR3_SPEC>;
#[doc = "Peripheral ID 3"]
pub mod pidr3;
#[doc = "CIDR0 (r) register accessor: an alias for `Reg<CIDR0_SPEC>`"]
pub type CIDR0 = crate::Reg<cidr0::CIDR0_SPEC>;
#[doc = "Component ID 0"]
pub mod cidr0;
#[doc = "CIDR1 (r) register accessor: an alias for `Reg<CIDR1_SPEC>`"]
pub type CIDR1 = crate::Reg<cidr1::CIDR1_SPEC>;
#[doc = "Component ID 1"]
pub mod cidr1;
#[doc = "CIDR2 (r) register accessor: an alias for `Reg<CIDR2_SPEC>`"]
pub type CIDR2 = crate::Reg<cidr2::CIDR2_SPEC>;
#[doc = "Component ID 2"]
pub mod cidr2;
#[doc = "CIDR3 (r) register accessor: an alias for `Reg<CIDR3_SPEC>`"]
pub type CIDR3 = crate::Reg<cidr3::CIDR3_SPEC>;
#[doc = "Component ID 3"]
pub mod cidr3;
