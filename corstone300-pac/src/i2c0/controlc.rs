// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CONTROLC` reader"]
pub struct R(crate::R<CONTROLC_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CONTROLC_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CONTROLC_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CONTROLC_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CONTROLC` writer"]
pub struct W(crate::W<CONTROLC_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CONTROLC_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CONTROLC_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CONTROLC_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `SCL` reader - Serial clock line"]
pub type SCL_R = crate::BitReader<bool>;
#[doc = "Field `SCL` writer - Serial clock line"]
pub type SCL_W<'a, const O: u8> = crate::BitWriter<'a, u32, CONTROLC_SPEC, bool, O>;
#[doc = "Field `SDA` reader - Serial data line"]
pub type SDA_R = crate::BitReader<bool>;
#[doc = "Field `SDA` writer - Serial data line"]
pub type SDA_W<'a, const O: u8> = crate::BitWriter<'a, u32, CONTROLC_SPEC, bool, O>;
impl R {
    #[doc = "Bit 0 - Serial clock line"]
    #[inline(always)]
    pub fn scl(&self) -> SCL_R {
        SCL_R::new((self.bits & 1) != 0)
    }
    #[doc = "Bit 1 - Serial data line"]
    #[inline(always)]
    pub fn sda(&self) -> SDA_R {
        SDA_R::new(((self.bits >> 1) & 1) != 0)
    }
}
impl W {
    #[doc = "Bit 0 - Serial clock line"]
    #[inline(always)]
    pub fn scl(&mut self) -> SCL_W<0> {
        SCL_W::new(self)
    }
    #[doc = "Bit 1 - Serial data line"]
    #[inline(always)]
    pub fn sda(&mut self) -> SDA_W<1> {
        SDA_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Control Clear\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [controlc](index.html) module"]
pub struct CONTROLC_SPEC;
impl crate::RegisterSpec for CONTROLC_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [controlc::R](R) reader structure"]
impl crate::Readable for CONTROLC_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [controlc::W](W) writer structure"]
impl crate::Writable for CONTROLC_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CONTROLC to value 0"]
impl crate::Resettable for CONTROLC_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
