// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `ALTFUNCSET` reader"]
pub struct R(crate::R<ALTFUNCSET_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<ALTFUNCSET_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<ALTFUNCSET_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<ALTFUNCSET_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `ALTFUNCSET` writer"]
pub struct W(crate::W<ALTFUNCSET_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<ALTFUNCSET_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<ALTFUNCSET_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<ALTFUNCSET_SPEC>) -> Self {
        W(writer)
    }
}
impl W {
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Alternate function set Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [altfuncset](index.html) module"]
pub struct ALTFUNCSET_SPEC;
impl crate::RegisterSpec for ALTFUNCSET_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [altfuncset::R](R) reader structure"]
impl crate::Readable for ALTFUNCSET_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [altfuncset::W](W) writer structure"]
impl crate::Writable for ALTFUNCSET_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets ALTFUNCSET to value 0"]
impl crate::Resettable for ALTFUNCSET_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
