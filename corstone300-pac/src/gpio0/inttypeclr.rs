// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `INTTYPECLR` reader"]
pub struct R(crate::R<INTTYPECLR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<INTTYPECLR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<INTTYPECLR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<INTTYPECLR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `INTTYPECLR` writer"]
pub struct W(crate::W<INTTYPECLR_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<INTTYPECLR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<INTTYPECLR_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<INTTYPECLR_SPEC>) -> Self {
        W(writer)
    }
}
impl W {
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Interrupt type clear Register\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [inttypeclr](index.html) module"]
pub struct INTTYPECLR_SPEC;
impl crate::RegisterSpec for INTTYPECLR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [inttypeclr::R](R) reader structure"]
impl crate::Readable for INTTYPECLR_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [inttypeclr::W](W) writer structure"]
impl crate::Writable for INTTYPECLR_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets INTTYPECLR to value 0"]
impl crate::Resettable for INTTYPECLR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
