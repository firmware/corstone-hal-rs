// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SYS_CONFIG0` reader"]
pub struct R(crate::R<SYS_CONFIG0_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SYS_CONFIG0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SYS_CONFIG0_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SYS_CONFIG0_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `NUM_VM_BANK` reader - Number of Volatile Memory Banks."]
pub type NUM_VM_BANK_R = crate::FieldReader<u8, u8>;
#[doc = "Field `VM_ADDR_WIDTH` reader - Volatile Memory Bank Address Width, where the size of each bank is equal to 2VM_ADDR_WIDTH bytes."]
pub type VM_ADDR_WIDTH_R = crate::FieldReader<u8, u8>;
#[doc = "Field `HAS_CRYPTO` reader - CryptoCell Included."]
pub type HAS_CRYPTO_R = crate::BitReader<HAS_CRYPTO_A>;
#[doc = "CryptoCell Included.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum HAS_CRYPTO_A {
    #[doc = "0: CryptoCell Not Included"]
    NO = 0,
    #[doc = "1: CryptoCell Included"]
    YES = 1,
}
impl From<HAS_CRYPTO_A> for bool {
    #[inline(always)]
    fn from(variant: HAS_CRYPTO_A) -> Self {
        variant as u8 != 0
    }
}
impl HAS_CRYPTO_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> HAS_CRYPTO_A {
        match self.bits {
            false => HAS_CRYPTO_A::NO,
            true => HAS_CRYPTO_A::YES,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == HAS_CRYPTO_A::NO
    }
    #[doc = "Checks if the value of the field is `YES`"]
    #[inline(always)]
    pub fn is_yes(&self) -> bool {
        *self == HAS_CRYPTO_A::YES
    }
}
#[doc = "Field `HAS_CSS` reader - Include CoreSight SoC-600 based Debug infrastructure."]
pub type HAS_CSS_R = crate::BitReader<HAS_CSS_A>;
#[doc = "Include CoreSight SoC-600 based Debug infrastructure.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum HAS_CSS_A {
    #[doc = "0: Not included."]
    NO = 0,
    #[doc = "1: Included."]
    YES = 1,
}
impl From<HAS_CSS_A> for bool {
    #[inline(always)]
    fn from(variant: HAS_CSS_A) -> Self {
        variant as u8 != 0
    }
}
impl HAS_CSS_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> HAS_CSS_A {
        match self.bits {
            false => HAS_CSS_A::NO,
            true => HAS_CSS_A::YES,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == HAS_CSS_A::NO
    }
    #[doc = "Checks if the value of the field is `YES`"]
    #[inline(always)]
    pub fn is_yes(&self) -> bool {
        *self == HAS_CSS_A::YES
    }
}
#[doc = "Field `PI_LEVEL` reader - Power Infrastructure Level"]
pub type PI_LEVEL_R = crate::FieldReader<u8, PI_LEVEL_A>;
#[doc = "Power Infrastructure Level\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum PI_LEVEL_A {
    #[doc = "1: Intermediate Level"]
    INTERMEDIATE_LEVEL = 1,
}
impl From<PI_LEVEL_A> for u8 {
    #[inline(always)]
    fn from(variant: PI_LEVEL_A) -> Self {
        variant as _
    }
}
impl PI_LEVEL_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<PI_LEVEL_A> {
        match self.bits {
            1 => Some(PI_LEVEL_A::INTERMEDIATE_LEVEL),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `INTERMEDIATE_LEVEL`"]
    #[inline(always)]
    pub fn is_intermediate_level(&self) -> bool {
        *self == PI_LEVEL_A::INTERMEDIATE_LEVEL
    }
}
#[doc = "Field `CPU0_TYPE` reader - CPU 0 Core Type"]
pub type CPU0_TYPE_R = crate::FieldReader<u8, CPU0_TYPE_A>;
#[doc = "CPU 0 Core Type\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum CPU0_TYPE_A {
    #[doc = "3: Cortex-M55"]
    CORTEX_M55 = 3,
}
impl From<CPU0_TYPE_A> for u8 {
    #[inline(always)]
    fn from(variant: CPU0_TYPE_A) -> Self {
        variant as _
    }
}
impl CPU0_TYPE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<CPU0_TYPE_A> {
        match self.bits {
            3 => Some(CPU0_TYPE_A::CORTEX_M55),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `CORTEX_M55`"]
    #[inline(always)]
    pub fn is_cortex_m55(&self) -> bool {
        *self == CPU0_TYPE_A::CORTEX_M55
    }
}
#[doc = "Field `CPU0_HAS_SYSTCM` reader - CPU 0 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level."]
pub type CPU0_HAS_SYSTCM_R = crate::BitReader<CPU0_HAS_SYSTCM_A>;
#[doc = "CPU 0 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU0_HAS_SYSTCM_A {
    #[doc = "0: Not included."]
    NO = 0,
    #[doc = "1: Included."]
    YES = 1,
}
impl From<CPU0_HAS_SYSTCM_A> for bool {
    #[inline(always)]
    fn from(variant: CPU0_HAS_SYSTCM_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU0_HAS_SYSTCM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU0_HAS_SYSTCM_A {
        match self.bits {
            false => CPU0_HAS_SYSTCM_A::NO,
            true => CPU0_HAS_SYSTCM_A::YES,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == CPU0_HAS_SYSTCM_A::NO
    }
    #[doc = "Checks if the value of the field is `YES`"]
    #[inline(always)]
    pub fn is_yes(&self) -> bool {
        *self == CPU0_HAS_SYSTCM_A::YES
    }
}
#[doc = "Field `CPU0_TCM_BANK_NUM` reader - The VM Bank that is the TCM memory for CPU 0."]
pub type CPU0_TCM_BANK_NUM_R = crate::FieldReader<u8, u8>;
#[doc = "Field `CPU1_TYPE` reader - CPU 1 Core Type"]
pub type CPU1_TYPE_R = crate::FieldReader<u8, CPU1_TYPE_A>;
#[doc = "CPU 1 Core Type\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum CPU1_TYPE_A {
    #[doc = "0: Does not exist."]
    NO = 0,
}
impl From<CPU1_TYPE_A> for u8 {
    #[inline(always)]
    fn from(variant: CPU1_TYPE_A) -> Self {
        variant as _
    }
}
impl CPU1_TYPE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<CPU1_TYPE_A> {
        match self.bits {
            0 => Some(CPU1_TYPE_A::NO),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == CPU1_TYPE_A::NO
    }
}
#[doc = "Field `CPU1_HAS_SYSTCM` reader - CPU 1 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level."]
pub type CPU1_HAS_SYSTCM_R = crate::BitReader<CPU1_HAS_SYSTCM_A>;
#[doc = "CPU 1 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU1_HAS_SYSTCM_A {
    #[doc = "0: Not included."]
    NO = 0,
    #[doc = "1: Included."]
    YES = 1,
}
impl From<CPU1_HAS_SYSTCM_A> for bool {
    #[inline(always)]
    fn from(variant: CPU1_HAS_SYSTCM_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU1_HAS_SYSTCM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU1_HAS_SYSTCM_A {
        match self.bits {
            false => CPU1_HAS_SYSTCM_A::NO,
            true => CPU1_HAS_SYSTCM_A::YES,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == CPU1_HAS_SYSTCM_A::NO
    }
    #[doc = "Checks if the value of the field is `YES`"]
    #[inline(always)]
    pub fn is_yes(&self) -> bool {
        *self == CPU1_HAS_SYSTCM_A::YES
    }
}
#[doc = "Field `CPU1_TCM_BANK_NUM` reader - The VM Bank that is the TCM memory for CPU 1."]
pub type CPU1_TCM_BANK_NUM_R = crate::FieldReader<u8, u8>;
impl R {
    #[doc = "Bits 0:3 - Number of Volatile Memory Banks."]
    #[inline(always)]
    pub fn num_vm_bank(&self) -> NUM_VM_BANK_R {
        NUM_VM_BANK_R::new((self.bits & 0x0f) as u8)
    }
    #[doc = "Bits 4:8 - Volatile Memory Bank Address Width, where the size of each bank is equal to 2VM_ADDR_WIDTH bytes."]
    #[inline(always)]
    pub fn vm_addr_width(&self) -> VM_ADDR_WIDTH_R {
        VM_ADDR_WIDTH_R::new(((self.bits >> 4) & 0x1f) as u8)
    }
    #[doc = "Bit 9 - CryptoCell Included."]
    #[inline(always)]
    pub fn has_crypto(&self) -> HAS_CRYPTO_R {
        HAS_CRYPTO_R::new(((self.bits >> 9) & 1) != 0)
    }
    #[doc = "Bit 10 - Include CoreSight SoC-600 based Debug infrastructure."]
    #[inline(always)]
    pub fn has_css(&self) -> HAS_CSS_R {
        HAS_CSS_R::new(((self.bits >> 10) & 1) != 0)
    }
    #[doc = "Bits 11:12 - Power Infrastructure Level"]
    #[inline(always)]
    pub fn pi_level(&self) -> PI_LEVEL_R {
        PI_LEVEL_R::new(((self.bits >> 11) & 3) as u8)
    }
    #[doc = "Bits 16:18 - CPU 0 Core Type"]
    #[inline(always)]
    pub fn cpu0_type(&self) -> CPU0_TYPE_R {
        CPU0_TYPE_R::new(((self.bits >> 16) & 7) as u8)
    }
    #[doc = "Bit 19 - CPU 0 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level."]
    #[inline(always)]
    pub fn cpu0_has_systcm(&self) -> CPU0_HAS_SYSTCM_R {
        CPU0_HAS_SYSTCM_R::new(((self.bits >> 19) & 1) != 0)
    }
    #[doc = "Bits 20:23 - The VM Bank that is the TCM memory for CPU 0."]
    #[inline(always)]
    pub fn cpu0_tcm_bank_num(&self) -> CPU0_TCM_BANK_NUM_R {
        CPU0_TCM_BANK_NUM_R::new(((self.bits >> 20) & 0x0f) as u8)
    }
    #[doc = "Bits 24:26 - CPU 1 Core Type"]
    #[inline(always)]
    pub fn cpu1_type(&self) -> CPU1_TYPE_R {
        CPU1_TYPE_R::new(((self.bits >> 24) & 7) as u8)
    }
    #[doc = "Bit 27 - CPU 1 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level."]
    #[inline(always)]
    pub fn cpu1_has_systcm(&self) -> CPU1_HAS_SYSTCM_R {
        CPU1_HAS_SYSTCM_R::new(((self.bits >> 27) & 1) != 0)
    }
    #[doc = "Bits 28:31 - The VM Bank that is the TCM memory for CPU 1."]
    #[inline(always)]
    pub fn cpu1_tcm_bank_num(&self) -> CPU1_TCM_BANK_NUM_R {
        CPU1_TCM_BANK_NUM_R::new(((self.bits >> 28) & 0x0f) as u8)
    }
}
#[doc = "System Hardware Configuration 0 register\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [sys_config0](index.html) module"]
pub struct SYS_CONFIG0_SPEC;
impl crate::RegisterSpec for SYS_CONFIG0_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [sys_config0::R](R) reader structure"]
impl crate::Readable for SYS_CONFIG0_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets SYS_CONFIG0 to value 0"]
impl crate::Resettable for SYS_CONFIG0_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
