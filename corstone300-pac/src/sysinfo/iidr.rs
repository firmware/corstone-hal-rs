// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `IIDR` reader"]
pub struct R(crate::R<IIDR_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<IIDR_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<IIDR_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<IIDR_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `IMP_IMPLEMENTATOR` reader - Contains the JEP106 code of the company that implemented the subsystem implementation."]
pub type IMP_IMPLEMENTATOR_R = crate::FieldReader<u16, u16>;
#[doc = "Field `IMP_REVISION` reader - IMPL_DEF value used to distinguish minor revisions of the subsystem implementation."]
pub type IMP_REVISION_R = crate::FieldReader<u8, u8>;
#[doc = "Field `IMP_VARIANT` reader - IMPL_DEF value variant or major revision of the subsystem implementation."]
pub type IMP_VARIANT_R = crate::FieldReader<u8, u8>;
#[doc = "Field `IMP_PRODUCT_ID` reader - IMPL_DEF value identifying the subsystem implementation."]
pub type IMP_PRODUCT_ID_R = crate::FieldReader<u16, u16>;
impl R {
    #[doc = "Bits 0:11 - Contains the JEP106 code of the company that implemented the subsystem implementation."]
    #[inline(always)]
    pub fn imp_implementator(&self) -> IMP_IMPLEMENTATOR_R {
        IMP_IMPLEMENTATOR_R::new((self.bits & 0x0fff) as u16)
    }
    #[doc = "Bits 12:15 - IMPL_DEF value used to distinguish minor revisions of the subsystem implementation."]
    #[inline(always)]
    pub fn imp_revision(&self) -> IMP_REVISION_R {
        IMP_REVISION_R::new(((self.bits >> 12) & 0x0f) as u8)
    }
    #[doc = "Bits 16:19 - IMPL_DEF value variant or major revision of the subsystem implementation."]
    #[inline(always)]
    pub fn imp_variant(&self) -> IMP_VARIANT_R {
        IMP_VARIANT_R::new(((self.bits >> 16) & 0x0f) as u8)
    }
    #[doc = "Bits 20:31 - IMPL_DEF value identifying the subsystem implementation."]
    #[inline(always)]
    pub fn imp_product_id(&self) -> IMP_PRODUCT_ID_R {
        IMP_PRODUCT_ID_R::new(((self.bits >> 20) & 0x0fff) as u16)
    }
}
#[doc = "Subsystem Implementation Identity Register.\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [iidr](index.html) module"]
pub struct IIDR_SPEC;
impl crate::RegisterSpec for IIDR_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [iidr::R](R) reader structure"]
impl crate::Readable for IIDR_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets IIDR to value 0"]
impl crate::Resettable for IIDR_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
