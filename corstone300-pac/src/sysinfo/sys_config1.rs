// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SYS_CONFIG1` reader"]
pub struct R(crate::R<SYS_CONFIG1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SYS_CONFIG1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SYS_CONFIG1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SYS_CONFIG1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `CPU2_TYPE` reader - CPU 2 Core Type."]
pub type CPU2_TYPE_R = crate::FieldReader<u8, CPU2_TYPE_A>;
#[doc = "CPU 2 Core Type.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum CPU2_TYPE_A {
    #[doc = "0: Does not exist."]
    NO = 0,
}
impl From<CPU2_TYPE_A> for u8 {
    #[inline(always)]
    fn from(variant: CPU2_TYPE_A) -> Self {
        variant as _
    }
}
impl CPU2_TYPE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<CPU2_TYPE_A> {
        match self.bits {
            0 => Some(CPU2_TYPE_A::NO),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == CPU2_TYPE_A::NO
    }
}
#[doc = "Field `CPU2_HAS_SYSTCM` reader - CPU 2 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level."]
pub type CPU2_HAS_SYSTCM_R = crate::BitReader<CPU2_HAS_SYSTCM_A>;
#[doc = "CPU 2 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU2_HAS_SYSTCM_A {
    #[doc = "0: Not included."]
    NO = 0,
    #[doc = "1: Included."]
    YES = 1,
}
impl From<CPU2_HAS_SYSTCM_A> for bool {
    #[inline(always)]
    fn from(variant: CPU2_HAS_SYSTCM_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU2_HAS_SYSTCM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU2_HAS_SYSTCM_A {
        match self.bits {
            false => CPU2_HAS_SYSTCM_A::NO,
            true => CPU2_HAS_SYSTCM_A::YES,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == CPU2_HAS_SYSTCM_A::NO
    }
    #[doc = "Checks if the value of the field is `YES`"]
    #[inline(always)]
    pub fn is_yes(&self) -> bool {
        *self == CPU2_HAS_SYSTCM_A::YES
    }
}
#[doc = "Field `CPU2_TCM_BANK_NUM` reader - The VM Bank that is the TCM memory for CPU 2."]
pub type CPU2_TCM_BANK_NUM_R = crate::FieldReader<u8, u8>;
#[doc = "Field `CPU3_TYPE` reader - CPU 3 Core Type."]
pub type CPU3_TYPE_R = crate::FieldReader<u8, CPU3_TYPE_A>;
#[doc = "CPU 3 Core Type.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum CPU3_TYPE_A {
    #[doc = "0: Does not exist."]
    NO = 0,
}
impl From<CPU3_TYPE_A> for u8 {
    #[inline(always)]
    fn from(variant: CPU3_TYPE_A) -> Self {
        variant as _
    }
}
impl CPU3_TYPE_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> Option<CPU3_TYPE_A> {
        match self.bits {
            0 => Some(CPU3_TYPE_A::NO),
            _ => None,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == CPU3_TYPE_A::NO
    }
}
#[doc = "Field `CPU3_HAS_SYSTCM` reader - CPU 3 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level."]
pub type CPU3_HAS_SYSTCM_R = crate::BitReader<CPU3_HAS_SYSTCM_A>;
#[doc = "CPU 3 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level.\n\nValue on reset: 0"]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CPU3_HAS_SYSTCM_A {
    #[doc = "0: Not included."]
    NO = 0,
    #[doc = "1: Included."]
    YES = 1,
}
impl From<CPU3_HAS_SYSTCM_A> for bool {
    #[inline(always)]
    fn from(variant: CPU3_HAS_SYSTCM_A) -> Self {
        variant as u8 != 0
    }
}
impl CPU3_HAS_SYSTCM_R {
    #[doc = "Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> CPU3_HAS_SYSTCM_A {
        match self.bits {
            false => CPU3_HAS_SYSTCM_A::NO,
            true => CPU3_HAS_SYSTCM_A::YES,
        }
    }
    #[doc = "Checks if the value of the field is `NO`"]
    #[inline(always)]
    pub fn is_no(&self) -> bool {
        *self == CPU3_HAS_SYSTCM_A::NO
    }
    #[doc = "Checks if the value of the field is `YES`"]
    #[inline(always)]
    pub fn is_yes(&self) -> bool {
        *self == CPU3_HAS_SYSTCM_A::YES
    }
}
#[doc = "Field `CPU3_TCM_BANK_NUM` reader - The VM Bank that is the TCM memory for CPU 3."]
pub type CPU3_TCM_BANK_NUM_R = crate::FieldReader<u8, u8>;
impl R {
    #[doc = "Bits 0:2 - CPU 2 Core Type."]
    #[inline(always)]
    pub fn cpu2_type(&self) -> CPU2_TYPE_R {
        CPU2_TYPE_R::new((self.bits & 7) as u8)
    }
    #[doc = "Bit 3 - CPU 2 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level."]
    #[inline(always)]
    pub fn cpu2_has_systcm(&self) -> CPU2_HAS_SYSTCM_R {
        CPU2_HAS_SYSTCM_R::new(((self.bits >> 3) & 1) != 0)
    }
    #[doc = "Bits 4:7 - The VM Bank that is the TCM memory for CPU 2."]
    #[inline(always)]
    pub fn cpu2_tcm_bank_num(&self) -> CPU2_TCM_BANK_NUM_R {
        CPU2_TCM_BANK_NUM_R::new(((self.bits >> 4) & 0x0f) as u8)
    }
    #[doc = "Bits 8:10 - CPU 3 Core Type."]
    #[inline(always)]
    pub fn cpu3_type(&self) -> CPU3_TYPE_R {
        CPU3_TYPE_R::new(((self.bits >> 8) & 7) as u8)
    }
    #[doc = "Bit 11 - CPU 3 has System TCM. Note that this is not the CPU's local ITCM or DTCM, but instead are TCMs implemented at system level."]
    #[inline(always)]
    pub fn cpu3_has_systcm(&self) -> CPU3_HAS_SYSTCM_R {
        CPU3_HAS_SYSTCM_R::new(((self.bits >> 11) & 1) != 0)
    }
    #[doc = "Bits 12:15 - The VM Bank that is the TCM memory for CPU 3."]
    #[inline(always)]
    pub fn cpu3_tcm_bank_num(&self) -> CPU3_TCM_BANK_NUM_R {
        CPU3_TCM_BANK_NUM_R::new(((self.bits >> 12) & 0x0f) as u8)
    }
}
#[doc = "System Hardware Configuration 0 register\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [sys_config1](index.html) module"]
pub struct SYS_CONFIG1_SPEC;
impl crate::RegisterSpec for SYS_CONFIG1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [sys_config1::R](R) reader structure"]
impl crate::Readable for SYS_CONFIG1_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets SYS_CONFIG1 to value 0"]
impl crate::Resettable for SYS_CONFIG1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
