// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `SOC_IDENTITY` reader"]
pub struct R(crate::R<SOC_IDENTITY_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<SOC_IDENTITY_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<SOC_IDENTITY_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<SOC_IDENTITY_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Field `SOC_IMPLEMENTATOR` reader - JEP106 code of the company that implemented the SoC"]
pub type SOC_IMPLEMENTATOR_R = crate::FieldReader<u16, u16>;
#[doc = "Field `SOC_REVISION` reader - IMPL_DEF value used to distinguish minor revisions of the SoC."]
pub type SOC_REVISION_R = crate::FieldReader<u8, u8>;
#[doc = "Field `SOC_VARIANT` reader - IMPL_DEF value variant or major revision of the SoC."]
pub type SOC_VARIANT_R = crate::FieldReader<u8, u8>;
#[doc = "Field `SOC_PRODUCT_ID` reader - IMPL_DEF value identifying the SoC."]
pub type SOC_PRODUCT_ID_R = crate::FieldReader<u16, u16>;
impl R {
    #[doc = "Bits 0:11 - JEP106 code of the company that implemented the SoC"]
    #[inline(always)]
    pub fn soc_implementator(&self) -> SOC_IMPLEMENTATOR_R {
        SOC_IMPLEMENTATOR_R::new((self.bits & 0x0fff) as u16)
    }
    #[doc = "Bits 12:15 - IMPL_DEF value used to distinguish minor revisions of the SoC."]
    #[inline(always)]
    pub fn soc_revision(&self) -> SOC_REVISION_R {
        SOC_REVISION_R::new(((self.bits >> 12) & 0x0f) as u8)
    }
    #[doc = "Bits 16:19 - IMPL_DEF value variant or major revision of the SoC."]
    #[inline(always)]
    pub fn soc_variant(&self) -> SOC_VARIANT_R {
        SOC_VARIANT_R::new(((self.bits >> 16) & 0x0f) as u8)
    }
    #[doc = "Bits 20:31 - IMPL_DEF value identifying the SoC."]
    #[inline(always)]
    pub fn soc_product_id(&self) -> SOC_PRODUCT_ID_R {
        SOC_PRODUCT_ID_R::new(((self.bits >> 20) & 0x0fff) as u16)
    }
}
#[doc = "System Identity Register\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [soc_identity](index.html) module"]
pub struct SOC_IDENTITY_SPEC;
impl crate::RegisterSpec for SOC_IDENTITY_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [soc_identity::R](R) reader structure"]
impl crate::Readable for SOC_IDENTITY_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets SOC_IDENTITY to value 0"]
impl crate::Resettable for SOC_IDENTITY_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
