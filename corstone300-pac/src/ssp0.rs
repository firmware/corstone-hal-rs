// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Control register 0"]
    pub cr0: CR0,
    #[doc = "0x04 - Control register 1"]
    pub cr1: CR1,
    #[doc = "0x08 - Data register"]
    pub dr: DR,
    #[doc = "0x0c - Status register"]
    pub sr: SR,
    #[doc = "0x10 - Clock prescale register"]
    pub cpsr: CPSR,
    #[doc = "0x14 - Interrupt mask set or clear register"]
    pub imsc: IMSC,
    #[doc = "0x18 - Raw interrupt status register"]
    pub ris: RIS,
    #[doc = "0x1c - Masked interrupt status register"]
    pub mis: MIS,
    #[doc = "0x20 - Interrupt clear register"]
    pub icr: ICR,
    #[doc = "0x24 - DMA control register"]
    pub dmacr: DMACR,
    _reserved10: [u8; 0x58],
    #[doc = "0x80 - Test control register"]
    pub tcr: TCR,
    #[doc = "0x84 - Integration test input register"]
    pub itip: ITIP,
    #[doc = "0x88 - Integration test output register"]
    pub itop: ITOP,
    #[doc = "0x8c - Test data register"]
    pub tdr: TDR,
}
#[doc = "CR0 (rw) register accessor: an alias for `Reg<CR0_SPEC>`"]
pub type CR0 = crate::Reg<cr0::CR0_SPEC>;
#[doc = "Control register 0"]
pub mod cr0;
#[doc = "CR1 (rw) register accessor: an alias for `Reg<CR1_SPEC>`"]
pub type CR1 = crate::Reg<cr1::CR1_SPEC>;
#[doc = "Control register 1"]
pub mod cr1;
#[doc = "DR (rw) register accessor: an alias for `Reg<DR_SPEC>`"]
pub type DR = crate::Reg<dr::DR_SPEC>;
#[doc = "Data register"]
pub mod dr;
#[doc = "SR (rw) register accessor: an alias for `Reg<SR_SPEC>`"]
pub type SR = crate::Reg<sr::SR_SPEC>;
#[doc = "Status register"]
pub mod sr;
#[doc = "CPSR (rw) register accessor: an alias for `Reg<CPSR_SPEC>`"]
pub type CPSR = crate::Reg<cpsr::CPSR_SPEC>;
#[doc = "Clock prescale register"]
pub mod cpsr;
#[doc = "IMSC (rw) register accessor: an alias for `Reg<IMSC_SPEC>`"]
pub type IMSC = crate::Reg<imsc::IMSC_SPEC>;
#[doc = "Interrupt mask set or clear register"]
pub mod imsc;
#[doc = "RIS (rw) register accessor: an alias for `Reg<RIS_SPEC>`"]
pub type RIS = crate::Reg<ris::RIS_SPEC>;
#[doc = "Raw interrupt status register"]
pub mod ris;
#[doc = "MIS (rw) register accessor: an alias for `Reg<MIS_SPEC>`"]
pub type MIS = crate::Reg<mis::MIS_SPEC>;
#[doc = "Masked interrupt status register"]
pub mod mis;
#[doc = "ICR (rw) register accessor: an alias for `Reg<ICR_SPEC>`"]
pub type ICR = crate::Reg<icr::ICR_SPEC>;
#[doc = "Interrupt clear register"]
pub mod icr;
#[doc = "DMACR (rw) register accessor: an alias for `Reg<DMACR_SPEC>`"]
pub type DMACR = crate::Reg<dmacr::DMACR_SPEC>;
#[doc = "DMA control register"]
pub mod dmacr;
#[doc = "TCR (rw) register accessor: an alias for `Reg<TCR_SPEC>`"]
pub type TCR = crate::Reg<tcr::TCR_SPEC>;
#[doc = "Test control register"]
pub mod tcr;
#[doc = "ITIP (rw) register accessor: an alias for `Reg<ITIP_SPEC>`"]
pub type ITIP = crate::Reg<itip::ITIP_SPEC>;
#[doc = "Integration test input register"]
pub mod itip;
#[doc = "ITOP (rw) register accessor: an alias for `Reg<ITOP_SPEC>`"]
pub type ITOP = crate::Reg<itop::ITOP_SPEC>;
#[doc = "Integration test output register"]
pub mod itop;
#[doc = "TDR (rw) register accessor: an alias for `Reg<TDR_SPEC>`"]
pub type TDR = crate::Reg<tdr::TDR_SPEC>;
#[doc = "Test data register"]
pub mod tdr;
