// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Watchdog control and Status."]
    pub wcs: WCS,
    _reserved1: [u8; 0x04],
    #[doc = "0x08 - Watchdog Offset Register."]
    pub wor: WOR,
    _reserved2: [u8; 0x04],
    #[doc = "0x10 - Watchdog Compare Value Lower Word."]
    pub wcv_low: WCV_LOW,
    #[doc = "0x14 - Watchdog Compare Value Higher Word."]
    pub wcv_high: WCV_HIGH,
    _reserved4: [u8; 0x0fb4],
    #[doc = "0xfcc - Control Frame Watchdog Interface Identification Register."]
    pub cnt_w_iidr: CNT_W_IIDR,
    _reserved5: [u8; 0x30],
    #[doc = "0x1000 - Watchdog Refresh Register."]
    pub wrr: WRR,
    _reserved6: [u8; 0x0fc8],
    #[doc = "0x1fcc - Refresh Frame Watchdog Interface Identification Register."]
    pub ref_w_iidr: REF_W_IIDR,
}
#[doc = "WCS (rw) register accessor: an alias for `Reg<WCS_SPEC>`"]
pub type WCS = crate::Reg<wcs::WCS_SPEC>;
#[doc = "Watchdog control and Status."]
pub mod wcs;
#[doc = "WOR (rw) register accessor: an alias for `Reg<WOR_SPEC>`"]
pub type WOR = crate::Reg<wor::WOR_SPEC>;
#[doc = "Watchdog Offset Register."]
pub mod wor;
#[doc = "WCV_LOW (rw) register accessor: an alias for `Reg<WCV_LOW_SPEC>`"]
pub type WCV_LOW = crate::Reg<wcv_low::WCV_LOW_SPEC>;
#[doc = "Watchdog Compare Value Lower Word."]
pub mod wcv_low;
#[doc = "WCV_HIGH (rw) register accessor: an alias for `Reg<WCV_HIGH_SPEC>`"]
pub type WCV_HIGH = crate::Reg<wcv_high::WCV_HIGH_SPEC>;
#[doc = "Watchdog Compare Value Higher Word."]
pub mod wcv_high;
#[doc = "CNT_W_IIDR (r) register accessor: an alias for `Reg<CNT_W_IIDR_SPEC>`"]
pub type CNT_W_IIDR = crate::Reg<cnt_w_iidr::CNT_W_IIDR_SPEC>;
#[doc = "Control Frame Watchdog Interface Identification Register."]
pub mod cnt_w_iidr;
#[doc = "WRR (w) register accessor: an alias for `Reg<WRR_SPEC>`"]
pub type WRR = crate::Reg<wrr::WRR_SPEC>;
#[doc = "Watchdog Refresh Register."]
pub mod wrr;
#[doc = "REF_W_IIDR (r) register accessor: an alias for `Reg<REF_W_IIDR_SPEC>`"]
pub type REF_W_IIDR = crate::Reg<ref_w_iidr::REF_W_IIDR_SPEC>;
#[doc = "Refresh Frame Watchdog Interface Identification Register."]
pub mod ref_w_iidr;
