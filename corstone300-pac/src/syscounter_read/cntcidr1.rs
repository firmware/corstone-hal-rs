// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTCIDR1` reader"]
pub struct R(crate::R<CNTCIDR1_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTCIDR1_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTCIDR1_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTCIDR1_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Component Identification Register 1\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntcidr1](index.html) module"]
pub struct CNTCIDR1_SPEC;
impl crate::RegisterSpec for CNTCIDR1_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntcidr1::R](R) reader structure"]
impl crate::Readable for CNTCIDR1_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTCIDR1 to value 0xf0"]
impl crate::Resettable for CNTCIDR1_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0xf0
    }
}
