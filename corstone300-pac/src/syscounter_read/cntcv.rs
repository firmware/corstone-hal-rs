// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTCV` reader"]
pub struct R(crate::R<CNTCV_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTCV_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTCV_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTCV_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Register `CNTCV` writer"]
pub struct W(crate::W<CNTCV_SPEC>);
impl core::ops::Deref for W {
    type Target = crate::W<CNTCV_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl core::ops::DerefMut for W {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl From<crate::W<CNTCV_SPEC>> for W {
    #[inline(always)]
    fn from(writer: crate::W<CNTCV_SPEC>) -> Self {
        W(writer)
    }
}
#[doc = "Field `Countvalue` reader - Indicates the countvalue"]
pub type COUNTVALUE_R = crate::FieldReader<u64, u64>;
#[doc = "Field `Countvalue` writer - Indicates the countvalue"]
pub type COUNTVALUE_W<'a, const O: u8> = crate::FieldWriter<'a, u64, CNTCV_SPEC, u64, u64, 64, O>;
impl R {
    #[doc = "Bits 0:63 - Indicates the countvalue"]
    #[inline(always)]
    pub fn countvalue(&self) -> COUNTVALUE_R {
        COUNTVALUE_R::new(self.bits)
    }
}
impl W {
    #[doc = "Bits 0:63 - Indicates the countvalue"]
    #[inline(always)]
    pub fn countvalue(&mut self) -> COUNTVALUE_W<0> {
        COUNTVALUE_W::new(self)
    }
    #[doc = "Writes raw bits to the register."]
    #[inline(always)]
    pub unsafe fn bits(&mut self, bits: u64) -> &mut Self {
        self.0.bits(bits);
        self
    }
}
#[doc = "Current count value\n\nThis register you can [`read`](crate::generic::Reg::read), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntcv](index.html) module"]
pub struct CNTCV_SPEC;
impl crate::RegisterSpec for CNTCV_SPEC {
    type Ux = u64;
}
#[doc = "`read()` method returns [cntcv::R](R) reader structure"]
impl crate::Readable for CNTCV_SPEC {
    type Reader = R;
}
#[doc = "`write(|w| ..)` method takes [cntcv::W](W) writer structure"]
impl crate::Writable for CNTCV_SPEC {
    type Writer = W;
}
#[doc = "`reset()` method sets CNTCV to value 0"]
impl crate::Resettable for CNTCV_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0
    }
}
