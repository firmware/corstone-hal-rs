// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTPIDR2` reader"]
pub struct R(crate::R<CNTPIDR2_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTPIDR2_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTPIDR2_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTPIDR2_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Peripheral Identification Register 2\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntpidr2](index.html) module"]
pub struct CNTPIDR2_SPEC;
impl crate::RegisterSpec for CNTPIDR2_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntpidr2::R](R) reader structure"]
impl crate::Readable for CNTPIDR2_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTPIDR2 to value 0x0b"]
impl crate::Resettable for CNTPIDR2_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x0b
    }
}
