// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#[doc = "Register `CNTCIDR0` reader"]
pub struct R(crate::R<CNTCIDR0_SPEC>);
impl core::ops::Deref for R {
    type Target = crate::R<CNTCIDR0_SPEC>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<crate::R<CNTCIDR0_SPEC>> for R {
    #[inline(always)]
    fn from(reader: crate::R<CNTCIDR0_SPEC>) -> Self {
        R(reader)
    }
}
#[doc = "Component Identification Register 0.\n\nThis register you can [`read`](crate::generic::Reg::read). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about available fields see [cntcidr0](index.html) module"]
pub struct CNTCIDR0_SPEC;
impl crate::RegisterSpec for CNTCIDR0_SPEC {
    type Ux = u32;
}
#[doc = "`read()` method returns [cntcidr0::R](R) reader structure"]
impl crate::Readable for CNTCIDR0_SPEC {
    type Reader = R;
}
#[doc = "`reset()` method sets CNTCIDR0 to value 0x0d"]
impl crate::Resettable for CNTCIDR0_SPEC {
    #[inline(always)]
    fn reset_value() -> Self::Ux {
        0x0d
    }
}
