/*
 * Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: MIT
 */

PROVIDE(NONSEC_WATCHDOG_RST_REQ = DefaultHandler);
PROVIDE(NONSEC_WATCHDOG_IRQ = DefaultHandler);
PROVIDE(S32K_TIMER = DefaultHandler);
PROVIDE(TIMER0 = DefaultHandler);
PROVIDE(TIMER1 = DefaultHandler);
PROVIDE(DUALTIMER = DefaultHandler);
PROVIDE(MPC = DefaultHandler);
PROVIDE(PPC = DefaultHandler);
PROVIDE(MSC = DefaultHandler);
PROVIDE(BRG = DefaultHandler);
PROVIDE(MGMT_PPU = DefaultHandler);
PROVIDE(SYS_PPU = DefaultHandler);
PROVIDE(CPU0_PPU = DefaultHandler);
PROVIDE(PPU_DEBUG = DefaultHandler);
PROVIDE(TIMER3 = DefaultHandler);
PROVIDE(SYSTEM_TIMESTAMP_COUNTER = DefaultHandler);
PROVIDE(UART0_RX = DefaultHandler);
PROVIDE(UART0_TX = DefaultHandler);
PROVIDE(UART1_RX = DefaultHandler);
PROVIDE(UART1_TX = DefaultHandler);
PROVIDE(UART2_RX = DefaultHandler);
PROVIDE(UART2_TX = DefaultHandler);
PROVIDE(UART3_RX = DefaultHandler);
PROVIDE(UART3_TX = DefaultHandler);
PROVIDE(UART4_RX = DefaultHandler);
PROVIDE(UART4_TX = DefaultHandler);
PROVIDE(UART0_COMBINED = DefaultHandler);
PROVIDE(UART1_COMBINED = DefaultHandler);
PROVIDE(UART2_COMBINED = DefaultHandler);
PROVIDE(UART3_COMBINED = DefaultHandler);
PROVIDE(UART4_COMBINED = DefaultHandler);
PROVIDE(UART_OVERFLOW = DefaultHandler);
PROVIDE(ETHERNET = DefaultHandler);
PROVIDE(AUDIO_I2S = DefaultHandler);
PROVIDE(TOUCH_SCREEN = DefaultHandler);
PROVIDE(USB = DefaultHandler);
PROVIDE(SPI2 = DefaultHandler);
PROVIDE(SPI0 = DefaultHandler);
PROVIDE(SPI1 = DefaultHandler);
PROVIDE(GPIO0_COMBINED = DefaultHandler);
PROVIDE(GPIO1_COMBINED = DefaultHandler);
PROVIDE(GPIO2_COMBINED = DefaultHandler);
PROVIDE(GPIO3_COMBINED = DefaultHandler);
PROVIDE(UART5_RX = DefaultHandler);
PROVIDE(UART5_TX = DefaultHandler);
PROVIDE(UART5_COMBINED = DefaultHandler);

