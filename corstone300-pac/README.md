<!--
Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>

SPDX-License-Identifier: MIT
-->
# Peripheral Access Crate for the Arm(R) Corstone(TM)-300 Reference System

To regenerate the PAC, use the contained `generate.sh`:

```bash
./generate.sh
```
