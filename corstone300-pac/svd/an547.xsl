<?xml version="1.0" encoding="utf-8"?>
<!--
Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>

SPDX-License-Identifier: MIT
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/device/peripherals/peripheral[name='DMA0']/registers/register['IntStatus']/size">
      8
  </xsl:template>
  <xsl:template match="/device/peripherals/peripheral[name='DMA0']/registers/register['IntTCStatus']/size">
      16
  </xsl:template>
  <xsl:template match="/device/peripherals/peripheral[name='DMA0']/registers/register['IntTCClear']/size">
      16
  </xsl:template>

  <xsl:template match="/device/peripherals/peripheral[name='Ethernet']">
    <xsl:copy>
      <xsl:apply-templates/>
            <registers>
                <register>
                    <name>RxDataFifo</name>
                    <description>RX Data FIFO Port</description>
                    <addressOffset>0x00</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                </register>
                <register>
                    <name>TxDataFifo</name>
                    <description>TX Data FIFO Port</description>
                    <addressOffset>0x20</addressOffset>
                    <size>32</size>
                    <access>write-only</access>
                </register>
                <register>
                    <name>RxStatusFifo</name>
                    <description>RX Status FIFO Port</description>
                    <addressOffset>0x40</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                </register>
                <register>
                    <name>RxStatusFifoPeek</name>
                    <description>RX Status FIFO PEEK Port</description>
                    <addressOffset>0x44</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                </register>
                <register>
                    <name>TxStatusFifo</name>
                    <description>TX Status FIFO Port</description>
                    <addressOffset>0x48</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                </register>
                <register>
                    <name>TxStatusFifoPeek</name>
                    <description>TX Status FIFO PEEK Port</description>
                    <addressOffset>0x4C</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                </register>
                <register>
                    <name>ID_REV</name>
                    <description>ID and Revision</description>
                    <addressOffset>0x50</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                    <resetValue>0x92200000</resetValue>
                    <fields>
                        <field>
                            <name>REV</name>
                            <bitRange>[15:0]</bitRange>
                        </field>
                        <field>
                            <name>ID</name>
                            <bitRange>[31:16]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>IRQ_CFG</name>
                    <description>Main Interrupt Configuration</description>
                    <addressOffset>0x54</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>IRQ_TYPE</name>
                            <description>IRQ Buffer Type</description>
                            <bitRange>[0:0]</bitRange>
                        </field>
                        <field>
                            <name>IRQ_POL</name>
                            <description>IRQ Polarity</description>
                            <bitRange>[4:4]</bitRange>
                        </field>
                        <field>
                            <name>IRQ_EN</name>
                            <description>IRQ Enable</description>
                            <bitRange>[8:8]</bitRange>
                        </field>
                        <field>
                            <name>IRQ_INT</name>
                            <description>Master Interrupt</description>
                            <bitRange>[12:12]</bitRange>
                            <access>read-only</access>
                        </field>
                        <field>
                            <name>INT_DEAS_STS</name>
                            <description>Interrupt Deassertion Status</description>
                            <bitRange>[13:13]</bitRange>
                            <access>read-only</access>
                        </field>
                        <field>
                            <name>INT_DEAS_CLR</name>
                            <description>Interrupt Deassertion Interval Clear</description>
                            <bitRange>[14:14]</bitRange>
                            <access>read-only</access>
                        </field>
                        <field>
                            <name>INT_DEAS</name>
                            <description>Interrupt Deassertion Interval</description>
                            <bitRange>[31:24]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>INT_STS</name>
                    <description>Interrupt Status Register</description>
                    <addressOffset>0x58</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>SW_INT</name>
                            <description>Software Interrupt</description>
                            <bitRange>[31:31]</bitRange>
                        </field>
                        <field>
                            <name>TXSTOP_INT</name>
                            <description>TX Stopped</description>
                            <bitRange>[25:25]</bitRange>
                        </field>
                        <field>
                            <name>RXSTOP_INT</name>
                            <description>RX Stopped</description>
                            <bitRange>[24:24]</bitRange>
                        </field>
                        <field>
                            <name>RXDFH_INT</name>
                            <description>RX Dropped Frame Counter Halfway</description>
                            <bitRange>[23:23]</bitRange>
                        </field>
                        <field>
                            <name>TX_IOC</name>
                            <description>TX IOC Interrupt</description>
                            <bitRange>[21:21]</bitRange>
                        </field>
                        <field>
                            <name>RXD_INT</name>
                            <description>RX DMA Interrupt</description>
                            <bitRange>[20:20]</bitRange>
                        </field>
                        <field>
                            <name>GPT_INT</name>
                            <description>GP Timer</description>
                            <bitRange>[19:19]</bitRange>
                        </field>
                        <field>
                            <name>PHY_INT</name>
                            <description>PHY</description>
                            <bitRange>[18:18]</bitRange>
                        </field>
                        <field>
                            <name>PME_INT</name>
                            <description>Power Management Event Interrupt</description>
                            <bitRange>[17:17]</bitRange>
                        </field>
                        <field>
                            <name>TXSO</name>
                            <description>TX Status Fifo Overflow</description>
                            <bitRange>[16:16]</bitRange>
                        </field>
                        <field>
                            <name>RWT</name>
                            <description>Receive Watchdog Time-out</description>
                            <bitRange>[15:15]</bitRange>
                        </field>
                        <field>
                            <name>RXE</name>
                            <description>Receiver Error</description>
                            <bitRange>[14:14]</bitRange>
                        </field>
                        <field>
                            <name>TXE</name>
                            <description>Transmitter Error</description>
                            <bitRange>[13:13]</bitRange>
                        </field>
                        <field>
                            <name>TDFO</name>
                            <description>TX Data FIFO Overrun Interrupt</description>
                            <bitRange>[10:10]</bitRange>
                        </field>
                        <field>
                            <name>TDFA</name>
                            <description>TX Data Available Interrupt</description>
                            <bitRange>[9:9]</bitRange>
                        </field>
                        <field>
                            <name>TSFF</name>
                            <description>TX Status FIFO Full Interrupt</description>
                            <bitRange>[8:8]</bitRange>
                        </field>
                        <field>
                            <name>TSFL</name>
                            <description>TX Status FIFO Level Interrupt</description>
                            <bitRange>[7:7]</bitRange>
                        </field>
                        <field>
                            <name>RXDF_INT</name>
                            <description>RX Dropped Frame Interrupt</description>
                            <bitRange>[6:6]</bitRange>
                        </field>
                        <field>
                            <name>RSFF</name>
                            <description>RX Status FIFO Full Interrupt</description>
                            <bitRange>[4:4]</bitRange>
                        </field>
                        <field>
                            <name>RSFL</name>
                            <description>RX Status FIFO Level Interrupt</description>
                            <bitRange>[3:3]</bitRange>
                        </field>
                        <field>
                            <name>GPIO2_INT</name>
                            <bitRange>[2:2]</bitRange>
                        </field>
                        <field>
                            <name>GPIO1_INT</name>
                            <bitRange>[1:1]</bitRange>
                        </field>
                        <field>
                            <name>GPIO0_INT</name>
                            <bitRange>[0:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>INT_EN</name>
                    <description>Interrupt Enable Register</description>
                    <addressOffset>0x5C</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>SW_INT_EN</name>
                            <description>Software Interrupt</description>
                            <bitRange>[31:31]</bitRange>
                        </field>
                        <field>
                            <name>TXSTOP_INT_EN</name>
                            <description>TX Stopped Interrupt Enable</description>
                            <bitRange>[25:25]</bitRange>
                        </field>
                        <field>
                            <name>RXSTOP_INT_EN</name>
                            <description>RX Stopped Interrupt Enable</description>
                            <bitRange>[24:24]</bitRange>
                        </field>
                        <field>
                            <name>RXDFH_INT_EN</name>
                            <description>RX Dropped Frame Counter Halfway Interrupt Enable</description>
                            <bitRange>[23:23]</bitRange>
                        </field>
                        <field>
                            <name>TIOC_INT_EN</name>
                            <description>TX IOC Interrupt Enable</description>
                            <bitRange>[21:21]</bitRange>
                        </field>
                        <field>
                            <name>RXD_INT</name>
                            <description>RX DMA Interrupt</description>
                            <bitRange>[20:20]</bitRange>
                        </field>
                        <field>
                            <name>GPT_INT_EN</name>
                            <description>GP Timer</description>
                            <bitRange>[19:19]</bitRange>
                        </field>
                        <field>
                            <name>PHY_INT_EN</name>
                            <description>PHY</description>
                            <bitRange>[18:18]</bitRange>
                        </field>
                        <field>
                            <name>PME_INT_EN</name>
                            <description>Power Management Event Interrupt Enable</description>
                            <bitRange>[17:17]</bitRange>
                        </field>
                        <field>
                            <name>TXSO_EN</name>
                            <description>TX Status FIFO Overflow</description>
                            <bitRange>[16:16]</bitRange>
                        </field>
                        <field>
                            <name>RWT_INT_EN</name>
                            <description>Receive Watchdog Time-out Interrupt</description>
                            <bitRange>[15:15]</bitRange>
                        </field>
                        <field>
                            <name>RXE_INT_EN</name>
                            <description>Receiver Error Interrupt</description>
                            <bitRange>[14:14]</bitRange>
                        </field>
                        <field>
                            <name>TXE_INT_EN</name>
                            <description>Transmitter Error Interrupt</description>
                            <bitRange>[13:13]</bitRange>
                        </field>
                        <field>
                            <name>TDFO_INT_EN</name>
                            <description>TX Data FIFO Overrun Interrupt</description>
                            <bitRange>[10:10]</bitRange>
                        </field>
                        <field>
                            <name>TDFA_INT_EN</name>
                            <description>TX Data FIFO Available Interrupt</description>
                            <bitRange>[9:9]</bitRange>
                        </field>
                        <field>
                            <name>TSFF_INT_EN</name>
                            <description>TX Status FIFO Full Interrupt</description>
                            <bitRange>[8:8]</bitRange>
                        </field>
                        <field>
                            <name>TSFL_INT_EN</name>
                            <description>TX Status FIFO Level Interrupt</description>
                            <bitRange>[7:7]</bitRange>
                        </field>
                        <field>
                            <name>RXDF_INT_EN</name>
                            <description>RX Dropped Frame Interrupt Enable</description>
                            <bitRange>[6:6]</bitRange>
                        </field>
                        <field>
                            <name>RSFF_INT_EN</name>
                            <description>RX Status FIFO Full Interrupt</description>
                            <bitRange>[4:4]</bitRange>
                        </field>
                        <field>
                            <name>RSFL_INT_EN</name>
                            <description>RX Status FIFO Level Interrupt</description>
                            <bitRange>[3:3]</bitRange>
                        </field>
                        <field>
                            <name>GPIO2_INT_EN</name>
                            <description>GPIO 2 Interrupt</description>
                            <bitRange>[2:2]</bitRange>
                        </field>
                        <field>
                            <name>GPIO1_INT_EN</name>
                            <description>GPIO 1 Interrupt</description>
                            <bitRange>[1:1]</bitRange>
                        </field>
                        <field>
                            <name>GPIO0_INT_EN</name>
                            <description>GPIO 0 Interrupt</description>
                            <bitRange>[0:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>TEST_BYTE</name>
                    <description>Byte Order Test Register</description>
                    <addressOffset>0x64</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                    <resetValue>0x87654321</resetValue>
                </register>
                <register>
                    <name>FIFO_INT</name>
                    <description>FIFO Level Interrupt</description>
                    <addressOffset>0x68</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x48000000</resetValue>
                    <fields>
                        <field>
                            <name>TDFL</name>
                            <description>TX Data FIFO Level</description>
                            <bitRange>[31:24]</bitRange>
                        </field>
                        <field>
                            <name>TSFL</name>
                            <description>TX Status FIFO Level</description>
                            <bitRange>[23:16]</bitRange>
                        </field>
                        <field>
                            <name>RSFL</name>
                            <description>RX Status FIFO Level</description>
                            <bitRange>[7:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>RX_CFG</name>
                    <description>Receive Configuration</description>
                    <addressOffset>0x6C</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>RX_END_ALIGN</name>
                            <description>RX End Alignment</description>
                            <bitRange>[31:30]</bitRange>
                        </field>
                        <field>
                            <name>RX_DMA_CNT</name>
                            <description>RX DMA Count</description>
                            <bitRange>[27:16]</bitRange>
                        </field>
                        <field>
                            <name>RX_DUMP</name>
                            <description>Force RX Discard</description>
                            <bitRange>[15:15]</bitRange>
                        </field>
                        <field>
                            <name>RXDOFF</name>
                            <description>RX Data Offset</description>
                            <bitRange>[12:8]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>TX_CFG</name>
                    <description>Transmit Configuration</description>
                    <addressOffset>0x70</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>TXS_DUMP</name>
                            <description>Force TX Status Discard</description>
                            <bitRange>[15:15]</bitRange>
                        </field>
                        <field>
                            <name>TXD_DUMP</name>
                            <description>Force TX Data Discard</description>
                            <bitRange>[14:14]</bitRange>
                        </field>
                        <field>
                            <name>TXSAO</name>
                            <description>TX Status Allow Overun</description>
                            <bitRange>[2:2]</bitRange>
                        </field>
                        <field>
                            <name>TX_ON</name>
                            <description>Transmitter Enable</description>
                            <bitRange>[1:1]</bitRange>
                        </field>
                        <field>
                            <name>STOP_TX</name>
                            <description>Stop Transmitter</description>
                            <bitRange>[0:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>HW_CFG</name>
                    <description>Hardware Configuration</description>
                    <addressOffset>0x74</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00050000</resetValue>
                    <fields>
                        <field>
                            <name>FPORTEND</name>
                            <description>FIFO Port Endian Ordering</description>
                            <bitRange>[29:29]</bitRange>
                            <enumeratedValues>
                                <enumeratedValue>
                                    <name>LE</name>
                                    <description>Little Endian</description>
                                    <value>0</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>BE</name>
                                    <description>Big Endian</description>
                                    <value>1</value>
                                </enumeratedValue>
                            </enumeratedValues>
                        </field>
                        <field>
                            <name>FSELEND</name>
                            <description>Direct FIFO Access Endian Ordering</description>
                            <bitRange>[28:28]</bitRange>
                            <enumeratedValues>
                                <enumeratedValue>
                                    <name>LE</name>
                                    <description>Little Endian</description>
                                    <value>0</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>BE</name>
                                    <description>Big Endian</description>
                                    <value>1</value>
                                </enumeratedValue>
                            </enumeratedValues>
                        </field>
                        <field>
                            <name>AMDIX_EN</name>
                            <description>AMDIX_EN Strap state</description>
                            <bitRange>[24:24]</bitRange>
                        </field>
                        <field>
                            <name>MBO</name>
                            <description>Must Be One</description>
                            <bitRange>[20:20]</bitRange>
                            <enumeratedValues>
                                <enumeratedValue>
                                    <name>One</name>
                                    <value>1</value>
                                </enumeratedValue>
                            </enumeratedValues>
                        </field>
                        <field>
                            <name>TX_FIF_SZ</name>
                            <description>TX FIFO Size</description>
                            <bitRange>[19:16]</bitRange>
                        </field>
                        <field>
                            <name>SRST_TO</name>
                            <description>Soft Reset Timeout</description>
                            <bitRange>[1:1]</bitRange>
                            <access>read-only</access>
                        </field>
                        <field>
                            <name>SRST</name>
                            <description>Soft Reset</description>
                            <bitRange>[0:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>RX_DP_CTL</name>
                    <description>RX Datapath Control</description>
                    <addressOffset>0x78</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>RX_FFWD</name>
                            <description>RX Data FIFO Fast Forward</description>
                            <bitRange>[31:31]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>RX_FIFO_INF</name>
                    <description>Receive FIFO Configuration</description>
                    <addressOffset>0x7C</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>RXSUSED</name>
                            <description>RX Status FIFO Used Space</description>
                            <bitRange>[23:16]</bitRange>
                        </field>
                        <field>
                            <name>RDFREE</name>
                            <description>RX Data FIFO Free Space</description>
                            <bitRange>[15:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>TX_FIFO_INF</name>
                    <description>Transmit FIFO Configuration</description>
                    <addressOffset>0x80</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00001200</resetValue>
                    <fields>
                        <field>
                            <name>TXSUSED</name>
                            <description>TX Status FIFO Used Space</description>
                            <bitRange>[23:16]</bitRange>
                        </field>
                        <field>
                            <name>TDFREE</name>
                            <description>TX Data FIFO Free Space</description>
                            <bitRange>[15:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>PMT_CTRL</name>
                    <description>Power Management Control</description>
                    <addressOffset>0x84</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>PM_MODE</name>
                            <description>Power Management Mode</description>
                            <bitRange>[13:12]</bitRange>
                            <enumeratedValues>
                                <enumeratedValue>
                                    <name>Normal</name>
                                    <value>0</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>WakeUpOrMagicPacket</name>
                                    <value>1</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>CanEnergyDetect</name>
                                    <value>2</value>
                                </enumeratedValue>
                            </enumeratedValues>
                        </field>
                        <field>
                            <name>PHY_RST</name>
                            <description>PHY Reset</description>
                            <bitRange>[10:10]</bitRange>
                        </field>
                        <field>
                            <name>WOL_EN</name>
                            <description>Wake-On-Lan Enable</description>
                            <bitRange>[9:9]</bitRange>
                        </field>
                        <field>
                            <name>ED_EN</name>
                            <description>Energy Detect Enable</description>
                            <bitRange>[8:8]</bitRange>
                        </field>
                        <field>
                            <name>PME_TYPE</name>
                            <description>PME Buffer Type</description>
                            <bitRange>[6:6]</bitRange>
                        </field>
                        <field>
                            <name>WUPS</name>
                            <description>WAKE-UP Status</description>
                            <bitRange>[5:4]</bitRange>
                            <enumeratedValues>
                                <enumeratedValue>
                                    <name>NoWakeUp</name>
                                    <value>0</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>Energy</name>
                                    <value>1</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>WakeUpOrMagicPacket</name>
                                    <value>2</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>Multiple</name>
                                    <value>3</value>
                                </enumeratedValue>
                            </enumeratedValues>
                        </field>
                        <field>
                            <name>PME_IND</name>
                            <description>PME Indication</description>
                            <bitRange>[3:3]</bitRange>
                        </field>
                        <field>
                            <name>PME_POL</name>
                            <description>PME Polarity</description>
                            <bitRange>[2:2]</bitRange>
                        </field>
                        <field>
                            <name>PME_EN</name>
                            <description>PME Enable</description>
                            <bitRange>[1:1]</bitRange>
                        </field>
                        <field>
                            <name>READY</name>
                            <description>Device Ready</description>
                            <bitRange>[0:0]</bitRange>
                            <access>read-only</access>
                        </field>
                        <!-- TODO: add fields -->
                    </fields>
                </register>
                <register>
                    <name>GPIO_CFG</name>
                    <description>General Purpose IO Configuration</description>
                    <addressOffset>0x88</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>LED2_EN</name>
                            <description>Led Enable</description>
                            <bitRange>[30:30]</bitRange>
                        </field>
                        <field>
                            <name>LED1_EN</name>
                            <description>Led Enable</description>
                            <bitRange>[29:29]</bitRange>
                        </field>
                        <field>
                            <name>LED0_EN</name>
                            <description>Led Enable</description>
                            <bitRange>[28:28]</bitRange>
                        </field>
                        <field>
                            <name>GPIO2_INT_POL</name>
                            <description>GPIO Interrupt Polarity</description>
                            <bitRange>[26:26]</bitRange>
                        </field>
                        <field>
                            <name>GPIO1_INT_POL</name>
                            <description>GPIO Interrupt Polarity</description>
                            <bitRange>[25:25]</bitRange>
                        </field>
                        <field>
                            <name>GPIO0_INT_POL</name>
                            <description>GPIO Interrupt Polarity</description>
                            <bitRange>[24:24]</bitRange>
                        </field>
                        <field>
                            <name>EEPR_EN</name>
                            <description>EEPROM Enable</description>
                            <bitRange>[22:20]</bitRange>
                            <enumeratedValues>
                                <enumeratedValue>
                                    <name>EEDIO_EECLK</name>
                                    <value>0</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>GPO3_GPO4</name>
                                    <value>1</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>GPO3_RX_DV</name>
                                    <value>3</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>TX_EN_GPO4</name>
                                    <value>5</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>TX_EN_RX_DV</name>
                                    <value>6</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>TX_CLK_RX_CLK</name>
                                    <value>7</value>
                                </enumeratedValue>
                            </enumeratedValues>
                        </field>
                        <field>
                            <name>GPIOBUF2</name>
                            <description>GPIO Buffer Type</description>
                            <bitRange>[18:18]</bitRange>
                        </field>
                        <field>
                            <name>GPIOBUF1</name>
                            <description>GPIO Buffer Type</description>
                            <bitRange>[17:17]</bitRange>
                        </field>
                        <field>
                            <name>GPIOBUF0</name>
                            <description>GPIO Buffer Type</description>
                            <bitRange>[16:16]</bitRange>
                        </field>
                        <field>
                            <name>GPDIR2</name>
                            <description>GPIO Direction</description>
                            <bitRange>[10:10]</bitRange>
                        </field>
                        <field>
                            <name>GPDIR1</name>
                            <description>GPIO Direction</description>
                            <bitRange>[9:9]</bitRange>
                        </field>
                        <field>
                            <name>GPDIR0</name>
                            <description>GPIO Direction</description>
                            <bitRange>[8:8]</bitRange>
                        </field>
                        <field>
                            <name>GPOD4</name>
                            <description>GPO Data</description>
                            <bitRange>[4:4]</bitRange>
                        </field>
                        <field>
                            <name>GPOD3</name>
                            <description>GPO Data</description>
                            <bitRange>[3:3]</bitRange>
                        </field>
                        <field>
                            <name>GPIOD2</name>
                            <description>GPIO Data</description>
                            <bitRange>[2:2]</bitRange>
                        </field>
                        <field>
                            <name>GPIOD1</name>
                            <description>GPIO Data</description>
                            <bitRange>[1:1]</bitRange>
                        </field>
                        <field>
                            <name>GPIOD0</name>
                            <description>GPIO Data</description>
                            <bitRange>[0:0]</bitRange>
                        </field>
                        <!-- TODO: add fields -->
                    </fields>
                </register>
                <register>
                    <name>GPT_CFG</name>
                    <description>General Purpose Timer Configuration</description>
                    <addressOffset>0x8C</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x0000FFFF</resetValue>
                    <fields>
                        <field>
                            <name>TIMER_EN</name>
                            <description>GP Timer Enable</description>
                            <bitRange>[29:29]</bitRange>
                        </field>
                        <field>
                            <name>GPT_LOAD</name>
                            <description>General Purpose Timer Pre-Load</description>
                            <bitRange>[15:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>GPT_CNT</name>
                    <description>General Purpose Timer Count</description>
                    <addressOffset>0x90</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                    <resetValue>0x0000FFFF</resetValue>
                    <fields>
                        <field>
                            <name>CNT</name>
                            <description>Counter</description>
                            <bitRange>[15:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>WORD_SWAP</name>
                    <description>WORD SWAP Register</description>
                    <addressOffset>0x98</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                </register>
                <register>
                    <name>FREE_RUN</name>
                    <description>Free Run Counter</description>
                    <addressOffset>0x9C</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                </register>
                <register>
                    <name>RX_DROP</name>
                    <description>RX Dropped Frames Counter</description>
                    <addressOffset>0xA0</addressOffset>
                    <size>32</size>
                    <access>read-only</access>
                    <resetValue>0x00000000</resetValue>
                </register>
                <register>
                    <name>MAC_CSR_CMD</name>
                    <description>MAC CSR Synchronizer Command</description>
                    <addressOffset>0xA4</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>BSY</name>
                            <description>CSR Busy</description>
                            <bitRange>[31:31]</bitRange>
                        </field>
                        <field>
                            <name>RnW</name>
                            <description>R/nW</description>
                            <bitRange>[30:30]</bitRange>
                        </field>
                        <field>
                            <name>Address</name>
                            <description>CSR Address</description>
                            <bitRange>[7:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>MAC_CSR_DATA</name>
                    <description>MAC CSR Synchronizer Data</description>
                    <addressOffset>0xA8</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                </register>
                <register>
                    <name>AFC_CFG</name>
                    <description>Automatic Flow Control Configuration</description>
                    <addressOffset>0xAC</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>AFC_HI</name>
                            <description>Automatic Flow Control High Level</description>
                            <bitRange>[23:16]</bitRange>
                        </field>
                        <field>
                            <name>AFC_LO</name>
                            <description>Automatic Flow Control Low Level</description>
                            <bitRange>[15:8]</bitRange>
                        </field>
                        <field>
                            <name>BACK_DUR</name>
                            <description>Backpresure duration</description>
                            <bitRange>[7:4]</bitRange>
                        </field>
                        <field>
                            <name>FCMULT</name>
                            <description>Flow Control on Multicast Frame</description>
                            <bitRange>[3:3]</bitRange>
                        </field>
                        <field>
                            <name>FCBRD</name>
                            <description>Flow Control on Broadcast Frame</description>
                            <bitRange>[2:2]</bitRange>
                        </field>
                        <field>
                            <name>FCADD</name>
                            <description>Flow Control on Address Decode</description>
                            <bitRange>[1:1]</bitRange>
                        </field>
                        <field>
                            <name>FCANY</name>
                            <description>Flow Control on Any Frame</description>
                            <bitRange>[0:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>E2P_CMD</name>
                    <description>EEPROM Command</description>
                    <addressOffset>0xB0</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>BSY</name>
                            <description>EPC Busy</description>
                            <bitRange>[31:31]</bitRange>
                        </field>
                        <field>
                            <name>CMD</name>
                            <description>EPC Command</description>
                            <bitRange>[30:28]</bitRange>
                            <enumeratedValues>
                                <enumeratedValue>
                                    <name>READ</name>
                                    <value>0</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>EWDS</name>
                                    <description>Erase/Write Disable</description>
                                    <value>1</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>EWEN</name>
                                    <description>Erase/Write Enable</description>
                                    <value>2</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>WRITE</name>
                                    <value>3</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>WRAL</name>
                                    <description>Write All</description>
                                    <value>4</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>ERASE</name>
                                    <value>5</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>ERAL</name>
                                    <description>Erase All</description>
                                    <value>6</value>
                                </enumeratedValue>
                                <enumeratedValue>
                                    <name>RELOAD</name>
                                    <description>MAC Address Reload</description>
                                    <value>7</value>
                                </enumeratedValue>
                            </enumeratedValues>
                        </field>
                        <field>
                            <name>TIME_OUT</name>
                            <description>EPC Time-out</description>
                            <bitRange>[9:9]</bitRange>
                        </field>
                        <field>
                            <name>MAC_LDR</name>
                            <description>MAC Address Loaded</description>
                            <bitRange>[8:8]</bitRange>
                        </field>
                        <field>
                            <name>Addr</name>
                            <description>EPC Address</description>
                            <bitRange>[7:0]</bitRange>
                        </field>
                    </fields>
                </register>
                <register>
                    <name>E2P_DATA</name>
                    <description>EEPROM Data</description>
                    <addressOffset>0xB4</addressOffset>
                    <size>32</size>
                    <access>read-write</access>
                    <resetValue>0x00000000</resetValue>
                    <fields>
                        <field>
                            <name>Data</name>
                            <description>EEPROM Data</description>
                            <bitRange>[7:0]</bitRange>
                        </field>
                    </fields>
                </register>
            </registers>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
