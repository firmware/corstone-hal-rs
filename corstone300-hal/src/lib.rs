// Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
//
// SPDX-License-Identifier: MIT

#![no_std]

pub use corstone300_pac as pac;

// UART = CMSDK UART
//  - 4x ttyUSB
//  - 2x Shield
// SCC = Serial Communication Controller
//  - Motherboard Configuration Controller
// SPI = PL022
//  - AD7490
//  - 2x Shield
// I2C = SBCon
//  - lcd screen's touch interface STMPE811QTR
//  - audio codec CS42L52
//  - 2x Shield
//  - DDR4 SODIMM's EEPROM
// RTC = PL031
// DMA = PL081
// GPIO = CMSDK AHB GPIO blocks
//  - shield
// I2S
//  - to SC42L52
//
// USB is a Static Memory Interface to an ISP1763
// mapped at 0x4150_0000 (NS) and 0x5150_0000 (Secure)
//
// On FPGA:
// Ethernet is a Static Memory Interface to an SMSC LAN9220
// mapped at 0x4140_0000 (NS) and 0x5140_0000 (Secure)
//
// On FVP:
// Ethernet is a Static Memory Interface to an SMSC 91C111
// mapped at 0x4140_0000 (NS) and 0x5140_0000 (Secure)

use fugit::HertzU32;

pub mod fpgaio;
pub mod serial;
pub mod shield;

// Clock input to the FPGA
pub mod ref_clock {
    use fugit::HertzU32;

    pub const REFCLK24MHZ: HertzU32 = HertzU32::MHz(24);
    pub const ACLK: HertzU32 = HertzU32::MHz(32);
    pub const MCLK: HertzU32 = HertzU32::MHz(50);
    pub const GPUCLK: HertzU32 = HertzU32::MHz(32);
    pub const AUDCLK: HertzU32 = HertzU32::MHz(32);
    pub const HDLCDCLK: HertzU32 = HertzU32::MHz(32);
}

/// Clock source for SSE-300 and all non-APB peripherals in the design
pub const MAIN_CLOCK: HertzU32 = HertzU32::MHz(32);

/// Clock source for APB peripherals
pub const PERIPHERAL_CLOCK: HertzU32 = HertzU32::MHz(25);

pub const AUDMCLK: HertzU32 = HertzU32::kHz(12_290);
pub const AUDSCLK: HertzU32 = HertzU32::kHz(3_070);
pub const SDMCLK: HertzU32 = HertzU32::MHz(50);
pub const CLK32KHZ: HertzU32 = HertzU32::kHz(32);
pub const CLK100HZ: HertzU32 = HertzU32::Hz(100);
pub const CLK1HZ: HertzU32 = HertzU32::Hz(1);
