/*
 * Copyright 2022 Arm Limited and/or its affiliates <open-source-office@arm.com>
 *
 * SPDX-License-Identifier: MIT
 */

MEMORY {
    FLASH (rx): ORIGIN = 0x10000000, LENGTH = 256K
    RAM (rwx): ORIGIN = 0x30000000, LENGTH = 256K
}
